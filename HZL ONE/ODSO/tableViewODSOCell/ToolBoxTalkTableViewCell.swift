//
//  ToolBoxTalkTableViewCell.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 09/02/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit

class ToolBoxTalkTableViewCell: UITableViewCell {
    @IBOutlet weak var btnUnit: UIButton!
    @IBOutlet weak var viewUnit: UIView!
    @IBOutlet weak var lblUnit: UILabel!
    
    @IBOutlet weak var lblArea: UILabel!
    @IBOutlet weak var btnArea: UIButton!
    @IBOutlet weak var viewArea: UIView!
    
    
    @IBOutlet weak var viewGiveno: UIView!
    @IBOutlet weak var textGiveno: UITextField!
    @IBOutlet weak var viewJob: UIView!
    @IBOutlet weak var textJob: UITextField!
    @IBOutlet weak var viewWorkers: UIView!
    @IBOutlet weak var textWorkers: UITextField!
    
    
    @IBOutlet weak var btnCamera: UIButton!
    @IBOutlet weak var imageViewPhoto: UIImageView!
    @IBOutlet weak var lblOptional: UIImageView!
    @IBOutlet weak var btnClose: UIButton!
    @IBOutlet weak var heightViewofClose: NSLayoutConstraint!
    @IBOutlet weak var heightImageselected: NSLayoutConstraint!
    @IBOutlet weak var heightViewImage: NSLayoutConstraint!
    override func awakeFromNib() {
        super.awakeFromNib()
        viewArea.layer.borderWidth = 1.0
        viewArea.layer.borderColor = UIColor.black.cgColor
        viewArea.layer.cornerRadius = 10.0
        viewUnit.layer.borderWidth = 1.0
        viewUnit.layer.borderColor = UIColor.black.cgColor
        viewUnit.layer.cornerRadius = 10.0
        
        viewGiveno.layer.borderWidth = 1.0
        viewGiveno.layer.borderColor = UIColor.black.cgColor
        viewGiveno.layer.cornerRadius = 10.0
        
        viewJob.layer.borderWidth = 1.0
        viewJob.layer.borderColor = UIColor.black.cgColor
        viewJob.layer.cornerRadius = 10.0
        
        viewWorkers.layer.borderWidth = 1.0
        viewWorkers.layer.borderColor = UIColor.black.cgColor
        viewWorkers.layer.cornerRadius = 10.0
        textWorkers.keyboardType = UIKeyboardType.numberPad
        textGiveno.keyboardType = UIKeyboardType.numberPad

        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
