//
//  ReedemHistoryViewController.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 21/02/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit

class ReedemHistoryViewController: CommonVSClass ,UITableViewDelegate, UITableViewDataSource{
    
    
    var RedeeemDB:[RedeeemDataModel] = []
    var RedeeemAPI = RedeeemDataAPI()
    
    var RedeeemLoadMoreDB : [RedeeemDataModel] = []
    var refreshControl = UIRefreshControl()
    
    
    
    var RedeeemLoadMoreAPI = RedeeemDataAPILoadMore()
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        getRedeeemData()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.estimatedRowHeight = 65
        tableView.rowHeight = UITableViewAutomaticDimension
        
        
        refreshControl.addTarget(self, action: #selector(getRedeeemData), for: .valueChanged)
        
        self.tableView.addSubview(refreshControl)
        let swipe = UISwipeGestureRecognizer(target: self, action: #selector(self.swipeRecognizer))
        swipe.direction = ([.down])
        self.view.addGestureRecognizer(swipe)
        
        tableView.tableFooterView = UIView()
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        // NotificationCenter.default.post(name: NSNotification.Name.init("sendReportFromHome"), object: nil)
        self.title = "Redeemed Card(s)"
        self.tabBarController?.tabBar.isHidden = true
        let nav = self.navigationController?.navigationBar
        nav?.tintColor = UIColor.white
       nav?.barTintColor = UIColor(hexString: "2c3e50", alpha: 1.0)
        nav?.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        
        
        
    }
    var reachability = Reachability()!
    @objc func swipeRecognizer(_ sender: UISwipeGestureRecognizer) {
        if (sender.direction == .down) {
            
            if(self.reachability.connection != .none)
            {
                
                self.getRedeeemData()
            }
            else
            {
                self.startLoading(view: self.view)
                DispatchQueue.main.asyncAfter(deadline: .now() + 4.5) {
                    self.noDataLabel(text: "No internet connection found. Check your internet connection and try again.")
                    self.stopLoading(view: self.view)
                }
            }
            
        }
    }
    var data: String?
    var lastObject: String?
    var indPath = IndexPath()
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        
        
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.RedeeemDB.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! ReedemHistoryTableViewCell
        
        
        if(self.RedeeemDB[indexPath.section].Date != ""){
            let dateFormatter = DateFormatter()
            dateFormatter.timeZone = NSTimeZone.system
            
            dateFormatter.dateFormat = "MMM dd yyyy"
            let date : Date = dateFormatter.date(from: self.RedeeemDB[indexPath.section].Date)!
            dateFormatter.string(from: date)
            let dateFormatter2 = DateFormatter()
            dateFormatter2.timeZone = NSTimeZone.system
            dateFormatter2.dateFormat = "dd MMM yyyy"
            let finalDate = dateFormatter2.string(from: date)
            cell.lblDate.text = finalDate
        }else{
            cell.lblDate.text = ""
        }
        
        cell.lblMoney.text = "Rs." + self.RedeeemDB[indexPath.section].Card_Amount + "/-"
       
        cell.textViewContribution.text = self.RedeeemDB[indexPath.section].Outlet_Remark
        cell.lblEmpName.text = self.RedeeemDB[indexPath.section].Employee_Name
        
        cell.moneyWidthCon.constant = cell.lblMoney.intrinsicContentSize.width
        self.data = String(self.RedeeemDB[indexPath.section].ID)
        self.lastObject = String(self.RedeeemDB[indexPath.section].ID)
        
        if ( self.data ==  self.lastObject && indexPath.section == self.RedeeemDB.count - 1)
        {
            
            self.getRedeeemDataLoadMore( ID: String(Int(self.RedeeemDB[indexPath.section].ID)))
            
        }
        
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableViewAutomaticDimension
        
    }
    @objc func getRedeeemData(){
        var param = [String:String]()
        
        param =  ["Employee_ID":UserDefaults.standard.string(forKey: "EmployeeID")!]
        
        RedeeemAPI.serviceCalling(obj: self,  param: param ) { (dict) in
            
            self.RedeeemDB = dict as! [RedeeemDataModel]
            
            self.tableView.reloadData()
            
        }
    }
    
    @objc func getRedeeemDataLoadMore(ID : String){
        var param = [String:String]()
        
        param =  ["Employee_ID":UserDefaults.standard.string(forKey: "EmployeeID")!,"ID":ID]
        RedeeemLoadMoreAPI.serviceCalling(obj: self,  param: param ) { (dict) in
            
            self.RedeeemLoadMoreDB =  [RedeeemDataModel]()
            self.RedeeemLoadMoreDB = dict as! [RedeeemDataModel]
            
            switch dict.count {
            case 0:
                break;
            default:
                self.RedeeemDB.append(contentsOf: self.RedeeemLoadMoreDB)
                self.tableView.reloadData()
                break;
            }
            
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
