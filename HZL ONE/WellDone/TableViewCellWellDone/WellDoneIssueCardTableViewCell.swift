
//
//  WellDoneIssueCardTableViewCell.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 21/02/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit

class WellDoneIssueCardTableViewCell:UITableViewCell {
    
    
    @IBOutlet weak var btnNext: UIButton!
 
    @IBOutlet weak var lblCompany: UILabel!
    @IBOutlet weak var viewCompany: UIView!
    
    @IBOutlet weak var btnGo: UIButton!
    
    @IBOutlet weak var stackViewHeightCon: NSLayoutConstraint!
    @IBOutlet weak var textViewHeightCon: NSLayoutConstraint!
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var viewName: UIView!
    @IBOutlet weak var textViewLSRComment: UITextView!
    @IBOutlet weak var viewEmpId: UIView!
    @IBOutlet weak var textEmpId: UITextField!
    
    @IBOutlet weak var lblContribution: UILabel!
    @IBOutlet weak var textViewContribution: UITextView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        textEmpId.keyboardType = UIKeyboardType.numberPad
        
    
        
        
        viewCompany.layer.borderWidth = 1.0
        viewCompany.layer.borderColor = UIColor.black.cgColor
        viewCompany.layer.cornerRadius = 10.0
        
        
        viewName.layer.borderWidth = 1.0
        viewName.layer.borderColor = UIColor.black.cgColor
        viewName.layer.cornerRadius = 10.0
        
        
     
        
        viewEmpId.layer.borderWidth = 1.0
        viewEmpId.layer.borderColor = UIColor.black.cgColor
        viewEmpId.layer.cornerRadius = 10.0
        
        textViewContribution.layer.borderWidth = 1.0
        textViewContribution.layer.borderColor = UIColor.black.cgColor
        textViewContribution.layer.cornerRadius = 10.0
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
