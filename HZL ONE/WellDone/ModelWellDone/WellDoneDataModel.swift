//
//  WellDoneDataModel.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 21/02/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import Foundation
class EmployeeListWellDoneModel: NSObject {
    
    
    
    var Employee_ID: String?
    var Employee_Name : String?
    var Company : String?
   
    
    func setDataInModel(cell:[String:AnyObject])
    {
        
        if cell["Employee_ID"] is NSNull || cell["Employee_ID"] == nil {
            self.Employee_ID = ""
        }else{
            let app = cell["Employee_ID"]
            self.Employee_ID = (app?.description)!
        }
        if cell["Employee_Name"] is NSNull || cell["Employee_Name"] == nil {
            self.Employee_Name = ""
        }else{
            let app = cell["Employee_Name"]
            self.Employee_Name = (app?.description)!
        }
        if cell["Company"] is NSNull || cell["Company"] == nil {
            self.Company = ""
        }else{
            let app = cell["Company"]
            self.Company = (app?.description)!
        }
        
      
    }
}


class EmployeelListWellDoneDataAPI
{
    
    var reachablty = Reachability()!
    
    
    func serviceCalling(obj:WellDoneIssueCardViewController,param : [String:String],empid : String, success:@escaping (AnyObject)-> Void)
    {
        
        obj.startLoadingPK(view: obj.view)
        
        if(reachablty.connection != .none)
        {
            
            WebServices.sharedInstances.sendPostRequest(url: URLConstants.Employee_List_By_ID_WellDone, parameters: param, successHandler: { (dict) in
                
                if let response = dict["response"]{
                    
                    let statusString : String = response["status"] as! String
                    
                    if(statusString == "success")
                    {
                        obj.onceDone = false
                        obj.cellData.textViewHeightCon.constant = 0
                        obj.cellData.stackViewHeightCon.constant = 80
                        obj.cellData.textViewLSRComment.text = ""
                        obj.cellData.textViewLSRComment.isHidden = true
                        
                        if let data = dict["data"] as? AnyObject
                        {
                            
                            var dataArray : [EmployeeListWellDoneModel] = []
                            if let celll = data as? [String:AnyObject]
                            {
                                let object = EmployeeListWellDoneModel()
                                object.setDataInModel(cell: celll)
                                dataArray.append(object)
                            }
                            
                            //                            for i in 0..<data.count
                            //                            {
                            //
                            
                            //
                            //                            }
                            
                            success(dataArray as AnyObject)
                            
                            
                        }
                        
                      
                        
                        obj.refresh.endRefreshing()
                         obj.stopLoadingPK(view: obj.view)
                        
                    }
                    else
                    {
                        if(obj.onceDone == true){
                        obj.view.makeToast("No employee found with id " + empid)
                        }
                        obj.cellData.lblContribution.isHidden = true
                        obj.cellData.textViewContribution.isHidden = true
                        obj.cellData.stackView.isHidden = true
                        obj.cellData.btnNext.isHidden = true
                        obj.cellData.textViewLSRComment.text = "No employee found with id " + empid
                        obj.cellData.textViewLSRComment.isHidden = false
                        obj.onceDone = true
                        obj.cellData.textViewHeightCon.constant = 40
                        obj.cellData.stackViewHeightCon.constant = 120
                        obj.refresh.endRefreshing()
                        
                       obj.stopLoadingPK(view: obj.view)
                        
                    }
                    
                    
                }
                
                
                
            }, failureHandler: { (error) in
                let errorStr : String = error.description
                
                print("DATA:error",errorStr)
                
                // obj.errorChecking(error: error)
               obj.stopLoadingPK(view: obj.view)
            })
            
            
        }
        else{
            // obj.noDataLabel(text:  "No internet connection found. Check your internet connection and try again.")
            obj.cellData.lblContribution.isHidden = true
            obj.cellData.textViewContribution.isHidden = true
            obj.cellData.stackView.isHidden = true
            obj.cellData.btnNext.isHidden = true
            obj.cellData.textViewLSRComment.text = "No internet connection found. Check your internet connection and try again."
            obj.cellData.textViewLSRComment.isHidden = false
            obj.cellData.textViewHeightCon.constant = 40
            obj.cellData.stackViewHeightCon.constant = 120
            obj.refresh.endRefreshing()
            obj.stopLoadingPK(view: obj.view)
        }
        
        
    }
    
    
}
