//
//  MyCardModel.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 21/02/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import Foundation
import Reachability


class MyCardDataModel: NSObject {
    
    
    var ID = Int()
    var Manager_EmpID = String()
    var Manager_Employee_Name = String()
    var Contribution = String()
    var DateOfIssue = String()
    var Emp_Location_ID = String()
    var Location_Name = String()
    var Status = String()
    var RedimedOn = String()
    var Redimed_Employee_Name = String()
    var RedimedBy = String()
    var Outlet_TnxId = String()
    var Employee_ID = String()
    var Employee_Name = String()
    
    

    
    
    func setDataInModel(str:[String:AnyObject])
    {
        if str["Employee_ID"] is NSNull || str["Employee_ID"] == nil{
            self.Employee_ID = ""
        }else{
            
            let ros = str["Employee_ID"]
            
            self.Employee_ID = (ros?.description)!
            
            
        }
        
        if str["Employee_Name"] is NSNull || str["Employee_Name"] == nil{
            self.Employee_Name = ""
        }else{
            
            let ros = str["Employee_Name"]
            
            self.Employee_Name = (ros?.description)!
            
            
        }
        
        
        
        
        
        
        
        if str["Manager_EmpID"] is NSNull || str["Manager_EmpID"] == nil{
            self.Manager_EmpID = ""
        }else{
            
            let ros = str["Manager_EmpID"]
            
            self.Manager_EmpID = (ros?.description)!
            
            
        }
        if str["Manager_Employee_Name"] is NSNull || str["Manager_Employee_Name"] == nil{
            self.Manager_Employee_Name =  ""
        }else{
            
            let fross = str["Manager_Employee_Name"]
            
            self.Manager_Employee_Name = (fross?.description)!
            
            
            //  self.Description = (str["Description"] as? String)!
        }
        if str["ID"] is NSNull || str["ID"] == nil{
            self.ID = 0
        }else{
            self.ID = (str["ID"] as? Int)!
        }
        if str["Contribution"] is NSNull || str["Contribution"] == nil{
            self.Contribution = "0"
        }else{
            let emp1 = str["Contribution"]
            
            self.Contribution = (emp1?.description)!
            // print(self.Employee_1)
        }
        if str["DateOfIssue"] is NSNull || str["DateOfIssue"] == nil{
            self.DateOfIssue = ""
        }else{
            
            let empname = str["DateOfIssue"]
            
            self.DateOfIssue = (empname?.description)!
            
        }
        
        if str["Emp_Location_ID"] is NSNull || str["Emp_Location_ID"] == nil{
            self.Emp_Location_ID = ""
        }else{
            let emp2 = str["Emp_Location_ID"]
            
            self.Emp_Location_ID = (emp2?.description)!
            
            
            
        }
        

        
        if str["Location_Name"] is NSNull || str["Location_Name"] == nil{
            self.Location_Name = ""
        }else{
            let empname2 = str["Location_Name"]
            
            self.Location_Name = (empname2?.description)!
            
        }
        
        
        if str["Status"] is NSNull || str["Status"] == nil{
            self.Status = ""
        }else{
            let locid = str["Status"]
            
            self.Status = (locid?.description)!
            
        }
        if str["Location_Name"] is NSNull || str["Location_Name"] == nil{
            self.Location_Name = ""
        }else{
            let locname = str["Location_Name"]
            
            self.Location_Name = (locname?.description)!
            
        }
        
        
        
        if str["RedimedOn"] is NSNull || str["RedimedOn"] == nil{
            self.RedimedOn = ""
        }else{
            let locname = str["RedimedOn"]
            
            self.RedimedOn = (locname?.description)!
            
        }
        if str["Redimed_Employee_Name"] is NSNull || str["Redimed_Employee_Name"] == nil{
            self.Redimed_Employee_Name = ""
        }else{
            let locname = str["Redimed_Employee_Name"]
            
            self.Redimed_Employee_Name = (locname?.description)!
            
        }
        if str["RedimedBy"] is NSNull || str["RedimedBy"] == nil{
            self.RedimedBy = ""
        }else{
            let locname = str["RedimedBy"]
            
            self.RedimedBy = (locname?.description)!
            
        }
        if str["Outlet_TnxId"] is NSNull || str["Outlet_TnxId"] == nil{
            self.Outlet_TnxId = ""
        }else{
            let locname = str["Outlet_TnxId"]
            
            self.Outlet_TnxId = (locname?.description)!
            
        }
        
    }
    
}





class MyCardDataAPI
{
    
    var reachablty = Reachability()!
    
    func serviceCalling(obj:MyCardsViewController, param : [String: String] , success:@escaping (AnyObject)-> Void)
    {
        obj.startLoading()
        
        if(reachablty.connection != .none)
        {
            
            
            WebServices.sharedInstances.sendPostRequest(url: URLConstants.My_Card, parameters: param , successHandler: { (dict) in
                
                if let response = dict["response"]{
                    
                    let statusString : String = response["status"] as! String
                    let objectmsg = MessageCallServerModel()
            let msg = objectmsg.sealizeMessage(cell: response as! [String : AnyObject])
                    print(response)
                    if(statusString == "success")
                    {
                        obj.tableView.isHidden = false
                        obj.noDataLabel(text: "" )
                        obj.label.isHidden = true
                        if let data = dict["data"] as? [AnyObject]
                        {
                            
                            var dataArray: [MyCardDataModel] = []
                            
                            for i in 0..<data.count
                            {
                                
                                if let cell = data[i] as? [String:AnyObject]
                                {
                                    let object = MyCardDataModel()
                                    object.setDataInModel(str: cell)
                                    dataArray.append(object)
                                }
                                
                            }
                            
                            success(dataArray as AnyObject)
                            
                            
                        }
                        
                        obj.refreshControl.endRefreshing()
                        obj.stopLoading()
                    }
                    else
                    {
                        obj.label.isHidden = false
                        obj.tableView.isHidden = true
                        obj.noDataLabel(text: msg )
                        obj.refreshControl.endRefreshing()
                        obj.stopLoading()
                        
                    }
                    
                    
                }
                
                
                
                
            })
            { (error) in
                
                let errorStr : String = error.description
                
                print("DATA:error",errorStr)
                
                //                obj.errorChecking(error: error)
                obj.stopLoading()
            }
            
            
        }
        else{
            obj.label.isHidden = false
            obj.tableView.isHidden = true
            obj.noDataLabel(text: "Internet is not available, please check your internet connection try again." )
            obj.stopLoading()
        }
        
        
    }
    
    
}

class MyCardDataAPILoadMore
{
    
    var reachablty = Reachability()!
    
    func serviceCalling(obj:MyCardsViewController, param : [String: String] , success:@escaping (AnyObject)-> Void)
    {
        obj.startLoading()
        
        if(reachablty.connection != .none)
        {
            
            
            WebServices.sharedInstances.sendPostRequest(url: URLConstants.My_Card, parameters: param , successHandler: { (dict) in
                
                if let response = dict["response"]{
                    
                    let statusString : String = response["status"] as! String
                    
                    if(statusString == "success")
                    {
                        //                        obj.tableView.isHidden = false
                        //                        obj.noDataLabel(text: "" )
                        
                        if let data = dict["data"] as? [AnyObject]
                        {
                            
                            var dataArray: [MyCardDataModel] = []
                            
                            for i in 0..<data.count
                            {
                                
                                if let cell = data[i] as? [String:AnyObject]
                                {
                                    let object = MyCardDataModel()
                                    object.setDataInModel(str: cell)
                                    dataArray.append(object)
                                }
                                
                            }
                            
                            success(dataArray as AnyObject)
                            
                            
                        }
                        
                        obj.refreshControl.endRefreshing()
                        obj.stopLoading()
                    }
                    else
                    {
                        
                        //                        obj.tableView.isHidden = true
                        //                        obj.noDataLabel(text: "No Data Found!" )
                        obj.refreshControl.endRefreshing()
                        obj.stopLoading()
                        
                    }
                    
                    
                }
                
                
                
                
            })
            { (error) in
                
                let errorStr : String = error.description
                
                print("DATA:error",errorStr)
                
                //                obj.errorChecking(error: error)
                obj.stopLoading()
            }
            
            
        }
        else{
            //            obj.tableView.isHidden = true
            //            obj.noDataLabel(text: "Internet is not available, please check your internet connection try again." )
            obj.stopLoading()
        }
        
        
    }
    
    
}
