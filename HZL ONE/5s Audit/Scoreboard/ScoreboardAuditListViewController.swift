//
//  ScoreboardAuditListViewController.swift
//  5s Audit
//
//  Created by SARVANG INFOTCH on 17/11/18.
//  Copyright © 2018 SARVANG INFOTCH. All rights reserved.
//

import UIKit

class ScoreboardAuditListViewController: CommonVSClass {
    
    var scoreListDB:[ScoreAuditListModel] = []
    
    var ScoListAPI = ScoreAuditListAPI()
    var Audit_Master_Id = String()
    var LocationID = String()
    var LocationLevel = String()
    var AuditType = String()
    var AuditTitle = String()
    @IBOutlet weak var AuditTypeLbl: UILabel!
    @IBOutlet weak var AuditTitleLbl: UILabel!
    @IBOutlet weak var tableViewTop: NSLayoutConstraint!
    
    var data: String?
    var lastObject: String?
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        ScoreListAudit()
        print(self.LocationLevel)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
        tableView.tableFooterView?.backgroundColor = UIColor.groupTableViewBackground
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        if(LocationID == String()){
            AuditTypeLbl.isHidden = true
            tableViewTop.constant = 70
        }else{
             AuditTypeLbl.isHidden = false
             tableViewTop.constant = 100
        }
        AuditTypeLbl.text = self.AuditType
        AuditTitleLbl.text = self.AuditTitle
        self.title = "5s Score Card"
        self.tabBarController?.tabBar.isHidden = true
        let nav = self.navigationController?.navigationBar
        nav?.barStyle = UIBarStyle.black
        nav?.tintColor = UIColor.white
        nav?.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        
    }
    @objc func ScoreListAudit() {
        var para = [String:String]()
        let parameter = ["AuditMasterID":self.Audit_Master_Id,
                         "LocationID":self.LocationID,
            "LocationLevel":self.LocationLevel]
     
                para = parameter.filter { $0.value != ""}
                print("para",para)
        self.ScoListAPI.serviceCalling(obj: self, parameter: para) { (dict) in
            
            self.scoreListDB = [ScoreAuditListModel]()
            self.scoreListDB = dict as! [ScoreAuditListModel]
            print(self.scoreListDB)
            self.tableView.reloadData()
        }
        
    }
    
    
    @objc func tapCell(_ sender: UITapGestureRecognizer) {
        let buttonPosition:CGPoint = sender.view!.convert(.zero, to:self.tableView)
        let indexPath = self.tableView.indexPathForRow(at: buttonPosition)
        self.LocationLevel = scoreListDB[(indexPath?.section)!].NextLevel!
        if(self.LocationLevel != "NA"){
       
        let storyboard = UIStoryboard(name: "Audit", bundle: nil)
        let ZIVC = storyboard.instantiateViewController(withIdentifier: "ScoreboardAuditListViewController") as! ScoreboardAuditListViewController
        
        
         ZIVC.Audit_Master_Id  = String(scoreListDB[(indexPath?.section)!].Audit_MasterID!)
        
        ZIVC.LocationID  = String(scoreListDB[(indexPath?.section)!].ID)
        
        ZIVC.LocationLevel  = scoreListDB[(indexPath?.section)!].NextLevel!
            ZIVC.AuditTitle  = AuditTitleLbl.text!
            if(self.AuditType == String()){
               ZIVC.AuditType  = scoreListDB[(indexPath?.section)!].LocationName!
            } else{
                ZIVC.AuditType  = AuditTypeLbl.text! + " > " + scoreListDB[(indexPath?.section)!].LocationName!
            }
            
        self.navigationController?.pushViewController(ZIVC, animated: true)
        }
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

extension ScoreboardAuditListViewController : UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
        // return 200
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return scoreListDB.count
    }
    
    
}

extension ScoreboardAuditListViewController : UITableViewDataSource {
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! ScoreListAuditTableViewCell
        
        
        
        cell.containerView.layer.cornerRadius = 8
       
        cell.scoreListLbl.text = scoreListDB[indexPath.row].LocationName
         cell.scoreLbl.text = scoreListDB[indexPath.row].TotalScore
        
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tapCell(_:)))
        cell.isUserInteractionEnabled = true;
        cell.addGestureRecognizer(tapGesture)
        

        
        
        return cell;
    }
}
