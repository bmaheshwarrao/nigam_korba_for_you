//
//  PendingObListModel.swift
//  5s Audit
//
//  Created by SARVANG INFOTCH on 19/11/18.
//  Copyright © 2018 SARVANG INFOTCH. All rights reserved.
//

import Foundation
import Reachability



class PendingObAuditListModel: NSObject {
    
    
    
    
    var Audit_ID : String?
    var CL_Point_ID : String?
    var CL_5S_Type : String?
    var CL_5S_Point : String?
    
   
    var Score : String?
    var Auditor_Comment : String?
    
    var Color_Code : String?

    
   
    
    func setDataInModel(cell:[String:AnyObject])
    {
        
        
        if cell["Audit_ID"] is NSNull{
            self.Audit_ID = ""
        }else{
            let idd1 : Int = (cell["Audit_ID"] as? Int)!
            self.Audit_ID = String(idd1)
            
        }
        
        if cell["CL_Point_ID"] is NSNull{
            self.CL_Point_ID = ""
        }else{
            let idd1 : Int = (cell["CL_Point_ID"] as? Int)!
            self.CL_Point_ID = String(idd1)
            
        }
        
        
        if cell["CL_5S_Type"] is NSNull{
            self.CL_5S_Type = ""
        }else{
            self.CL_5S_Type = (cell["CL_5S_Type"] as? String)!
        }
        if cell["CL_5S_Point"] is NSNull{
            self.CL_5S_Point = ""
        }else{
            self.CL_5S_Point = (cell["CL_5S_Point"] as? String)!
        }
        
        if cell["Auditor_Comment"] is NSNull{
            self.Auditor_Comment = ""
        }else{
            self.Auditor_Comment = (cell["Auditor_Comment"] as? String)!
        }
        if cell["Score"] is NSNull{
            self.Score = ""
        }else{
            let idd1 : Int = (cell["Score"] as? Int)!
            self.Score = String(idd1)
            
        }
        if cell["Color_Code"] is NSNull{
            self.Color_Code = ""
        }else{
            self.Color_Code = (cell["Color_Code"] as? String)!
        }
        
        
        
      
        
    }
}

public struct CSType {
    var name: String
    var dataItems: [PendingObAuditListModel]
   var color: String
    
    
    
}
class PendingObAuditListAPI
{
    
    var reachablty = Reachability()!
   
    
    func serviceCalling(obj:PendingObListViewController,parameter:[String:String], success:@escaping (AnyObject)-> Void)
    {
        
        obj.startLoading()
        
        if(reachablty.connection != .none)
        {
            
            WebServices.sharedInstances.sendPostRequest(url: URLConstants.Pending_ObservationsAuditList, parameters: parameter, successHandler: { (dict) in
                
                if let response = dict["response"]{
                    let objectmsg = MessageCallServerModel()
            let msg = objectmsg.sealizeMessage(cell: response as! [String : AnyObject])
                    print(response)
                    let statusString : String = response["status"] as! String
                    
                    if(statusString == "success")
                    {
                        
                        obj.tableView.isHidden = false
                        obj.noDataLabel(text: "" )
                        obj.label.isHidden = true
                        
                        if let data = dict["data"] as? [AnyObject]
                        {
                            
                            var dataArray : [PendingObAuditListModel] = []
                            
                            var dataArrayType : [CSType] = []
                            var cat1 = String()
                            var cat2 = String()
                            var strColor = String()
                            for i in 0..<data.count
                            {
                                
                                if let cell = data[i] as? [String:AnyObject]
                                {
                                    if(i != 0) {
                                        cat1 = (data[i]["CL_5S_Type"] as? String)!
                                        cat2 = (data[i-1]["CL_5S_Type"] as? String)!
                                        strColor  = (data[i-1]["Color_Code"] as? String)!
                                    } else {
                                        cat1 = (data[i]["CL_5S_Type"] as? String)!
                                        strColor  = (data[i]["Color_Code"] as? String)!
                                    }
                                   
                                    
                                    print(cat1)
                                    print(cat2)
                                    
                                    
                                    if(i != data.count-1) {
                                        if(cat1 != cat2) {
                                            if(cat2 != "") {
                                            
                                                let objectData1 = CSType(name: cat2, dataItems: dataArray, color: strColor)
                                                dataArrayType.append(objectData1)
                                                dataArray = []
                                                let object = PendingObAuditListModel()
                                                object.setDataInModel(cell: cell)
                                                dataArray.append(object)
                                            }
                                            else {
                                                let object = PendingObAuditListModel()
                                                object.setDataInModel(cell: cell)
                                                dataArray.append(object)
                                            }
                                        } else {
                                            let object = PendingObAuditListModel()
                                            object.setDataInModel(cell: cell)
                                            dataArray.append(object)
                                        }
                                    } else {
                                        if(cat1 != cat2) {
                                            dataArray = []
                                        }
                                        let object = PendingObAuditListModel()
                                        object.setDataInModel(cell: cell)
                                        dataArray.append(object)
                                        
                                        let objectData1 = CSType(name: cat1, dataItems: dataArray, color: strColor)
                                        dataArrayType.append(objectData1)
                                    }
                                }
                                
                            
                            
                            
                                
                            }
                            
                            success(dataArrayType as AnyObject)
                            
                            
                        }
                        
                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                    }
                    else
                    {
                        
                        
                        print("DATA:fail")
                        obj.tableView.isHidden = true
                        obj.noDataLabel(text: msg)
                        obj.refresh.endRefreshing()
                        obj.label.isHidden = false
                        obj.stopLoading()
                        
                    }
                    
                    
                }
                
                
                
            }, failureHandler: { (error) in
                let errorStr : String = error.description
                
                print("DATA:error",errorStr)
                
                // obj.errorChecking(error: error)
                obj.stopLoading()
            })
            
            
        }
        else{
            obj.noDataLabel(text:  "No internet connection found. Check your internet connection and try again.")
            obj.tableView.isHidden = true
            obj.label.isHidden = false
            obj.refresh.endRefreshing()
            obj.stopLoading()
        }
        
        
    }
    
    
}







