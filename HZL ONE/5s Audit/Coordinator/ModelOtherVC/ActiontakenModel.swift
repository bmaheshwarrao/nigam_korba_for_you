//
//  ActiontakenModel.swift
//  5s Audit
//
//  Created by SARVANG INFOTCH on 26/11/18.
//  Copyright © 2018 SARVANG INFOTCH. All rights reserved.
//

import Foundation
import Reachability



class ActionTakenModel: NSObject {
    
    
    
    
    var Audit_ID : String?
    var Point_Id : String?
    var Audit_Action_DateTime : String?
    var ImageURL : String?
    
    
    var Audit_Action_Status : String?
    var Comment : String?
    
    
   
    
    
    
    func setDataInModel(cell:[String:AnyObject])
    {
        
        
        if cell["Audit_Id"] is NSNull{
            self.Audit_ID = ""
        }else{
            let idd1 : Int = (cell["Audit_Id"] as? Int)!
            self.Audit_ID = String(idd1)
            
        }
        
        if cell["Point_Id"] is NSNull{
            self.Point_Id = ""
        }else{
            let idd1 : Int = (cell["Point_Id"] as? Int)!
            self.Point_Id = String(idd1)
            
        }
        
        
        if cell["Comment"] is NSNull{
            self.Comment = ""
        }else{
            self.Comment = (cell["Comment"] as? String)!
        }
        if cell["Audit_Action_DateTime"] is NSNull{
            self.Audit_Action_DateTime = ""
        }else{
            self.Audit_Action_DateTime = (cell["Audit_Action_DateTime"] as? String)!
        }
        
        if cell["ImageURL"] is NSNull{
            self.ImageURL = ""
        }else{
            self.ImageURL = (cell["ImageURL"] as? String)!
        }
        if cell["Audit_Action_Status"] is NSNull{
            self.Audit_Action_Status = ""
        }else{
          
            self.Audit_Action_Status = (cell["Audit_Action_Status"] as? String)!
            
        }
        
        
        
        
        
        
    }
}


class ActionTakenListAPI
{
    
    var reachablty = Reachability()!
    
    
    func serviceCalling(obj:ActionTakenViewController,parameter:[String:String], success:@escaping (AnyObject)-> Void)
    {
        
        obj.startLoading()
        
        if(reachablty.connection != .none)
        {
            
            WebServices.sharedInstances.sendPostRequest(url: URLConstants.Action_Taken_On_Open_Observation, parameters: parameter, successHandler: { (dict) in
                
                if let response = dict["response"]{
                    let objectmsg = MessageCallServerModel()
            let msg = objectmsg.sealizeMessage(cell: response as! [String : AnyObject])
                    print(response)
                    let statusString : String = response["status"] as! String
                    
                    if(statusString == "success")
                    {
                        
                        obj.tableView.isHidden = false
                        obj.noDataLabel(text: "" )
                        obj.label.isHidden = true
                        
                        if let data = dict["data"] as? [AnyObject]
                        {
                            
                            var dataArray : [ActionTakenModel] = []
                            
                            for i in 0..<data.count
                            {
                                
                                if let celll = data[i] as? [String:AnyObject]
                                {
                                    let object = ActionTakenModel()
                                    object.setDataInModel(cell: celll)
                                    dataArray.append(object)
                                }
                                
                            }
                            
                            success(dataArray as AnyObject)
                            
                            
                        }
                        
                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                    }
                    else
                    {
                        
                        
                        print("DATA:fail")
                        obj.tableView.isHidden = true
                        obj.noDataLabel(text: msg)
                        obj.refresh.endRefreshing()
                        obj.label.isHidden = false
                        obj.stopLoading()
                        
                    }
                    
                    
                }
                
                
                
            }, failureHandler: { (error) in
                let errorStr : String = error.description
                
                print("DATA:error",errorStr)
                
                // obj.errorChecking(error: error)
                obj.stopLoading()
            })
            
            
        }
        else{
            obj.noDataLabel(text:  "No internet connection found. Check your internet connection and try again.")
            obj.tableView.isHidden = true
            obj.label.isHidden = false
            obj.refresh.endRefreshing()
            obj.stopLoading()
        }
        
        
    }
    
    
}








