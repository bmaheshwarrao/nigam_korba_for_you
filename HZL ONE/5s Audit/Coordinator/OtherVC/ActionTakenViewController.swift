//
//  ActionTakenViewController.swift
//  5s Audit
//
//  Created by SARVANG INFOTCH on 26/11/18.
//  Copyright © 2018 SARVANG INFOTCH. All rights reserved.
//

import UIKit
import SDWebImage

class ActionTakenViewController: CommonVSClass {
    
    var ActionTakenDB:[ActionTakenModel] = []
    var Color_Code = String()
   // @IBOutlet weak var textViewPoint: UITextView!
    @IBOutlet weak var textViewDesc: UITextView!
    
    @IBOutlet weak var textViewPoint: UITextView!
    @IBOutlet weak var scoreLbl: UILabel!
    // @IBOutlet weak var statusPointLbl: UILabel!
    @IBOutlet weak var titleStatusLbl: UILabel!
    var ActionTakListAPI = ActionTakenListAPI()
    var screenStatus = UserDefaults.standard.string(forKey: "screenStatus")!
    var pointId = String()
    var auditId = String()
    var screen = UserDefaults.standard.string(forKey: "screen")!
    var CL_5S_Type = String()
    var CL_5S_Point = String()
       var score = String()
    var Auditor_Comment = String()
   
    var data: String?
    var lastObject: String?
    
    
    
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        titleStatusLbl.text = CL_5S_Type
        
       titleStatusLbl.textColor = UIColor.colorwithHexString(Color_Code, alpha: 1.0)
        scoreLbl.text = "Score : " + score
 textViewPoint.text = CL_5S_Point
       textViewDesc.text =  Auditor_Comment
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
        tableView.tableFooterView?.backgroundColor = UIColor.groupTableViewBackground
        // Do any additional setup after loading the view.
           refresh.addTarget(self, action: #selector(ActionList), for: .valueChanged)
        let swipe = UISwipeGestureRecognizer(target: self, action: #selector(self.swipeRecognizer))
        swipe.direction = ([.down])
        self.view.addGestureRecognizer(swipe)
        
        
        self.tableView.addSubview(refresh)
        
        
        
    }
    var reachability = Reachability()!
    @objc func swipeRecognizer(_ sender: UISwipeGestureRecognizer) {
        if (sender.direction == .down) {
            
            if(self.reachability.connection != .none)
            {
                
                self.ActionList()
            }
            else
            {
                self.startLoading(view: self.view)
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                    self.noDataLabel(text: "No internet connection found. Check your internet connection and try again.")
                    self.stopLoading(view: self.view)
                }
            }
            
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        
        
        
        if(screenStatus == "Pending"){
            self.title = "Pending Observation"
        }else if(screenStatus == "Upcoming"){
            self.title = "Upcoming"
        }else{
            self.title = "Completed"
        }
        self.tabBarController?.tabBar.isHidden = true
        let nav = self.navigationController?.navigationBar
        nav?.barStyle = UIBarStyle.black
        nav?.tintColor = UIColor.white
        nav?.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        ActionList()
    }
    @objc func ActionList() {
        var para = [String:String]()
        let parameter = ["Audit_ID":self.auditId , "Point_ID":self.pointId ]
        
        para = parameter.filter { $0.value != ""}
        print("para",para)
        self.ActionTakListAPI.serviceCalling(obj: self, parameter: para) { (dict) in
            
            self.ActionTakenDB = [ActionTakenModel]()
            self.ActionTakenDB = dict as! [ActionTakenModel]
            print(self.ActionTakenDB)
            self.tableView.reloadData()
        }
        
    }
    
    @IBAction func btnprocessClicked(_ sender: UIButton) {
        
            let storyboard = UIStoryboard(name: "Audit", bundle: nil)
        let ZIVC = storyboard.instantiateViewController(withIdentifier: "PendingObSubmitCoordinateViewController") as! PendingObSubmitCoordinateViewController
           
                    ZIVC.pointId = pointId
                    ZIVC.auditId = auditId
                    self.navigationController?.pushViewController(ZIVC, animated: true)
        
    }
    func imageResize (image:UIImage, sizeChange:CGSize)-> UIImage{
        
        let hasAlpha = true
        let scale: CGFloat = 0.0 // Use scale factor of main screen
        
        UIGraphicsBeginImageContextWithOptions(sizeChange, !hasAlpha, scale)
        image.draw(in: CGRect(origin: CGPoint.zero, size: sizeChange))
        
        let scaledImage = UIGraphicsGetImageFromCurrentImageContext()
        return scaledImage!
    }
    
    @objc func tapCell(_ sender: UITapGestureRecognizer) {
//        if(screenStatus == "Pending"){
//            let buttonPosition:CGPoint = sender.view!.convert(.zero, to:self.tableView)
//            let indexPath = self.tableView.indexPathForRow(at: buttonPosition)
//            let storyboard = UIStoryboard(name: "Audit", bundle: nil)
//            let ZIVC = storyboard.instantiateViewController(withIdentifier: "ActionTakenSubmitCoordinateViewController") as! ActionTakenSubmitCoordinateViewController
//
//            ZIVC.pointId = ActionTakenservationDB[(indexPath?.row)!].CL_Point_ID!
//            ZIVC.auditId = ActionTakenservationDB[(indexPath?.row)!].Audit_ID!
//            self.navigationController?.pushViewController(ZIVC, animated: true)
//        }
        
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

extension ActionTakenViewController : UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
        // return 200
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       
        return ActionTakenDB.count
    
    }
    
}

extension ActionTakenViewController : UITableViewDataSource {
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
      
        let cell = tableView.dequeueReusableCell(withIdentifier: "action", for: indexPath) as! ActionTakenTableViewCell
        
        
        
        cell.containerView.layer.cornerRadius = 8
        cell.StatusLbl.text = ActionTakenDB[indexPath.row].Audit_Action_Status!
        cell.actionCommentLbl.text =  ActionTakenDB[indexPath.row].Comment!

            let imageView = UIImageView()
            let urlString = self.ActionTakenDB[indexPath.row].ImageURL!
            let urlShow = urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
            if let url = NSURL(string: self.ActionTakenDB[indexPath.row].ImageURL!) {
                
                SDWebImageManager.shared().downloadImage(with: NSURL(string: urlShow!) as URL!, options: .continueInBackground, progress: {
                    (receivedSize :Int, ExpectedSize :Int) in
                    
                }, completed: {
                    (image : UIImage?, error : Error?, cacheType : SDImageCacheType, finished : Bool, url : URL?) in
                    if(image != nil) {
                        let size = CGSize(width: 100, height: 100)
                        let imageCell = self.imageResize(image: image!, sizeChange: size)
                        cell.imageFile.backgroundColor = UIColor(patternImage: imageCell)
                    }else{
                        imageView.image =  UIImage(named: "placed")
                        let size = CGSize(width: 100, height: 100)
                        let imageCell = self.imageResize(image: imageView.image!, sizeChange: size)
                        cell.imageFile.image = imageCell
                        //  cell.viewImage.backgroundColor = UIColor(patternImage: imageCell)
                    }
                })
            }else{
                imageView.image =  UIImage(named: "placed")
                let size = CGSize(width: 100, height: 100)
                let imageCell = imageResize(image: imageView.image!, sizeChange: size)
                cell.imageFile.image = imageCell
                //cell.viewImage.backgroundColor = UIColor(patternImage: imageCell)
            }

        
    
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tapCell(_:)))
        cell.isUserInteractionEnabled = true;
        cell.addGestureRecognizer(tapGesture)
        
        
        
        return cell;
        
    }
}
