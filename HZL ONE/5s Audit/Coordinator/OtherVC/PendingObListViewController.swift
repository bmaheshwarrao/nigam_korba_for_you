//
//  PendingObListViewController.swift
//  5s Audit
//
//  Created by SARVANG INFOTCH on 19/11/18.
//  Copyright © 2018 SARVANG INFOTCH. All rights reserved.
//

import UIKit

class PendingObListViewController: CommonVSClass {
    
    var PendingObservationDB:[CSType] = []
    
    var PendingObListAPI = PendingObAuditListAPI()
    var screenStatus = UserDefaults.standard.string(forKey: "screenStatus")!
    var screen = UserDefaults.standard.string(forKey: "screen")!
    var auditId = String()
    var data: String?
    var lastObject: String?
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
       
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
        tableView.tableFooterView?.backgroundColor = UIColor.groupTableViewBackground
        // Do any additional setup after loading the view.
        refresh.addTarget(self, action: #selector(AuditList), for: .valueChanged)
        let swipe = UISwipeGestureRecognizer(target: self, action: #selector(self.swipeRecognizer))
        swipe.direction = ([.down])
        self.view.addGestureRecognizer(swipe)
        
        
        self.tableView.addSubview(refresh)
        
        
        
    }
    var reachability = Reachability()!
    @objc func swipeRecognizer(_ sender: UISwipeGestureRecognizer) {
        if (sender.direction == .down) {
            
            if(self.reachability.connection != .none)
            {
                
                self.AuditList()
            }
            else
            {
                self.startLoading(view: self.view)
                DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                    self.noDataLabel(text: "No internet connection found. Check your internet connection and try again.")
                    self.stopLoading(view: self.view)
                }
            }
            
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
         screenStatus = UserDefaults.standard.string(forKey: "screenStatus")!
         screen = UserDefaults.standard.string(forKey: "screen")!
        
      
        
        if(screenStatus == "Pending"){
            self.title = "Pending Observation"
        }else if(screenStatus == "Upcoming"){
            self.title = "Upcoming"
        }else{
            self.title = "Completed"
        }
        self.tabBarController?.tabBar.isHidden = true
        let nav = self.navigationController?.navigationBar
        nav?.barStyle = UIBarStyle.black
        nav?.tintColor = UIColor.white
        nav?.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
         AuditList()
    }
    @objc func AuditList() {
        var para = [String:String]()
        let parameter = ["Audit_ID":self.auditId]
        
        para = parameter.filter { $0.value != ""}
        print("para",para)
        self.PendingObListAPI.serviceCalling(obj: self, parameter: para) { (dict) in
            
            self.PendingObservationDB = [CSType]()
            self.PendingObservationDB = dict as! [CSType]
            print(self.PendingObservationDB)
            self.tableView.reloadData()
        }
        
    }
    
    
 
    @objc func tapCell(_ sender: UITapGestureRecognizer) {
        if(screenStatus == "Pending"){
                let buttonPosition:CGPoint = sender.view!.convert(.zero, to:self.tableView)
                let indexPath = self.tableView.indexPathForRow(at: buttonPosition)
                let storyboard = UIStoryboard(name: "Audit", bundle: nil)
                let ZIVC = storyboard.instantiateViewController(withIdentifier: "ActionTakenViewController") as! ActionTakenViewController
       
            
            
            
            
            ZIVC.pointId =  PendingObservationDB[(indexPath?.section)!].dataItems[(indexPath?.row)!].CL_Point_ID!
        ZIVC.auditId = PendingObservationDB[(indexPath?.section)!].dataItems[(indexPath?.row)!].Audit_ID!


            ZIVC.CL_5S_Type = PendingObservationDB[(indexPath?.section)!].dataItems[(indexPath?.row)!].CL_5S_Type!
            ZIVC.CL_5S_Point = PendingObservationDB[(indexPath?.section)!].dataItems[(indexPath?.row)!].CL_5S_Point!
            ZIVC.Auditor_Comment = PendingObservationDB[(indexPath?.section)!].dataItems[(indexPath?.row)!].Auditor_Comment!
     ZIVC.score = PendingObservationDB[(indexPath?.section)!].dataItems[(indexPath?.row)!].Score!
            ZIVC.Color_Code = PendingObservationDB[(indexPath?.section)!].dataItems[(indexPath?.row)!].Color_Code!
            
            
            
            
       
                self.navigationController?.pushViewController(ZIVC, animated: true)
        }
        
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

extension PendingObListViewController : UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
    
        let header = tableView.dequeueReusableHeaderFooterView(withIdentifier: "header") as? ViewAuditHeaderTableViewHeader ?? ViewAuditHeaderTableViewHeader(reuseIdentifier: "header")
        
        header.titleLabel.text = PendingObservationDB[section].name + "-" + String(PendingObservationDB[section].dataItems.count)
        print(PendingObservationDB[section].color)
        
        header.titleView.backgroundColor = UIColor.colorwithHexString(PendingObservationDB[section].color, alpha: 1.0)


        
        
    
        header.section = section
        header.delegate = self
       
        
        
        
        
        
   
      
        
        
        
        
        
        return header
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 44.0
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 1.0
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
        // return 200
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return PendingObservationDB.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
          return  PendingObservationDB[section].dataItems.count
    }
    
    
}

extension PendingObListViewController : UITableViewDataSource {
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! PendingObListTableViewCell
        
        
        
        cell.containerView.layer.cornerRadius = 8
        
        
   
        

//        cell.PointIdLbl.text = "Id #" +  PendingObservationDB[indexPath.section].dataItems[indexPath.row].CL_Point_ID!
//        cell.PointType.text =  PendingObservationDB[indexPath.section].dataItems[indexPath.row].CL_5S_Type!
        cell.textViewPoint.text =  PendingObservationDB[indexPath.section].dataItems[indexPath.row].CL_5S_Point!
         cell.textViewDesc.text =  PendingObservationDB[indexPath.section].dataItems[indexPath.row].Auditor_Comment!
cell.lblNumber.text = String(indexPath.row + 1)
        
            cell.viewSetColor.backgroundColor = UIColor.colorwithHexString(PendingObservationDB[indexPath.section].dataItems[indexPath.row].Color_Code!, alpha: 1.0)
            cell.viewSetColor.shadowColor = UIColor.colorwithHexString("C4DB5F", alpha: 1.0)
            cell.viewSetColor.shadowOpacity = 0.5
//        }else{
//            cell.viewSetColor.backgroundColor = UIColor.colorwithHexString("C4DB5F", alpha: 1.0)
//            cell.viewSetColor.shadowColor = UIColor.colorwithHexString("C4DB5F", alpha: 1.0)
//            cell.viewSetColor.shadowOpacity = 0.5
//        }
        
        
        
        
        //        if(PendingObservationDB[indexPath.row].flag! == "not submitted"){
//            cell.containerView.backgroundColor = UIColor.lightGray
//        }else{
//            cell.containerView.backgroundColor = UIColor.colorwithHexString("51852F", alpha: 1.0)
//        }
        
        
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tapCell(_:)))
        cell.isUserInteractionEnabled = true;
        cell.addGestureRecognizer(tapGesture)
        
        
        
        return cell;
    }
}
extension PendingObListViewController: ViewAuditHeaderTableViewHeaderDelegate {
    func toggleSection(_ header: ViewAuditHeaderTableViewHeader, section: Int) {
        
    }
    
    
  
    
}
