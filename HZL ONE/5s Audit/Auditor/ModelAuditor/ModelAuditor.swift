//
//  ModelAuditor.swift
//  5s Audit
//
//  Created by SARVANG INFOTCH on 17/11/18.
//  Copyright © 2018 SARVANG INFOTCH. All rights reserved.
//


import Foundation
import Reachability



class AuditListModel: NSObject {
    
    
    
    
    var Audit_ID : String?
    var Audit_Title : String?
    var Audit_Type : String?
    var Start_Date : String?
    
    var Last_Date : String?
    var LocationType : String?
    var LocationID : String?
    var Auditor_EmpID : String?
    
    
    var Mobile_Number : String?
    var Email_ID : String?
    var First_Name : String?
    var Coordinator_EmpID : String?
    
   
    var Location_Title : String?
    var AuditMobile_Number : String?
    var AuditEmail_ID : String?
    var AuditName : String?
  
    
    func setDataInModel(cell:[String:AnyObject])
    {
        if cell["ID"] is NSNull || cell["ID"] == nil {
            self.Audit_ID = ""
        }else{
            let app = cell["ID"]
            self.Audit_ID = (app?.description)!
        }
        
        if cell["Audit_Title"] is NSNull || cell["Audit_Title"] == nil {
            self.Audit_Title = ""
        }else{
            let app = cell["Audit_Title"]
            self.Audit_Title = (app?.description)!
        }
        
        
       
        if cell["Audit_Type"] is NSNull || cell["Audit_Type"] == nil {
            self.Audit_Type = ""
        }else{
            let app = cell["Audit_Type"]
            self.Audit_Type = (app?.description)!
        }
        if cell["Start_Date"] is NSNull || cell["Start_Date"] == nil {
            self.Start_Date = ""
        }else{
            let app = cell["Start_Date"]
            self.Start_Date = (app?.description)!
        }
       
        if cell["End_Date"] is NSNull || cell["End_Date"] == nil {
            self.Last_Date = ""
        }else{
            let app = cell["End_Date"]
            self.Last_Date = (app?.description)!
        }
        if cell["LocationType"] is NSNull || cell["LocationType"] == nil {
            self.LocationType = ""
        }else{
            let app = cell["LocationType"]
            self.LocationType = (app?.description)!
        }
        if cell["LocationID"] is NSNull || cell["LocationID"] == nil {
            self.LocationID = ""
        }else{
            let app = cell["LocationID"]
            self.LocationID = (app?.description)!
        }
      
        if cell["AuditorId"] is NSNull || cell["AuditorId"] == nil {
            self.Auditor_EmpID = ""
        }else{
            let app = cell["AuditorId"]
            self.Auditor_EmpID = (app?.description)!
        }
        if cell["AuditorMobileNo"] is NSNull || cell["AuditorMobileNo"] == nil {
            self.AuditMobile_Number = ""
        }else{
            let app = cell["AuditorMobileNo"]
            self.AuditMobile_Number = (app?.description)!
        }
        if cell["AuditorEmailId"] is NSNull || cell["AuditorEmailId"] == nil {
            self.AuditEmail_ID = ""
        }else{
            let app = cell["AuditorEmailId"]
            self.AuditEmail_ID = (app?.description)!
        }
       
        if cell["AuditorName"] is NSNull || cell["AuditorName"] == nil {
            self.AuditName = ""
        }else{
            let app = cell["AuditorName"]
            self.AuditName = (app?.description)!
        }
        
        
        
        if cell["CoordinatorMobileNo"] is NSNull || cell["CoordinatorMobileNo"] == nil {
            self.Mobile_Number = ""
        }else{
            let app = cell["CoordinatorMobileNo"]
            self.Mobile_Number = (app?.description)!
        }
        
        
        
        if cell["CoordinatorEmailId"] is NSNull || cell["CoordinatorEmailId"] == nil {
            self.Email_ID = ""
        }else{
            let app = cell["CoordinatorEmailId"]
            self.Email_ID = (app?.description)!
        }
        
        if cell["CoordinatorName"] is NSNull || cell["CoordinatorName"] == nil {
            self.First_Name = ""
        }else{
            let app = cell["CoordinatorName"]
            self.First_Name = (app?.description)!
        }
        
        if cell["CoordinatorId"] is NSNull || cell["CoordinatorId"] == nil {
            self.Coordinator_EmpID = ""
        }else{
            let app = cell["CoordinatorId"]
            self.Coordinator_EmpID = (app?.description)!
        }
        
       
        
        
        
        
      
        if cell["Location_Title"] is NSNull || cell["Location_Title"] == nil {
            self.Location_Title = ""
        }else{
            let app = cell["Location_Title"]
            self.Location_Title = (app?.description)!
        }
    }
}


class AuditListAPI
{
    
    var reachablty = Reachability()!
    
    
    func serviceCalling(obj:AuditorViewController,parameter:[String:String], success:@escaping (AnyObject)-> Void)
    {
        
        obj.startLoading()
        
        if(reachablty.connection != .none)
        {
            
            WebServices.sharedInstances.sendPostRequest(url: URLConstants.Get_AuditPending_List, parameters: parameter, successHandler: { (dict) in
                
                if let response = dict["response"]{
                    
                    let statusString : String = response["status"] as! String
                    let objectmsg = MessageCallServerModel()
            let msg = objectmsg.sealizeMessage(cell: response as! [String : AnyObject])
                    print(response)
                    if(statusString == "success")
                    {
                        
                        obj.tableView.isHidden = false
                        obj.noDataLabel(text: "" )
                        obj.label.isHidden = true
                        
                        if let data = dict["data"] as? [AnyObject]
                        {
                            
                            var dataArray : [AuditListModel] = []
                            
                            for i in 0..<data.count
                            {
                                
                                if let celll = data[i] as? [String:AnyObject]
                                {
                                    let object = AuditListModel()
                                    object.setDataInModel(cell: celll)
                                    dataArray.append(object)
                                }
                                
                            }
                            
                            success(dataArray as AnyObject)
                            
                            
                        }
                        
                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                    }
                    else
                    {
                        
                        
                        print("DATA:fail")
                        obj.tableView.isHidden = true
                        obj.noDataLabel(text: msg)
                        obj.refresh.endRefreshing()
                        obj.label.isHidden = false
                        obj.stopLoading()
                        
                    }
                    
                    
                }
                
                
                
            }, failureHandler: { (error) in
                let errorStr : String = error.description
                
                print("DATA:error",errorStr)
                
                // obj.errorChecking(error: error)
                obj.stopLoading()
            })
            
            
        }
        else{
            obj.noDataLabel(text:  "No internet connection found. Check your internet connection and try again.")
            obj.tableView.isHidden = true
            obj.label.isHidden = false
            obj.refresh.endRefreshing()
            obj.stopLoading()
        }
        
        
    }
    
    
}





