//
//  FivesTypesViewController.swift
//  5s FivesType
//
//  Created by SARVANG INFOTCH on 27/11/18.
//  Copyright © 2018 SARVANG INFOTCH. All rights reserved.
//

import UIKit
import EzPopup
class FivesTypesViewController:CommonVSClass {
    
    var FivesTypeListDB:[FivesTypeListModel] = []
    
    var ScoListAPI = FivesTypeListAPI()
    var auditId = String()
    var screen = UserDefaults.standard.string(forKey: "screen")!
    var data: String?
    var lastObject: String?
    var scroreAll = Int()
    var totalQuestion = Int()
    var totalAttempted = Int()
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
//        FivesTypeList()
        self.btnFinalSubmit.isHidden = true
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
        tableView.tableFooterView?.backgroundColor = UIColor.groupTableViewBackground
          NotificationCenter.default.addObserver(self, selector: #selector(self.finalSubmit), name: NSNotification.Name?.init(NSNotification.Name(rawValue: "finalSubmit")), object: nil)
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        if( finalResult.isFinal == true){
             finalResult.isFinal = false
            self.navigationController?.popViewController(animated: true)
        }
         self.btnFinalSubmit.isHidden = true
        self.title = "5s Types"
        self.tabBarController?.tabBar.isHidden = true
        let nav = self.navigationController?.navigationBar
        nav?.barStyle = UIBarStyle.black
        nav?.tintColor = UIColor.white
        nav?.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        FivesTypeList()
    }

    @IBOutlet weak var btnFinalSubmit: UIButton!
        let finalAlertVC = FinalSubmitViewController.instantiate()
    @IBAction func FinalSubmitClicked(_ sender: UIButton) {
        guard let finalAlertVC = finalAlertVC else { return }
        
      finalAlertVC.score = String(scroreAll)
        let popupVC = PopupViewController(contentController: finalAlertVC, popupWidth: 300)
        
        popupVC.cornerRadius = 5
        popupVC.canTapOutsideToDismiss = false
        present(popupVC, animated: true, completion: nil)
        
        
    }
    @objc func finalSubmit(){
       
            
            self.startLoadingPK(view: self.view)
            
         
           
            let parameter = ["Audit_ID":self.auditId,
                             
                             "Auditor_Comment": textComment
                            
                
                ] as! [String:Any]
            
            
            
            print("parameter",parameter)
            
            WebServices.sharedInstances.sendPostRequest(url: URLConstants.Submit_Final_Audit, parameters: parameter, successHandler: { (response:[String : AnyObject]) in
                
                let respon = response["response"] as! [String:AnyObject]
                let objectmsg = MessageCallServerModel()
                let msg = objectmsg.sealizeMessage(cell: respon as! [String : AnyObject])
                if respon["status"] as! String == "success" {
                    //  self.showSingleButtonWithMessage(title: "Validation!", message: "Successfully Submitted.", buttonName: "OK")
                    self.view.makeToast(msg)
                    textComment = ""
                    self.stopLoadingPK(view: self.view)
                    
            
                    let storyboard1 = UIStoryboard(name: "Audit", bundle: nil)
                    let finalContinueVC = storyboard1.instantiateViewController(withIdentifier: "AfterFinalSubmitViewController") as! AfterFinalSubmitViewController
                   
                    self.navigationController?.isNavigationBarHidden = true
                    self.navigationController?.pushViewController(finalContinueVC, animated: true)
                    
                    
                }else{
                    self.stopLoadingPK(view: self.view)
                    
                    self.view.makeToast(msg)
                }
                
            }) { (err) in
                self.stopLoadingPK(view: self.view)
                
                print(err.description)
            }
        
    }
    @objc func FivesTypeList() {
        var para = [String:String]()
        let parameter = ["AuditID":auditId]
        
        para = parameter.filter { $0.value != ""}
        print("para",para)
        self.ScoListAPI.serviceCalling(obj: self, parameter: para) { (dict) in
            self.scroreAll = 0
            self.totalQuestion = 0
            self.totalAttempted = 0
            self.FivesTypeListDB = [FivesTypeListModel]()
            self.FivesTypeListDB = dict as! [FivesTypeListModel]
            print(self.FivesTypeListDB)
            self.tableView.reloadData()
           
        }
        
    }
    
    
    @objc func tapCell(_ sender: UITapGestureRecognizer) {
        let buttonPosition:CGPoint = sender.view!.convert(.zero, to:self.tableView)
        let indexPath = self.tableView.indexPathForRow(at: buttonPosition)
        let storyboard = UIStoryboard(name: "Audit", bundle: nil)
        let ZIVC = storyboard.instantiateViewController(withIdentifier: "AuditCheckListViewController") as! AuditCheckListViewController
        
        
      ZIVC.auditId = auditId
       ZIVC.auditType = FivesTypeListDB[(indexPath?.row)!].CL_5S_Type!
        
        self.navigationController?.pushViewController(ZIVC, animated: true)
        
    }
   
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

extension FivesTypesViewController : UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
        // return 200
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return FivesTypeListDB.count
    }
    
    
}

extension FivesTypesViewController : UITableViewDataSource {
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! FivesTypeTableViewCell
        
       
       
        cell.containerView.layer.cornerRadius = 8
      
        if(FivesTypeListDB[indexPath.row].TotalQuestion! == FivesTypeListDB[indexPath.row].Attempt!){
           cell.countStatusLbl.text = "All " + FivesTypeListDB[indexPath.row].Attempt! + " observation closed."
        }else{
            cell.countStatusLbl.text = "You have " + FivesTypeListDB[indexPath.row].Unattempt! + " open and " + FivesTypeListDB[indexPath.row].Attempt! + " closed observation."
        }
        
        
        scroreAll = scroreAll + Int(FivesTypeListDB[indexPath.row].Score!)!
        cell.typeLbl.text = FivesTypeListDB[indexPath.row].CL_5S_Type!
        cell.containerView.backgroundColor = UIColor.colorwithHexString(FivesTypeListDB[indexPath.row].Color_Code!, alpha: 1.0)
        
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tapCell(_:)))
        cell.containerView.isUserInteractionEnabled = true;
        cell.containerView.addGestureRecognizer(tapGesture)
        
        
        
        if(Int(FivesTypeListDB[indexPath.row].TotalQuestion!)! != 0 && Int(FivesTypeListDB[indexPath.row].Attempt!)!  != 0){
            if(Int(FivesTypeListDB[indexPath.row].TotalQuestion!)! == Int(FivesTypeListDB[indexPath.row].Attempt!)!){
                cell.imageViewTick.isHidden = false
                cell.scoreLbl.text = "Score : " + FivesTypeListDB[indexPath.row].Score!
                cell.heightScore.constant = 18
                
            }else{
                cell.imageViewTick.isHidden = true
                cell.heightScore.constant = 0
                cell.frame.size.height = cell.frame.size.height - 18
            }
        }else{
            cell.imageViewTick.isHidden = true
            cell.heightScore.constant = 0
            cell.frame.size.height = cell.frame.size.height - 18
        }

        
        
        
        
        
        
        self.totalQuestion =  self.totalQuestion + Int(FivesTypeListDB[indexPath.row].TotalQuestion!)!
        self.totalAttempted = self.totalAttempted + Int(FivesTypeListDB[indexPath.row].Attempt!)!
        
        if(FivesTypeListDB.count - 1 == indexPath.row){
            if(self.totalQuestion != 0 && self.totalAttempted != 0){
                if(self.totalQuestion == self.totalAttempted){
                    self.btnFinalSubmit.isHidden = false
                }else{
                    self.btnFinalSubmit.isHidden = true
                }
            }else{
                //self.btnFinalSubmit.isHidden = true
            }
        }
        
        return cell;
    }
}
