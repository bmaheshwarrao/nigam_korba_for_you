//
//  SubmitTableViewCell.swift
//  5s Audit
//
//  Created by SARVANG INFOTCH on 28/11/18.
//  Copyright © 2018 SARVANG INFOTCH. All rights reserved.
//

import UIKit
import DLRadioButton
class SubmitTableViewCell: UITableViewCell {

    @IBOutlet weak var textViewObservation: UITextView!
    @IBOutlet weak var btnScore0: RoundShapeButton!
    @IBOutlet weak var btnScore1: RoundShapeButton!
    @IBOutlet weak var btnScore2: RoundShapeButton!
    @IBOutlet weak var btnScore3: RoundShapeButton!
    @IBOutlet weak var btnScore4: RoundShapeButton!
    @IBOutlet weak var collectionViewLoadImage: UICollectionView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var viewPhotoUpload: UIView!
    @IBOutlet weak var radioClosed: DLRadioButton!
    @IBOutlet weak var radioOpen: DLRadioButton!
    
    @IBOutlet weak var viewRate0: UIViewX!
    @IBOutlet weak var viewRate1: UIViewX!
    @IBOutlet weak var viewRate2: UIViewX!
    @IBOutlet weak var viewRate3: UIViewX!
    @IBOutlet weak var viewRate4: UIViewX!
    @IBOutlet weak var bottomCell: NSLayoutConstraint!
    
    @IBOutlet weak var cameraViewTop: NSLayoutConstraint!
    @IBOutlet weak var heightUploadedImages: NSLayoutConstraint!
    @IBOutlet weak var heightAnotherView: NSLayoutConstraint!
    @IBOutlet weak var viewAnotherColection: UIView!
    @IBOutlet weak var lblUploadImages: UILabel!
    @IBOutlet weak var heightAnotherCollection: NSLayoutConstraint!
    
    
    @IBOutlet weak var heightImageBrowse: NSLayoutConstraint!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
