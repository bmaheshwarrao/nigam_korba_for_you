//
//  AuditCheckListTableViewCell.swift
//  5s Audit
//
//  Created by SARVANG INFOTCH on 17/11/18.
//  Copyright © 2018 SARVANG INFOTCH. All rights reserved.
//

import UIKit

class AuditCheckListTableViewCell: UITableViewCell {
    @IBOutlet weak var viewStatusColor: UIViewX!
//    @IBOutlet weak var PointIdLbl: UILabel!
//    @IBOutlet weak var PointType: UILabel!
    @IBOutlet weak var btnCell: UIButton!
    @IBOutlet weak var textViewPoint: UITextView!
     @IBOutlet weak var containerView : UIViewX!
    @IBOutlet weak var lblQuestionNo: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
