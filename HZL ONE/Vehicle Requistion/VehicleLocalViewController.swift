//
//  VehicleLocalViewController.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 25/02/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit
import DLRadioButton
import SwiftyJSON
import Foundation
class VehicleLocalViewController: CommonVSClass , WWCalendarTimeSelectorProtocol {
    
    
    
    
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        getPurposeList()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.tabBarController?.tabBar.isHidden = true
        self.title = "Vehicle Local"
        let nav = self.navigationController?.navigationBar
        nav?.tintColor = UIColor.white
       nav?.barTintColor = UIColor(hexString: "2c3e50", alpha: 1.0)
        nav?.isTranslucent = false
        nav?.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        
        if(FilterDataFromServer.Authority_Name != "") {
            updateData()
        }
       
    }
   var reachablty = Reachability()!
    @IBAction func btnSubmitClicked(_ sender: UIButton) {
        
        if reachablty.connection == .none{
            
            self.view.makeToast("Internet is not available, please check your internet connection try again.")
            
        }
        else if cellData.txtName.text == "" {
            
            self.view.makeToast("Enter Name")
        } else if cellData.txtContact.text == "" {
            
            self.view.makeToast("Enter Contact")
        }else if cellData.lblPurpose.text == "" || cellData.lblPurpose.text == "Select" {
            
            self.view.makeToast("Select Purpose of Journey")
        }else if cellData.lblJourneyDate.text == "" {
            
            self.view.makeToast("Please Select Journey Date")
        }
        else if cellData.lblFromTime.text == "" {
            
            self.view.makeToast("Please Select From Time")
        }else if cellData.lblToTime.text == "" {
            
            self.view.makeToast("Please Select To Time")
        }else if cellData.textViewPickAddress.text == "" {
            
            self.view.makeToast("Please Enter Pick-Up Address")
        }else if cellData.textViewDropAddress.text == "" {
            
            self.view.makeToast("Please Enter Drop Address")
        }else if cellData.lblApprovalAuth.text == "" || cellData.lblApprovalAuth.text == "Select" {
            
            self.view.makeToast("Please Select Approval Authority")
        }
        else{
           
            
            
            let Duration_From = cellData.lblJourneyDate.text! + " " + cellData.lblFromTime.text!
            let Duration_To = cellData.lblJourneyDate.text! + " " + cellData.lblToTime.text!
            
                let dateSet = "/Date(" + millisecJourneyDate + ")/"
           
            self.startLoadingPK(view: self.view)
              let IDData : String = UserDefaults.standard.string(forKey: "IDData")!
         
            let param = ["ID":"0",
                         "Request_ID":"" ,
                         "Req_Type": requestFor,
                         "Emp_No": IDData,
                         "Name": cellData.txtName.text!,
                         "Mobile_No": cellData.txtContact.text!,
                         "Purpose_of_Journey": idPurpose,
                         "Date_of_Traval": dateSet,
                         "From_Place": "",
                         "PickUp_Address": cellData.textViewPickAddress.text!,
                         "Drop_Address": cellData.textViewDropAddress.text!,
                         "No_Of_Persons": cellData.lblPerson.text!,
                         "Duration_From": Duration_From,
                         "Duration_To": Duration_To,
                         "Type_Of_Vehicle": "",
                         "Approval_Authority": FilterDataFromServer.Authority_Id
                        
                         
                         
                         
                         
                         
                         ]
                as [String:String]
            
            

           
            let jsonParam  = JSON(param)
           
            
          
            
           
            let empId : String = UserDefaults.standard.string(forKey: "EmployeeID")!
            let Profile_AuthKey = UserDefaults.standard.string(forKey: "Profile_AuthKey")!
            let parameter = ["UserID":empId,
                             "AuthKey":Profile_AuthKey ,
                             "Req":param
                
                ] as [String:Any]
            
            
            
            //print("parameter",parameter)
            
            WebServices.sharedInstances.sendPostRequest(url: URLConstants.LocalRequest, parameters: parameter, successHandler: { (response:[String : AnyObject]) in
                
                let respon = response["response"] as! [String:AnyObject]
                self.stopLoadingPK(view: self.view)
                let objectmsg = MessageCallServerModel()
                
                let message = objectmsg.sealizeMessage(cell: respon as! [String : AnyObject])
                if respon["status"] as! String == "ok" {
                    
                    self.stopLoadingPK(view: self.view)
                    
                    MoveStruct.isMove = true
                    MoveStruct.message = message
                    self.navigationController?.popViewController(animated: false)
                    
                    
                }else{
                    self.stopLoadingPK(view: self.view)
                    self.view.makeToast(message)
                }
                
            }) { (err) in
                self.stopLoadingPK(view: self.view)
                print(err.description)
            }
            
   
        
        
    }
    }
    var idPurpose = String()
    @IBAction func btnPurposeClicked(_ sender: UIButton) {
        
        DispatchQueue.main.async(execute: {() -> Void in
            let popup = PopUpTableViewController()
            
            popup.itemsArray = self.purposeListArray
            
            popup.sourceView = self.cellData.lblPurpose
            popup.isScroll = true
            popup.backgroundColor = UIColor.white
            if popup.itemsArray.count > 5{
                popup.popUpHeight = 200
            }
            else{
                popup.popUpHeight = CGFloat(popup.itemsArray.count)*45
            }
            popup.popUpWidth = UIScreen.main.bounds.size.width-60
            popup.backgroundImage = nil
            popup.itemTitleColor = UIColor.white
            popup.itemSelectionColor = UIColor.lightGray
            popup.arrowDirections = .any
            popup.arrowColor = UIColor.white
            popup.popCellBlock = {(_ popupVC: PopUpTableViewController, _ popupCell: UITableViewCell, _ row: Int, _ section: Int) -> Void in
                
                
                
                
                
                self.cellData.lblPurpose.text = self.purposeListArray[row]
                self.cellData.lblPurpose.textColor = UIColor.black
                self.idPurpose = self.purposeIdArray[row]
                popupVC.dismiss(animated: false, completion: nil)
                
            }
            self.present(popup, animated: true, completion: {() -> Void in
            })
            
            
        })
        
    }
    @objc func updateData(){
        if(FilterDataFromServer.Authority_Name != "") {
            cellData.lblApprovalAuth.text = FilterDataFromServer.Authority_Name
            cellData.lblApprovalAuth.textColor = UIColor.black
        } else {
            cellData.lblApprovalAuth.text = ""
            cellData.lblApprovalAuth.textColor = UIColor.lightGray
        }
        
        
        // tableView.reloadData()
    }
    var noOfPerson : [String] = ["1","2","3","4","5","6","7","8","9","10"]
    @IBAction func btnNoofPersonClicked(_ sender: UIButton) {
        DispatchQueue.main.async(execute: {() -> Void in
            let popup = PopUpTableViewController()
            
            popup.itemsArray = self.noOfPerson
            
            popup.sourceView = self.cellData.lblPerson
            popup.isScroll = true
            popup.backgroundColor = UIColor.white
            if popup.itemsArray.count > 5{
                popup.popUpHeight = 200
            }
            else{
                popup.popUpHeight = CGFloat(popup.itemsArray.count)*45
            }
            popup.popUpWidth = UIScreen.main.bounds.size.width-60
            popup.backgroundImage = nil
            popup.itemTitleColor = UIColor.white
            popup.itemSelectionColor = UIColor.lightGray
            popup.arrowDirections = .any
            popup.arrowColor = UIColor.white
            popup.popCellBlock = {(_ popupVC: PopUpTableViewController, _ popupCell: UITableViewCell, _ row: Int, _ section: Int) -> Void in
                
                
                
                
                
                self.cellData.lblPerson.text = self.noOfPerson[row]
                self.cellData.lblPerson.textColor = UIColor.black
           
                popupVC.dismiss(animated: false, completion: nil)
                
            }
            self.present(popup, animated: true, completion: {() -> Void in
            })
            
            
        })
        
    }
 
    
    
    var purposeDB:[PurposeDataModel] = []
    
    var purposeAPI = PurposeDataAPI()
    @objc func getPurposeList(){
        
        let empId = UserDefaults.standard.string(forKey: "EmployeeID")!
        let Profile_AuthKey = UserDefaults.standard.string(forKey: "Profile_AuthKey")!
        let paramm  : [String:String] = ["UserID": empId , "AuthKey" :Profile_AuthKey ]
        
        self.purposeAPI.serviceCalling(obj: self , param : paramm) { (dict) in
            
            self.purposeDB = [PurposeDataModel]()
            self.purposeDB = dict as! [PurposeDataModel]
            if(self.purposeDB.count > 0){
                for i in 0...self.purposeDB.count - 1 {
                    self.purposeListArray.append(self.purposeDB[i].Name!)
                    self.purposeIdArray.append(self.purposeDB[i].ID!)
                }
            }
            
            
            
            
        }
    }
    var purposeListArray : [String] = []
     var purposeIdArray : [String] = []
  
    var requestFor = String()
    @IBAction func btnReuestForClicked(_ sender: DLRadioButton) {
        for button in sender.selectedButtons() {
            
            requestFor =   button.titleLabel!.text!
        }
        print(requestFor)
       requestForData()
    }
    func requestForData(){
        if(requestFor == "Self"){
            var name = String()
            let mobileData = UserDefaults.standard.string(forKey: "mobileData")!
            let fname = UserDefaults.standard.string(forKey: "FirstName")
            let Mname = UserDefaults.standard.string(forKey: "MiddleName")
            let Lname = UserDefaults.standard.string(forKey: "Lastname")
            
            
            if(Mname == ""){
                name = fname! + " " + Lname!
            }else{
                name = fname! + " " + Mname!  + " " + Lname!
            }
            cellData.txtName.text = name
            
            cellData.txtContact.text = mobileData
            cellData.txtName.isEnabled = false
            cellData.txtContact.isEnabled = false
        }else{
            cellData.txtName.text = ""
            cellData.txtContact.text = ""
            cellData.txtName.isEnabled = true
            cellData.txtContact.isEnabled = true
        }
    }
    fileprivate var singleDate: Date = Date()
    fileprivate var multipleDates: [Date] = []
    var selectdateVal = 0
    @IBAction func btnJourneyDateClicked(_ sender: UIButton) {
        
        let selector = UIStoryboard(name: "WWCalendarTimeSelector", bundle: nil).instantiateViewController(withIdentifier: "WWCalendarTimeSelector") as! WWCalendarTimeSelector
        
        selectdateVal = 1
        dateData = 0
        
        selector.delegate = self
        selector.optionCurrentDate = singleDate
        selector.optionCurrentDates = Set(multipleDates)
        selector.optionCurrentDateRange.setStartDate(multipleDates.first ?? singleDate)
        selector.optionCurrentDateRange.setEndDate(multipleDates.last ?? singleDate)
         selector.optionRangeOfEnabledDates.setStartDate(singleDate)
        
        
        selector.optionStyles.showDateMonth(true)
        selector.optionStyles.showMonth(false)
        selector.optionStyles.showYear(true)
        selector.optionStyles.showTime(false)
        
        
        present(selector, animated: true, completion: nil)
    }
    
    @IBAction func btnFromTimeClicked(_ sender: UIButton) {
        
        let selector = UIStoryboard(name: "WWCalendarTimeSelector", bundle: nil).instantiateViewController(withIdentifier: "WWCalendarTimeSelector") as! WWCalendarTimeSelector
        
        
        selectdateVal = 2
        
        selector.delegate = self
        selector.optionCurrentDate = singleDate
        selector.optionCurrentDates = Set(multipleDates)
        selector.optionCurrentDateRange.setStartDate(multipleDates.first ?? singleDate)
        selector.optionCurrentDateRange.setEndDate(multipleDates.last ?? singleDate)
        
        selector.optionStyles.showDateMonth(false)
        selector.optionStyles.showMonth(false)
        selector.optionStyles.showYear(false)
        selector.optionStyles.showTime(true)
        
        dateData = 0
        
        present(selector, animated: true, completion: nil)
        
        
        
        
    }
    
    @IBAction func btnToTimeClicked(_ sender: UIButton) {
        
        let selector = UIStoryboard(name: "WWCalendarTimeSelector", bundle: nil).instantiateViewController(withIdentifier: "WWCalendarTimeSelector") as! WWCalendarTimeSelector
        
        
        selectdateVal = 3
        
        selector.delegate = self
        selector.optionCurrentDate = singleDate
        selector.optionCurrentDates = Set(multipleDates)
        selector.optionCurrentDateRange.setStartDate(multipleDates.first ?? singleDate)
        selector.optionCurrentDateRange.setEndDate(multipleDates.last ?? singleDate)
       
        selector.optionStyles.showDateMonth(false)
        selector.optionStyles.showMonth(false)
        selector.optionStyles.showYear(false)
        selector.optionStyles.showTime(true)
        
        dateData = 0
        
        present(selector, animated: true, completion: nil)
        
        
        
        
    }
    
    var idApproval = String()
    @IBAction func btnApprovalAuthClicked(_ sender: UIButton) {
        
        FilterDataFromServer.Authority_Name = String()
      FilterDataFromServer.Authority_Id = String()
        let storyBoard = UIStoryboard(name: "VehicleRequistion", bundle: nil)
        let ZIVC = storyBoard.instantiateViewController(withIdentifier: "ApprovalAuthorityViewController") as! ApprovalAuthorityViewController
        ZIVC.titleStr = "Select Approval Authority"
        self.navigationController?.pushViewController(ZIVC, animated: true)
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    var millisecJourneyDate = String()
    func getMilliseconds(date1 : Date) -> String{
        //let strDate = date1
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd MMM yyyy  hh:mma"
        //let date = dateFormatter.date(from: strDate)
        
        let millieseconds = self.getDiffernce(toTime: date1)
        print(millieseconds)
        return String(millieseconds)
    }
    func getDiffernce(toTime:Date) -> Int{
        let elapsed = toTime.timeIntervalSince1970
        return Int(elapsed * 1000)
    }
    
    
    var cellData : VehicleLocalTableViewCell!
    
    func WWCalendarTimeSelectorDone(_ selector: WWCalendarTimeSelector, date: Date) {
        print("Selected \n\(date)\n---")
        singleDate = date
        
        
        
        if(selectdateVal == 1){
            cellData.lblJourneyDate.text = date.stringFromFormat("d' 'MMM' 'yyyy'")
            millisecJourneyDate = getMilliseconds(date1: date)
        }
        if(selectdateVal == 2){
            cellData.lblFromTime.text = date.stringFromFormat("h':'mm a'")
        }
        if(selectdateVal == 3){
            cellData.lblToTime.text = date.stringFromFormat("h':'mm a'")
        }
    }
    
    func WWCalendarTimeSelectorDone(_ selector: WWCalendarTimeSelector, dates: [Date]) {
        print("Selected Multiple Dates \n\(dates)\n---")
        if let date = dates.first {
            singleDate = date
            // dateLabel.text = date.stringFromFormat("d' 'MMMM' 'yyyy', 'h':'mma")
        }
        else {
            //dateLabel.text = "No Date Selected"
        }
        multipleDates = dates
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
extension VehicleLocalViewController : UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 608
        
    }
}
extension VehicleLocalViewController : UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! VehicleLocalTableViewCell
        
        cellData = cell
        if(self.idPurpose == String()){
        cell.lblPurpose.text = "Select"
        }
        cell.lblPerson.text = "1"
        if(requestFor == String()){
        cell.btnSelf.isSelected = true
        requestFor = "Self"
        requestForData()
        }
        if(FilterDataFromServer.Authority_Name == "") {
            cell.lblApprovalAuth.text = "Select"
        }
        let dateFormatter2 = DateFormatter()
        dateFormatter2.timeZone = NSTimeZone.system
        dateFormatter2.dateFormat = "dd MMM yyyy"
        
        
        let date_TimeStr = dateFormatter2.string(from: Date())
        cell.lblJourneyDate.text = date_TimeStr
        millisecJourneyDate = getMilliseconds(date1: Date())
        
        
        
        cell.separatorInset = UIEdgeInsetsMake(0, 10000, 0, 0);
        return cell
        
        
        
    }
}
