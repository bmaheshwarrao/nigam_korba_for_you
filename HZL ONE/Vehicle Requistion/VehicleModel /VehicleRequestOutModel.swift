//
//  VehicleRequestOutModel.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 27/02/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import Foundation
class MyRequestVehicleTourDataModel: NSObject {
    
    
    var ID = Int()
    var ReqData = String()
    var PassDetails = String()
    var Emp_No = String()
    var Location_ID = String()
    var Unit_ID = String()
    var Purpose_of_Journey = String()
    var SAP_TR_No = String()
    var Request_Type = String()
    var Request_Date_Time = String()
    var StartDate = String()
    
    var EndDate = String()
    var LH_EmpID = String()
    var LH_App_Date = String()
    var LH_App_Status = String()
    var AH_EmpID = String()
    var AH_App_Date = String()
    var AH_App_Status = String()
    var Booking_Status = String()
    var Booking_Date_Time = String()
    var Remarks = String()
    
    var Status = String()
    var Date_Time = String()
    var Feedback_Status = String()
    var SelectedApprover = String()
    var TCReq = String()
    var TCRes = String()
    var BookURL = String()
    var BookURLMobile = String()
    

  
 
    
    
    
    
    func setDataInModel(str:[String:AnyObject])
    {
        if str["ReqData"] is NSNull || str["ReqData"] == nil{
            self.ReqData = ""
        }else{
            
            let ros = str["ReqData"]
            
            self.ReqData = (ros?.description)!
            
            
        }
        if str["PassDetails"] is NSNull || str["PassDetails"] == nil{
            self.PassDetails =  ""
        }else{
            
            let fross = str["PassDetails"]
            
            self.PassDetails = (fross?.description)!
            
            
            //  self.Description = (str["Description"] as? String)!
        }
        if str["ID"] is NSNull || str["ID"] == nil{
            self.ID = 0
        }else{
            self.ID = (str["ID"] as? Int)!
        }
        if str["Emp_No"] is NSNull || str["Emp_No"] == nil{
            self.Emp_No = ""
        }else{
            let emp1 = str["Emp_No"]
            
            self.Emp_No = (emp1?.description)!
            // print(self.Employee_1)
        }
        
        if str["Location_ID"] is NSNull || str["Location_ID"] == nil{
            self.Location_ID = ""
        }else{
            
            let empname = str["Location_ID"]
            
            self.Location_ID = (empname?.description)!
            
        }
        
        if str["Unit_ID"] is NSNull || str["Unit_ID"] == nil{
            self.Unit_ID = ""
        }else{
            let emp2 = str["Unit_ID"]
            
            self.Unit_ID = (emp2?.description)!
            
            
            
        }
        
        
        
        if str["Purpose_of_Journey"] is NSNull || str["Purpose_of_Journey"] == nil{
            self.Purpose_of_Journey = ""
        }else{
            let empname2 = str["Purpose_of_Journey"]
            
            self.Purpose_of_Journey = (empname2?.description)!
            
        }
        
        
        if str["SAP_TR_No"] is NSNull || str["SAP_TR_No"] == nil{
            self.SAP_TR_No = ""
        }else{
            let locid = str["SAP_TR_No"]
            
            self.SAP_TR_No = (locid?.description)!
            
        }
  
        
        if str["Request_Type"] is NSNull || str["Request_Type"] == nil{
            self.Request_Type = ""
        }else{
            let locname = str["Request_Type"]
            
            self.Request_Type = (locname?.description)!
            
        }
        
        
        
        if str["Request_Date_Time"] is NSNull || str["Request_Date_Time"] == nil{
            self.Request_Date_Time = ""
        }else{
            let locname = str["Request_Date_Time"]
            
            self.Request_Date_Time = (locname?.description)!
            
        }
        if str["StartDate"] is NSNull || str["StartDate"] == nil{
            self.StartDate = ""
        }else{
            let locname = str["StartDate"]
            
            self.StartDate = (locname?.description)!
            
        }
        
     
        if str["EndDate"] is NSNull || str["EndDate"] == nil{
            self.EndDate = ""
        }else{
            let locname = str["EndDate"]
            
            self.EndDate = (locname?.description)!
            
        }
        
        
        if str["LH_EmpID"] is NSNull || str["LH_EmpID"] == nil{
            self.LH_EmpID = ""
        }else{
            let locname = str["LH_EmpID"]
            
            self.LH_EmpID = (locname?.description)!
            
        }
        if str["LH_App_Date"] is NSNull || str["LH_App_Date"] == nil{
            self.LH_App_Date = ""
        }else{
            let locname = str["LH_App_Date"]
            
            self.LH_App_Date = (locname?.description)!
            
        }
        if str["LH_App_Status"] is NSNull || str["LH_App_Status"] == nil{
            self.LH_App_Status = ""
        }else{
            let locname = str["LH_App_Status"]
            
            self.LH_App_Status = (locname?.description)!
            
        }
        if str["AH_EmpID"] is NSNull || str["AH_EmpID"] == nil{
            self.AH_EmpID = ""
        }else{
            let locname = str["AH_EmpID"]
            
            self.AH_EmpID = (locname?.description)!
            
        }
       
        if str["AH_App_Date"] is NSNull || str["AH_App_Date"] == nil{
            self.AH_App_Date = ""
        }else{
            let locname = str["AH_App_Date"]
            
            self.AH_App_Date = (locname?.description)!
            
        }
        if str["AH_App_Status"] is NSNull || str["AH_App_Status"] == nil{
            self.AH_App_Status = ""
        }else{
            let locname = str["AH_App_Status"]
            
            self.AH_App_Status = (locname?.description)!
            
        }
        if str["Booking_Status"] is NSNull || str["Booking_Status"] == nil{
            self.Booking_Status = ""
        }else{
            let locname = str["Booking_Status"]
            
            self.Booking_Status = (locname?.description)!
            
        }
        if str["Booking_Date_Time"] is NSNull || str["Booking_Date_Time"] == nil{
            self.Booking_Date_Time = ""
        }else{
            let locname = str["Booking_Date_Time"]
            
            self.Booking_Date_Time = (locname?.description)!
            
        }
        if str["Remarks"] is NSNull || str["Remarks"] == nil{
            self.Remarks = ""
        }else{
            let locname = str["Remarks"]
            
            self.Remarks = (locname?.description)!
            
        }
        
  
        if str["Status"] is NSNull || str["Status"] == nil{
            self.Status = ""
        }else{
            let locname = str["Status"]
            
            self.Status = (locname?.description)!
            
        }
        if str["Date_Time"] is NSNull || str["Date_Time"] == nil{
            self.Date_Time = ""
        }else{
            let locname = str["Date_Time"]
            
            self.Date_Time = (locname?.description)!
            
        }
        if str["Feedback_Status"] is NSNull || str["Feedback_Status"] == nil{
            self.Feedback_Status = ""
        }else{
            let locname = str["Feedback_Status"]
            
            self.Feedback_Status = (locname?.description)!
            
        }
        if str["SelectedApprover"] is NSNull || str["SelectedApprover"] == nil{
            self.SelectedApprover = ""
        }else{
            let locname = str["SelectedApprover"]
            
            self.SelectedApprover = (locname?.description)!
            
        }
        
        if str["TCReq"] is NSNull || str["TCReq"] == nil{
            self.TCReq = ""
        }else{
            let locname = str["TCReq"]
            
            self.TCReq = (locname?.description)!
            
        }
        if str["TCRes"] is NSNull || str["TCRes"] == nil{
            self.TCRes = ""
        }else{
            let locname = str["TCRes"]
            
            self.TCRes = (locname?.description)!
            
        }
        if str["BookURL"] is NSNull || str["BookURL"] == nil{
            self.BookURL = ""
        }else{
            let locname = str["BookURL"]
            
            self.BookURL = (locname?.description)!
            
        }
        if str["BookURLMobile"] is NSNull || str["BookURLMobile"] == nil{
            self.BookURLMobile = ""
        }else{
            let locname = str["BookURLMobile"]
            
            self.BookURLMobile = (locname?.description)!
            
        }
    }
    
}





class MyRequestVehicleTourDataAPI
{
    
    var reachablty = Reachability()!
    
    func serviceCalling(obj:MyRequestVehicleTourViewController, param : [String: String] , success:@escaping (AnyObject)-> Void)
    {
        obj.startLoading()
        
        if(reachablty.connection != .none)
        {
            
            
            WebServices.sharedInstances.sendPostRequest(url: URLConstants.MyRequestVehicle, parameters: param , successHandler: { (dict) in
                
                if let response = dict["response"]{
                    
                    let statusString : String = response["status"] as! String
                    let objectmsg = MessageCallServerModel()
                    
                    let msg = objectmsg.sealizeMessage(cell: response as! [String : AnyObject])
                    if(statusString == "ok")
                    {
                        obj.tableView.isHidden = false
                        obj.noDataLabel(text: "" )
                        obj.label.isHidden = true
                        if let data = dict["data"] as? [AnyObject]
                        {
                            
                            var dataArray: [MyRequestVehicleTourDataModel] = []
                            
                            for i in 0..<data.count
                            {
                                
                                if let cell = data[i] as? [String:AnyObject]
                                {
                                    let object = MyRequestVehicleTourDataModel()
                                    object.setDataInModel(str: cell)
                                    dataArray.append(object)
                                }
                                
                            }
                            
                            success(dataArray as AnyObject)
                            
                            
                        }
                        
                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                    }
                    else
                    {
                        obj.label.isHidden = false
                        obj.tableView.isHidden = true
                        obj.noDataLabel(text: msg )
                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                        
                    }
                    
                    
                }
                
                
                
                
            })
            { (error) in
                
                let errorStr : String = error.description
                
                print("DATA:error",errorStr)
                
                //                obj.errorChecking(error: error)
                obj.stopLoading()
            }
            
            
        }
        else{
            obj.label.isHidden = false
            obj.tableView.isHidden = true
            obj.noDataLabel(text: "Internet is not available, please check your internet connection try again." )
            obj.stopLoading()
        }
        
        
    }
    
    
}

class MyRequestVehicleTourDataAPILoadMore
{
    
    var reachablty = Reachability()!
    
    func serviceCalling(obj:MyRequestVehicleTourViewController, param : [String: String] , success:@escaping (AnyObject)-> Void)
    {
        obj.startLoading()
        
        if(reachablty.connection != .none)
        {
            
            
            WebServices.sharedInstances.sendPostRequest(url: URLConstants.MyRequestVehicle, parameters: param , successHandler: { (dict) in
                
                if let response = dict["response"]{
                    
                    let statusString : String = response["status"] as! String
                    let objectmsg = MessageCallServerModel()
                    
                    let msg = objectmsg.sealizeMessage(cell: response as! [String : AnyObject])
                    if(statusString == "ok")
                    {
                        //                        obj.tableView.isHidden = false
                        //                        obj.noDataLabel(text: "" )
                        
                        if let data = dict["data"] as? [AnyObject]
                        {
                            
                            var dataArray: [MyRequestVehicleTourDataModel] = []
                            if(data.count > 0){
                            for i in 0..<data.count
                            {
                                
                                if let cell = data[i] as? [String:AnyObject]
                                {
                                    let object = MyRequestVehicleTourDataModel()
                                    object.setDataInModel(str: cell)
                                    dataArray.append(object)
                                }
                                
                            }
                            
                            success(dataArray as AnyObject)
                            }else{
                                obj.label.isHidden = false
                                obj.tableView.isHidden = true
                                obj.noDataLabel(text: msg )
                                obj.refresh.endRefreshing()
                                obj.stopLoading()
                            }
                            
                            
                        }
                        
                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                    }
                    else
                    {
                        
                        //                        obj.tableView.isHidden = true
                        //                        obj.noDataLabel(text: "No Data Found!" )
                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                        
                    }
                    
                    
                }
                
                
                
                
            })
            { (error) in
                
                let errorStr : String = error.description
                
                print("DATA:error",errorStr)
                
                //                obj.errorChecking(error: error)
                obj.stopLoading()
            }
            
            
        }
        else{
            //            obj.tableView.isHidden = true
            //            obj.noDataLabel(text: "Internet is not available, please check your internet connection try again." )
            obj.stopLoading()
        }
        
        
    }
    
    
}



class PendingApprovalDataModel: NSObject {
    
    
 
    var Action_Text = String()
    var Count_Record = String()
    var Action_Date_Time = String()
    var Action_Data = String()
    

  
    
    
    
    func setDataInModel(str:[String:AnyObject])
    {
        if str["Action_Text"] is NSNull || str["Action_Text"] == nil{
            self.Action_Text = ""
        }else{
            
            let ros = str["Action_Text"]
            
            self.Action_Text = (ros?.description)!
            
            
        }
        if str["Count_Record"] is NSNull || str["Count_Record"] == nil{
            self.Count_Record =  ""
        }else{
            
            let fross = str["Count_Record"]
            
            self.Count_Record = (fross?.description)!
            
            
            //  self.Description = (str["Description"] as? String)!
        }
        
        if str["Action_Date_Time"] is NSNull || str["Action_Date_Time"] == nil{
            self.Action_Date_Time = ""
        }else{
            let emp1 = str["Action_Date_Time"]
            
            self.Action_Date_Time = (emp1?.description)!
            // print(self.Employee_1)
        }
        
        if str["Action_Data"] is NSNull || str["Action_Data"] == nil{
            self.Action_Data = ""
        }else{
            
            let empname = str["Action_Data"]
            
            self.Action_Data = (empname?.description)!
            
        }
        
        
    }
    
}

class PendingApprovalDataAPI
{
    
    var reachablty = Reachability()!
    
    func serviceCalling(obj:VehicleDisplayPendingApprovalViewController, param : [String: String] , success:@escaping (AnyObject)-> Void)
    {
        obj.startLoading()
        
        if(reachablty.connection != .none)
        {
            
            
            WebServices.sharedInstances.sendPostRequest(url: URLConstants.MyApprovalCountVehicle, parameters: param , successHandler: { (dict) in
                
                if let response = dict["response"]{
                    
                    let statusString : String = response["status"] as! String
                    let objectmsg = MessageCallServerModel()
                    
                    let msg = objectmsg.sealizeMessage(cell: response as! [String : AnyObject])
                    if(statusString == "ok")
                    {
                        obj.tableDisplayHazAction.isHidden = false
                        obj.noDataLabel(text: "" )
                        obj.label.isHidden = true
                        if let data = dict["data"] as? [AnyObject]
                        {
                            
                            var dataArray: [PendingApprovalDataModel] = []
                            
                            for i in 0..<data.count
                            {
                                
                                if let cell = data[i] as? [String:AnyObject]
                                {
                                    let object = PendingApprovalDataModel()
                                    object.setDataInModel(str: cell)
                                    dataArray.append(object)
                                }
                                
                            }
                            
                            success(dataArray as AnyObject)
                            
                            
                        }
                        
                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                    }
                    else
                    {
                        obj.label.isHidden = false
                        obj.tableDisplayHazAction.isHidden = true
                        obj.noDataLabel(text: msg )
                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                        
                    }
                    
                    
                }
                
                
                
                
            })
            { (error) in
                
                let errorStr : String = error.description
                
                print("DATA:error",errorStr)
                
                //                obj.errorChecking(error: error)
                obj.stopLoading()
            }
            
            
        }
        else{
            obj.label.isHidden = false
            obj.tableDisplayHazAction.isHidden = true
            obj.noDataLabel(text: "Internet is not available, please check your internet connection try again." )
            obj.stopLoading()
        }
        
        
    }
    
    
}
