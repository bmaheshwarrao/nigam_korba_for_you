
//
//  ApprovalAuthorityViewController.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 25/02/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit

class ApprovalAuthorityViewController: CommonVSClass {
    
    var titleStr = String()
    @IBOutlet weak var tableViewASUZ: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableViewASUZ.delegate = self;
        tableViewASUZ.dataSource = self;
        let swipe = UISwipeGestureRecognizer(target: self, action: #selector(self.swipeRecognizer))
        swipe.direction = ([.down])
        self.view.addGestureRecognizer(swipe)
        callDataListData()
    }
    
    var reachability = Reachability()!
    @objc func swipeRecognizer(_ sender: UISwipeGestureRecognizer) {
        if (sender.direction == .down) {
            
            if(self.reachability.connection != .none)
            {
                
                self.callDataListData()
            }
            else
            {
                self.startLoading(view: self.view)
                DispatchQueue.main.asyncAfter(deadline: .now() + 4.5) {
                    self.noDataLabel(text: "No internet connection found. Check your internet connection and try again.")
                    self.stopLoading(view: self.view)
                }
                
                
            }
            
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.title = titleStr
        self.tabBarController?.tabBar.isHidden = true
        let nav = self.navigationController?.navigationBar
        nav?.tintColor = UIColor.white
       nav?.barTintColor = UIColor(hexString: "2c3e50", alpha: 1.0)
        nav?.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
    }
    
    var reachablty = Reachability()!
    
    var DataListModelDB:[ApprovalAuthDataListModel] = []
    
    var DataListCallApi = ApprovalAuthorityListApi()
    @objc func callDataListData(){
        
        let empId = UserDefaults.standard.string(forKey: "EmployeeID")!
        let Profile_AuthKey = UserDefaults.standard.string(forKey: "Profile_AuthKey")!
        let paramm  : [String:String] = ["UserID": empId , "AuthKey" :Profile_AuthKey ]
        
        self.DataListCallApi.serviceCalling(obj: self , param : paramm) { (dict) in
       
            self.DataListModelDB = dict as! [ApprovalAuthDataListModel]
            
            self.tableViewASUZ.reloadData()
            
        }
    }
    @objc func tapViewTapClicked(_ sender: UITapGestureRecognizer) {
        
        let buttonPosition:CGPoint = sender.view!.convert(.zero, to:self.tableViewASUZ)
        let indexPath = self.tableViewASUZ.indexPathForRow(at: buttonPosition)
        
        
     
            
            FilterDataFromServer.Authority_Id = DataListModelDB[(indexPath?.row)!].id
            FilterDataFromServer.Authority_Name = DataListModelDB[(indexPath?.row)!].name
        
        self.navigationController?.popViewController(animated: true)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected selfect to the new view controller.
     }
     */
    
}
extension ApprovalAuthorityViewController : UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
    }
}
extension ApprovalAuthorityViewController : UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return DataListModelDB.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! NDSOASUZTableViewCell
        let TapGesture = UITapGestureRecognizer(target: self, action: #selector(self.tapViewTapClicked(_:)))
        cell.lblASUZ.addGestureRecognizer(TapGesture)
        cell.lblASUZ.isUserInteractionEnabled = true
        TapGesture.numberOfTapsRequired = 1
        cell.lblASUZ.text = DataListModelDB[indexPath.row].name
        
        return cell
        
        
        
    }
}


