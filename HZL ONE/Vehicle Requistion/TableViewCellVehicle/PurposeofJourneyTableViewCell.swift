//
//  PurposeofJourneyTableViewCell.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 26/02/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit

class PurposeofJourneyTableViewCell: UITableViewCell {
    @IBOutlet weak var txtPurpose: UITextField!
    @IBOutlet weak var viewPurpose: UIView!
    @IBOutlet weak var lblAddJourneyDetail: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        viewPurpose.layer.borderWidth = 1.0
        viewPurpose.layer.borderColor = UIColor.black.cgColor
        viewPurpose.layer.cornerRadius = 10.0
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
