//
//  ApprovalAuthorityTableViewCell.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 26/02/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit

class ApprovalAuthorityTableViewCell: UITableViewCell {
    @IBOutlet weak var btnApprovalAuth: UIButton!
    @IBOutlet weak var lblApprovalAuth: UILabel!
    @IBOutlet weak var viewApprovalAuth: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        viewApprovalAuth.layer.borderWidth = 1.0
        viewApprovalAuth.layer.borderColor = UIColor.black.cgColor
        viewApprovalAuth.layer.cornerRadius = 10.0
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
