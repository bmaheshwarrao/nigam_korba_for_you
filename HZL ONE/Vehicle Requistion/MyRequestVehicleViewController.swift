//
//  MyRequestVehicleViewController.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 26/02/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit
import ViewPager_Swift
class MyRequestVehicleViewController: CommonVSClass {
    
    @IBOutlet weak var lblLocal: UILabel!
    @IBOutlet weak var lblOutStation: UILabel!
    @IBOutlet weak var lblTour: UILabel!
    @IBOutlet weak var viewSplitter: UIView!
    @IBOutlet weak var viewStack: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        viewStack.backgroundColor = UIColor(hexString: "2c3e50", alpha: 1.0)!
        Reportdata()
        let myOptions = ViewPagerOptions(viewPagerWithFrame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height))
        
        myOptions.tabType = ViewPagerTabType.basic
        
        
        
        
        
        
        myOptions.isTabHighlightAvailable = true
        // myOptions.is
        // If I want indicator bar to show below current page tab
        myOptions.isTabIndicatorAvailable = true
        myOptions.fitAllTabsInView = true;
        // Oh! and let's change color of tab to red
        myOptions.tabIndicatorViewBackgroundColor = UIColor.white
        myOptions.tabViewBackgroundHighlightColor = UIColor(hexString: "2c3e50", alpha: 1.0)!
        myOptions.tabViewBackgroundDefaultColor = UIColor(hexString: "2c3e50", alpha: 1.0)!
        myOptions.tabViewTextDefaultColor = UIColor.white
        myOptions.tabViewTextHighlightColor = UIColor.white
        myOptions.tabViewTextFont = UIFont.systemFont(ofSize: 15.0, weight: .medium)
        let viewPager = ViewPagerController()
        
        viewPager.options = myOptions
        viewPager.dataSource = self
        
        //Now let me add this to my viewcontroller
        self.addChildViewController(viewPager)
        
        viewPager.view.translatesAutoresizingMaskIntoConstraints = false
        
        viewSplitter.addSubview(viewPager.view)
        
        viewPager.view.topAnchor.constraint(equalTo: viewSplitter.topAnchor, constant: 0.0).isActive = true
        viewPager.view.bottomAnchor.constraint(equalTo: viewSplitter.bottomAnchor, constant: 0.0).isActive = true
        viewPager.view.leadingAnchor.constraint(equalTo: viewSplitter.leadingAnchor, constant: 0.0).isActive = true
        viewPager.view.trailingAnchor.constraint(equalTo: viewSplitter.trailingAnchor, constant: 0.0).isActive = true
        viewPager.didMove(toParentViewController: self)
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.tabBarController?.tabBar.isHidden = true
        self.title = "My Request's"
        let nav = self.navigationController?.navigationBar
        
        nav?.barTintColor = UIColor(hexString: "2c3e50", alpha: 1.0)
        nav?.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    let reachability = Reachability()!
    @objc func Reportdata() {
        if(self.reachability.connection != .none) {
            let pno : String = (UserDefaults.standard.string(forKey: "EmployeeID")!)
            let Profile_AuthKey = UserDefaults.standard.string(forKey: "Profile_AuthKey")!
            let parameters = ["UserID": pno ,"AuthKey": Profile_AuthKey  ]
            
            self.startLoading()
            print(parameters)
            WebServices.sharedInstances.sendPostRequest(url: URLConstants.MyRequestCount,parameters: parameters, successHandler: { (dict) in
                
                //self.stopLoading()
                
                print("",dict)
                
                if let response = dict["response"] {
                    
                    let statusString : String = response["status"] as! String
                    let objectmsg = MessageCallServerModel()
                    
                    let msg = objectmsg.sealizeMessage(cell: response as! [String : AnyObject])
                    print(msg)
                    if(statusString == "ok")
                    {
                        self.stopLoading()
                        
                        
                        
                        if let data = dict["data"]
                        {
                            
                            self.refresh.endRefreshing()
                            let local : Int = Int(data["Locals"] as! NSNumber)
                            let outs : Int = Int(data["Outs"] as! NSNumber)
                            let tour : Int = Int(data["Tours"] as! NSNumber)
                            self.lblLocal.text = String(local)
                            self.lblOutStation.text = String(outs)
                            self.lblTour.text = String(tour)
                            
                            
                            
                            
                        }
                        
                        
                    }
                    else
                    {self.stopLoading()
                        self.refresh.endRefreshing()
                        self.showSingleButtonWithMessage(title: statusString, message: msg, buttonName: "OK")
                    }
                }
                
                
                
                
            }, failureHandler: { (error) in
                
                self.refresh.endRefreshing()
                self.stopLoading()
                
                self.errorChecking(error: error[0])
                
                
                
            })
            
            
            
        }
        else {
            
            // self.showSingleButtonWithMessage(title: "No Network!", message: "No internet connection found. Check your internet connection or try again.", buttonName: "OK")
            self.refresh.endRefreshing()
            self.stopLoading()
            
        }
        
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
extension MyRequestVehicleViewController : ViewPagerControllerDataSource {
    func numberOfPages() -> Int {
        return 3
    }
    
    func viewControllerAtPosition(position: Int) -> UIViewController {
        let storyBoard : UIStoryboard = UIStoryboard(name: "VehicleRequistion",bundle : nil)
        if(position == 0) {
            
            let empVC = storyBoard.instantiateViewController(withIdentifier: "MyRequestVehicleLocalViewController") as! MyRequestVehicleLocalViewController
            return  empVC as CommonVSClass
        }else  if(position == 1) {
            
            let empVC = storyBoard.instantiateViewController(withIdentifier: "MyRequestVehicleOutViewController") as! MyRequestVehicleOutViewController
            return  empVC as CommonVSClass
        }else {
            let nonEmpVC = storyBoard.instantiateViewController(withIdentifier: "MyRequestVehicleTourViewController") as! MyRequestVehicleTourViewController
            return  nonEmpVC as CommonVSClass
        }
    }
    
    func tabsForPages() -> [ViewPagerTab] {
        return [ViewPagerTab(title: "LOCAL" , image: #imageLiteral(resourceName: "BackBlack")) , ViewPagerTab(title: "OUTSTATION" , image: #imageLiteral(resourceName: "BackBlack")), ViewPagerTab(title: "TOUR" , image: #imageLiteral(resourceName: "BackBlack"))]
    }
    
    
}
extension MyRequestVehicleViewController : ViewPagerControllerDelegate {
    
}



