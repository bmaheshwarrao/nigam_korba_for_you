//
//  NewsCollectionViewCell.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 01/02/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit

class NewsCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var textViewNews: UITextView!
    @IBOutlet weak var newsCollectionView: UIImageView!
}
