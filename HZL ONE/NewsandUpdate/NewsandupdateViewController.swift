//
//  NewsandupdateViewController.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 01/02/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit
import SDWebImage
class NewsandupdateViewController: CommonVSClass ,UITableViewDelegate,UITableViewDataSource{

    
    
    @IBOutlet weak var tableView: UITableView!
    var refreshControl = UIRefreshControl()
    
    var NewsDB:[NewsDataModel] = []
    var messagesAPI = messagesDataAPI()
    
    var NewsLoadMoreDB : [NewsDataModel] = []
    
    var newsStrNav = String()
    
    var data: String?
    var lastObject: String?
    var messagesLoadMoreAPI = messagesDataAPILoadMore()
    override func viewDidLoad() {
        super.viewDidLoad()
       getNewsData()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        
 
        
        
        refreshControl.addTarget(self, action: #selector(getNewsData), for: .valueChanged)
        
        self.tableView.addSubview(refreshControl)
        let swipe = UISwipeGestureRecognizer(target: self, action: #selector(self.swipeRecognizer))
        swipe.direction = ([.down])
        self.view.addGestureRecognizer(swipe)
        // Do any additional setup after loading the view.
    }
    var reachability = Reachability()!
    @objc func swipeRecognizer(_ sender: UISwipeGestureRecognizer) {
        if (sender.direction == .down) {
            
            if(self.reachability.connection != .none)
            {
                
                self.getNewsData()
            }
            else
            {
                self.startLoading(view: self.view)
                DispatchQueue.main.asyncAfter(deadline: .now() + 4.5) {
                    self.noDataLabel(text: "No internet connection found. Check your internet connection and try again.")
                    self.stopLoading(view: self.view)
                }
            }
            
        }
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
       
        self.tabBarController?.tabBar.isHidden = true
        self.title = newsStrNav
        let nav = self.navigationController?.navigationBar
        nav?.tintColor = UIColor.white
       nav?.barTintColor = UIColor(hexString: "2c3e50", alpha: 1.0)
        nav?.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        
    }
    
    @objc func tapViewTapClicked(_ sender: UITapGestureRecognizer) {
        let buttonPosition:CGPoint = sender.view!.convert(.zero, to:self.tableView)
     
        
       let indexPath = self.tableView.indexPathForRow(at: buttonPosition)
        let storyBoard = UIStoryboard(name: "NewsUpdate", bundle: nil)
        let newsVC = storyBoard.instantiateViewController(withIdentifier: "NewsupdateDetailsViewController") as! NewsupdateDetailsViewController
        newsVC.File_Description = self.NewsDB[(indexPath?.row)!].Description
         newsVC.File_Url = self.NewsDB[(indexPath?.row)!].File_Url
         newsVC.File_title = self.NewsDB[(indexPath?.row)!].Title
        self.navigationController?.pushViewController(newsVC, animated: true)
        
    }
    
    
    
    @objc func getNewsData(){
        var param = [String:String]()
        
        param =  [:]
        messagesAPI.serviceCalling(obj: self,  param: param ) { (dict) in
            
            self.NewsDB = dict as! [NewsDataModel]
            
            self.tableView.reloadData()
            
        }
    }
    
    @objc func getNewsDataLoadMore(ID : String){
        var param = [String:String]()
        
        param =  ["ID":ID]
        messagesLoadMoreAPI.serviceCalling(obj: self,  param: param ) { (dict) in
            
            self.NewsLoadMoreDB =  [NewsDataModel]()
            self.NewsLoadMoreDB = dict as! [NewsDataModel]
            
            switch dict.count {
            case 0:
                break;
            default:
                self.NewsDB.append(contentsOf: self.NewsLoadMoreDB)
                self.tableView.reloadData()
                break;
            }
            
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func imageResize (image:UIImage, sizeChange:CGSize)-> UIImage{
        
        let hasAlpha = true
        let scale: CGFloat = 0.0 // Use scale factor of main screen
        
        UIGraphicsBeginImageContextWithOptions(sizeChange, !hasAlpha, scale)
        image.draw(in: CGRect(origin: CGPoint.zero, size: sizeChange))
        
        let scaledImage = UIGraphicsGetImageFromCurrentImageContext()
        return scaledImage!
    }
   
    var indPath = IndexPath()
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        
        
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.NewsDB.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! NewsUpdateTableViewCell
        
        
        
        let TapGesture = UITapGestureRecognizer(target: self, action: #selector(self.tapViewTapClicked(_:)))
        cell.addGestureRecognizer(TapGesture)
        cell.isUserInteractionEnabled = true
        TapGesture.numberOfTapsRequired = 1
        
        
        
        cell.textViewNews.text = self.NewsDB[indexPath.row].Title
        
        self.data = String(self.NewsDB[indexPath.row].ID)
        self.lastObject = String(self.NewsDB[indexPath.row].ID)
        
        if ( self.data ==  self.lastObject && indexPath.row == self.NewsDB.count - 1)
        {
            
            self.getNewsDataLoadMore( ID: String(Int(self.NewsDB[indexPath.row].ID)))
            
        }
        
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableViewAutomaticDimension
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
