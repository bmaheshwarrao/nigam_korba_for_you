//
//  KpiOverviewSheetViewController.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 27/03/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit
import CoreData
import SpreadsheetView

class KpiOverviewSheetViewController: CommonVSClass,UITextFieldDelegate, SpreadsheetViewDataSource, SpreadsheetViewDelegate {
    
    
    var OverViewSheetListApi = MyKpiOverviewSheetAPI()
    var OverViewSheetListDB : [MyKpiOverviewModel] = []
    var spreadsheetView = SpreadsheetView()
    @IBOutlet weak var spreadView: UIView!
    let dates = ["FTDZone1", "FTDZone2", "FTDZone3", "FTDZone4", "FTDZone5", "FTDZone6", "MTDZone1", "MTDZone2", "MTDZone3", "MTDZone4", "MTDZone5", "MTDZone6","YTDZone1", "YTDZone2", "YTDZone3", "YTDZone4", "YTDZone5", "YTDZone6"]
    
    
    let menuBarBtn = UIButton(type: .custom)
    var menuBarItemBtn = UIBarButtonItem()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
       
        // Do any additional setup after loading the view.
    }
    
   
    
    
    @IBOutlet weak var viewShow: UIView!
    var OverViewSheetAPI = MyKpiOverviewSheetAPI()
    var OverViewSheetFinDB:[MyKpiOverviewModel] = []
    var parameter = [String:String]()
    var dictWithoutNilValues = [String:String]()
    @objc func callOverViewData(){
        
        
        
        let nowDate = Date()
       
        let dateFormatter2 = DateFormatter()
        dateFormatter2.timeZone = NSTimeZone.system
        dateFormatter2.dateFormat = "yyyy-MM-dd"
        let date_TimeFrom = dateFormatter2.string(from: nowDate)
        
        self.OverViewSheetFinDB = [MyKpiOverviewModel]()
        
        dictWithoutNilValues = [ "FTD":date_TimeFrom
        ]
        
        parameter = dictWithoutNilValues.filter { $0.value != ""}
        
        
        
        
        print("initialPar",parameter)
        OverViewSheetAPI.serviceCalling(obj: self, param: parameter) { (dict) in
            
            self.OverViewSheetFinDB = [MyKpiOverviewModel]()
            self.OverViewSheetFinDB = dict as! [MyKpiOverviewModel]
            
            self.spreadsheetView.reloadData()
        }
        self.spreadsheetView.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(true)
        self.tabBarController?.tabBar.isHidden = true
        self.title = "Overview Sheet"
       
  
        
        self.spreadsheetView.frame = CGRect(x: 0, y: 0, width: self.spreadView.frame.width, height: self.spreadView.frame.height)
        spreadsheetView.dataSource = self
        spreadsheetView.delegate = self
        
        spreadsheetView.contentInset = UIEdgeInsets(top: 4, left: 0, bottom: 4, right: 0)
        
        spreadsheetView.intercellSpacing = CGSize(width: 4, height: 1)
        spreadsheetView.gridStyle = .solid(width: 1.0, color: color)
        
        spreadsheetView.register(DateCell.self, forCellWithReuseIdentifier: String(describing: DateCell.self))
        spreadsheetView.register(TimeTitleCell.self, forCellWithReuseIdentifier: String(describing: TimeTitleCell.self))
        spreadsheetView.register(TimeCell.self, forCellWithReuseIdentifier: String(describing: TimeCell.self))
        spreadsheetView.register(DayTitleCell.self, forCellWithReuseIdentifier: String(describing: DayTitleCell.self))
        spreadsheetView.register(ScheduleCell.self, forCellWithReuseIdentifier: String(describing: ScheduleCell.self))
        self.spreadView.addSubview(self.spreadsheetView)
        callOverViewData()
        
        
        
        
    }
    
    
    // MARK: DataSource
    
    func numberOfColumns(in spreadsheetView: SpreadsheetView) -> Int {
        return 1 + dates.count
    }
    
    func numberOfRows(in spreadsheetView: SpreadsheetView) -> Int {
        return 1  + OverViewSheetFinDB.count
    }
    
    func spreadsheetView(_ spreadsheetView: SpreadsheetView, widthForColumn column: Int) -> CGFloat {
        if case 0 = column {
            return 150
        } else {
            return 70
        }
    }
    
    func spreadsheetView(_ spreadsheetView: SpreadsheetView, heightForRow row: Int) -> CGFloat {
        
        return 60
        
    }
    
    func frozenColumns(in spreadsheetView: SpreadsheetView) -> Int {
        return 1
    }
    
    func frozenRows(in spreadsheetView: SpreadsheetView) -> Int {
        return 1
    }
    let color = UIColor.lightGray
    func spreadsheetView(_ spreadsheetView: SpreadsheetView, cellForItemAt indexPath: IndexPath) -> Cell? {
        if case (1...(dates.count + 1), 0) = (indexPath.column, indexPath.row) {
            let cell = spreadsheetView.dequeueReusableCell(withReuseIdentifier: String(describing: DateCell.self), for: indexPath) as! DateCell
            cell.label.text = dates[indexPath.column - 1]
            
            return cell
        } else if case (0, 0) = (indexPath.column, indexPath.row) {
             let cell = spreadsheetView.dequeueReusableCell(withReuseIdentifier: String(describing: TimeCell.self), for: indexPath) as! TimeCell
          
            cell.label.text = "Name"
            cell.label.font = UIFont.systemFont(ofSize: 14, weight: .medium)
            cell.label.textColor = UIColor.black
            
            
            
            return cell
        }
            
        else if case (0, 1...(OverViewSheetFinDB.count ) ) = (indexPath.column, indexPath.row) {
            
            
            
            
            
            
            let cell = spreadsheetView.dequeueReusableCell(withReuseIdentifier: String(describing: TimeCell.self), for: indexPath) as! TimeCell
            
            cell.label.text = OverViewSheetFinDB[indexPath.row - 1].Incident_Name
            
            
            
            
            
            return cell
        }
            
        else if case (1...(dates.count + 1), 1...(OverViewSheetFinDB.count ) ) = (indexPath.column, indexPath.row) {
            
            
            
            
            
            
            let cell = spreadsheetView.dequeueReusableCell(withReuseIdentifier: String(describing: TimeCell.self), for: indexPath) as! TimeCell
            
            
            if(indexPath.column == 1){
                cell.label.text = String(OverViewSheetFinDB[indexPath.row - 1].FTDZone1)
            }else if(indexPath.column == 2){
                cell.label.text = String(OverViewSheetFinDB[indexPath.row - 1].FTDZone3)
            }else if(indexPath.column == 3){
                cell.label.text = String(OverViewSheetFinDB[indexPath.row - 1].FTDZone3)
            }else if(indexPath.column == 4){
                cell.label.text = String(OverViewSheetFinDB[indexPath.row - 1].FTDZone4)
            }else if(indexPath.column == 5){
                cell.label.text = String(OverViewSheetFinDB[indexPath.row - 1].FTDZone5)
            }else if(indexPath.column == 6){
                cell.label.text = String(OverViewSheetFinDB[indexPath.row - 1].FTDZone5)
            }else if(indexPath.column == 7){
                cell.label.text = String(OverViewSheetFinDB[indexPath.row - 1].MTDZone1)
            }else if(indexPath.column == 8){
                cell.label.text = String(OverViewSheetFinDB[indexPath.row - 1].MTDZone2)
            }else if(indexPath.column == 9){
                cell.label.text = String(OverViewSheetFinDB[indexPath.row - 1].MTDZone3)
            }else if(indexPath.column == 10){
                cell.label.text = String(OverViewSheetFinDB[indexPath.row - 1].MTDZone4)
            }else if(indexPath.column == 11){
                cell.label.text = String(OverViewSheetFinDB[indexPath.row - 1].MTDZone5)
            }else if(indexPath.column == 12){
                cell.label.text = String(OverViewSheetFinDB[indexPath.row - 1].MTDZone6)
            }else if(indexPath.column == 13){
                cell.label.text = String(OverViewSheetFinDB[indexPath.row - 1].YTDZone1)
            }else if(indexPath.column == 14){
                cell.label.text = String(OverViewSheetFinDB[indexPath.row - 1].YTDZone2)
            }else if(indexPath.column == 15){
                cell.label.text = String(OverViewSheetFinDB[indexPath.row - 1].YTDZone3)
            }else if(indexPath.column == 16){
                cell.label.text = String(OverViewSheetFinDB[indexPath.row - 1].YTDZone4)
            }else if(indexPath.column == 17){
                cell.label.text = String(OverViewSheetFinDB[indexPath.row - 1].YTDZone5)
            }else if(indexPath.column == 18){
                cell.label.text = String(OverViewSheetFinDB[indexPath.row - 1].YTDZone6)
            }
            
            
            
            
            
            
            return cell
        }
        
        
        
        
        return nil
    }
    
    /// Delegate
    
    func spreadsheetView(_ spreadsheetView: SpreadsheetView, didSelectItemAt indexPath: IndexPath) {
        print("Selected: (row: \(indexPath.row), column: \(indexPath.column))")
    }
   
    
    
    
    
    
    
}
