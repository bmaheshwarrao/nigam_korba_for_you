//
//  FilterKpiOverviewViewController.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 27/03/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit

class FilterKpiOverviewViewController: CommonVSClass,UITextFieldDelegate {
    
   
    
    let reachablty = Reachability()!
    
   
    @IBOutlet weak var bottomHeight: NSLayoutConstraint!
    
    
    var AreaIDVal = String()
    
    var SubAreaIDVal = String()
    
    
    
    
  
    
    @IBOutlet weak var fromDateTF: UITextField!
    @IBOutlet weak var toDateTF: UITextField!
    
    var datePicker : UIDatePicker!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        //  getLocation_MasterData()
        
        self.fromDateTF.delegate = self
        self.toDateTF.delegate = self
        
        //
      
        
        
        switch FilterGraphStruct.fromDate {
        case "":
            fromDateTF.placeholder = "From Date"
            break;
        default:
            fromDateTF.text = FilterGraphStruct.fromDate
            break;
        }
        
        switch FilterGraphStruct.toDate {
        case "":
            toDateTF.placeholder = "To Date"
            break;
        default:
            toDateTF.text = FilterGraphStruct.toDate
            break;
        }
        
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        
        self.tabBarController?.tabBar.isHidden = true
        self.title = "Filter"
        let nav = self.navigationController?.navigationBar
        nav?.tintColor = UIColor.white
        nav?.barTintColor = UIColor(hexString: "2c3e50", alpha: 1.0)
        nav?.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
       
        
    }
    
  
    
    var identifierStr = String()
    
    @IBAction func applyFilterAction(_ sender: Any) {
        
        if reachablty.connection == .none{
            
            self.view.makeToast("Internet is not available, please check your internet connection try again.")
            
        }else{
            
            if(FilterGraphStruct.fromDate == String()){
                self.view.makeToast("Please Select From Date")
            }else if(FilterGraphStruct.toDate == String()){
                self.view.makeToast("Please Select To Date")
            }else{
            switch identifierStr {
            case "SearchKpiOverviewBar":
         
                NotificationCenter.default.post(name:NSNotification.Name.init("BarOverviewFilter"), object: nil)
                self.navigationController?.popViewController(animated: true)
                break;
            case "SearchKpiOverviewPie":
                
                NotificationCenter.default.post(name:NSNotification.Name.init("PieOverviewFilter"), object: nil)
                self.navigationController?.popViewController(animated: true)
                break;
            default:
                
                break;
            }
            }
            
        }
    }
    
    @IBAction func resetFilterAction(_ sender: Any) {
        
      
        
        FilterGraphStruct.fromDate = String()
        FilterGraphStruct.toDate = String()
        
        
        
   
        self.fromDateTF.text = nil
        self.toDateTF.text = nil
        self.fromDateTF.placeholder = "From Date"
        self.toDateTF.placeholder = "To Date"
        
       
        
    }
    
    
    
    func pickUpFromDate(_ textField : UITextField){
        
        // DatePicker
        self.datePicker = UIDatePicker(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
        self.datePicker.backgroundColor = UIColor.white
        self.datePicker.datePickerMode = UIDatePickerMode.date
        if FilterGraphStruct.toDate.isEmpty == false{
            dateValidation(datePicker: self.datePicker, date: FilterGraphStruct.toDate, type: "from")
        }
        textField.inputView = self.datePicker
        
        // ToolBar
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(self.doneClickFromTF))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(self.cancelClick))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        textField.inputAccessoryView = toolBar
        
    }
    
    func pickUpToDate(_ textField : UITextField){
        
        // DatePicker
        self.datePicker = UIDatePicker(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
        self.datePicker.backgroundColor = UIColor.white
        self.datePicker.datePickerMode = UIDatePickerMode.date
        if FilterGraphStruct.fromDate.isEmpty == false{
            dateValidation(datePicker: self.datePicker, date: FilterGraphStruct.fromDate, type: "to")
        }
        textField.inputView = self.datePicker
        
        
        
        // ToolBar
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(self.doneClickToTF))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(self.cancelClick))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        textField.inputAccessoryView = toolBar
        
    }
    
    func dateValidation(datePicker:UIDatePicker, date:String, type:String) {
        
        let dateFormatterFinal = DateFormatter()
        dateFormatterFinal.dateFormat = "yyyy/MM/dd"
        let minDate: Date =  dateFormatterFinal.date(from: date)!
        switch type {
        case "to":
            datePicker.minimumDate = minDate
            break;
        default:
            datePicker.maximumDate = minDate
            break;
        }
        
    }
    
    @objc func doneClickFromTF() {
        
        let dateFormatter1 = DateFormatter()
        dateFormatter1.dateStyle = .medium
        dateFormatter1.timeStyle = .none
        self.fromDateTF.text = dateFormatter1.string(from: datePicker.date)
        let dateFormatterFinal = DateFormatter()
        dateFormatterFinal.timeZone = NSTimeZone.system
        dateFormatterFinal.dateFormat = "yyyy/MM/dd"
        FilterGraphStruct.fromDate = dateFormatterFinal.string(from: datePicker.date)
        self.fromDateTF.resignFirstResponder()
        
    }
    
    @objc func doneClickToTF() {
        
        let dateFormatter1 = DateFormatter()
        dateFormatter1.dateStyle = .medium
        dateFormatter1.timeStyle = .none
        
        self.toDateTF.text = dateFormatter1.string(from: datePicker.date)
        
        let dateFormatterFinal = DateFormatter()
        dateFormatterFinal.timeZone = NSTimeZone.system
        dateFormatterFinal.dateFormat = "yyyy/MM/dd"
        FilterGraphStruct.toDate = dateFormatterFinal.string(from: datePicker.date)
        
        self.toDateTF.resignFirstResponder()
    }
    
    @objc func cancelClick() {
        
        self.fromDateTF.resignFirstResponder()
        //
        self.toDateTF.resignFirstResponder()
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField == self.fromDateTF{
            self.pickUpFromDate(self.fromDateTF)
        }else if textField == self.toDateTF {
            self.pickUpToDate(self.toDateTF)
        }else
        {
            //self.toDateTF.resignFirstResponder()
        }
    }
    
}


