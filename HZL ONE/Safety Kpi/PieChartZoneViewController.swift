//
//  PieChartZoneViewController.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 27/03/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//


import UIKit
import CoreData
import Charts


class PieChartZoneViewController: CommonVSClass ,UITableViewDelegate, UITableViewDataSource,ChartViewDelegate{
    
    @IBOutlet weak var tableView: UITableView!
    
    var refreshControl = UIRefreshControl()
    var Zone_MasterDB:[Zone_DB_Data] = []
    var PieChartZoneDB:[Zone_DB_Data] = []
    var PieChartZoneCountDB:[Zone_DB_Data] = []
    var PieChartZoneAPI = ZoneSafetyKPIDataAPI()
    @IBOutlet weak var lblToDate: UILabel!
    @IBOutlet weak var lblFromDate: UILabel!
    @IBOutlet weak var heightCon: NSLayoutConstraint!
    
    @IBOutlet weak var FromHeightCon: NSLayoutConstraint!
    @IBOutlet weak var ToHeightCon: NSLayoutConstraint!
    @IBOutlet weak var filterHeightCon: NSLayoutConstraint!

    override func viewDidLoad() {
        super.viewDidLoad()
        
      
        
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.estimatedRowHeight = 65
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.tableFooterView = UIView()
        
        refreshControl.addTarget(self, action: #selector(getPieChartZoneData), for: .valueChanged)
        
        self.tableView.addSubview(refreshControl)
        let swipe = UISwipeGestureRecognizer(target: self, action: #selector(self.swipeRecognizer))
        swipe.direction = ([.down])
        self.view.addGestureRecognizer(swipe)
        
         NotificationCenter.default.addObserver(self, selector: #selector(self.getPieChartZoneData), name: NSNotification.Name?.init(NSNotification.Name(rawValue: "PieOverviewFilter")), object: nil)
        
        
        
    }
    
    @IBAction func btnPieChartZoneFilterClicked(_ sender: UIBarButtonItem) {
            let storyBoard = UIStoryboard(name: "SafetyKpi", bundle: nil)
            let SearchFilterVC = storyBoard.instantiateViewController(withIdentifier: "FilterKpiOverviewViewController") as! FilterKpiOverviewViewController
            SearchFilterVC.identifierStr = "SearchKpiOverviewPie"
            self.navigationController?.pushViewController(SearchFilterVC, animated: true)
    }
    @objc func getZone_MasterData() {
        
        self.Zone_MasterDB = [Zone_DB_Data]()
        
        let fetchRequest: NSFetchRequest<Zone_DB_Data> = Zone_DB_Data.fetchRequest()
        let sort = NSSortDescriptor(key: #keyPath(Zone_DB_Data.sl), ascending: true)
        fetchRequest.sortDescriptors = [sort]
        do {
            
            self.Zone_MasterDB = try context.fetch(Zone_DB_Data.fetchRequest())
            let zoneDR = self.Zone_MasterDB.unique{$0.sl}
            self.PieChartZoneDB = zoneDR.sorted { $0.incident_Name! < $1.incident_Name! }
            let zoneDR1 = self.Zone_MasterDB.unique{$0.zone_id}
            self.PieChartZoneCountDB = zoneDR1.sorted { $0.incident_Name! < $1.incident_Name!}
            print(PieChartZoneCountDB.count)
            self.tableView.reloadData()
            
        } catch {
            print("Fetching Failed")
        }
        
    }
    var PieChartInternalDB : [Zone_DB_Data] = []
    func countData(incident_Name : String) -> [Zone_DB_Data]{
        if(self.Zone_MasterDB.count > 0) {
            PieChartInternalDB = []
            for i in 0...self.Zone_MasterDB.count - 1 {
                if(self.Zone_MasterDB[i].incident_Name == incident_Name){
                    PieChartInternalDB.append(self.Zone_MasterDB[i])
                }
            }
        }
        return PieChartInternalDB
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.tabBarController?.tabBar.isHidden = true
        self.title = "KPI Overview"
        let nav = self.navigationController?.navigationBar
        nav?.tintColor = UIColor.white
        nav?.barTintColor = UIColor(hexString: "2c3e50", alpha: 1.0)
        nav?.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        if(FilterGraphStruct.fromDate == String() && FilterGraphStruct.toDate == String()){
            heightCon.constant = 0
            ToHeightCon.constant = 0
            FromHeightCon.constant = 0
            filterHeightCon.constant = 0
        }else{
            heightCon.constant = 60
            lblFromDate.text = "From Date : " + FilterGraphStruct.fromDate
            lblToDate.text = "To Date : " + FilterGraphStruct.toDate
            ToHeightCon.constant = 18
            FromHeightCon.constant = 18
            filterHeightCon.constant = 18
        }
          getPieChartZoneData()
    }
    var reachability = Reachability()!
    @objc func swipeRecognizer(_ sender: UISwipeGestureRecognizer) {
        if (sender.direction == .down) {
            
            if(self.reachability.connection != .none)
            {
                
                self.getPieChartZoneData()
            }
            else
            {
                self.startLoading(view: self.view)
                DispatchQueue.main.asyncAfter(deadline: .now() + 4.5) {
                    self.noDataLabel(text: "No internet connection found. Check your internet connection and try again.")
                    self.stopLoading(view: self.view)
                }
            }
            
        }
    }
    
    @IBOutlet var tapView: UITapGestureRecognizer!
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func setDataCount(_ count: Int, range: UInt32) {
        
    }
    var data: String?
    var lastObject: String?
    var indPath = IndexPath()
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.PieChartZoneDB.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! PieChartZoneTableViewCell
        let dbLine : [Zone_DB_Data] =  countData(incident_Name: self.PieChartZoneDB[indexPath.row].incident_Name!)
        cell.lblZoneName.text = self.PieChartZoneDB[indexPath.row].incident_Name!
        cell.cellConfigure(LineDB: dbLine)
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 500
        
    }
    
    @objc func getPieChartZoneData(){
        var param = [String:String]()
        
        
        if(FilterGraphStruct.fromDate == String() && FilterGraphStruct.toDate == String()){
        let frDate = Date().beginningOfMonth
        let toDate = Date().endOfMonth
        let dateFormatter2 = DateFormatter()
        dateFormatter2.timeZone = NSTimeZone.system
        dateFormatter2.dateFormat = "yyyy-MM-dd"
        let date_TimeFrom = dateFormatter2.string(from: frDate)
        let date_TimeTo = dateFormatter2.string(from: toDate)
        
        
        param = ["FromDate": date_TimeFrom ,"ToDate": date_TimeTo  ]
        }else{
            let dateFormatter2 = DateFormatter()
            dateFormatter2.timeZone = NSTimeZone.system
            dateFormatter2.dateFormat = "yyyy-MM-dd"
            let dateFrom = dateFormatter2.date(from: FilterGraphStruct.fromDate)
            let dateTo = dateFormatter2.date(from: FilterGraphStruct.toDate)
            let date_TimeFrom = dateFormatter2.string(from: dateFrom!)
            let date_TimeTo = dateFormatter2.string(from: dateTo!)
            
            
            param = ["FromDate": date_TimeFrom ,"ToDate": date_TimeTo ]
        }
        
        PieChartZoneAPI.serviceCallingPieChart(obj: self,  param: param ) { (dict) in
            
            self.getZone_MasterData()
            
            self.tableView.reloadData()
            
        }
    }
    
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
extension Date {
    func startOfMonth() -> Date {
        return Calendar.current.date(from: Calendar.current.dateComponents([.year, .month], from: Calendar.current.startOfDay(for: self)))!
    }
    
    func endingOfMonth() -> Date {
        return Calendar.current.date(byAdding: DateComponents(month: 1, day: -1), to: self.startOfMonth())!
    }
}
