//
//  GenerateReportFrontViewController.swift
//  VallSafety
//
//  Created by Bunga Maheshwar Rao on 05/10/18.
//  Copyright © 2018 Bunga Maheshwar Rao. All rights reserved.
//

import UIKit
import SpreadsheetView
import Charts
class GenerateReportFrontViewController: UIViewController {
   
    @IBOutlet weak var heightStack: NSLayoutConstraint!
    @IBAction func ReportDashboardClicked(_ sender: UIButton) {
       
        FilterDataFromServer.department_Id = Int()
        FilterDataFromServer.department_Name = String()
        FilterDataFromServer.area_Id = Int()
        FilterDataFromServer.area_Name = String()
        FilterDataFromServer.sub_Area_Id = Int()
        FilterDataFromServer.sub_Area_Name = String()
        FilterDataFromServer.location_name = String()
        FilterDataFromServer.location_id = Int()
        FilterDataFromServer.hazard_Name = String()
        FilterDataFromServer.hazard_Id = Int()
        
        
        
        
        FilterGraphStruct.area_Name = "Select Area"
        FilterGraphStruct.sub_area_Name = "Select Sub-Area"
        FilterGraphStruct.hazard_Name = "Select Hazard Type"
        FilterGraphStruct.department_Name = "Select Department"
         FilterGraphStruct.location_Name = "Select Location"
        FilterGraphStruct.RiskLevel = "Risk Level"
        FilterGraphStruct.minRiskLevel = String()
        FilterGraphStruct.mixRiskLevel = String()
         FilterGraphStruct.department_NameID = String()
        FilterGraphStruct.location_ID = String()
        FilterGraphStruct.area_ID = String()
        FilterGraphStruct.sub_area_ID = String()
        FilterGraphStruct.hazard_Name_ID = String()
        FilterGraphStruct.locationType = String()
        
        FilterGraphStruct.fromDate = String()
        FilterGraphStruct.toDate = String()
        
        // FilterGraphStruct.filterBylocation_Name = String()
        //  FilterGraphStruct.filterBylocation_ID = String()
        
        
        
        //cross
        //    self.businessCrosBtn.setBackgroundImage(UIImage(named: "icons8-sort"), for: .normal)
        //   self.locationCrosBtn.setBackgroundImage(UIImage(named: "icons8-sort"), for: .normal)
        
        // locationViewBtn.backgroundColor = UIColor.clear
        FilterGraphStruct.isHazardView = String()
        FilterGraphStruct.hazard_NameByFilter = String()
        
        
         let storyBoard = UIStoryboard(name: "ThemeSafety", bundle: nil)
        
        let ReportDashVC = storyBoard.instantiateViewController(withIdentifier: "DashboardTypeHazard") as! DashboardTypeHazardViewController
        
        
        self.navigationController?.pushViewController(ReportDashVC, animated: true)
    }
    
    @IBAction func TopPendencyClicked(_ sender: UIButton) {
        FilterDataFromServer.unit_Id = Int()
        FilterDataFromServer.unit_Name = String()
        FilterDataFromServer.area_Id = Int()
        FilterDataFromServer.area_Name = String()
        FilterDataFromServer.sub_Area_Id = Int()
        FilterDataFromServer.sub_Area_Name = String()
        FilterDataFromServer.location_name = String()
        FilterDataFromServer.location_id = Int()
        FilterDataFromServer.hazard_Name = String()
        FilterDataFromServer.hazard_Id = Int()
        
        
        FilterPendencyStruct.toppendencyFlag = "PendancyHazard"
        FilterPendencyStruct.area_Name = String()
        FilterPendencyStruct.sub_Area_Name = String()
        FilterPendencyStruct.hazard_Name = String()
        FilterPendencyStruct.unit_Name = String()
        FilterPendencyStruct.location_name = String()
        FilterPendencyStruct.RiskLevel = String()
        FilterPendencyStruct.minRiskLevel = String()
        FilterPendencyStruct.mixRiskLevel = String()
        FilterPendencyStruct.department_NameID = String()
        FilterPendencyStruct.location_id = String()
        FilterPendencyStruct.area_Id = String()
        FilterPendencyStruct.sub_Area_Id = String()
        FilterPendencyStruct.unit_Id = String()
        FilterPendencyStruct.hazard_Id = Int()
        FilterPendencyStruct.hazard_Name_ID = String()
        FilterPendencyStruct.locationType = String()
        
        FilterPendencyStruct.fromDate = String()
        FilterPendencyStruct.toDate = String()
        
        // FilterPendencyStruct.filterBylocation_Name = String()
        //  FilterPendencyStruct.filterBylocation_ID = String()
        
        
        
        //cross
        //    self.businessCrosBtn.setBackgroundImage(UIImage(named: "icons8-sort"), for: .normal)
        //   self.locationCrosBtn.setBackgroundImage(UIImage(named: "icons8-sort"), for: .normal)
        
        // locationViewBtn.backgroundColor = UIColor.clear
        FilterPendencyStruct.isHazardView = String()
        FilterPendencyStruct.hazard_NameByFilter = String()
        
        
        
        
         let storyBoard = UIStoryboard(name: "ThemeSafety", bundle: nil)
        
        let AllTopPendencyVC = storyBoard.instantiateViewController(withIdentifier: "AllTopPendency") as! AllTopPendencyViewController
        
        
        self.navigationController?.pushViewController(AllTopPendencyVC, animated: true)
        
        
    }
    @IBAction func TopAchieversClicked(_ sender: UIButton) {
        FilterDataFromServer.unit_Id = Int()
        FilterDataFromServer.unit_Name = String()
        FilterDataFromServer.area_Id = Int()
        FilterDataFromServer.area_Name = String()
        FilterDataFromServer.sub_Area_Id = Int()
        FilterDataFromServer.sub_Area_Name = String()
        FilterDataFromServer.location_name = String()
        FilterDataFromServer.location_id = Int()
        FilterDataFromServer.hazard_Name = String()
        FilterDataFromServer.hazard_Id = Int()
        
        FilterAchieverStruct.topAchieverFlag = "Hazard"
        
        
        
        FilterAchieverStruct.area_Name = String()
        FilterAchieverStruct.sub_Area_Name = String()
        FilterAchieverStruct.hazard_Name = String()
        
        FilterAchieverStruct.RiskLevel = String()
        FilterAchieverStruct.minRiskLevel = String()
        FilterAchieverStruct.mixRiskLevel = String()
        //   FilterAchieverStruct.business_NameID = String()
        //   FilterAchieverStruct.location_ID = String()
        FilterAchieverStruct.area_Id = String()
        FilterAchieverStruct.sub_Area_Id = String()
        FilterAchieverStruct.hazard_Name_ID = String()
        FilterAchieverStruct.locationType = String()
        
        FilterAchieverStruct.fromDate = String()
        FilterAchieverStruct.toDate = String()
        
        // FilterAchieverStruct.filterBylocation_Name = String()
        //  FilterAchieverStruct.filterBylocation_ID = String()
        
        
        
        //cross
        //    self.businessCrosBtn.setBackgroundImage(UIImage(named: "icons8-sort"), for: .normal)
        //   self.locationCrosBtn.setBackgroundImage(UIImage(named: "icons8-sort"), for: .normal)
        
        // locationViewBtn.backgroundColor = UIColor.clear
        FilterAchieverStruct.isHazardView = String()
        FilterAchieverStruct.hazard_NameByFilter = String()
         let storyBoard = UIStoryboard(name: "ThemeSafety", bundle: nil)
        let TopAchieversVC = storyBoard.instantiateViewController(withIdentifier: "TopTenArchievers") as! TopTenArchieversViewController
        
        
        self.navigationController?.pushViewController(TopAchieversVC, animated: true)
    }
    @IBAction func btnGrahEnlarge(_ sender: UIButton) {
        FilterDataFromServer.unit_Id = Int()
        FilterDataFromServer.unit_Name = String()
        FilterDataFromServer.area_Id = Int()
        FilterDataFromServer.area_Name = String()
        FilterDataFromServer.sub_Area_Id = Int()
        FilterDataFromServer.sub_Area_Name = String()
        FilterDataFromServer.location_name = String()
        FilterDataFromServer.location_id = Int()
        FilterDataFromServer.hazard_Name = String()
        FilterDataFromServer.hazard_Id = Int()
        
        
        
        
        FilterGraphStruct.area_Name = "Select Area"
        FilterGraphStruct.sub_area_Name = "Select Sub-Area"
        FilterGraphStruct.hazard_Name = "Select Hazard Type"
        FilterGraphStruct.department_Name = "Select Department"
        FilterGraphStruct.location_Name = "Select Location"
        FilterGraphStruct.RiskLevel = "Risk Level"
        FilterGraphStruct.minRiskLevel = String()
        FilterGraphStruct.mixRiskLevel = String()
        FilterGraphStruct.department_NameID = String()
        FilterGraphStruct.location_ID = String()
        FilterGraphStruct.area_ID = String()
        FilterGraphStruct.sub_area_ID = String()
        FilterGraphStruct.hazard_Name_ID = String()
        FilterGraphStruct.locationType = String()
        
        FilterGraphStruct.fromDate = String()
        FilterGraphStruct.toDate = String()
        
        // FilterGraphStruct.filterBylocation_Name = String()
        //  FilterGraphStruct.filterBylocation_ID = String()
        
        
        
        //cross
        //    self.businessCrosBtn.setBackgroundImage(UIImage(named: "icons8-sort"), for: .normal)
        //   self.locationCrosBtn.setBackgroundImage(UIImage(named: "icons8-sort"), for: .normal)
        
        // locationViewBtn.backgroundColor = UIColor.clear
        FilterGraphStruct.isHazardView = String()
        FilterGraphStruct.hazard_NameByFilter = String()
         let storyBoard = UIStoryboard(name: "ThemeSafety", bundle: nil)
        let GraphVC = storyBoard.instantiateViewController(withIdentifier: "GraphViewController") as! GraphViewController
        
        
        self.navigationController?.pushViewController(GraphVC, animated: true)
        
        
    }
    @IBAction func SearchHazrdsClicked(_ sender: UIButton) {
          let storyBoard = UIStoryboard(name: "ThemeSafety", bundle: nil)
        let NSearchHazardResult = storyBoard.instantiateViewController(withIdentifier: "SearchViewController") as! SearchViewController
        
     
        self.navigationController?.pushViewController(NSearchHazardResult, animated: true)
    }
    var GrapListOverallStatsAPI = GraphDataOverallAPI()
    var GrapListOverallStatsDB:[MyGraphModel] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        
        
     
    }
    override func viewWillAppear(_ animated: Bool) {
        heightStack.constant = (self.view.frame.height/2) + 20
        PieChartOverallStats.drawHoleEnabled = false
        
        PieChartOverallStats.drawEntryLabelsEnabled = false
        
        PieChartOverallStats.chartDescription?.text = ""
        PieChartOverallStats.delegate = self
        PieChartOverallStats.layer.borderColor = UIColor.lightGray.cgColor
        
        
        // entry label styling
        PieChartOverallStats.entryLabelColor = .white
        PieChartOverallStats.entryLabelFont = .systemFont(ofSize: 15, weight: .medium)
        self.navigationController?.navigationBar.isTranslucent = false
        self.title = "Generate Report"
        
        PieChartOverallStats.animate(xAxisDuration: 1.4, easingOption: .easeOutBack)
        
        
        getDataGraphOverallStats()
    }
    @IBOutlet weak var PieChartOverallStats: PieChartView!
    func setChart1(dataPoints: [String], values: [Int] , colorCode : [String] ) {
        
        
        
        var dataEntries1: [PieChartDataEntry] = []
        
        
        
        for (index1,_) in dataPoints.enumerated()
        {
            
            
            let pieDataEntry1 = PieChartDataEntry(value: Double(values[index1]), label: dataPoints[index1],data:dataPoints[index1] as AnyObject?)
            
            dataEntries1.append(pieDataEntry1)
            
        }
        
        
        print("\(dataEntries1)")
        
        var colors2  = [UIColor]()
        
        for index2 in colorCode
        {
            
            let someColor = UIColor(hexString: index2, alpha: 1.0)
            
            colors2.append(someColor!)
            
        }
        
        print("colors\(colors2)")
        
        let chartDataSet = PieChartDataSet(values: dataEntries1, label: "")
        let chartData = PieChartData()
        
      
      
        chartData.addDataSet(chartDataSet)
        
        chartDataSet.colors = colors2
        let format = NumberFormatter()
        format.numberStyle = .none
        let formatter = DefaultValueFormatter(formatter: format)
        chartData.setValueFormatter(formatter)
          PieChartOverallStats.data = chartData
        
        //        PieChartOverallStats.drawEntryLabelsEnabled = true
        //        PieChartOverallStats.drawCenterTextEnabled = true
        //
        // PieChartOverallStats.contentScaleFactor = 2.0
        //
        //        chartDataSet.sliceSpace = 2
        chartDataSet.selectionShift = 5
        //PieChartOverallStats.drawSlicesUnderHoleEnabled = true
        
    }
    
    var OverallStatsTotalCount : [Int] = []
    var OverallStatsStatus : [String] = []
    var OverallStatsColor : [String] = []
    var OverallStatsStatusShow : [String] = []
    @objc func getDataGraphOverallStats(){
        self.GrapListOverallStatsDB = []
        self.OverallStatsTotalCount = []
        self.OverallStatsStatus = []
        self.OverallStatsStatusShow = []
        self.OverallStatsColor = []
        let param : [String : String] = [:]
        GrapListOverallStatsAPI.serviceCalling(obj: self ,parameters: param ) { (dict) in
            //
            
            self.GrapListOverallStatsDB = [MyGraphModel]()
            self.GrapListOverallStatsDB = dict as! [MyGraphModel]
            
            
            for i in 0..<self.GrapListOverallStatsDB.count {
                let tot : Int = self.GrapListOverallStatsDB[i].Total_Count!
                let total : String = String(tot)
                self.OverallStatsTotalCount.append(tot)
                self.OverallStatsStatus.append(self.GrapListOverallStatsDB[i].Status! + "-" + String(total))
                self.OverallStatsColor.append(self.GrapListOverallStatsDB[i].Color_Code!)
                 self.OverallStatsStatusShow.append(self.GrapListOverallStatsDB[i].Status!)
                
            }
            self.setChart1(dataPoints: self.OverallStatsStatus, values: self.OverallStatsTotalCount , colorCode :self.OverallStatsColor)
        }
    }
}

extension GenerateReportFrontViewController : ChartViewDelegate {
    func chartValueSelected(_ chartView: ChartViewBase, entry: ChartDataEntry, highlight: Highlight) {
        
        
        
        
        if let dataSet = chartView.data?.dataSets[ highlight.dataSetIndex] {
            
            let sliceIndex: Int = dataSet.entryIndex( entry: entry)
            print( "Selected slice index: \( sliceIndex)")
            
            let sta = self.OverallStatsStatusShow[sliceIndex]
           
                 UserDefaults.standard.set(6, forKey: "hazard")
               UserDefaults.standard.set(sta, forKey: "Status")
            let storyBoard = UIStoryboard(name: "ThemeSafety", bundle: nil)
            let ZIVC = storyBoard.instantiateViewController(withIdentifier: "ReportedHazardViewController") as! ReportedHazardViewController
            
           // ZIVC.hazardtypeId = String(DashboardDB[(indexPath?.row)!].Type_ID)
            
            ZIVC.statusStr = sta
            ZIVC.locationStr = String()
            ZIVC.unitStr = String()
            ZIVC.AreaStr = String()
            ZIVC.departmentStr = String()
            FilterGraphStruct.fromDate = String()
         FilterGraphStruct.toDate = String()
            self.navigationController?.pushViewController(ZIVC, animated: true)
            
            
         
            
//            let ReportedHazardVC = self.storyboard?.instantiateViewController(withIdentifier: "ReportedHazardViewController") as! ReportedHazardViewController
//
//            self.navigationController?.pushViewController(ReportedHazardVC, animated: true)
        }
    }
}
