//
//  TopPendencyForClosureViewController.swift
//  Balco Sustainability Plus
//
//  Created by sudheer-kumar on 17/03/18.
//  Copyright © 2018 safiqul islam. All rights reserved.
//

import UIKit

class TopPendencyForClosureViewController: CommonVSClass,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var totalHazard: UILabel!
    
    var TopCloseApi = TopPendencyForClosureAPI()
    var TopCloseDB : [TopPendencyForClosureModel] = []
    var TopCloseLoadMoreDB : [TopPendencyForClosureModel] = []
    var TopCloseLoadMoreApi = TopPendencyForClosureLoadMoreAPI()
    
    

    var data: String?
    var lastObject: String?
    var refresher = UIRefreshControl()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
       
            
        
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.tableFooterView = UIView()
        tableView.estimatedRowHeight = 113
        tableView.rowHeight = UITableViewAutomaticDimension
        
        refresh.addTarget(self, action: #selector(self.callTopReportedData), for: .valueChanged)
        self.tableView.addSubview(refresh)
        
        let swipe = UISwipeGestureRecognizer(target: self, action: #selector(self.swipeRecognizer))
        swipe.direction = ([.down])
        self.view.addGestureRecognizer(swipe)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.callTopReportedData), name: NSNotification.Name?.init(NSNotification.Name(rawValue: "fromTopTenPendancyTypeFilter")), object: nil)
        
        callTopReportedData()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    var reachability = Reachability()!
    @objc func swipeRecognizer(_ sender: UISwipeGestureRecognizer) {
        if (sender.direction == .down) {
            
        
            if(self.reachability.connection != .none)
            {
                
                self.callTopReportedData()
            }
            else
            {
              
                self.startLoading(view: self.view)
                DispatchQueue.main.asyncAfter(deadline: .now() + 4.5) {
                    self.noDataLabel(text: "No internet connection found. Check your internet connection and try again.")
                    self.stopLoading(view: self.view)
                }
               
                
            }
            
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return TopCloseDB.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "closure", for: indexPath) as! TopPendencyForClosureTableViewCell
        
        cell.totalHazard.text = String(describing: self.TopCloseDB[indexPath.row].TotalPending)
        cell.employeeName.text = self.TopCloseDB[indexPath.row].Name
      //  cell.SBUDepartment.text = self.TopCloseDB[indexPath.row].Sbu_Department
        cell.greaterThanThirtyHazard.text = String(describing:self.TopCloseDB[indexPath.row].Pending30)
        cell.greaterThanNintyHazard.text = String(describing:self.TopCloseDB[indexPath.row].Pending90)
        
        self.data = self.TopCloseDB[indexPath.row].Name
        self.lastObject = self.TopCloseDB[indexPath.row].Name
        
        if ( self.data ==  self.lastObject && indexPath.row == self.TopCloseDB.count - 1)
        {
            
            self.callTopReportedDataLoadMore(SrNo: String(self.TopCloseDB[indexPath.row].SrNo))
            
        }
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
    }
    
    @objc func callTopReportedData(){
        
        switch FilterPendencyStruct.toppendencyFlag {
        case "PendancyHazard":
            
            var para = [String:String]()
            
          
            let parameter = [
                "FromDate":FilterPendencyStruct.fromDate,
                "ToDate":FilterPendencyStruct.toDate
                ,"Hazard_Type_ID":FilterPendencyStruct.hazard_Name_ID
                ,"Unit_ID":FilterPendencyStruct.unit_Id
                ,"Zone_ID":FilterPendencyStruct.location_id
                ,"Area_ID":FilterPendencyStruct.area_Id
                ,"Sub_Area_ID":FilterPendencyStruct.sub_Area_Id]
            
            para = parameter.filter { $0.value != ""}
            
            
            print("para",para)
            
            self.TopCloseApi.serviceCalling(obj: self, Url: URLConstants.Dashbord_List_Pending_For_Closer, parameter:para) { (dict) in
                
                self.TopCloseDB = [TopPendencyForClosureModel]()
                self.TopCloseDB = dict as! [TopPendencyForClosureModel]
                
                self.tableView.reloadData()
                
            }
           
            break;
        default:
            
//            var para = [String:String]()
//
//
//            let parameter = [
//                "SrNo":""]
//
//            para = parameter.filter { $0.value != ""}
//
//
//            print("para",para)
//
//            self.TopCloseApi.serviceCalling(obj: self, Url:  URLConstants.Dashbord_Type_Hazard, parameter:para) { (dict) in
//
//                self.TopCloseDB = [TopPendencyForClosureModel]()
//                self.TopCloseDB = dict as! [TopPendencyForClosureModel]
//
//                self.tableView.reloadData()
//
//            }
//
            break;
        }
        
        
    }
    
    @objc func callTopReportedDataLoadMore(SrNo:String){
        
   
        switch FilterPendencyStruct.toppendencyFlag {
        case "PendancyHazard":
            
            var para = [String:String]()
            
            
            let parameter = [
                "FromDate":FilterPendencyStruct.fromDate,
                "ToDate":FilterPendencyStruct.toDate
                ,"Hazard_Type_ID":FilterPendencyStruct.hazard_Name_ID
                ,"Unit_ID":FilterPendencyStruct.unit_Id
                ,"Zone_ID":FilterPendencyStruct.location_id
                ,"Area_ID":FilterPendencyStruct.area_Id
                ,"Sub_Area_ID":FilterPendencyStruct.sub_Area_Id
                 ,"ID":SrNo]
            
            para = parameter.filter { $0.value != ""}
            
            
            print("para",para)
            
            self.TopCloseLoadMoreApi.serviceCalling(obj: self, Url:  URLConstants.Dashbord_List_Pending_For_Closer, parameter:para) { (dict) in
                
                self.TopCloseLoadMoreDB = [TopPendencyForClosureModel]()
                self.TopCloseLoadMoreDB = dict as! [TopPendencyForClosureModel]
                switch self.TopCloseLoadMoreDB.count {
                case 0:
                    break;
                default:
                    self.TopCloseDB.append(contentsOf: self.TopCloseLoadMoreDB)
                    self.tableView.reloadData()
                    break;
                }
                
            }
            
            break;
        default:
            
            var para = [String:String]()
            
            
            let parameter = [
                 "SrNo":SrNo]
            
            para = parameter.filter { $0.value != ""}
            
            
            print("para",para)
            
            self.TopCloseLoadMoreApi.serviceCalling(obj: self, Url:  URLConstants.Hazard_Report_Dashboard, parameter:para) { (dict) in
                
                self.TopCloseLoadMoreDB = [TopPendencyForClosureModel]()
                self.TopCloseLoadMoreDB = dict as! [TopPendencyForClosureModel]
                switch self.TopCloseLoadMoreDB.count {
                case 0:
                    break;
                default:
                    self.TopCloseDB.append(contentsOf: self.TopCloseLoadMoreDB)
                    self.tableView.reloadData()
                    break;
                }
                
            }
            
            break;
        }
        
        
    }
    
    
    
}


