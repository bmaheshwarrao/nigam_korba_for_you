//
//  TopPendencyFilterViewController.swift
//  VallSafety
//
//  Created by SARVANG INFOTCH on 16/10/18.
//  Copyright © 2018 Bunga Maheshwar Rao. All rights reserved.
//

import UIKit
import Reachability
import CoreData
class TopPendencyFilterViewController: CommonVSClass,UITextFieldDelegate {
    
    var hazard_ID = String()
    var locationStr = String()
    var subLocationStr = String()
    
    let reachablty = Reachability()!
    
    @IBOutlet weak var departmentBtn: UIButton!
    @IBOutlet weak var locationBtn: UIButton!
    @IBOutlet weak var areaBtn: UIButton!
    @IBOutlet weak var subAreaBtn: UIButton!
    @IBOutlet weak var hazardTypeBtn: UIButton!
    
    
    @IBOutlet weak var departmentCrosBtn: UIButton!
    @IBOutlet weak var locationCrosBtn: UIButton!
    @IBOutlet weak var areaCrosBtn: UIButton!
    @IBOutlet weak var subAreaCrosBtn: UIButton!
    @IBOutlet weak var hazardCrosBtn: UIButton!
    
    @IBOutlet weak var departmentView: UIView!
    @IBOutlet weak var locationView: UIView!
    @IBOutlet weak var subAreaView: UIView!
    @IBOutlet weak var areaView: UIView!
    @IBOutlet weak var hazardView: UIView!
    
    
    @IBOutlet weak var bottomHeight: NSLayoutConstraint!
    
    
    var AreaIDVal = String()
    
    var SubAreaIDVal = String()
    
    
   
    
    var business_NameArray = [String]()
    var location_nameArray = [String]()
    
    var Area_NameArray = [String]()
    var Sub_Area_NameArray = [String]()
    
 
    var hazard_NameArray = [String]()
    
    
    
    
    @IBOutlet weak var fromDateTF: UITextField!
    @IBOutlet weak var toDateTF: UITextField!
    var datePicker : UIDatePicker!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
   
       // getLocation_MasterData()
        
        self.fromDateTF.delegate = self
        self.toDateTF.delegate = self
        
        
                switch FilterPendencyStruct.sub_Area_Name {
                case "":
                    FilterPendencyStruct.sub_Area_Name = String()
                    break;
                default:
                    break;
                }
        
                switch FilterPendencyStruct.location_name {
                case "":
                    FilterPendencyStruct.location_name = String()
                    break;
                default:
        
                    break;
                }
        
        switch FilterPendencyStruct.area_Name {
        case "":
            FilterPendencyStruct.area_Name = String()
            break;
        default:
            
            break;
        }
        
        switch FilterPendencyStruct.sub_Area_Name {
        case "":
            FilterPendencyStruct.sub_Area_Name = String()
            break;
        default:
            
            break;
        }
        
        switch FilterPendencyStruct.hazard_Name {
        case "":
            FilterPendencyStruct.hazard_Name = String()
            break;
        default:
            
            break;
        }
        
        switch FilterPendencyStruct.fromDate {
        case "":
            fromDateTF.placeholder = "From Date"
            break;
        default:
            fromDateTF.text = FilterPendencyStruct.fromDate
            break;
        }
        
        switch FilterPendencyStruct.toDate {
        case "":
            toDateTF.placeholder = "To Date"
            break;
        default:
            toDateTF.text = FilterPendencyStruct.toDate
            break;
        }
        
        locationUpdate()
        
        
        
                departmentView.layer.cornerRadius = 10
                departmentView.layer.borderWidth = 0.5
                departmentView.layer.borderColor = UIColor.black.cgColor
        
                locationView.layer.cornerRadius = 10
                locationView.layer.borderWidth = 0.5
                locationView.layer.borderColor = UIColor.black.cgColor
        
        areaView.layer.cornerRadius = 10
        areaView.layer.borderWidth = 0.5
        areaView.layer.borderColor = UIColor.black.cgColor
        
        subAreaView.layer.cornerRadius = 10
        subAreaView.layer.borderWidth = 0.5
        subAreaView.layer.borderColor = UIColor.black.cgColor
        
        
        hazardView.layer.cornerRadius = 10
        hazardView.layer.borderWidth = 0.5
        hazardView.layer.borderColor = UIColor.black.cgColor
        
        
        let navheight = UIApplication.shared.statusBarFrame.height +
            self.navigationController!.navigationBar.frame.height
        let screenSize = UIScreen.main.bounds
        bottomHeight.constant = screenSize.height - navheight - 445
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        
        self.tabBarController?.tabBar.isHidden = true
        self.title = "Filter"
        let nav = self.navigationController?.navigationBar
        nav?.tintColor = UIColor.white
       nav?.barTintColor = UIColor(hexString: "2c3e50", alpha: 1.0)
        nav?.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        updateData()
        locationUpdate()
        
    }
    
    @IBAction func departmentAction(_ sender: Any) {
        departmentData()
    }
    
    @IBAction func locationAction(_ sender: Any) {
        locationData()
    }
    
    
    @IBAction func areaAction(_ sender: Any) {
        areaData()
    }
    
    @IBAction func subAreaAction(_ sender: Any) {
        subAreaData()
    }
    
    @IBAction func hazardTypeAction(_ sender: Any){
        hazardData()
    }
    
    @IBAction func departmentTap(_ sender: Any) {
        
        
        //        if (businessCrosBtn!.currentBackgroundImage?.isEqual(UIImage(named: "icons8-sort")))! {
        //            businessData()
        //
        //        }else{
        //
        //            FilterPendencyStruct.business_Name = "Select Business"
        //            FilterPendencyStruct.location_name = "Select Location"
        //            FilterPendencyStruct.area_Name = "Select Area"
        //            FilterPendencyStruct.sub_Area_Name = "Select Sub-Area"
        //
        //            self.businessBtn.setTitle( FilterPendencyStruct.business_Name, for: .normal)
        //            self.locationBtn.setTitle( FilterPendencyStruct.location_name, for: .normal)
        //            self.areaBtn.setTitle( FilterPendencyStruct.area_Name, for: .normal)
        //            self.subAreaBtn.setTitle( FilterPendencyStruct.sub_Area_Name, for: .normal)
        //
        //
        //            FilterPendencyStruct.business_NameID = String()
        //            FilterPendencyStruct.business_Name = String()
        //            FilterPendencyStruct.location_ID = String()
        //            FilterPendencyStruct.area_ID = String()
        //            FilterPendencyStruct.sub_Area_Id = String()
        //
        //
        //
        //            self.businessBtn.setTitleColor(UIColor.darkGray, for: .normal)
        //            self.businessCrosBtn.setBackgroundImage(UIImage(named: "icons8-sort"), for: .normal)
        //
        //
        //            self.locationBtn.setTitleColor(UIColor.darkGray, for: .normal)
        //            self.locationCrosBtn.setBackgroundImage(UIImage(named: "icons8-sort"), for: .normal)
        //
        //
        //            self.areaBtn.setTitleColor(UIColor.darkGray, for: .normal)
        //            self.areaCrosBtn.setBackgroundImage(UIImage(named: "icons8-sort"), for: .normal)
        //
        //
        //            self.subAreaBtn.setTitleColor(UIColor.darkGray, for: .normal)
        //            self.subAreaCrosBtn.setBackgroundImage(UIImage(named: "icons8-sort"), for: .normal)
        //
        //
        //        }
        
    }
    
    @objc func locationUpdate(){
        
        
        if  FilterGraphStruct.area_ID == "" && FilterGraphStruct.sub_area_ID == "" {
            //            FilterGraphStruct.filterBylocation_name = String()
            //            FilterGraphStruct.filterBylocation_ID = String()
        }else{
            //
        }
        
     
        
        
        
        switch FilterPendencyStruct.hazard_Name == "" {
        case true:
            self.hazardTypeBtn.setTitleColor(UIColor.darkGray, for: .normal)
            self.hazardCrosBtn.setBackgroundImage(UIImage(named: "icons8-sort"), for: .normal)
            self.hazardTypeBtn.setTitle("Select Hazard Type", for: .normal)
            break;
        default:
            self.hazardTypeBtn.setTitleColor(UIColor.black, for: .normal)
            self.hazardCrosBtn.setBackgroundImage(UIImage(named: "cross"), for: .normal)
            self.hazardTypeBtn.setTitle(FilterPendencyStruct.hazard_Name, for: .normal)
            break;
        }
        
        
        switch FilterPendencyStruct.unit_Name == "" {
        case true:
            self.areaBtn.setTitleColor(UIColor.darkGray, for: .normal)
            self.areaCrosBtn.setBackgroundImage(UIImage(named: "icons8-sort"), for: .normal)
            self.areaBtn.setTitle("Select Unit", for: .normal)
            break;
        default:
            self.areaBtn.setTitleColor(UIColor.black, for: .normal)
            self.areaCrosBtn.setBackgroundImage(UIImage(named: "cross"), for: .normal)
            self.areaBtn.setTitle(FilterPendencyStruct.unit_Name, for: .normal)
            break;
        }
        
        switch FilterPendencyStruct.area_Name == "" {
        case true:
            self.subAreaBtn.setTitleColor(UIColor.darkGray, for: .normal)
            self.subAreaCrosBtn.setBackgroundImage(UIImage(named: "icons8-sort"), for: .normal)
            self.subAreaBtn.setTitle("Select Area", for: .normal)
            break;
        default:
            self.subAreaBtn.setTitleColor(UIColor.black, for: .normal)
            self.subAreaCrosBtn.setBackgroundImage(UIImage(named: "cross"), for: .normal)
            self.subAreaBtn.setTitle(FilterPendencyStruct.area_Name, for: .normal)
            break;
        }
        
        
        
        switch FilterPendencyStruct.location_name == "" {
        case true:
            self.locationBtn.setTitleColor(UIColor.darkGray, for: .normal)
            self.locationCrosBtn.setBackgroundImage(UIImage(named: "icons8-sort"), for: .normal)
            self.locationBtn.setTitle("Select Zone", for: .normal)
            break;
        default:
            self.locationBtn.setTitleColor(UIColor.black, for: .normal)
            self.locationCrosBtn.setBackgroundImage(UIImage(named: "cross"), for: .normal)
            self.locationBtn.setTitle(FilterPendencyStruct.location_name, for: .normal)
            break;
        }
        
        
        switch FilterPendencyStruct.sub_Area_Name == "" {
        case true:
            self.departmentBtn.setTitleColor(UIColor.darkGray, for: .normal)
            self.departmentCrosBtn.setBackgroundImage(UIImage(named: "icons8-sort"), for: .normal)
            self.departmentBtn.setTitle("Select Area Location", for: .normal)
            break;
        default:
            self.departmentBtn.setTitleColor(UIColor.black, for: .normal)
            self.departmentCrosBtn.setBackgroundImage(UIImage(named: "cross"), for: .normal)
            self.departmentBtn.setTitle(FilterPendencyStruct.sub_Area_Name, for: .normal)
            break;
        }
        
        
        
    }
    @IBAction func hazardCrosAction(_ sender: Any) {
        if (hazardCrosBtn!.currentBackgroundImage?.isEqual(UIImage(named: "icons8-sort")))! {
            hazardData()
            
        }else{
            FilterGraphStruct.hazard_Name = "Select Hazard Type"
            FilterGraphStruct.hazard_Name_ID = String()
         
            FilterPendencyStruct.hazard_Name_ID = String()
            FilterPendencyStruct.hazard_Name = String()
            FilterPendencyStruct.hazard_Id = Int()
            self.hazardTypeBtn.setTitleColor(UIColor.darkGray, for: .normal)
            self.hazardCrosBtn.setBackgroundImage(UIImage(named: "icons8-sort"), for: .normal)
            
            
            locationUpdate()
        }
        
    }
    @IBAction func areaTap(_ sender: Any) {
        
        
        if (self.areaCrosBtn!.currentBackgroundImage?.isEqual(UIImage(named: "icons8-sort")))! {
            areaData()
            
            
        }else{
            
            
            //   FilterGraphStruct.locationType = "Location : "+FilterGraphStruct.location_name
            FilterGraphStruct.unit_Name = "Select Unit"
            FilterGraphStruct.area_Name = "Select Area"
            
            self.areaBtn.setTitle( FilterGraphStruct.unit_Name, for: .normal)
            self.subAreaBtn.setTitle( FilterGraphStruct.area_Name, for: .normal)
            
            //    self.businessCrosBtn.setBackgroundImage(UIImage(named: "cross"), for: .normal)
            //   self.locationCrosBtn.setBackgroundImage(UIImage(named: "cross"), for: .normal)
            self.areaCrosBtn.setBackgroundImage(UIImage(named: "icons8-sort"), for: .normal)
            self.subAreaCrosBtn.setBackgroundImage(UIImage(named: "icons8-sort"), for: .normal)
            
            //     self.businessBtn.setTitleColor(UIColor.black, for: .normal)
            
            //    self.locationBtn.setTitleColor(UIColor.black, for: .normal)
            
            self.areaBtn.setTitleColor(UIColor.darkGray, for: .normal)
            
            self.subAreaBtn.setTitleColor(UIColor.darkGray, for: .normal)
            
            FilterGraphStruct.area_ID = String()
            FilterGraphStruct.sub_area_ID = String()
            FilterGraphStruct.sub_area_Name = "Select Area Location"
            self.departmentBtn.setTitle( FilterGraphStruct.sub_area_Name, for: .normal)
            
            
            
            
            
            
            
            
            
            self.departmentBtn.setTitleColor(UIColor.darkGray, for: .normal)
            self.departmentCrosBtn.setBackgroundImage(UIImage(named: "icons8-sort"), for: .normal)
            
            FilterPendencyStruct.unit_Id = String()
            FilterPendencyStruct.unit_Name = String()
            FilterPendencyStruct.area_Id = String()
            FilterPendencyStruct.area_Name = String()
            FilterPendencyStruct.sub_Area_Id = String()
            FilterPendencyStruct.sub_Area_Name = String()
            locationUpdate()
            
            
            
            
            
        }
        
    }
    
    @IBAction func subAreaTap(_ sender: Any) {
        
        
        if (self.subAreaCrosBtn!.currentBackgroundImage?.isEqual(UIImage(named: "icons8-sort")))! {
            subAreaData()
            
            
        }else{
            
            
            FilterGraphStruct.locationType = "Area : "+FilterGraphStruct.area_Name
            FilterGraphStruct.area_Name = "Select Area"
            
            self.subAreaBtn.setTitle( FilterGraphStruct.area_Name, for: .normal)
            
            //            self.businessCrosBtn.setBackgroundImage(UIImage(named: "cross"), for: .normal)
            //            self.locationCrosBtn.setBackgroundImage(UIImage(named: "cross"), for: .normal)
            self.areaCrosBtn.setBackgroundImage(UIImage(named: "cross"), for: .normal)
            self.subAreaCrosBtn.setBackgroundImage(UIImage(named: "icons8-sort"), for: .normal)
            
            //            self.businessBtn.setTitleColor(UIColor.black, for: .normal)
            //
            //            self.locationBtn.setTitleColor(UIColor.black, for: .normal)
            
            self.areaBtn.setTitleColor(UIColor.black, for: .normal)
            
            self.subAreaBtn.setTitleColor(UIColor.darkGray, for: .normal)
            
            
            
            
            FilterGraphStruct.sub_area_ID = String()
            
            FilterGraphStruct.sub_area_Name = "Select Area Location"
            self.departmentBtn.setTitle( FilterGraphStruct.sub_area_Name, for: .normal)
            
            
            
            
            
            
            
            
            
            self.departmentBtn.setTitleColor(UIColor.darkGray, for: .normal)
            self.departmentCrosBtn.setBackgroundImage(UIImage(named: "icons8-sort"), for: .normal)
            
            FilterPendencyStruct.sub_Area_Id = String()
            FilterPendencyStruct.sub_Area_Name = String()
            
            FilterPendencyStruct.area_Id = String()
            FilterPendencyStruct.area_Name = String()
            locationUpdate()
            
            
            
        }
        
    }
    
    
    @IBAction func departmentCrossAction(_ sender: Any) {
        
        
        if (departmentCrosBtn!.currentBackgroundImage?.isEqual(UIImage(named: "icons8-sort")))! {
            departmentData()
            
        }else{
            
            
            
            
            FilterGraphStruct.sub_area_Name = "Select Area Location"
            self.departmentBtn.setTitle( FilterGraphStruct.sub_area_Name, for: .normal)
            
            
            
            
            FilterGraphStruct.department_NameID = String()
            
            
            
            
            self.departmentBtn.setTitleColor(UIColor.darkGray, for: .normal)
            self.departmentCrosBtn.setBackgroundImage(UIImage(named: "icons8-sort"), for: .normal)
            
            FilterPendencyStruct.sub_Area_Id = String()
            FilterPendencyStruct.sub_Area_Name = String()
            FilterDataFromServer.sub_Area_Id = Int()
            FilterDataFromServer.sub_Area_Name = String()
            
            locationUpdate()
            
            //            self.locationBtn.setTitleColor(UIColor.darkGray, for: .normal)
            //            self.locationCrosBtn.setBackgroundImage(UIImage(named: "icons8-sort"), for: .normal)
            //
            //
            //            self.areaBtn.setTitleColor(UIColor.darkGray, for: .normal)
            //            self.areaCrosBtn.setBackgroundImage(UIImage(named: "icons8-sort"), for: .normal)
            //
            //
            //            self.subAreaBtn.setTitleColor(UIColor.darkGray, for: .normal)
            //            self.subAreaCrosBtn.setBackgroundImage(UIImage(named: "icons8-sort"), for: .normal)
            //
            //            FilterGraphStruct.locationType = String()
            
            
        }
        //
    }
    
    @IBAction func locationCrossAction(_ sender: Any) {
        
        
        if (self.locationCrosBtn!.currentBackgroundImage?.isEqual(UIImage(named: "icons8-sort")))! {
            locationData()
            
            
        }else{
            
            
            
            FilterGraphStruct.location_Name = "Select Location"
            FilterGraphStruct.area_Name = "Select Area"
            FilterGraphStruct.sub_area_Name = "Select Sub-Area"
            
            
            self.locationBtn.setTitle( FilterGraphStruct.location_Name, for: .normal)
            
            
            
            self.locationCrosBtn.setBackgroundImage(UIImage(named: "icons8-sort"), for: .normal)
            
            
            
            
            self.locationBtn.setTitleColor(UIColor.darkGray, for: .normal)
            
            FilterGraphStruct.location_ID = String()
            
            FilterGraphStruct.location_Name = String()
            FilterGraphStruct.area_Name = "Select Area"
            FilterGraphStruct.sub_area_Name = "Select Sub-Area"
            
            self.areaBtn.setTitle( FilterGraphStruct.area_Name, for: .normal)
            self.subAreaBtn.setTitle( FilterGraphStruct.sub_area_Name, for: .normal)
            
            //    self.businessCrosBtn.setBackgroundImage(UIImage(named: "cross"), for: .normal)
            //   self.locationCrosBtn.setBackgroundImage(UIImage(named: "cross"), for: .normal)
            self.areaCrosBtn.setBackgroundImage(UIImage(named: "icons8-sort"), for: .normal)
            self.subAreaCrosBtn.setBackgroundImage(UIImage(named: "icons8-sort"), for: .normal)
            
            //     self.businessBtn.setTitleColor(UIColor.black, for: .normal)
            
            //    self.locationBtn.setTitleColor(UIColor.black, for: .normal)
            
            self.areaBtn.setTitleColor(UIColor.darkGray, for: .normal)
            
            self.subAreaBtn.setTitleColor(UIColor.darkGray, for: .normal)
            
            FilterGraphStruct.area_ID = String()
            FilterGraphStruct.sub_area_ID = String()
            FilterGraphStruct.sub_area_Name = "Select Area Location"
            self.departmentBtn.setTitle( FilterGraphStruct.sub_area_Name, for: .normal)
            
            
            
            
            
            
            
            
            
            self.departmentBtn.setTitleColor(UIColor.darkGray, for: .normal)
            self.departmentCrosBtn.setBackgroundImage(UIImage(named: "icons8-sort"), for: .normal)
            
            FilterPendencyStruct.sub_Area_Id = String()
            FilterPendencyStruct.sub_Area_Name = String()
            FilterPendencyStruct.area_Id = String()
            FilterPendencyStruct.area_Name = String()
            FilterPendencyStruct.unit_Id = String()
            FilterPendencyStruct.unit_Name = String()
            FilterPendencyStruct.location_name = String()
            FilterPendencyStruct.location_id = String()
            FilterDataFromServer.location_name = String()
            FilterDataFromServer.location_id = Int()
            FilterDataFromServer.unit_Id = Int()
            FilterDataFromServer.unit_Name = String()
            FilterDataFromServer.area_Id = Int()
            FilterDataFromServer.area_Name = String()
            FilterDataFromServer.sub_Area_Id = Int()
            FilterDataFromServer.sub_Area_Name = String()
            locationUpdate()
            
        }
        
    }
    @IBAction func areaCrossAction(_ sender: Any) {
        
        
        if (self.areaCrosBtn!.currentBackgroundImage?.isEqual(UIImage(named: "icons8-sort")))! {
            areaData()
            
            
        }else{
            
            
            //   FilterGraphStruct.locationType = "Location : "+FilterGraphStruct.location_name
            FilterGraphStruct.area_Name = "Select Area"
            FilterGraphStruct.sub_area_Name = "Select Sub-Area"
            
            self.areaBtn.setTitle( FilterGraphStruct.area_Name, for: .normal)
            self.subAreaBtn.setTitle( FilterGraphStruct.sub_area_Name, for: .normal)
            
            //    self.businessCrosBtn.setBackgroundImage(UIImage(named: "cross"), for: .normal)
            //   self.locationCrosBtn.setBackgroundImage(UIImage(named: "cross"), for: .normal)
            self.areaCrosBtn.setBackgroundImage(UIImage(named: "icons8-sort"), for: .normal)
            self.subAreaCrosBtn.setBackgroundImage(UIImage(named: "icons8-sort"), for: .normal)
            
            //     self.businessBtn.setTitleColor(UIColor.black, for: .normal)
            
            //    self.locationBtn.setTitleColor(UIColor.black, for: .normal)
            
            self.areaBtn.setTitleColor(UIColor.darkGray, for: .normal)
            
            self.subAreaBtn.setTitleColor(UIColor.darkGray, for: .normal)
            
            FilterGraphStruct.area_ID = String()
            FilterGraphStruct.sub_area_ID = String()
            FilterGraphStruct.sub_area_Name = "Select Area Location"
            self.departmentBtn.setTitle( FilterGraphStruct.sub_area_Name, for: .normal)
            
            
            
            
            FilterGraphStruct.department_NameID = String()
            
            
            
            
            self.departmentBtn.setTitleColor(UIColor.darkGray, for: .normal)
            self.departmentCrosBtn.setBackgroundImage(UIImage(named: "icons8-sort"), for: .normal)
            
            FilterPendencyStruct.sub_Area_Id = String()
            FilterPendencyStruct.sub_Area_Name = String()
            FilterPendencyStruct.area_Id = String()
            FilterPendencyStruct.area_Name = String()
            FilterPendencyStruct.unit_Id = String()
            FilterPendencyStruct.unit_Name = String()
            FilterDataFromServer.unit_Id = Int()
            FilterDataFromServer.unit_Name = String()
            FilterDataFromServer.area_Id = Int()
            FilterDataFromServer.area_Name = String()
            FilterDataFromServer.sub_Area_Id = Int()
            FilterDataFromServer.sub_Area_Name = String()
            locationUpdate()
            
        }
        
    }
    @IBAction func subAreaCrossAction(_ sender: Any) {
        
        
        if (self.subAreaCrosBtn!.currentBackgroundImage?.isEqual(UIImage(named: "icons8-sort")))! {
            subAreaData()
            
            
        }else{
            
            FilterGraphStruct.locationType = "Area : "+FilterGraphStruct.area_Name
            FilterGraphStruct.sub_area_Name = "Select Sub-Area"
            
            self.subAreaBtn.setTitle( FilterGraphStruct.sub_area_Name, for: .normal)
            
            //            self.businessCrosBtn.setBackgroundImage(UIImage(named: "cross"), for: .normal)
            //            self.locationCrosBtn.setBackgroundImage(UIImage(named: "cross"), for: .normal)
            self.areaCrosBtn.setBackgroundImage(UIImage(named: "cross"), for: .normal)
            self.subAreaCrosBtn.setBackgroundImage(UIImage(named: "icons8-sort"), for: .normal)
            
            //            self.businessBtn.setTitleColor(UIColor.black, for: .normal)
            //
            //            self.locationBtn.setTitleColor(UIColor.black, for: .normal)
            
            self.areaBtn.setTitleColor(UIColor.black, for: .normal)
            
            self.subAreaBtn.setTitleColor(UIColor.darkGray, for: .normal)
            
            
            
            
            FilterGraphStruct.sub_area_ID = String()
            
            FilterGraphStruct.sub_area_Name = "Select Area Location"
            self.departmentBtn.setTitle( FilterGraphStruct.sub_area_Name, for: .normal)
            
            
            
            
            
            
            
            
            
            self.departmentBtn.setTitleColor(UIColor.darkGray, for: .normal)
            self.departmentCrosBtn.setBackgroundImage(UIImage(named: "icons8-sort"), for: .normal)
            
            FilterPendencyStruct.sub_Area_Id = String()
            FilterPendencyStruct.sub_Area_Name = String()
            
            FilterPendencyStruct.sub_Area_Id = String()
            FilterPendencyStruct.sub_Area_Name = String()
            
            FilterDataFromServer.area_Id = Int()
            FilterDataFromServer.area_Name = String()
            FilterDataFromServer.sub_Area_Id = Int()
            FilterDataFromServer.sub_Area_Name = String()
          
            locationUpdate()
            
        }
        
    }
    
    
    
    
    func departmentData(){
        
        
        if(FilterPendencyStruct.unit_Name != "" && FilterPendencyStruct.area_Name != "" && FilterPendencyStruct.location_name != "" ) {
            FilterDataFromServer.filterType = "Sub Area"
            
            
//            FilterPendencyStruct.sub_Area_Id = Int()
//            FilterPendencyStruct.sub_Area_Name = String()
            updateData()
            let storyBoard = UIStoryboard(name: "ThemeSafety", bundle: nil)
            let ZIVC = storyBoard.instantiateViewController(withIdentifier: "ThemeBasedASUZViewController") as! ThemeBasedASUZViewController
            ZIVC.titleStr = "Select Area Location"
            self.navigationController?.pushViewController(ZIVC, animated: true)
        }
        
        
        
        
        
        
        
    }
    //
    func locationData(){
        
        
        FilterDataFromServer.filterType = "Location"
        FilterPendencyStruct.unit_Id = String()
        FilterPendencyStruct.unit_Name = String()
        FilterPendencyStruct.area_Id = String()
        FilterPendencyStruct.area_Name = String()
        FilterPendencyStruct.sub_Area_Id = String()
        FilterPendencyStruct.sub_Area_Name = String()
//        FilterPendencyStruct.location_name = String()
//        FilterPendencyStruct.location_id = Int()
        
        FilterDataFromServer.unit_Id = Int()
        FilterDataFromServer.unit_Name = String()
        FilterDataFromServer.area_Id = Int()
        FilterDataFromServer.area_Name = String()
        FilterDataFromServer.sub_Area_Id = Int()
        FilterDataFromServer.sub_Area_Name = String()
        updateData()
        
        
        let storyBoard = UIStoryboard(name: "ThemeSafety", bundle: nil)
        let ZIVC = storyBoard.instantiateViewController(withIdentifier: "ThemeBasedASUZViewController") as! ThemeBasedASUZViewController
        ZIVC.titleStr = "Select Zone"
        self.navigationController?.pushViewController(ZIVC, animated: true)
        
    }
    
    func areaData(){
        
        
        
        if(FilterPendencyStruct.location_name != "") {
            FilterDataFromServer.filterType = "Unit"
//            FilterPendencyStruct.unit_Id = Int()
//            FilterPendencyStruct.unit_Name = String()
            FilterPendencyStruct.area_Id = String()
            FilterPendencyStruct.area_Name = String()
            FilterPendencyStruct.sub_Area_Id = String()
            FilterPendencyStruct.sub_Area_Name = String()
            
            FilterDataFromServer.area_Id = Int()
            FilterDataFromServer.area_Name = String()
            FilterDataFromServer.sub_Area_Id = Int()
            FilterDataFromServer.sub_Area_Name = String()
            updateData()
            
            
            let storyBoard = UIStoryboard(name: "ThemeSafety", bundle: nil)
            let ZIVC = storyBoard.instantiateViewController(withIdentifier: "ThemeBasedASUZViewController") as! ThemeBasedASUZViewController
            ZIVC.titleStr = "Select Unit"
            self.navigationController?.pushViewController(ZIVC, animated: true)
        }
        
    }
    
    func subAreaData(){
        
        
        if(FilterPendencyStruct.unit_Name != "" && FilterPendencyStruct.location_name != "") {
            
            
            FilterDataFromServer.filterType = "Area"
//            FilterPendencyStruct.area_Id = Int()
//            FilterPendencyStruct.area_Name = String()
            
            FilterPendencyStruct.sub_Area_Id = String()
            FilterPendencyStruct.sub_Area_Name = String()
            
            FilterDataFromServer.sub_Area_Id = Int()
            FilterDataFromServer.sub_Area_Name = String()
            updateData()
            
            let storyBoard = UIStoryboard(name: "ThemeSafety", bundle: nil)
            let ZIVC = storyBoard.instantiateViewController(withIdentifier: "ThemeBasedASUZViewController") as! ThemeBasedASUZViewController
            ZIVC.titleStr = "Select Area"
            self.navigationController?.pushViewController(ZIVC, animated: true)
            
            
            
            
        }
        
    }
    @objc func updateData(){
       
       
        
       
       
       
        
        if(FilterDataFromServer.location_name != "") {
            FilterPendencyStruct.location_id = String(FilterDataFromServer.location_id)
            FilterPendencyStruct.location_name = FilterDataFromServer.location_name
        }
        if(FilterDataFromServer.unit_Name != "") {
            FilterPendencyStruct.unit_Name = FilterDataFromServer.unit_Name
            FilterPendencyStruct.unit_Id = String(FilterDataFromServer.unit_Id)
        }
        if(FilterDataFromServer.area_Name != "") {
            FilterPendencyStruct.area_Name = FilterDataFromServer.area_Name
            FilterPendencyStruct.area_Id = String(FilterDataFromServer.area_Id)
        }
        if(FilterDataFromServer.sub_Area_Name != "") {
            FilterPendencyStruct.sub_Area_Id = String(FilterDataFromServer.sub_Area_Id)
            FilterPendencyStruct.sub_Area_Name = FilterDataFromServer.sub_Area_Name
        }
        if(FilterDataFromServer.hazard_Name != "") {
            FilterPendencyStruct.hazard_Name = FilterDataFromServer.hazard_Name
            FilterPendencyStruct.hazard_Id = FilterDataFromServer.hazard_Id
            FilterPendencyStruct.hazard_Name_ID = String(FilterDataFromServer.hazard_Id)
        }
        
        
        
        
        
        if(FilterPendencyStruct.location_name != "") {
            FilterGraphStruct.location_Name = FilterPendencyStruct.location_name
            FilterGraphStruct.location_ID = FilterPendencyStruct.location_id
        } else {
            FilterGraphStruct.location_Name = String()
            FilterGraphStruct.location_ID = String()
        }
        if(FilterPendencyStruct.location_name != "" && FilterPendencyStruct.unit_Name != "") {
            FilterGraphStruct.unit_Name = FilterPendencyStruct.unit_Name
            FilterGraphStruct.unit_Id = FilterPendencyStruct.unit_Id
        } else {
            
            FilterGraphStruct.unit_Name = String()
            FilterGraphStruct.unit_Id = String()
        }
        if(FilterPendencyStruct.location_name != "" && FilterDataFromServer.area_Name != "" && FilterPendencyStruct.sub_Area_Name != "") {
            FilterGraphStruct.area_Name = FilterPendencyStruct.area_Name
            FilterGraphStruct.area_ID = FilterPendencyStruct.area_Id
        } else {
            
            FilterGraphStruct.area_Name = String()
            FilterGraphStruct.area_ID = String()
        }
        if(FilterPendencyStruct.location_name != "" && FilterPendencyStruct.area_Name != "" && FilterPendencyStruct.sub_Area_Name != "" && FilterPendencyStruct.sub_Area_Name != "") {
            FilterGraphStruct.sub_area_Name = FilterPendencyStruct.sub_Area_Name
            FilterGraphStruct.sub_area_ID = FilterPendencyStruct.sub_Area_Id
        } else {
            
            FilterGraphStruct.department_Name = String()
            FilterGraphStruct.department_NameID = String()
        }
        if(FilterPendencyStruct.hazard_Name != "") {
            
            FilterGraphStruct.hazard_Name = FilterPendencyStruct.hazard_Name
            FilterGraphStruct.hazard_Name_ID = String(FilterPendencyStruct.hazard_Id)
        } else {
            
            
            FilterGraphStruct.hazard_Name = String()
            FilterGraphStruct.hazard_Name_ID = String()
        }
    }
    
    
    func hazardData(){
        
        
        
        FilterDataFromServer.filterType = "Hazard"
//        FilterPendencyStruct.hazard_Id = Int()
//        FilterPendencyStruct.hazard_Name = String()
        
        
        updateData()
        let storyBoard = UIStoryboard(name: "ThemeSafety", bundle: nil)
        let ZIVC = storyBoard.instantiateViewController(withIdentifier: "ThemeBasedASUZViewController") as! ThemeBasedASUZViewController
        ZIVC.titleStr = "Select Hazard Type"
        self.navigationController?.pushViewController(ZIVC, animated: true)
        
    }
    
    
    
   
    var identifierStr = String()
    
    @IBAction func applyFilterAction(_ sender: Any) {
        
        if reachablty.connection == .none{
            
            self.view.makeToast("Internet is not available, please check your internet connection try again.")
            
        }else{
            
            
            switch identifierStr {
            case "SearchHazard":
                NotificationCenter.default.post(name:NSNotification.Name.init("fromsearchHazardTypeFilter"), object: nil)
                self.navigationController?.popViewController(animated: true)
                break;
            case "PendancyHazard":
                
                NotificationCenter.default.post(name:NSNotification.Name.init("fromTopTenPendancyTypeFilter"), object: nil)
                self.navigationController?.popViewController(animated: true)
                break;
            default:
                updateData()
                NotificationCenter.default.post(name:NSNotification.Name.init("fromTopTenTypeFilter"), object: nil)
                self.navigationController?.popViewController(animated: true)
                break;
            }
            
            
        }
    }
    
    @IBAction func resetFilterAction(_ sender: Any) {
        
        FilterDataFromServer.location_name = String()
        FilterDataFromServer.location_id = Int()
        FilterDataFromServer.unit_Name = String()
        FilterDataFromServer.unit_Id = Int()
        FilterDataFromServer.area_Name = String()
        FilterDataFromServer.area_Id = Int()
        FilterDataFromServer.sub_Area_Name = String()
        FilterDataFromServer.sub_Area_Id = Int()
        FilterDataFromServer.hazard_Name = String()
        FilterDataFromServer.hazard_Id = Int()
        FilterPendencyStruct.sub_Area_Name = "Select Area Location"
        FilterPendencyStruct.location_name = "Select Zone"
        FilterPendencyStruct.area_Name = "Select Area"
        FilterPendencyStruct.unit_Name = "Select Unit"
        FilterPendencyStruct.hazard_Name = "Select Hazard Type"
        FilterPendencyStruct.unit_Id = String()
        FilterPendencyStruct.department_NameID = String()
        FilterPendencyStruct.location_id = String()
        FilterPendencyStruct.area_Id = String()
        FilterPendencyStruct.sub_Area_Id = String()
        FilterPendencyStruct.hazard_Name_ID = String()
        FilterPendencyStruct.locationType = String()
        
        FilterPendencyStruct.fromDate = String()
        FilterPendencyStruct.toDate = String()
        
        // FilterPendencyStruct.filterBylocation_name = String()
        // FilterPendencyStruct.filterBylocation_ID = String()
        
        //title
        self.hazardTypeBtn.setTitle(FilterPendencyStruct.hazard_Name, for: .normal)
        self.departmentBtn.setTitle(FilterPendencyStruct.department_Name, for: .normal)
        self.locationBtn.setTitle(FilterPendencyStruct.location_name, for: .normal)
        self.areaBtn.setTitle(FilterPendencyStruct.area_Name, for: .normal)
        self.subAreaBtn.setTitle(FilterPendencyStruct.sub_Area_Name, for: .normal)
        self.fromDateTF.text = nil
        self.toDateTF.text = nil
        self.fromDateTF.placeholder = "From Date"
        self.toDateTF.placeholder = "To Date"
        
        //cross
        self.departmentCrosBtn.setBackgroundImage(UIImage(named: "icons8-sort"), for: .normal)
        self.locationCrosBtn.setBackgroundImage(UIImage(named: "icons8-sort"), for: .normal)
        self.areaCrosBtn.setBackgroundImage(UIImage(named: "icons8-sort"), for: .normal)
        self.subAreaCrosBtn.setBackgroundImage(UIImage(named: "icons8-sort"), for: .normal)
        self.hazardCrosBtn.setBackgroundImage(UIImage(named: "icons8-sort"), for: .normal)
        
        FilterPendencyStruct.isHazardView = String()
        FilterPendencyStruct.hazard_NameByFilter = String()
        
        self.locationUpdate()
        
    }
    
    
    
    func pickUpFromDate(_ textField : UITextField){
        
        // DatePicker
        self.datePicker = UIDatePicker(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
        self.datePicker.backgroundColor = UIColor.white
        self.datePicker.datePickerMode = UIDatePickerMode.date
        if FilterPendencyStruct.toDate.isEmpty == false{
            dateValidation(datePicker: self.datePicker, date: FilterPendencyStruct.toDate, type: "from")
        }
        textField.inputView = self.datePicker
        
        // ToolBar
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(self.doneClickFromTF))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(self.cancelClick))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        textField.inputAccessoryView = toolBar
        
    }
    
    func pickUpToDate(_ textField : UITextField){
        
        // DatePicker
        self.datePicker = UIDatePicker(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
        self.datePicker.backgroundColor = UIColor.white
        self.datePicker.datePickerMode = UIDatePickerMode.date
        if FilterPendencyStruct.fromDate.isEmpty == false{
            dateValidation(datePicker: self.datePicker, date: FilterPendencyStruct.fromDate, type: "to")
        }
        textField.inputView = self.datePicker
        
        
        
        // ToolBar
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(self.doneClickToTF))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(self.cancelClick))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        textField.inputAccessoryView = toolBar
        
    }
    
    func dateValidation(datePicker:UIDatePicker, date:String, type:String) {
        
        let dateFormatterFinal = DateFormatter()
        dateFormatterFinal.dateFormat = "yyyy/MM/dd"
        let minDate: Date =  dateFormatterFinal.date(from: date)!
        switch type {
        case "to":
            datePicker.minimumDate = minDate
            break;
        default:
            datePicker.maximumDate = minDate
            break;
        }
        
    }
    
    @objc func doneClickFromTF() {
        
        let dateFormatter1 = DateFormatter()
        dateFormatter1.dateStyle = .medium
        dateFormatter1.timeStyle = .none
        self.fromDateTF.text = dateFormatter1.string(from: datePicker.date)
        let dateFormatterFinal = DateFormatter()
        dateFormatterFinal.timeZone = NSTimeZone.system
        dateFormatterFinal.dateFormat = "yyyy/MM/dd"
        FilterPendencyStruct.fromDate = dateFormatterFinal.string(from: datePicker.date)
        self.fromDateTF.resignFirstResponder()
        
    }
    
    @objc func doneClickToTF() {
        
        let dateFormatter1 = DateFormatter()
        dateFormatter1.dateStyle = .medium
        dateFormatter1.timeStyle = .none
        
        self.toDateTF.text = dateFormatter1.string(from: datePicker.date)
        
        let dateFormatterFinal = DateFormatter()
        dateFormatterFinal.timeZone = NSTimeZone.system
        dateFormatterFinal.dateFormat = "yyyy/MM/dd"
        FilterPendencyStruct.toDate = dateFormatterFinal.string(from: datePicker.date)
        
        self.toDateTF.resignFirstResponder()
    }
    
    @objc func cancelClick() {
        
        self.fromDateTF.resignFirstResponder()
        //
        self.toDateTF.resignFirstResponder()
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField == self.fromDateTF{
            self.pickUpFromDate(self.fromDateTF)
        }else if textField == self.toDateTF {
            self.pickUpToDate(self.toDateTF)
        }else
        {
            //self.toDateTF.resignFirstResponder()
        }
    }
    
}


