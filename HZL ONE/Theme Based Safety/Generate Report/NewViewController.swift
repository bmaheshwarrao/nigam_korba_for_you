//
//  NewViewController.swift
//  VallSafety
//
//  Created by Bunga Maheshwar Rao on 05/10/18.
//  Copyright © 2018 Bunga Maheshwar Rao. All rights reserved.
//

import UIKit
import SpreadsheetView
class NewViewController: UIViewController , SpreadsheetViewDataSource {
   var spreadsheetView  = SpreadsheetView()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        spreadsheetView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
        spreadsheetView.gridStyle = .solid(width: 1, color: .lightGray)
        
        
        
        spreadsheetView.dataSource = self
        self.view.addSubview(spreadsheetView)
    }
    
    func numberOfColumns(in spreadsheetView: SpreadsheetView) -> Int {
        return 6
    }
    
    func numberOfRows(in spreadsheetView: SpreadsheetView) -> Int {
        return 6
    }
    
    func spreadsheetView(_ spreadsheetView: SpreadsheetView, widthForColumn column: Int) -> CGFloat {
        if(column == 0) {
          return 150
        }else {
        return 50
        }
    }
    
    func spreadsheetView(_ spreadsheetView: SpreadsheetView, heightForRow row: Int) -> CGFloat {
        return 40
    }
}
