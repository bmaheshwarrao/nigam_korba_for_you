//
//  DashboardTypeHazardFilterTableViewCell.swift
//  Balco Sustainability Plus
//
//  Created by sudheer-kumar on 25/01/18.
//  Copyright © 2018 safiqul islam. All rights reserved.
//

import UIKit

class DashboardTypeHazardFilterTableViewCell: UITableViewCell {

    
    @IBOutlet weak var firstthCon: NSLayoutConstraint!
    
    @IBOutlet weak var secondThCon: NSLayoutConstraint!
    @IBOutlet weak var thirdthCon: NSLayoutConstraint!
    
    @IBOutlet weak var forthcon: NSLayoutConstraint!
    @IBOutlet weak var filterAppcon: NSLayoutConstraint!
    @IBOutlet weak var hazardTypeLabel: UILabel!
    @IBOutlet weak var locationTypeLabel: UILabel!
    @IBOutlet weak var fromDateLabel: UILabel!
    @IBOutlet weak var toDateLabel: UILabel!
    @IBOutlet weak var filterAppLabel: UILabel!
    @IBOutlet weak var viewOf: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
