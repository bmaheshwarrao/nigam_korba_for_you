//
//  Dashbord_Type_HazardModel.swift
//  Balco Sustainability Plus
//
//  Created by sudheer-kumar on 20/01/18.
//  Copyright © 2018 safiqul islam. All rights reserved.
//

import Foundation
import Reachability



class DashboardTypeHazardDataModel: NSObject {
    
    
    var TypeName = String()
    var Type_ID = String()
    var Yesterday = Int()
    var MTD = Int()
    var YTD = Int()
    var Closed = Int()
    var Open = Int()
    
    
    
    var totalYesterday = Int()
    var totalMTD = Int()
    var totalYTD = Int()
    var totalClosed = Int()
    var totalOpen = Int()
    
    
    func setDataInModel(cell:[String:AnyObject])
    {
        if cell["Type_Name"] is NSNull{
            self.TypeName = ""
        }else{
            self.TypeName = (cell["Type_Name"] as? String)!
        }
        if cell["Type_ID"] is NSNull{
            self.Type_ID = ""
        }else{
            
            
            let aid = cell["Type_ID"]
            let aaid = aid as! Int
            self.Type_ID = String(describing: aaid)
        }
        if cell["Yesterday"] is NSNull{
            self.Yesterday = 0
        }else{
            self.Yesterday = (cell["Yesterday"] as? Int)!
        }
        if cell["MTD"] is NSNull{
            self.MTD = 0
        }else{
            self.MTD = (cell["MTD"] as? Int)!
        }
        if cell["YTD"] is NSNull{
            self.YTD = 0
        }else{
            self.YTD = (cell["YTD"] as? Int)!
        }
        if cell["Closer"] is NSNull{
            self.Closed = 0
        }else{
            self.Closed = (cell["Closer"] as? Int)!
        }
        if cell["Opened"] is NSNull{
            self.Open = 0
        }else{
            self.Open = (cell["Opened"] as? Int)!
        }
        
     
        
        
        if cell["Total_Yesterday"] is NSNull{
            self.totalYesterday = 0
        }else{
            self.totalYesterday = (cell["Total_Yesterday"] as? Int)!
        }
        if cell["Total_MTD"] is NSNull{
            self.totalMTD = 0
        }else{
            self.totalMTD = (cell["Total_MTD"] as? Int)!
        }
        if cell["Total_YTD"] is NSNull{
            self.totalYTD = 0
        }else{
            self.totalYTD = (cell["Total_YTD"] as? Int)!
        }
        if cell["Total_Closer"] is NSNull{
            self.totalClosed = 0
        }else{
            self.totalClosed = (cell["Total_Closer"] as? Int)!
        }
        if cell["Total_Opened"] is NSNull{
            self.totalOpen = 0
        }else{
            self.totalOpen = (cell["Total_Opened"] as? Int)!
        }
        
        
    }
}


class DashboardTypeHazardAPI
{
    
    var reachablty = Reachability()!
    
    
    func serviceCalling(obj:DashboardTypeHazardViewController,parameter: [String:String], success:@escaping (AnyObject)-> Void)
    {
        
  var statusString = String()
        
        if(reachablty.connection != .none)
        {
               obj.startLoading(view: obj.view)
             obj.tableView.isHidden = true
            WebServices.sharedInstances.sendPostRequest(url: URLConstants.Hazard_Report_Dashboard, parameters: parameter, successHandler: { (dict) in
                //print("dict",dict)
                if let response = dict["response"]{
                    
                     statusString  = response["status"] as! String
                    let objectmsg = MessageCallServerModel()
            let msg = objectmsg.sealizeMessage(cell: response as! [String : AnyObject])
                    print(response)
                    if(statusString == "success")
                    {
                        
                        obj.tableView.isHidden = false
                        obj.noDataLabel(text: "" )
                        obj.label.isHidden = true;
                        
                        if let data = dict["data"] as? [AnyObject]
                        {
                            
                            var dataArray : [DashboardTypeHazardDataModel] = []
                            
                            for i in 0..<data.count
                            {
                                
                                if let celll = data[i] as? [String:AnyObject]
                                {
                                    let object = DashboardTypeHazardDataModel()
                                    object.setDataInModel(cell: celll)
                                    dataArray.append(object)
                                }
                                
                            }
                            
                            success(dataArray as AnyObject)
                            
                            
                        }
                           obj.stopLoading(view: obj.view)
                        obj.tableView.reloadData()
                        obj.refresh.endRefreshing()
                       
                    }
                    else
                    {
                       
                        print("DATA:fail")
                        obj.stopLoading(view: obj.view)
                        obj.tableView.isHidden = true
                        obj.label.isHidden = false;
                        obj.noDataLabel(text: msg )
                        obj.refresh.endRefreshing()
                        statusString = "success"
                        
                    }
                    
                    
                }
                
                
                
                
            })
            { (error) in
                
                let errorStr : String = error.description
                
                print("DATA:error",errorStr)
                statusString = "fail"
            
                
            }
            DispatchQueue.main.asyncAfter(deadline: .now() + 4.0) {
                if(statusString != "success") {
                    obj.noDataLabel(text:  "Check your internet connection and try again.")
                    obj.label.isHidden = false
                    obj.tableView.isHidden = true
                    obj.refresh.endRefreshing()
                    obj.stopLoading(view: obj.tableView)
                }
            }
            
        }
        else{
            obj.noDataLabel(text:  "No internet connection found. Check your internet connection and try again.")
             obj.tableView.isHidden = true
            obj.label.isHidden = false;
            obj.refresh.endRefreshing()
          
        }
        
        
    }
    
    
}
