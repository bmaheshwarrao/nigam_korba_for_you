//
//  SearchModelData.swift
//  VallSafety
//
//  Created by SARVANG INFOTCH on 14/10/18.
//  Copyright © 2018 Bunga Maheshwar Rao. All rights reserved.
//

import Foundation
import Reachability



class NSearchHazardResultModel: NSObject {
    
    
    
    var date_Time: String?
    
    var Name : String?
    var areaId : String?
    var area_Name : String?
    var subareaId : String?
    var subarea_Name : String?
    var ID  = Int()
    
    var hazard_Type_ID : String?
    var hazard_Type_Name : String?
    var descriptionn : String?
    var image_path : String?
    var risk_level : Int?
    var likelihood : Int?
    var Consequence : Int?
    var Employee_ID : String?
    var Employee_Name : String?
    var status : String?
    
    var Unit_ID : String?
    var Zone_ID : String?
    var Unit_Name : String?
    var Zone_Name : String?
    //    var Sbu_Department : String?
    //  var location_ID  : Int?
    // var sub_location_ID : Int?
    //   var Sub_location_Name : String?
    //  var main_location : String?
    // var business : String?
    //var main_location_ID : Int?
    
    func setDataInModel(cell:[String:AnyObject])
    {
        
        
        
        if cell["Date_Time"] is NSNull{
            self.date_Time = ""
        }else{
            self.date_Time = (cell["Date_Time"] as? String)!
        }
        if cell["Employee_Name"] is NSNull{
            self.Employee_Name = ""
        }else{
            self.Employee_Name = (cell["Employee_Name"] as? String)!
        }
        if cell["Employee_ID"] is NSNull{
            self.Employee_ID = ""
        }else{
            self.Employee_ID = (cell["Employee_ID"] as? String)!
        }
        if cell["Area_ID"] is NSNull || cell["Area_ID"] == nil{
            self.areaId = "0"
        }else{
            let aid : NSNumber = (cell["Area_ID"] as? NSNumber)!
            self.areaId = aid.stringValue
        }
        if cell["Area_Name"] is NSNull{
            self.area_Name = ""
        }else{
            self.area_Name = (cell["Area_Name"] as? String)!
        }
        
        
        if cell["Sub_Area_ID"] is NSNull || cell["Sub_Area_ID"] == nil{
            self.subareaId = "0"
        }else{
            let aid : NSNumber = (cell["Sub_Area_ID"] as? NSNumber)!
            self.subareaId = aid.stringValue
        }
        if cell["SubArea_Name"] is NSNull{
            self.subarea_Name = ""
        }else{
            self.subarea_Name = (cell["SubArea_Name"] as? String)!
        }
        
        
        if cell["Unit_ID"] is NSNull || cell["Unit_ID"] == nil{
            self.Unit_ID = "0"
        }else{
            let aid : NSNumber = (cell["Unit_ID"] as? NSNumber)!
            self.Unit_ID = aid.stringValue
        }
        if cell["Unit_Name"] is NSNull{
            self.Unit_Name = ""
        }else{
            self.Unit_Name = (cell["Unit_Name"] as? String)!
        }
        if cell["Zone_ID"] is NSNull || cell["Likelihood"] == nil{
            self.Zone_ID = "0"
        }else{
            let aid : NSNumber = (cell["Zone_ID"] as? NSNumber)!
            self.Zone_ID = aid.stringValue
        }
        if cell["Zone_Name"] is NSNull{
            self.Zone_Name = ""
        }else{
            self.Zone_Name = (cell["Zone_Name"] as? String)!
        }
        
        
        if cell["Consequence"] is NSNull || cell["Consequence"] == nil{
            self.Consequence = 0
        }else{
            self.Consequence = (cell["Consequence"] as? Int)!
        }
        
        if cell["Likelihood"] is NSNull || cell["Likelihood"] == nil{
            self.likelihood = 0
        }else{
            self.likelihood = (cell["Likelihood"] as? Int)!
        }
        
        
        if cell["Risk_Level"] is NSNull  || cell["Risk_Level"] == nil {
            self.risk_level = 0
        }else{
            self.risk_level = (cell["Risk_Level"] as? Int)!
        }
        
        
        if cell["Status"] is NSNull{
            
        }else{
            self.status = (cell["Status"] as? String)!
        }
        
        if cell["Employee_Name"] is NSNull{
            self.Name = ""
        }else{
            self.Name = (cell["Employee_Name"] as? String)!
        }
        if cell["ID"] is NSNull  || cell["ID"] == nil{
            self.ID = 0
        }else{
            self.ID = (cell["ID"] as? Int)!
        }
        
        if cell["Hazard_Type_ID"] is NSNull || cell["Hazard_Type_ID"] == nil{
            self.hazard_Type_ID = "0"
        }else{
            let aid : NSNumber = (cell["Sub_Area_ID"] as? NSNumber)!
            self.hazard_Type_ID = aid.stringValue
        }
        if cell["Hazard_Name"] is NSNull{
            self.hazard_Type_Name = ""
        }else{
            self.hazard_Type_Name = (cell["Hazard_Name"] as? String)!
        }
        if cell["Description"] is NSNull{
            self.descriptionn = ""
        }else{
            self.descriptionn = (cell["Description"] as? String)!
        }
        if cell["Image_path"] is NSNull{
            self.image_path = ""
        }else{
            self.image_path = (cell["Image_path"] as? String)!
        }
        
        
        
        
        
        
        //     
        
        //        if cell["Sbu_Department"] is NSNull{
        //            self.Sbu_Department = ""
        //        }else{
        //            self.Sbu_Department = (cell["Sbu_Department"] as? String)!
        //        }
        //        if cell["location_Name"] is NSNull{
        //            self.location_Name = ""
        //        }else{
        //            self.location_Name = (cell["location_Name"] as? String)!
        //        }
        
        //        if cell["main_location_ID"] is NSNull{
        //
        //        }else{
        //            self.main_location_ID = (cell["main_location_ID"] as? Int)!
        //        }
        //        if cell["business_ID"] is NSNull{
        //
        //        }else{
        //            self.business_ID = (cell["business_ID"] as? Int)!
        //        }
        //        if cell["location_ID"] is NSNull{
        //
        //        }else{
        //            self.location_ID = (cell["location_ID"] as? Int)!
        //        }
        //        if cell["sub_location_ID"] is NSNull{
        //
        //        }else{
        //            self.sub_location_ID = (cell["sub_location_ID"] as? Int)!
        //        }
        
        
        
        
    }
}


class NSearchHazardResultAPI
{
    
    var reachablty = Reachability()!
    
    
    func serviceCalling(obj:NSearchHazardResultViewController,parameter:[String:String], success:@escaping (AnyObject)-> Void)
    {
        
        obj.startLoading()
        
        if(reachablty.connection != .none)
        {
            
            WebServices.sharedInstances.sendPostRequest(url: URLConstants.search_hazards, parameters: parameter, successHandler: { (dict) in
                
                if let response = dict["response"]{
                    
                    let statusString : String = response["status"] as! String
                    
                    if(statusString == "success")
                    {
                        
                        obj.tableView.isHidden = false
                        obj.noDataLabel(text: "" )
                        obj.label.isHidden = true
                        
                        if let data = dict["data"] as? [AnyObject]
                        {
                            
                            var dataArray : [NSearchHazardResultModel] = []
                            
                            for i in 0..<data.count
                            {
                                
                                if let celll = data[i] as? [String:AnyObject]
                                {
                                    let object = NSearchHazardResultModel()
                                    object.setDataInModel(cell: celll)
                                    dataArray.append(object)
                                }
                                
                            }
                            
                            success(dataArray as AnyObject)
                            
                            
                        }
                        
                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                    }
                    else
                    {
                        
                        
                        print("DATA:fail")
                        obj.tableView.isHidden = true
                        obj.noDataLabel(text: "No hazard found with "+parameter["Search"]! )
                        obj.refresh.endRefreshing()
                        obj.label.isHidden = false
                       obj.stopLoading()
                        
                    }
                    
                    
                }
                
                
                
            }, failureHandler: { (error) in
                let errorStr : String = error.description
                
                print("DATA:error",errorStr)
                
               // obj.errorChecking(error: error)
              obj.stopLoading()
            })
            
            
        }
        else{
            obj.noDataLabel(text:  "No internet connection found. Check your internet connection and try again.")
            obj.tableView.isHidden = true
              obj.label.isHidden = false
            obj.refresh.endRefreshing()
            obj.stopLoading()
        }
        
        
    }
    
    
}

class NSearchHazardResultLoadMoreAPI
{
    
    var reachablty = Reachability()!
    
    
    func serviceCalling(obj:NSearchHazardResultViewController,parameter:[String:String], success:@escaping (AnyObject)-> Void)
    {
        
        obj.startLoading()
        
        if(reachablty.connection != .none)
        {
            
            WebServices.sharedInstances.sendPostRequest(url: URLConstants.search_hazards, parameters: parameter, successHandler: { (dict) in
                
                if let response = dict["response"]{
                    
                    let statusString : String = response["status"] as! String
                    
                    if(statusString == "success")
                    {
                        
                        //                        obj.tableView.isHidden = false
                        //                        obj.noDataLabel(text: "" )
                        
                        if let data = dict["data"] as? [AnyObject]
                        {
                            
                            var dataArray : [NSearchHazardResultModel] = []
                            
                            for i in 0..<data.count
                            {
                                
                                if let celll = data[i] as? [String:AnyObject]
                                {
                                    let object = NSearchHazardResultModel()
                                    object.setDataInModel(cell: celll)
                                    dataArray.append(object)
                                }
                                
                            }
                            
                            success(dataArray as AnyObject)
                            
                            
                        }
                        
                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                    }
                    else
                    {
                        
                        
                        print("DATA:fail")
                        //                        obj.tableView.isHidden = true
                        //                        obj.noDataLabel(text: "No hazard found with "+parameter["Search"]! )
                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                        
                    }
                    
                    
                }
                
                
                
            }, failureHandler: { (error) in
                let errorStr : String = error.description
                
                print("DATA:error",errorStr)
                
                //obj.errorChecking(error: error)
                obj.stopLoading()
            })
            
            
        }
        else{
            //            obj.noDataLabel(text:  "No internet connection found. Check your internet connection and try again.")
            //            obj.tableView.isHidden = true
            obj.refresh.endRefreshing()
            obj.stopLoading()
        }
        
        
    }
    
    
}

