//
//  ReportedHazardViewController.swift
//  VallSafety
//
//  Created by Bunga Maheshwar Rao on 04/10/18.
//  Copyright © 2018 Bunga Maheshwar Rao. All rights reserved.
//

import UIKit
import SDWebImage

class ReportedHazardViewController: CommonVSClass ,UITextViewDelegate{

//    var ID : [String] = ["16" ,"17"]
    //var Date : [String] = ["27-Sep-2018","27-Sep-2018"]
   
      var imagedata : [String] = []
    var hazarddisplay = Int()

    //@IBOutlet weak var textViewHeight: NSLayoutConstraint!
    // @IBOutlet weak var textViewHeight: NSLayoutConstraint!
    
    var DataAPI = ReportedHazardForStatus()
    var hazardDB : [ReportedHazardStatus] = []
    var data: String?
    var lastObject: String?
    var indPath = IndexPath()
    var hazardLoadMoreDB : [ReportedHazardStatus] = []
    

@IBOutlet weak var tableViewReported: UITableView!

var ViewStr = String()
var statusStr  = String()
var hazardTypeStr  = String()
var locationStr  = String()
var AreaStr  = String()
    var unitStr  = String()
    var departmentStr  = String()
    
    var hazardtypeId  = String()
    
    var DataLoadMoreAPI = ReportedHazardForStatusLoadMore()
   
    
    override func viewDidLoad() {
        super.viewDidLoad()
         tableViewReported.contentInset = UIEdgeInsetsMake(0.0, 0.0, 0.0, 0.0)
        tableViewReported.delegate = self;
        tableViewReported.dataSource = self;
        tableViewReported.estimatedRowHeight = 290
      tableViewReported.rowHeight = UITableViewAutomaticDimension
       
        
        refresh.addTarget(self, action: #selector(ReportdatabyStatus), for: .valueChanged)
        
        self.tableViewReported.addSubview(refresh)
        let swipe = UISwipeGestureRecognizer(target: self, action: #selector(self.swipeRecognizer))
        swipe.direction = ([.down])
        self.view.addGestureRecognizer(swipe)
          tableViewReported.tableFooterView = UIView()
        

       // ReportdatabyStatus()
        
        
//barSetup()
        
        
        // Do any additional setup after loading the view.
    }
    @objc func swipeRecognizer(_ sender: UISwipeGestureRecognizer) {
        if (sender.direction == .down) {
            
            if(self.reachability.connection != .none)
            {
                
                self.ReportdatabyStatus()
            }
            else
            {
                self.startLoading(view: self.view)
                DispatchQueue.main.asyncAfter(deadline: .now() + 4.5) {
                    self.noDataLabel(text: "No internet connection found. Check your internet connection and try again.")
                    self.stopLoading(view: self.view)
                }
                
            }
            
        }
    }
    func setUp() {
        hazarddisplay = UserDefaults.standard.integer(forKey: "hazard")
        print(hazarddisplay)
        if(hazarddisplay == 1) {
            let status : String = UserDefaults.standard.value(forKey: "Status") as! String
            if(status == "Pending") {
                self.title = "Pending Hazard's"
            } else if(status == "Close") {
                self.title = "Closed Hazard's"
            }else if(status == "Reject") {
                self.title = "Rejected Hazards"
            }else {
                self.title = "Reported Hazard's"
            }
        } else if(hazarddisplay == 4) {
            self.title = "Hazards"
            
        } else if(hazarddisplay == 5) {
            
            let status : String = UserDefaults.standard.value(forKey: "Status") as! String
            if(status == "Pending") {
                self.title = "Pending Hazard's"
            } else if(status == "Close") {
                self.title = "Closed Hazard's"
            }else if(status == "Reject") {
                self.title = "Rejected Hazard's"
            }else {
                self.title = "Reported Hazard's"
            }
            
        }else if(hazarddisplay == 6) {
            
            let status : String = UserDefaults.standard.value(forKey: "Status") as! String
            if(status == "Pending") {
                self.title = "Pending Hazard's"
            } else if(status == "Close") {
                self.title = "Closed Hazard's"
            }else if(status == "Reject") {
                self.title = "Rejected Hazard's"
            }else {
                self.title = "Reported Hazard's"
            }
            
        }
        
        
        else {
            let status : String = UserDefaults.standard.value(forKey: "Status") as! String
            if(status == "Pending") {
                self.title = "Action I need to close"
            } else if(status == "Close") {
                self.title = "Hazards I have closed"
            }else if(status == "Assign_Pending_To_Me") {
                self.title = "Assigned but not yet closed"
            }else {
                self.title = "Actions I need to assign"
            }
        }
        self.tabBarController?.tabBar.isHidden = true
        let nav = self.navigationController?.navigationBar
        nav?.tintColor = UIColor.white
       nav?.barTintColor = UIColor(hexString: "2c3e50", alpha: 1.0)
        nav?.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        ReportdatabyStatus()
    }
    override func viewDidAppear(_ animated: Bool) {

    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
    setUp()
        
        if(MoveStruct.isMove == true) {
            MoveStruct.isMove = false
            self.view.makeToast(MoveStruct.message)
           
        }
    }
    override func viewWillDisappear(_ animated: Bool) {
      
    }
    
    func barSetup() {
        let view = UIView(frame: CGRect(x: 0, y: 0, width: self.view.bounds.width , height: 44))
        view.backgroundColor = UIColor.clear
        let imgView = UIImageView(frame: CGRect(x: 0, y: 5, width: 25, height: 25))
        imgView.image = UIImage(named: "leftArrowWhite")
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tapBack(_:)))
        imgView.isUserInteractionEnabled = true;
        imgView.addGestureRecognizer(tapGesture)
        let txtBarRight = UITextField(frame: CGRect(x: 100, y: 5, width: self.view.bounds.width , height: 30))
        txtBarRight.text = "Report Hazard"
        txtBarRight.textColor = UIColor.white
        
        txtBarRight.font = UIFont(name: "Georgia", size: 17.0)
        view.addSubview(txtBarRight)
        view.addSubview(imgView)
        let barButtonItem = UIBarButtonItem(customView: view)
        self.navigationItem.leftBarButtonItem = barButtonItem
        
        
    }
    @objc func tapBack(_ sender : UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    @objc func ImageViewTapped(_ sender : UITapGestureRecognizer) {
        let buttonPosition:CGPoint = sender.view!.convert(.zero, to:self.tableViewReported)
        let indexPath = self.tableViewReported.indexPathForRow(at: buttonPosition)
        
        if hazardDB[(indexPath?.row)!].image_path != "" {
            
            let urlString = self.hazardDB[(indexPath?.row)!].image_path!
            let urlShow = urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
            
            let ZIVC = self.storyboard?.instantiateViewController(withIdentifier: "ImageZoom") as! ImageZoomViewController
            ZIVC.zoomImageUrl = urlShow!
            self.navigationController?.pushViewController(ZIVC, animated: true)
            
        }
    }

    @objc func tapCell(_ sender : UITapGestureRecognizer) {
        
        
        let buttonPosition:CGPoint = sender.view!.convert(.zero, to:self.tableViewReported)
        let indexPath = self.tableViewReported.indexPathForRow(at: buttonPosition)
        let status : String = UserDefaults.standard.value(forKey: "Status") as! String
        if(hazarddisplay == 2 && status == "Pending") {
            

            
            let storyBoard = UIStoryboard(name: "ThemeSafety", bundle: nil)
            
            let ZIVC = storyBoard.instantiateViewController(withIdentifier: "CloseViewController") as! CloseViewController
            ZIVC.Hazardname = String(describing: hazardDB[(indexPath?.row)!].hazard_Type_Name!)
            ZIVC.AreaName = String(describing: hazardDB[(indexPath?.row)!].area_Name!)
            ZIVC.SubAreaName = String(describing: hazardDB[(indexPath?.row)!].subarea_Name!)
            ZIVC.ZoneName = String(describing: hazardDB[(indexPath?.row)!].Zone_Name!)
            ZIVC.UnitName = String(describing: hazardDB[(indexPath?.row)!].Unit_Name!)
            ZIVC.employeeId = String(describing: hazardDB[(indexPath?.row)!].Employee_ID!)
            ZIVC.employeeName = String(describing: hazardDB[(indexPath?.row)!].Employee_Name!)
            ZIVC.notify = 0
            ZIVC.riskLevel = String(describing: hazardDB[(indexPath?.row)!].risk_level!)
            ZIVC.dateVal = String(describing: hazardDB[(indexPath?.row)!].date_Time!)
            ZIVC.desc = String(describing: hazardDB[(indexPath?.row)!].descriptionn!)
            ZIVC.status = String(describing: hazardDB[(indexPath?.row)!].status!)
            ZIVC.hazardID = String(describing: hazardDB[(indexPath?.row)!].ID)
            let urlString = self.hazardDB[(indexPath?.row)!].image_path!
            let urlShow = urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
            ZIVC.imgdata = String(describing: urlShow!)
            
            self.navigationController?.pushViewController(ZIVC, animated: true)
            
            
            
        } else {
             let storyBoard = UIStoryboard(name: "ThemeSafety", bundle: nil)
        let ZIVC = storyBoard.instantiateViewController(withIdentifier: "HazardDetailsViewController") as! HazardDetailsViewController
        ZIVC.hazardID = String(describing: hazardDB[(indexPath?.row)!].ID)
        ZIVC.empId = hazardDB[(indexPath?.row)!].Employee_ID!
        self.navigationController?.pushViewController(ZIVC, animated: true)
        }
    }
    let reachability = Reachability()!
    var countID : Int = 0
  
    @objc func ReportdatabyStatus() {
        

        
        var para = [String:String]()
        let status : String = UserDefaults.standard.value(forKey: "Status") as! String
        let empId : String = UserDefaults.standard.string(forKey: "EmployeeID")!
        
  print(status)
        var parameter : [String : String ] = [:]
        var  dictWithoutNilValues = [String:String]()
        if(hazarddisplay == 6){
            
            dictWithoutNilValues = ["Hazard_Type_ID":hazardtypeId,
                                    "Status" : statusStr ,
                                    "Area_ID":AreaStr,
                                    "Sub_Area_ID":departmentStr,
                                    "Zone_ID":locationStr,
                                    "Unit_ID":unitStr,
                                    
                                    "FromDate":FilterGraphStruct.fromDate,
                                    "ToDate":FilterGraphStruct.toDate
            ]
            
            parameter = dictWithoutNilValues.filter { $0.value != ""}
            
            
            
            
            
            
            
        }else{
        if(hazarddisplay != 4 ) {
        if(status == "All") {
            parameter = ["Employee_ID": empId ]
        } else if(status == "Change") {
            parameter = ["Employee_ID": empId  ]
        }
        else {
            parameter = ["Employee_ID": empId , "Status" : status ]
        }
        } else {
     
           
            dictWithoutNilValues = ["Hazard_Type_ID":hazardtypeId,
                                    "Status" : statusStr ,
                                    "Area_ID":AreaStr,
                                    "Sub_Area_ID":departmentStr,
                                       "Zone_ID":locationStr,
                                    "Unit_ID":unitStr,
                                    "View":FilterGraphStruct.isHazardView,
                                    "FromDate":FilterGraphStruct.fromDate,
                                    "ToDate":FilterGraphStruct.toDate,
                                    "FromNo":FilterGraphStruct.minRiskLevel,
                                    "ToNo":FilterGraphStruct.mixRiskLevel]
            
            parameter = dictWithoutNilValues.filter { $0.value != ""}
        }
        }
        print(parameter)
           //para = parameter.filter { $0.value != ""}
        var url : String = ""
        if(hazarddisplay == 1) {
            url = URLConstants.hazard_by_status
        } else if (hazarddisplay == 4) {
            url = URLConstants.Hazard_Report_Dashboard_Details
        }else if (hazarddisplay == 6) {
            url = URLConstants.Dashbord_List_New_Hazard
        }
        else if (hazarddisplay == 5) {
            url = URLConstants.hazard_by_status
        }
        else {
            if(status == "Change") {
                url = URLConstants.hazard_by_actions_change
            } else {
                url = URLConstants.hazard_by_actions
            }
        }
       print(parameter)
        print(url)
        self.DataAPI.serviceCalling(obj: self, parameter: parameter , status: status, statusData: self.title! , url: url) { (dict) in
            
            self.hazardDB = [ReportedHazardStatus]()
            self.hazardDB = dict as! [ReportedHazardStatus]
            self.tableViewReported.reloadData()
        }
        
    }
    @objc func callMyReportedReportedHazardLoadMore(Id:String) {
        
        let status : String = UserDefaults.standard.value(forKey: "Status") as! String
        let empId : String = UserDefaults.standard.string(forKey: "EmployeeID")!
        var url : String = ""
        var  dictWithoutNilValues = [String:String]()
        if(hazarddisplay == 1) {
            url = URLConstants.hazard_by_status
        } else if (hazarddisplay == 4) {
         url = URLConstants.Hazard_Report_Dashboard_Details
        }else if (hazarddisplay == 6) {
            url = URLConstants.Dashbord_List_New_Hazard
        }
        else {
            if(status == "Change") {
            url = URLConstants.hazard_by_actions_change
            } else {
                 url = URLConstants.hazard_by_actions
            }
        }
         var parameter : [String : String ] = [:]
        
        if(hazarddisplay == 6){
            
            dictWithoutNilValues = ["Hazard_Type_ID":hazardtypeId,
                                    "Status" : statusStr ,
                                    "Area_ID":AreaStr,
                                    "Sub_Area_ID":departmentStr,
                                    "Zone_ID":locationStr,
                                    "Unit_ID":unitStr,
                                    "ID" : Id,
                                    "FromDate":FilterGraphStruct.fromDate,
                                    "ToDate":FilterGraphStruct.toDate
            ]
            
            parameter = dictWithoutNilValues.filter { $0.value != ""}
            
            
            
            
            
            
            
        }else{
        
        if(hazarddisplay != 4 ) {
       
        if(status == "All") {
            parameter = ["Employee_ID": empId , "ID":Id]
        } else {
            parameter = ["Employee_ID": empId , "Status" : status , "ID":Id]
        }
        
        if(status == "Change") {
            parameter = ["Employee_ID": empId , "ID":Id]
        }
        } else {
            
       
        dictWithoutNilValues = ["Hazard_Type_ID":hazardtypeId,
        "Status" : statusStr ,
        "Hazard_ID" : Id,
        "Area_ID":AreaStr,
        "Sub_Area_ID":departmentStr,
        "Zone_ID":locationStr,
        "Unit_ID":unitStr,
        "View":FilterGraphStruct.isHazardView,
        "FromDate":FilterGraphStruct.fromDate,
        "ToDate":FilterGraphStruct.toDate,
        "FromNo":FilterGraphStruct.minRiskLevel,
        "ToNo":FilterGraphStruct.mixRiskLevel] as! [String : String]
            
       
            
            parameter = dictWithoutNilValues.filter { $0.value != ""}
        }
        }
       print(parameter)
        print(url)
        self.DataLoadMoreAPI.serviceCalling(obj: self, parameter: parameter , url : url) { (dict) in
            
            self.hazardLoadMoreDB = [ReportedHazardStatus]()
            self.hazardLoadMoreDB = dict as! [ReportedHazardStatus]
            
            switch dict.count {
            case 0:
                break;
            default:
                self.hazardDB.append(contentsOf: self.hazardLoadMoreDB)
                self.tableViewReported.reloadData()
                break;
            }
            
        }
        
    }
    func imageResize (image:UIImage, sizeChange:CGSize)-> UIImage{
        
        let hasAlpha = true
        let scale: CGFloat = 0.0 // Use scale factor of main screen
        
        UIGraphicsBeginImageContextWithOptions(sizeChange, !hasAlpha, scale)
        image.draw(in: CGRect(origin: CGPoint.zero, size: sizeChange))
        
        let scaledImage = UIGraphicsGetImageFromCurrentImageContext()
        return scaledImage!
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    var imageData : Data? = nil

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
extension ReportedHazardViewController : UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if(section == 0) {
            return 10.0
        } else {
            return 0.0
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return hazardDB.count
    }
   
    
}

extension ReportedHazardViewController : UITableViewDataSource {
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! ReportedDataTableViewCell
        
        
        cell.statusTextView.tag = indexPath.row
        
        
        cell.statusOfHazard.text = self.hazardDB[indexPath.row].status
        cell.idOfHazard.text = "ID #"+String(describing: self.hazardDB[indexPath.row].ID)
        
        let paragraph1 = NSMutableParagraphStyle()
        paragraph1.alignment = .left
        paragraph1.lineSpacing = 0
        let titleStr = NSMutableAttributedString(string: self.hazardDB[indexPath.row].descriptionn!, attributes: [NSAttributedStringKey.foregroundColor: UIColor.black, NSAttributedStringKey.font: UIFont.systemFont(ofSize: CGFloat(15.0))])
        titleStr.addAttribute(NSAttributedStringKey.paragraphStyle, value: paragraph1, range: NSRange(location: 0, length: titleStr.length))
        cell.titleTextView.attributedText = titleStr
        
        
        switch cell.statusOfHazard.text! {
        case "Close":
            cell.statusOfHazard.textColor = UIColor.colorwithHexString("#5c9834", alpha: 1)
            break;
        case "Pending":
            cell.statusOfHazard.textColor = UIColor.colorwithHexString("#cfcf00", alpha: 1)
            break;
        case "Reject":
            cell.statusOfHazard.textColor = UIColor.colorwithHexString("#FF0000", alpha: 1)
            break;
        default:
            break;
        }
        
        
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = NSTimeZone.system
        dateFormatter.dateFormat = "MMM dd yyyy HH:mm:ss:SSSa"
        let date = dateFormatter.date(from: self.hazardDB[indexPath.row].date_Time!)
        print(self.hazardDB[indexPath.row].date_Time!)
        let dateFormatter2 = DateFormatter()
        dateFormatter2.timeZone = NSTimeZone.system
        dateFormatter2.dateFormat = "dd MMM yyyy"
        
        switch date {
        case nil:
            let date_TimeStr = dateFormatter2.string(from: Date())
            cell.dateLabel.text = date_TimeStr
            break;
        default:
            let date_TimeStr = dateFormatter2.string(from: date!)
            
            cell.dateLabel.text = date_TimeStr
            break;
        }
        
        
        let statusTextViewTapGesture = UITapGestureRecognizer(target: self, action: #selector(self.tapCell(_:)))
        cell.statusTextView.addGestureRecognizer(statusTextViewTapGesture)
        cell.statusTextView.isUserInteractionEnabled = true
        statusTextViewTapGesture.numberOfTapsRequired = 1
        cell.statusTextView.delegate = self
        
        
        // cell.titleTextView.text = self.hazardDB[indexPath.row].descriptionn
        cell.titleTextView.font = UIFont.systemFont(ofSize: 15.0)
        let paragraph = NSMutableParagraphStyle()
        paragraph.alignment = .left
        paragraph.lineSpacing = 0
        let empId : String = UserDefaults.standard.string(forKey: "EmployeeID")!
        var startString : String = "You "
        if(empId != hazardDB[indexPath.row].Employee_ID){
            startString =  hazardDB[indexPath.row].Employee_Name! + "-" +   String(hazardDB[indexPath.row].Employee_ID!) + " "
        }
        let  mBuilder = startString  + "submitted hazard of type " + self.hazardDB[indexPath.row].hazard_Type_Name! + " for location " +  self.hazardDB[indexPath.row].Zone_Name! + " area " + self.hazardDB[indexPath.row].Unit_Name! +
            " subarea " + self.hazardDB[indexPath.row].area_Name! + " department " +  self.hazardDB[indexPath.row].subarea_Name!
        
        let agreeAttributedString = NSMutableAttributedString(string: mBuilder, attributes: [NSAttributedStringKey.foregroundColor: UIColor.black, NSAttributedStringKey.font: UIFont.systemFont(ofSize: CGFloat(15.0))])
        
        
        //Submitted
        let SubmittedAttributedString = NSAttributedString(string:"submitted hazard of type ", attributes: [NSAttributedStringKey.foregroundColor: UIColor.gray, NSAttributedStringKey.font: UIFont.systemFont(ofSize: 15.0)])
        
        let range: NSRange = (agreeAttributedString.string as NSString).range(of: "submitted hazard of type ")
        if range.location != NSNotFound {
            agreeAttributedString.replaceCharacters(in: range, with: SubmittedAttributedString)
        }
        
        //for
        let forAttributedString = NSAttributedString(string:"for location ", attributes: [NSAttributedStringKey.foregroundColor: UIColor.gray, NSAttributedStringKey.font: UIFont.systemFont(ofSize: 15.0)])
        
        let forRange: NSRange = (agreeAttributedString.string as NSString).range(of: "for location ")
        if forRange.location != NSNotFound {
            agreeAttributedString.replaceCharacters(in: forRange, with: forAttributedString)
        }
        
        let areaAttributedString = NSAttributedString(string:"area", attributes: [NSAttributedStringKey.foregroundColor: UIColor.gray, NSAttributedStringKey.font: UIFont.systemFont(ofSize: 15.0)])
        
        let areaRange: NSRange = (agreeAttributedString.string as NSString).range(of: "area")
        if areaRange.location != NSNotFound {
            agreeAttributedString.replaceCharacters(in: areaRange, with: areaAttributedString)
        }
        
        
        
        
        let subAreaAttributedString = NSAttributedString(string:"subarea", attributes: [NSAttributedStringKey.foregroundColor: UIColor.gray, NSAttributedStringKey.font: UIFont.systemFont(ofSize: 15.0)])
        let subAreaRange: NSRange = (agreeAttributedString.string as NSString).range(of: "subarea")
        if subAreaRange.location != NSNotFound {
            agreeAttributedString.replaceCharacters(in: subAreaRange, with: subAreaAttributedString)
        }
        
        let deptAttributedString = NSAttributedString(string:"department", attributes: [NSAttributedStringKey.foregroundColor: UIColor.gray, NSAttributedStringKey.font: UIFont.systemFont(ofSize: 15.0)])
        
        let deptRange: NSRange = (agreeAttributedString.string as NSString).range(of: "department")
        if deptRange.location != NSNotFound {
            agreeAttributedString.replaceCharacters(in: deptRange, with: deptAttributedString)
        }
        
        agreeAttributedString.addAttribute(NSAttributedStringKey.paragraphStyle, value: paragraph, range: NSRange(location: 0, length: agreeAttributedString.length))
        
        
        print(cell.statusTextView.contentSize.height)
        cell.statusTextView.attributedText = agreeAttributedString
        cell.statusTextView.font = UIFont.systemFont(ofSize: 15.0)
        self.data = self.hazardDB[indexPath.row].descriptionn!
        self.lastObject = self.hazardDB[indexPath.row].descriptionn!
        
        
        let imageViewDataCell = UIImageView()
        imageViewDataCell.image = UIImage(named : "placed")
        let postImageTapGesture = UITapGestureRecognizer(target: self, action: #selector(self.ImageViewTapped(_:)))
        if(cell.imageFile != nil) {
            cell.imageFile.addGestureRecognizer(postImageTapGesture)
            cell.imageFile.isUserInteractionEnabled = true
            postImageTapGesture.numberOfTapsRequired = 1
        }
        
        
        
        
        
        
        
        
        cell.statusTextView.isEditable = false
        cell.statusTextView.isSelectable = false
        cell.statusTextView.isScrollEnabled = false
        cell.titleTextView.isEditable = false
        cell.titleTextView.isSelectable = false
        cell.titleTextView.isScrollEnabled = false
        cell.titleTextView.textContainer.maximumNumberOfLines = 3
        cell.titleTextView.textContainer.lineBreakMode = .byTruncatingTail
        cell.titleTextView.textColor = UIColor(hexString: "2c3e50", alpha: 1.0)
        cell.selectionStyle = .none
      
        
       
        let urlString = self.hazardDB[indexPath.row].image_path!
        let imageView = UIImageView()
        
        let urlShow = urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
        if let url = NSURL(string: self.hazardDB[indexPath.row].image_path!) {
            
            SDWebImageManager.shared().downloadImage(with: NSURL(string: urlShow!) as URL!, options: .continueInBackground, progress: {
                (receivedSize :Int, ExpectedSize :Int) in
                
            }, completed: {
                (image : UIImage?, error : Error?, cacheType : SDImageCacheType, finished : Bool, url : URL?) in
                if(image != nil) {
                    let size = CGSize(width: 120, height: 100)
                    let imageCell = self.imageResize(image: image!, sizeChange: size)
                    cell.imageFile.image = imageCell
                    //  cell.viewImageHold.backgroundColor = UIColor(patternImage: imageCell)
                    
                }else{
                    imageView.image =  UIImage(named: "placed")
                    let size = CGSize(width: 120, height: 100)
                    let imageCell = self.imageResize(image: imageView.image!, sizeChange: size)
                    cell.imageFile.image = imageCell
                    // cell.viewImageHold.backgroundColor = UIColor(patternImage: imageCell)
                }
            })
        }else{
            imageView.image =  UIImage(named: "placed")
            let size = CGSize(width: 120, height: 100)
            let imageCell = imageResize(image: imageView.image!, sizeChange: size)
            cell.imageFile.image = imageCell
            //cell.viewImageHold.backgroundColor = UIColor(patternImage: imageCell)
        }
        
        //   cell.heightImage.constant = cell.statusTextView.contentSize.height + cell.titleTextView.contentSize.height
        
        var riskLevlaval = 0
        
        
        
        let cal : Int = Int(self.hazardDB[indexPath.row].risk_level!)
        if(cal == 0){
         cell.viewRiskLevel.backgroundColor = UIColor.clear
        }
        else if(cal <= 4 && cal > 0) {
            
            cell.viewRiskLevel.backgroundColor = UIColor.colorwithHexString("87ceeb", alpha: 1.0)
        } else if(cal <= 9 && cal > 4) {
            
            cell.viewRiskLevel.backgroundColor = UIColor.colorwithHexString("CCCC00", alpha: 1.0)
        } else if(cal <= 16 && cal > 9 ) {
            
            cell.viewRiskLevel.backgroundColor = UIColor.colorwithHexString("ffa500", alpha: 1.0)
        } else if(cal <= 25 && cal > 16 ) {
            
            cell.viewRiskLevel.backgroundColor = UIColor.colorwithHexString("FF0000", alpha: 1.0)
        }
        
        //   cell.heightImage.constant = cell.heightRiskLevel.constant
        
        if ( self.data ==  self.lastObject && indexPath.row == self.hazardDB.count - 1)
        {
            
           self.callMyReportedReportedHazardLoadMore( Id: String(Int(self.hazardDB[indexPath.row].ID)))
            
        }
        
        cell.separatorInset = UIEdgeInsetsMake(0, 10000, 0, 0);
        
        return cell
        
        
        
   
        
    }
}


