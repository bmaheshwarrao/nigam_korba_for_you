import UIKit
import Alamofire
import MobileCoreServices
import CoreData
import SDWebImage
import DLRadioButton
class CloseViewController: CommonVSClass ,UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    
    
    
    
    // @IBOutlet weak var textViewHeight: NSLayoutConstraint!
    var employeeId = String()
    var AreaName = String()
    var SubAreaName = String()
    var Hazardname = String()
    var hazardID = String()
    var desc = String()
    var dateVal = String()
    var status = String()
    var imgdata = String()
    var employeeName = String()
    var riskLevel = String()
    var notify = Int()
    var ZoneName = String()
    var UnitName = String()
    
    @IBOutlet weak var tableView: UITableView!
    
    
    var imagedata: Data? = nil
    let pickerr = UIImagePickerController()
    let pickerImage = UIImage()
    
    
    var MyReportedHazardDetailsAPI = HazardDetailsDataAPICLOSE()
    var MyReportedHazardDB:[MyReportedHazardDetailsDataModel] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.isHidden = true
        tableView.delegate = self
        tableView.dataSource = self
        pickerr.delegate = self
        imagedata = nil
        
        print(UnitName)
        print(ZoneName)
        if(notify == 1){
            showOnlineData()
        }else{
            if(imgdata == ""){
                   self.tableView.isHidden = false
                self.tableView.reloadData()
            }else{
                showOnlineData()
            }
        }
        // Do any additional setup after loading the view.
        
        
    }
    //    func scrollViewDidScroll(_ scrollView: UIScrollView) {
    //        scrollView.contentOffset.x = 0
    //    }
    func imageResize (image:UIImage, sizeChange:CGSize)-> UIImage{
        
        let hasAlpha = true
        let scale: CGFloat = 0.0 // Use scale factor of main screen
        
        UIGraphicsBeginImageContextWithOptions(sizeChange, !hasAlpha, scale)
        image.draw(in: CGRect(origin: CGPoint.zero, size: sizeChange))
        
        let scaledImage = UIGraphicsGetImageFromCurrentImageContext()
        return scaledImage!
    }
    @objc func showOnlineData() {
        
        self.MyReportedHazardDetailsAPI.serviceCalling(obj: self, hazardID: self.hazardID, pNo: UserDefaults.standard.string(forKey: "EmployeeID")!) { (dict) in
            
            self.MyReportedHazardDB = [MyReportedHazardDetailsDataModel]()
            self.MyReportedHazardDB = dict as! [MyReportedHazardDetailsDataModel]
            
            
            self.employeeId = self.MyReportedHazardDB[0].Employee_ID!
            self.employeeName = self.MyReportedHazardDB[0].Employee_Name!
            self.AreaName = self.MyReportedHazardDB[0].area_Name!
            self.SubAreaName = self.MyReportedHazardDB[0].subarea_Name!
            self.dateVal = self.MyReportedHazardDB[0].Date_Time!
            let urlString = self.MyReportedHazardDB[0].Image_path!
            let urlShow = urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
            self.UnitName = self.MyReportedHazardDB[0].Unit_Name!
            self.ZoneName = self.MyReportedHazardDB[0].Zone_Name!
            self.imgdata = urlShow!
            print(String(self.MyReportedHazardDB[0].Risk_Level!))
            self.riskLevel = String(self.MyReportedHazardDB[0].Risk_Level!)
            self.desc = self.MyReportedHazardDB[0].Description!
            self.hazardID = String(self.MyReportedHazardDB[0].ID)
            self.Hazardname = self.MyReportedHazardDB[0].Hazard_Name!
            self.status = self.MyReportedHazardDB[0].Status!
            print(self.status)
            self.tableView.isHidden = false
            self.tableView.reloadData()
            
        }
    }
    @IBAction func CloseButton(_ sender: Any) {
        let buttonPosition:CGPoint = (sender as AnyObject).convert(.zero, to:self.tableView)
        let indexPath = self.tableView.indexPathForRow(at: buttonPosition)
        let cell = cellData
        cell?.heightImageDrop.constant = 0
        cell?.CloseBtn.isHidden = true
        cell?.imgViewDrop.image = nil
        imagedata = nil
        cell?.lblAttachPhoto.isHidden = false
        cell?.cameraBtn.isHidden = false
        cell?.imageConst.constant = 20
      cell?.closeConst.constant = 49
        tableView.reloadData()
        
        
    }
    var reachability = Reachability()!
    @IBAction func btnProcessClicked(_ sender: UIButton) {
        let buttonPosition:CGPoint = sender.convert(.zero, to:self.tableView)
        let indexPath = self.tableView.indexPathForRow(at: buttonPosition)
        // let cell = tableView.dequeueReusableCell(withIdentifier: "process", for: indexPath!) as! CloseTableViewCell
        if reachability.connection == .none{
            
            self.view.makeToast("Internet is not available, please check your internet connection try again.")
            
        }
        else if cellData.textViewData.text == "" || cellData.textViewData.text == "Type message here..."{
            
            self.view.makeToast("Please write message")
        }else{
            
            
            
            self.startLoadingPK(view: self.view)
            
            
            
            let empId : String = UserDefaults.standard.string(forKey: "EmployeeID")!
            
            let parameter =  ["Employee_ID": empId ,
                              "Hazard_ID": hazardID ,
                              "Remark":cellData.textViewData.text!,
                              "Status":"Close"] as! [String:String]
            
            print(parameter)
            let jsonData: Data? = try? JSONSerialization.data(withJSONObject: parameter, options: [])
            let para: [String: Data] = ["data": jsonData!]
            
            Alamofire.upload(multipartFormData: { multipartFormData in
                
                if !(self.imagedata == nil) {
                    var res : UIImage = UIImage()
                    // res =  (self.imgViewDrop.image?.resizedTo1MB())!
                    let size = CGSize(width: 150, height: 150)
                    res = self.imageResize(image: self.cellData.imgViewDrop.image!,sizeChange: size)
                    
                    self.imagedata = UIImageJPEGRepresentation(res, 1.0)!
                    multipartFormData.append(self.imagedata!, withName: "file", fileName: "file.jpg", mimeType: "image/jpeg")
                }
                
                for (key, value) in para {
                    multipartFormData.append(value, withName: key)
                    
                    
                }
                
            },
                             to:URLConstants.Hazard_Process_Submit)
            { (result) in
                switch result {
                case .success(let upload, _, _):
                    
                    upload.uploadProgress(closure: { (progress) in
                        
                    })
                    
                    
                    upload.responseJSON { response in
                        
                        
                        if let json = response.result.value
                        {
                            var dict = json as! [String: AnyObject]
                            
                            if let response = dict["response"]{
                                print(response)
                                let statusString = response["status"] as! String
                                let objectmsg = MessageCallServerModel()
                    let msg = objectmsg.sealizeMessage(cell: response as! [String : AnyObject])
                                print("Ras Dict :",dict)
                                if(statusString == "success")
                                {
                                    
                                    
                                     self.stopLoadingPK(view: self.view)

                                    
                                    MoveStruct.isMove = true
                                 
                                         MoveStruct.message = "Successfully Closed Post."
                                   
                                   
                                    self.navigationController?.popViewController(animated: false)
                                    
                                    
                                    
                                }
                                else{
                                    
                                     self.stopLoadingPK(view: self.view)
                                    self.view.makeToast(msg)
                                    
                                }
                                
                                
                                
                            }
                            
                            
                        }
                        
                        
                    }
                    
                case .failure(let encodingError):
                    
                     self.stopLoadingPK(view: self.view)
                    self.errorChecking(error: encodingError)
                    
                    print(encodingError.localizedDescription)
                }
            }
        }
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.tabBarController?.tabBar.isHidden = true
        self.title = "Close Hazard"
        let nav = self.navigationController?.navigationBar
        nav?.tintColor = UIColor.white
       nav?.barTintColor = UIColor(hexString: "2c3e50", alpha: 1.0)
        nav?.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        
        //        IQKeyboardManager.shared.enable = true
        //        IQKeyboardManager.shared.enableAutoToolbar = true
        
        
    }
    
    var imageDropping = UIImage()
    @IBAction func seggestionImageTap(_ sender: Any) {
        //        let buttonPosition:CGPoint = (sender as AnyObject).convert(.zero, to:self.tableView)
        //        let indexPath = self.tableView.indexPathForRow(at: buttonPosition)
        //          let cell = tableView.dequeueReusableCell(withIdentifier: "process", for: indexPath!) as! CloseTableViewCell
        //        cellData = cell
        selectImage()
        
        
    }
    
    func selectImage(){
        
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.0) {
            
            let actionSheetController: UIAlertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
            
            let camera = UIAlertAction(title: "Take Photo", style: .default, handler: { (alert) in
                
                self.camera()
                
            })
            let Library = UIAlertAction(title: "Choose From Library", style: .default, handler: { (alert) in
                
                self.gallery()
                
            })
            
            
            
            let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: { (alert) in
                
            })
            
            actionSheetController.addAction(camera)
            actionSheetController.addAction(Library)
            actionSheetController.addAction(cancel)
            self.present(actionSheetController, animated: true, completion: nil)
            
            
        }
        
        
    }
    
    var cellData : CloseTableViewCell!
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        if let mediaType = info[UIImagePickerControllerMediaType] as? String {
            
            if mediaType == (kUTTypeImage as String)
            {
                
                let cell = cellData
                let image = info[UIImagePickerControllerOriginalImage] as! UIImage
                //imageDropping = image
                // let resizedImage = image.resizedTo1MB()
                cell?.imgViewDrop.isHidden = false
                cell?.CloseBtn.isHidden = false
                cell?.imgViewDrop.contentMode = .scaleAspectFill
                cell?.heightImageDrop.constant = 120
                cell?.imgViewDrop.image = image
                cell?.CloseBtn.isHidden = false
                cell?.lblAttachPhoto.isHidden = true
                cell?.cameraBtn.isHidden = true
                imagedata = UIImageJPEGRepresentation(image, 1.0)!
                cell?.imageConst.constant = -10
                cell?.closeConst.constant = 19
                
                tableView.reloadData()
                dismiss(animated:true, completion: nil)
            }
            else
            {
                self.dismiss(animated: true, completion: nil)
            }
        }
        
        
    }
      var btnStatus = UIButton()
    var lblStatus = UILabel()
    func createDesign(){
         btnStatus = UIButton()
        btnStatus.frame = CGRect(x: 5, y: 5, width: cellData.viewStatus.frame.width - 10, height: 40)
        btnStatus.layer.borderWidth = 1.0
        btnStatus.addTarget(self, action: #selector(pressStatus(_:)), for: UIControlEvents.touchUpInside)
        let imgViewde = UIImageView(frame: CGRect(x: btnStatus.frame.width - 30, y: 15, width: 15    , height: 15))
        imgViewde.image = UIImage(named: "downArrow")
        btnStatus.addSubview(imgViewde)
        
        lblStatus = UILabel(frame: CGRect(x: 10, y: 5, width: btnStatus.frame.width - 40, height: 30))
        lblStatus.text = "Select Status"
        lblStatus.font  = UIFont.systemFont(ofSize: 17.0)
        lblStatus.textColor = UIColor.lightGray
        btnStatus.addSubview(lblStatus)
        btnStatus.layer.borderColor = UIColor.black.cgColor
        btnStatus.layer.cornerRadius = 10.0
        btnStatus.clipsToBounds = true;
        
        cellData.viewStatus.addSubview(btnStatus)
    }
    var statusValue  = String()
    var arrStatus = ["Pending",
                     "Close"]
    @objc func pressStatus(_ sender : UIButton) {
        
        
        
                DispatchQueue.main.async(execute: {() -> Void in
                    let popup = PopUpTableViewController()
        
                    popup.itemsArray = self.arrStatus
        
                    popup.sourceView = self.btnStatus
                    popup.isScroll = true
                    popup.backgroundColor = UIColor.white
                    if popup.itemsArray.count > 5{
                        popup.popUpHeight = 200
                    }
                    else{
                        popup.popUpHeight = CGFloat(popup.itemsArray.count)*45
                    }
                    popup.popUpWidth = UIScreen.main.bounds.size.width-60
                    popup.backgroundImage = nil
                    popup.itemTitleColor = UIColor.white
                    popup.itemSelectionColor = UIColor.lightGray
                    popup.arrowDirections = .any
                    popup.arrowColor = UIColor.white
                    popup.popCellBlock = {(_ popupVC: PopUpTableViewController, _ popupCell: UITableViewCell, _ row: Int, _ section: Int) -> Void in
        
        
        
                        self.statusValue = self.arrStatus[row]
        
                
                        popupVC.dismiss(animated: false, completion: nil)
        
                    }
                    self.present(popup, animated: true, completion: {() -> Void in
                    })
        
        
                })
   
        
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
        
    }
    
    func camera()
    {
        pickerr.allowsEditing = false
        pickerr.sourceType = UIImagePickerControllerSourceType.camera
        pickerr.cameraCaptureMode = .photo
        pickerr.modalPresentationStyle = .fullScreen
        present(pickerr,animated: true,completion: nil)
        
    }
    
    func gallery()
    {
        pickerr.allowsEditing = false
        pickerr.sourceType = .photoLibrary
        present(pickerr, animated: true, completion: nil)
    }
    //    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
    //
    //        self.textViewData.resignFirstResponder()
    //
    //    }
    
    //    func ShowDataOffline(){
    //
    //
    //
    //
    //        let empIdVal : String = UserDefaults.standard.string(forKey: "EmployeeID")!
    //
    //
    //
    //
    //
    //        let paragraph = NSMutableParagraphStyle()
    //        paragraph.alignment = .left
    //        paragraph.lineSpacing = 0
    //        let empId : String = UserDefaults.standard.string(forKey: "EmployeeID")!
    //        var startString : String = "You "
    //        if(empId != employeeId){
    //            startString =  employeeName + "-" +   String(employeeId) + " "
    //        }
    //        let  mBuilder = startString + "Submitted hazard of type " + Hazardname + " for Area-" +  AreaName +
    //            ", SubArea-" + SubAreaName
    //        print(mBuilder)
    //        let agreeAttributedString = NSMutableAttributedString(string: mBuilder, attributes: [NSAttributedStringKey.foregroundColor: UIColor.black, NSAttributedStringKey.font: UIFont.systemFont(ofSize: CGFloat(15.0))])
    //
    //
    //        //Submitted
    //        let SubmittedAttributedString = NSAttributedString(string:"Submitted hazard of type ", attributes: [NSAttributedStringKey.foregroundColor: UIColor.gray, NSAttributedStringKey.font: UIFont.systemFont(ofSize: 15.0)])
    //
    //        let range: NSRange = (agreeAttributedString.string as NSString).range(of: "Submitted hazard of type ")
    //        if range.location != NSNotFound {
    //            agreeAttributedString.replaceCharacters(in: range, with: SubmittedAttributedString)
    //        }
    //
    //        //for
    //        let forAttributedString = NSAttributedString(string:"for Area-", attributes: [NSAttributedStringKey.foregroundColor: UIColor.gray, NSAttributedStringKey.font: UIFont.systemFont(ofSize: 15.0)])
    //
    //        let forRange: NSRange = (agreeAttributedString.string as NSString).range(of: "for Area-")
    //        if forRange.location != NSNotFound {
    //            agreeAttributedString.replaceCharacters(in: forRange, with: forAttributedString)
    //        }
    //        let subAreaAttributedString = NSAttributedString(string:"SubArea-", attributes: [NSAttributedStringKey.foregroundColor: UIColor.gray, NSAttributedStringKey.font: UIFont.systemFont(ofSize: 15.0)])
    //        let subAreaRange: NSRange = (agreeAttributedString.string as NSString).range(of: "SubArea-")
    //        if subAreaRange.location != NSNotFound {
    //            agreeAttributedString.replaceCharacters(in: subAreaRange, with: subAreaAttributedString)
    //        }
    //
    //        agreeAttributedString.addAttribute(NSAttributedStringKey.paragraphStyle, value: paragraph, range: NSRange(location: 0, length: agreeAttributedString.length))
    //
    //        print(agreeAttributedString)
    //        textView.attributedText = agreeAttributedString
    //        lblId.text = "ID #" + hazardID
    //
    //
    //
    //        print(textView.contentSize.height)
    //      //  textViewHeight.constant =  textView.contentSize.height + 150
    //        let paragraph1 = NSMutableParagraphStyle()
    //        paragraph1.alignment = .left
    //        paragraph1.lineSpacing = 0
    //        let titleStr = NSMutableAttributedString(string: desc, attributes: [NSAttributedStringKey.foregroundColor: UIColor.black, NSAttributedStringKey.font: UIFont.systemFont(ofSize: CGFloat(15.0))])
    //        titleStr.addAttribute(NSAttributedStringKey.paragraphStyle, value: paragraph1, range: NSRange(location: 0, length: titleStr.length))
    //        textViewDesc.attributedText = titleStr
    //        textViewDesc.textContainer.maximumNumberOfLines = 3
    //        textViewDesc.textContainer.lineBreakMode = .byTruncatingTail
    //        textViewDesc.textColor = UIColor.colorwithHexString("295890", alpha: 1.0)
    //        lblStatus.text = status
    //        switch lblStatus.text! {
    //        case "Close":
    //            lblStatus.textColor = UIColor.colorwithHexString("#5c9834", alpha: 1)
    //            break;
    //        case "Pending":
    //            lblStatus.textColor = UIColor.colorwithHexString("#cfcf00", alpha: 1)
    //            break;
    //        case "Reject":
    //            lblStatus.textColor = UIColor.colorwithHexString("#FF0000", alpha: 1)
    //            break;
    //        default:
    //            break;
    //        }
    ////        if let url = NSURL(string: imgdata) {
    ////
    ////            imgView.sd_setImage(with: url as URL!, placeholderImage: UIImage.init(named: "placed"))
    ////        }
    //        let dateFormatter = DateFormatter()
    //        dateFormatter.timeZone = NSTimeZone.system
    //        dateFormatter.dateFormat = "MMM dd yyyy HH:mm:ss:SSSa"
    //        let date = dateFormatter.date(from: dateVal)
    //
    //        let dateFormatter2 = DateFormatter()
    //        dateFormatter2.timeZone = NSTimeZone.system
    //        dateFormatter2.dateFormat = "dd MMM yyyy"
    //
    //        switch date {
    //        case nil:
    //            let date_TimeStr = dateFormatter2.string(from: Date())
    //            lbldate.text = date_TimeStr
    //            break;
    //        default:
    //            let date_TimeStr = dateFormatter2.string(from: date!)
    //
    //            lbldate.text = date_TimeStr
    //            break;
    //        }
    //        let layoutManager:NSLayoutManager = textViewDesc.layoutManager
    //        let numberOfGlyphs = layoutManager.numberOfGlyphs
    //        var numberOfLines = 0
    //        var index = 0
    //        var lineRange:NSRange = NSRange()
    //
    //        while (index < numberOfGlyphs) {
    //            layoutManager.lineFragmentRect(forGlyphAt: index, effectiveRange: &lineRange)
    //            index = NSMaxRange(lineRange);
    //            numberOfLines = numberOfLines + 1
    //        }
    //
    //        let layoutManager1:NSLayoutManager = textView.layoutManager
    //        let numberOfGlyphs1 = layoutManager1.numberOfGlyphs
    //        var numberOfLines1 = 0
    //        var index1 = 0
    //        var lineRange1:NSRange = NSRange()
    //
    //        while (index1 < numberOfGlyphs1) {
    //            layoutManager1.lineFragmentRect(forGlyphAt: index1, effectiveRange: &lineRange1)
    //            index1 = NSMaxRange(lineRange1);
    //            numberOfLines1 = numberOfLines1 + 1
    //        }
    //
    //        print(numberOfLines)
    //        var v1 = 0
    //        var v2 = 0
    //        let urlString = self.imgdata
    //        let urlShow = urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
    //        if let url = NSURL(string: urlShow!) {
    //            imgView.sd_setImage(with: url as URL?, placeholderImage: UIImage.init(named: "placed"))
    //            if(numberOfLines == 1 || numberOfLines == 0) {
    //                v1 = 30
    //            }  else if(numberOfLines == 2) {
    //                v1 = 40
    //            } else {
    //                v1 = 50
    //            }
    //            if(numberOfLines1 == 1 || numberOfLines1 == 0) {
    //                v2 = 40
    //            }  else if(numberOfLines1 == 2) {
    //                v2 = 60
    //            }else if(numberOfLines1 == 3) {
    //                v2 = 75
    //            }
    //            else {
    //                v2 = 85
    //            }
    //
    //            heightImage.constant = CGFloat(v1 + v2)
    //            print(heightImage.constant)
    //            print(url)
    //        } else {
    //            imgView.image =  UIImage(named: "placed")
    //
    //            if(numberOfLines == 1 || numberOfLines == 0) {
    //                v1 = 30
    //            }  else if(numberOfLines == 2) {
    //                v1 = 40
    //            } else {
    //                v1 = 50
    //            }
    //            if(numberOfLines1 == 1 || numberOfLines1 == 0) {
    //                v2 = 40
    //            }  else if(numberOfLines1 == 2) {
    //                v2 = 60
    //            }else if(numberOfLines1 == 3) {
    //                v2 = 75
    //            }
    //            else {
    //                v2 = 85
    //            }
    //            heightImage.constant = CGFloat(v1 + v2)
    //            print(heightImage.constant)
    //        }
    //        image1Left.constant = self.view.frame.width/3
    //        image2Left.constant = self.view.frame.width/3
    //        //   cell.heightImage.constant = cell.statusTextView.contentSize.height + cell.titleTextView.contentSize.height
    //
    //        let cal : Int = Int(self.riskLevel)!
    //
    //        if(cal <= 4 ) {
    //
    //            viewRiskLevel.backgroundColor = UIColor.colorwithHexString("87ceeb", alpha: 1.0)
    //        } else if(cal <= 9 && cal > 4) {
    //
    //            viewRiskLevel.backgroundColor = UIColor.colorwithHexString("CCCC00", alpha: 1.0)
    //        } else if(cal <= 16 && cal > 9 ) {
    //
    //           viewRiskLevel.backgroundColor = UIColor.colorwithHexString("ffb6c1", alpha: 1.0)
    //        } else if(cal <= 25 && cal > 16 ) {
    //
    //            viewRiskLevel.backgroundColor = UIColor.colorwithHexString("FF0000", alpha: 1.0)
    //        }
    //
    //        if(self.status == "Close") {
    //
    //
    //
    //            textViewData.isHidden = true
    //            processBTN.isHidden = true
    //            cameraBtn.isHidden = true
    //            lblReason.isHidden = true
    //            lblAttachPhoto.isHidden = true
    //            CloseBtn.isHidden = true
    //           imgViewDrop.isHidden = true
    //
    //        }
    //
    //    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @IBAction func radioButtonClicked(_ sender : DLRadioButton){
        for button in sender.selectedButtons() {
            self.statusValue = (button.titleLabel!.text!)
            print(String(format: "%@ is selected.\n", self.statusValue));
            
        }
    }
    var cellClose : ReportedDataTableViewCell!
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
extension CloseViewController : UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if(section == 0) {
            return 10.0
        } else {
            return 5.0
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if(indexPath.section == 0){
            return UITableViewAutomaticDimension
        }else{
            return 475
        }
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    
}

extension CloseViewController : UITableViewDataSource {
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if(indexPath.section == 0){
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! ReportedDataTableViewCell
            
            cellClose = cell
            
         
            cell.statusTextView.tag = indexPath.section
            
            
            cell.statusOfHazard.text = status
            cell.idOfHazard.text =  "ID #" + hazardID
            
            let paragraph1 = NSMutableParagraphStyle()
            paragraph1.alignment = .left
            paragraph1.lineSpacing = 0
            let titleStr = NSMutableAttributedString(string: desc, attributes: [NSAttributedStringKey.foregroundColor: UIColor.black, NSAttributedStringKey.font: UIFont.systemFont(ofSize: CGFloat(15.0))])
            titleStr.addAttribute(NSAttributedStringKey.paragraphStyle, value: paragraph1, range: NSRange(location: 0, length: titleStr.length))
            cell.titleTextView.attributedText = titleStr
            
            
            switch cell.statusOfHazard.text! {
            case "Close":
                cell.statusOfHazard.textColor = UIColor.colorwithHexString("#5c9834", alpha: 1)
                
                break;
            case "Pending":
                cell.statusOfHazard.textColor = UIColor.colorwithHexString("#cfcf00", alpha: 1)
                break;
            case "Reject":
                cell.statusOfHazard.textColor = UIColor.colorwithHexString("#FF0000", alpha: 1)
                break;
            default:
                break;
            }
            
            
            let dateFormatter = DateFormatter()
            dateFormatter.timeZone = NSTimeZone.system
            dateFormatter.dateFormat = "MMM dd yyyy HH:mm:ss:SSSa"
            let date = dateFormatter.date(from: dateVal)
            
            let dateFormatter2 = DateFormatter()
            dateFormatter2.timeZone = NSTimeZone.system
            dateFormatter2.dateFormat = "dd MMM yyyy"
            
            switch date {
            case nil:
                let date_TimeStr = dateFormatter2.string(from: Date())
                cell.dateLabel.text = date_TimeStr
                break;
            default:
                let date_TimeStr = dateFormatter2.string(from: date!)
                
                cell.dateLabel.text = date_TimeStr
                break;
            }
            
            
            
            //  cell.statusTextView.delegate = self
            
            
            // cell.titleTextView.text = self.hazardDB[indexPath.section].descriptionn
            cell.titleTextView.font = UIFont.systemFont(ofSize: 15.0)
            let paragraph = NSMutableParagraphStyle()
            paragraph.alignment = .left
            paragraph.lineSpacing = 0
            let empId : String = UserDefaults.standard.string(forKey: "EmployeeID")!
            var startString : String = "You "
            
            if(empId != employeeId){
                startString =  employeeName + "-" +   String(employeeId) + " "
            }
            let  mBuilder = startString  + "submitted hazard of type " + Hazardname + " for location " +  ZoneName + " area " + UnitName +
                " subarea " + AreaName + " department " +  SubAreaName
            
            let agreeAttributedString = NSMutableAttributedString(string: mBuilder, attributes: [NSAttributedStringKey.foregroundColor: UIColor.black, NSAttributedStringKey.font: UIFont.systemFont(ofSize: CGFloat(15.0))])
            
            
            //Submitted
            let SubmittedAttributedString = NSAttributedString(string:"submitted hazard of type ", attributes: [NSAttributedStringKey.foregroundColor: UIColor.gray, NSAttributedStringKey.font: UIFont.systemFont(ofSize: 15.0)])
            
            let range: NSRange = (agreeAttributedString.string as NSString).range(of: "submitted hazard of type ")
            if range.location != NSNotFound {
                agreeAttributedString.replaceCharacters(in: range, with: SubmittedAttributedString)
            }
            
            //for
            let forAttributedString = NSAttributedString(string:"for location ", attributes: [NSAttributedStringKey.foregroundColor: UIColor.gray, NSAttributedStringKey.font: UIFont.systemFont(ofSize: 15.0)])
            
            let forRange: NSRange = (agreeAttributedString.string as NSString).range(of: "for location ")
            if forRange.location != NSNotFound {
                agreeAttributedString.replaceCharacters(in: forRange, with: forAttributedString)
            }
            
            let areaAttributedString = NSAttributedString(string:"area", attributes: [NSAttributedStringKey.foregroundColor: UIColor.gray, NSAttributedStringKey.font: UIFont.systemFont(ofSize: 15.0)])
            
            let areaRange: NSRange = (agreeAttributedString.string as NSString).range(of: "area")
            if areaRange.location != NSNotFound {
                agreeAttributedString.replaceCharacters(in: areaRange, with: areaAttributedString)
            }
            
            
            
            
            let subAreaAttributedString = NSAttributedString(string:"subarea", attributes: [NSAttributedStringKey.foregroundColor: UIColor.gray, NSAttributedStringKey.font: UIFont.systemFont(ofSize: 15.0)])
            let subAreaRange: NSRange = (agreeAttributedString.string as NSString).range(of: "subarea")
            if subAreaRange.location != NSNotFound {
                agreeAttributedString.replaceCharacters(in: subAreaRange, with: subAreaAttributedString)
            }
            
            let deptAttributedString = NSAttributedString(string:"department", attributes: [NSAttributedStringKey.foregroundColor: UIColor.gray, NSAttributedStringKey.font: UIFont.systemFont(ofSize: 15.0)])
            
            let deptRange: NSRange = (agreeAttributedString.string as NSString).range(of: "department")
            if deptRange.location != NSNotFound {
                agreeAttributedString.replaceCharacters(in: deptRange, with: deptAttributedString)
            }
            
            agreeAttributedString.addAttribute(NSAttributedStringKey.paragraphStyle, value: paragraph, range: NSRange(location: 0, length: agreeAttributedString.length))
            
            
            print(cell.statusTextView.contentSize.height)
            cell.statusTextView.attributedText = agreeAttributedString
            cell.statusTextView.font = UIFont.systemFont(ofSize: 15.0)
            
            
            
            let imageViewDataCell = UIImageView()
            imageViewDataCell.image = UIImage(named : "placed")
            //        let postImageTapGesture = UITapGestureRecognizer(target: self, action: #selector(self.ImageViewTapped(_:)))
            //        if(cell.imageFile != nil) {
            //            cell.imageFile.addGestureRecognizer(postImageTapGesture)
            //            cell.imageFile.isUserInteractionEnabled = true
            //            postImageTapGesture.numberOfTapsRequired = 1
            //        }
            //
            
            
            
            
            
            
            
            cell.statusTextView.isEditable = false
            cell.statusTextView.isSelectable = false
            cell.statusTextView.isScrollEnabled = false
            cell.titleTextView.isEditable = false
            cell.titleTextView.isSelectable = false
            cell.titleTextView.isScrollEnabled = false
            cell.titleTextView.textContainer.maximumNumberOfLines = 3
            cell.titleTextView.textContainer.lineBreakMode = .byTruncatingTail
            cell.titleTextView.textColor = UIColor.colorwithHexString("295890", alpha: 1.0)
            cell.selectionStyle = .none
         
            
         
            let imageView = UIImageView()
            let urlString = self.imgdata
            let urlShow = urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
            if let url = NSURL(string: self.imgdata) {
                
                SDWebImageManager.shared().downloadImage(with: NSURL(string: urlShow!) as URL!, options: .continueInBackground, progress: {
                    (receivedSize :Int, ExpectedSize :Int) in
                    
                }, completed: {
                    (image : UIImage?, error : Error?, cacheType : SDImageCacheType, finished : Bool, url : URL?) in
                    if(image != nil) {
                        let size = CGSize(width: 120, height: 100)
                        let imageCell = self.imageResize(image: image!, sizeChange: size)
                        cell.imageFile.image = imageCell
                        //  cell.viewImageHold.backgroundColor = UIColor(patternImage: imageCell)
                        
                    }else{
                        imageView.image =  UIImage(named: "placed")
                        let size = CGSize(width: 120, height: 100)
                        let imageCell = self.imageResize(image: imageView.image!, sizeChange: size)
                        cell.imageFile.image = imageCell
                        // cell.viewImageHold.backgroundColor = UIColor(patternImage: imageCell)
                    }
                })
            }else{
                imageView.image =  UIImage(named: "placed")
                let size = CGSize(width: 120, height: 100)
                let imageCell = imageResize(image: imageView.image!, sizeChange: size)
                cell.imageFile.image = imageCell
                //cell.viewImageHold.backgroundColor = UIColor(patternImage: imageCell)
            }
            
            //   cell.heightImage.constant = cell.statusTextView.contentSize.height + cell.titleTextView.contentSize.height
            if(riskLevel == ""){
                riskLevel = "0"
            }
            let cal : Int = Int(riskLevel)!
            
            if(cal == 0 ) {
                
                cell.viewRiskLevel.backgroundColor = UIColor.clear
            }else if(cal <= 4 && cal > 0) {
                
                cell.viewRiskLevel.backgroundColor = UIColor.colorwithHexString("87ceeb", alpha: 1.0)
            } else if(cal <= 9 && cal > 4) {
                
                cell.viewRiskLevel.backgroundColor = UIColor.colorwithHexString("CCCC00", alpha: 1.0)
            } else if(cal <= 16 && cal > 9 ) {
                
                cell.viewRiskLevel.backgroundColor = UIColor.colorwithHexString("ffa500", alpha: 1.0)
            } else if(cal <= 25 && cal > 16 ) {
                
                cell.viewRiskLevel.backgroundColor = UIColor.colorwithHexString("FF0000", alpha: 1.0)
            }
            
            //   cell.heightImage.constant = cell.heightRiskLevel.constant
            
            
            cell.separatorInset = UIEdgeInsetsMake(0, 10000, 0, 0);
            
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "process", for: indexPath) as! CloseTableViewCell
            cell.separatorInset = UIEdgeInsetsMake(0, 10000, 0, 0);
             cell.isHidden = true
            switch cellClose.statusOfHazard.text! {
            case "Close":
               cell.isHidden = true
                
                break;
           
            default:
                cell.isHidden = false
                break;
            }
            
            cellData = cell
            return cell
        }
        
        
        
        
        
        
        
        
    }
}



