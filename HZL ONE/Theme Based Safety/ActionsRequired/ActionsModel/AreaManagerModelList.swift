//
//  AreaManagerModelList.swift
//  VallSafety
//
//  Created by SARVANG INFOTCH on 14/10/18.
//  Copyright © 2018 Bunga Maheshwar Rao. All rights reserved.
//

import Foundation
import Reachability

class AreaManagersAssignListDataModel: NSObject {
    
    var name = String()
    var mobileNo = String()
    var email = String()
    var empId = Double()
    var user_type = String()
    func setDataInModel(str:[String:AnyObject])
    {
      
        if str["Employee_Name"] is NSNull || str["Employee_Name"] == nil{
            self.name = ""
        }else{
            self.name = (str["Employee_Name"] as? String)!
        }
        if str["Mobile_Number"] is NSNull || str["Mobile_Number"] == nil{
            self.mobileNo = ""
        }else{
            self.mobileNo = (str["Mobile_Number"] as? String)!
        }
        if str["Employee_ID"] is NSNull || str["Employee_ID"] == nil{
            self.empId = 0
        }else{
            self.empId = (str["Employee_ID"] as! NSString).doubleValue
        }
        if str["Email_ID"] is NSNull || str["Email_ID"] == nil{
            self.email = ""
        }else{
            self.email = (str["Email_ID"] as? String)!
        }
        if str["User_Type"] is NSNull || str["User_Type"] == nil{
            self.user_type = ""
        }else{
            self.user_type = (str["User_Type"] as? String)!
        }
        
    }
}

class AreaManagersAssignListDataAPI
{
    
    var reachablty = Reachability()!
    
    
    func serviceCalling(obj:AssignSavedViewController, name:String, success:@escaping (AnyObject)-> Void)
    {
        
        obj.stopLoading()
        
        if(reachablty.connection != .none)
        {
            
            WebServices.sharedInstances.sendPostRequest(url: URLConstants.Area_Managers_List, parameters: ["Name":name], successHandler: { (dict) in
                
                if let response = dict["response"]{
                    
                    let statusString : String = response["status"] as! String
                    
                    if(statusString == "success")
                    {
                        
                        obj.tableView.isHidden = false
                        obj.noDataLabel(text: "" )
                        
                        if let data = dict["data"] as? [AnyObject]
                        {
                            
                            var dataArray : [AreaManagersListDataModel] = []
                            
                            for i in 0..<data.count
                            {
                                
                                if let cell = data[i] as? [String:AnyObject]
                                {
                                    let object = AreaManagersListDataModel()
                                    object.setDataInModel(str: cell)
                                    dataArray.append(object)
                                }
                                
                            }
                            
                            success(dataArray as AnyObject)
                            
                            
                        }
                        
                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                    }
                    else
                    {
                        
                        print("DATA:fail")
                        obj.tableView.isHidden = true
                        obj.noDataLabel(text: "Sorry! No area manager found with name "+name)
                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                        
                    }
                    
                    
                }
                
                
                
            }, failureHandler: { (error) in
                let errorStr : String = error.description
                
                print("DATA:error",errorStr)
                
               // obj.errorChecking(error: error)
                obj.stopLoading()
            })
            
            
        }
        else{
            obj.noDataLabel(text:  "No internet connection found. Check your internet connection and try again.")
            obj.tableView.isHidden = true
            obj.refresh.endRefreshing()
            obj.stopLoading()
        }
        
        
    }
    
    
}






