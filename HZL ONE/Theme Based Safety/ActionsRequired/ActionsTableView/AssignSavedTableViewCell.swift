//
//  AssignSavedTableViewCell.swift
//  VallSafety
//
//  Created by SARVANG INFOTCH on 14/10/18.
//  Copyright © 2018 Bunga Maheshwar Rao. All rights reserved.
//

import UIKit

class AssignSavedTableViewCell: UITableViewCell {
    @IBOutlet weak var employeeNameLabel: UILabel!
    @IBOutlet weak var employeeMobileNoLabel: UILabel!
    @IBOutlet weak var employeeEmaillabel: UILabel!
    
    @IBOutlet weak var clearBtn: UIButton!
    @IBOutlet weak var areaView: UIViewX!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
