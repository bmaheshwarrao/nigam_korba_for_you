
//
//  DataHazardListModel.swift
//  VallSafety
//
//  Created by SARVANG INFOTCH on 13/10/18.
//  Copyright © 2018 Bunga Maheshwar Rao. All rights reserved.
//

import Foundation
import Reachability

class ReportedHazardStatus: NSObject {
    
   
 
    
    var date_Time: String?
    
    var Name : String?
 var areaId : String?
    var area_Name : String?
    var subareaId : String?
    var subarea_Name : String?
    var ID  = Int()
  
    var hazard_Type_ID : String?
    var hazard_Type_Name : String?
    var descriptionn : String?
    var image_path : String?
    var risk_level : Int?
    var likelihood : Int?
    var Consequence : Int?
    var Employee_ID : String?
    var Employee_Name : String?
    var status : String?
   
    var Unit_ID : String?
    var Zone_ID : String?
    var Unit_Name : String?
    var Zone_Name : String?
    //    var Sbu_Department : String?
    //  var location_ID  : Int?
    // var sub_location_ID : Int?
    //   var Sub_location_Name : String?
    //  var main_location : String?
    // var business : String?
    //var main_location_ID : Int?
    
    func setDataInModel(cell:[String:AnyObject])
    {
        
        if cell["Date_Time"] is NSNull || cell["Date_Time"] == nil{
            self.date_Time = ""
        }else{
            let emp1 = cell["Date_Time"]
            
            self.date_Time = (emp1?.description)!
        }
        if cell["Employee_Name"] is NSNull || cell["Employee_Name"] == nil{
            self.Employee_Name = ""
        }else{
            let emp1 = cell["Employee_Name"]
            
            self.Employee_Name = (emp1?.description)!
        }
        
        if cell["Employee_ID"] is NSNull || cell["Employee_ID"] == nil{
            self.Employee_ID = ""
        }else{
            let emp1 = cell["Employee_ID"]
            
            self.Employee_ID = (emp1?.description)!
        }
        if cell["Area_ID"] is NSNull || cell["Area_ID"] == nil{
            self.areaId = "0"
        }else{
            let emp1 = cell["Area_ID"]
            
            self.areaId = (emp1?.description)!
        }
        if cell["Area_Name"] is NSNull || cell["Area_Name"] == nil{
            self.area_Name = ""
        }else{
            let emp1 = cell["Area_Name"]
            
            self.area_Name = (emp1?.description)!
        }
        if cell["Employee_Name"] is NSNull || cell["Employee_Name"] == nil{
            self.Employee_Name = ""
        }else{
            let emp1 = cell["Employee_Name"]
            
            self.Employee_Name = (emp1?.description)!
        }
        
        if cell["Sub_Area_ID"] is NSNull || cell["Sub_Area_ID"] == nil{
            self.subareaId = "0"
        }else{
            let emp1 = cell["Sub_Area_ID"]
            
            self.subareaId = (emp1?.description)!
        }
       
        if cell["SubArea_Name"] is NSNull || cell["SubArea_Name"] == nil{
            self.subarea_Name = ""
        }else{
            let emp1 = cell["SubArea_Name"]
            
            self.subarea_Name = (emp1?.description)!
        }
        if cell["Unit_ID"] is NSNull || cell["Unit_ID"] == nil{
            self.Unit_ID = "0"
        }else{
            let emp1 = cell["Unit_ID"]
            
            self.Unit_ID = (emp1?.description)!
        }
        
        if cell["Unit_Name"] is NSNull || cell["Unit_Name"] == nil{
            self.Unit_Name = "0"
        }else{
            let emp1 = cell["Unit_Name"]
            
            self.Unit_Name = (emp1?.description)!
        }
       
        if cell["Zone_ID"] is NSNull || cell["Zone_ID"] == nil{
            self.Zone_ID = "0"
        }else{
            let emp1 = cell["Zone_ID"]
            
            self.Zone_ID = (emp1?.description)!
        }
        
        if cell["Zone_Name"] is NSNull || cell["Zone_Name"] == nil{
            self.Zone_Name = ""
        }else{
            let emp1 = cell["Zone_Name"]
            
            self.Zone_Name = (emp1?.description)!
        }
      
        
        if cell["Consequence"] is NSNull || cell["Consequence"] == nil{
            self.Consequence = 0
        }else{
            let emp1 = cell["Zone_Name"]
            let cons = Int((emp1?.description)!)
            self.Consequence = cons
        }
        if cell["Likelihood"] is NSNull || cell["Likelihood"] == nil{
            self.likelihood = 0
        }else{
            let emp1 = cell["Likelihood"]
            let like = Int((emp1?.description)!)
            self.likelihood = like
          
        }
       
        if cell["Risk_Level"] is NSNull || cell["Risk_Level"] == nil{
            self.risk_level = 0
        }else{
            let emp1 = cell["Risk_Level"]
            let risk = Int((emp1?.description)!)
            self.risk_level = risk
            
        }
        
        if cell["Status"] is NSNull || cell["Status"] == nil{
            self.status = ""
        }else{
            let emp1 = cell["Status"]
            
            self.status = (emp1?.description)!
        }
        
        if cell["Employee_Name"] is NSNull || cell["Employee_Name"] == nil{
            self.Name = ""
        }else{
            let emp1 = cell["Employee_Name"]
            
            self.Name = (emp1?.description)!
        }
        if cell["ID"] is NSNull || cell["ID"] == nil{
            self.ID = 0
        }else{
            let emp1 = cell["ID"]
            let id = Int((emp1?.description)!)
            self.ID = id!
            
        }
      
        if cell["Hazard_Type_ID"] is NSNull || cell["Hazard_Type_ID"] == nil{
            self.hazard_Type_ID = "0"
        }else{
            let emp1 = cell["Hazard_Type_ID"]
         
            self.hazard_Type_ID = (emp1?.description)!
            
        }
        if cell["Hazard_Name"] is NSNull || cell["Hazard_Name"] == nil{
            self.hazard_Type_Name = ""
        }else{
            let emp1 = cell["Hazard_Name"]
            
            self.hazard_Type_Name = (emp1?.description)!
            
        }
        if cell["Description"] is NSNull || cell["Description"] == nil{
            self.descriptionn = ""
        }else{
            let emp1 = cell["Description"]
            
            self.descriptionn = (emp1?.description)!
            
        }
        if cell["Image_path"] is NSNull || cell["Image_path"] == nil{
            self.image_path = ""
        }else{
            let emp1 = cell["Image_path"]
            
            self.image_path = (emp1?.description)!
            
        }
       
       
        
    }
}


class ReportedHazardForStatus
{
    
    var reachablty = Reachability()!
    
    
    func serviceCalling(obj:ReportedHazardViewController,parameter:[String:String],status:String,statusData:String  , url : String , success:@escaping (AnyObject)-> Void)
    {
        
        obj.startLoading(view: obj.tableViewReported)
        
        if(reachablty.connection != .none)
        {
    
            WebServices.sharedInstances.sendPostRequest(url: url, parameters: parameter, successHandler: { (dict) in
                
                if let response = dict["response"]{
                    print(url)
                    print(parameter)
                    let statusString : String = response["status"] as! String
                    let objectmsg = MessageCallServerModel()
            let msg = objectmsg.sealizeMessage(cell: response as! [String : AnyObject])
                    print(response)
                    if(statusString == "success")
                    {
                        
                        obj.tableViewReported.isHidden = false
                        obj.noDataLabel(text: "" )
                        obj.label.isHidden = true
                         obj.stopLoading(view: obj.tableViewReported)
                        if let data = dict["data"] as? [AnyObject]
                        {
                            
                            var dataArray : [ReportedHazardStatus] = []
                            
                            for i in 0..<data.count
                            {
                                
                                if let celll = data[i] as? [String:AnyObject]
                                {
                                    let object = ReportedHazardStatus()
                                    object.setDataInModel(cell: celll)
                                    dataArray.append(object)
                                }
                                
                            }
                            
                            success(dataArray as AnyObject)
                            
                            
                        }
                        
                        obj.refresh.endRefreshing()
                        obj.stopLoading(view: obj.tableViewReported)
                    }
                    else
                    {
                        
                        
                        print("DATA:fail")
                        obj.tableViewReported.isHidden = true
                        obj.label.isHidden = false
                        obj.noDataLabel(text: msg)
                        obj.refresh.endRefreshing()
                        obj.stopLoading(view: obj.tableViewReported)
                        //  obj.stopLoading()
                        
                    }
                    
                    
                }
                
                
                
            }, failureHandler: { (error) in
                let errorStr : String = error.description
                obj.label.isHidden = true
                print("DATA:error",errorStr)
                
                //obj.errorChecking(error: error)
                //  obj.stopLoading()
            })
            
            
        }
        else{
            obj.label.isHidden = false
            obj.noDataLabel(text:  "No internet connection found. Check your internet connection and try again.")
            obj.tableViewReported.isHidden = true
            obj.refresh.endRefreshing()
            // obj.stopLoading()
        }
        
        
    }
    
    
}


class ReportedHazardForStatusLoadMore
{
    
    var reachablty = Reachability()!
    
    
    func serviceCalling(obj:ReportedHazardViewController,parameter:[String:String] , url : String, success:@escaping (AnyObject)-> Void)
    {
        
        obj.startLoading()
        
        if(reachablty.connection != .none)
        {
            
            WebServices.sharedInstances.sendPostRequest(url: url, parameters: parameter, successHandler: { (dict) in
                
                if let response = dict["response"]{
                    
                    let statusString : String = response["status"] as! String
                    
                    if(statusString == "success")
                    {
                        
                        //obj.tableView.isHidden = false
                        //obj.noDataLabel(text: "" )
                        
                        if let data = dict["data"] as? [AnyObject]
                        {
                            
                            var dataArray : [ReportedHazardStatus] = []
                            
                            for i in 0..<data.count
                            {
                                
                                if let celll = data[i] as? [String:AnyObject]
                                {
                                    let object = ReportedHazardStatus()
                                    object.setDataInModel(cell: celll)
                                    dataArray.append(object)
                                }
                                
                            }
                            
                            success(dataArray as AnyObject)
                            
                            
                        }
                        
                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                    }
                    else
                    {
                        
                        
                        print("DATA:fail")
                        //obj.tableView.isHidden = true
                        //obj.noDataLabel(text: "No "+status+" hazard found" )
                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                        
                    }
                    
                    
                }
                
                
                
            }, failureHandler: { (error) in
                let errorStr : String = error.description
                
                print("DATA:error",errorStr)
                
                //obj.errorChecking(error: error)
                obj.stopLoading()
            })
            
            
        }
        else{
            //obj.noDataLabel(text:  "No internet connection found. Check your internet connection and try again.")
            //obj.tableView.isHidden = true
            obj.refresh.endRefreshing()
            obj.stopLoading()
        }
        
        
    }
    
    
}

