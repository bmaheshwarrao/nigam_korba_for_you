//
//  ModelThemeASUZ.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 12/02/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import Foundation
class ThemeBasedListApi
{
    var reachablty = Reachability()!
    func serviceCalling(obj:ThemeBasedASUZViewController, success:@escaping (AnyObject)-> Void)
    {
        
        obj.startLoading(view: obj.tableViewASUZ)
        var statusString = String()
        
        if(reachablty.connection != .none)
        {
            
            var urlString = String()
            var keyString = String()
           
            if(FilterDataFromServer.filterType == "Location") {
                urlString = Base_Url_TBS+"iProfile/Location_Zone/"
                keyString = "Zone_Name"
                
            } else if(FilterDataFromServer.filterType == "Unit"){
                
             
                urlString = Base_Url_TBS+"iProfile/Location_Unit/?Zone_ID=\(FilterDataFromServer.location_id)"
                keyString = "Unit_Name"
            } else if(FilterDataFromServer.filterType == "Area"){
                
                
                urlString = Base_Url_TBS+"iProfile/Location_Area/?unit_ID=\(FilterDataFromServer.unit_Id)"
                keyString = "Area_Name"
            } else if(FilterDataFromServer.filterType == "Sub Area"){
                
               
                urlString = Base_Url_TBS+"iProfile/Location_Sub_Area/?Area_ID=\(FilterDataFromServer.area_Id)"
                keyString = "SubArea_Name"
            }
            else if(FilterDataFromServer.filterType == "Theme"){
                
                
                urlString = Base_Url_TBS+"Hazard/Theme_List/"
                keyString = "ThemeName"
            }
            else if(FilterDataFromServer.filterType == "Hazard"){
                

                urlString = Base_Url_TBS+"Hazard/HazardType_List/"
                keyString = "Hazard_Name"
            }
          
            print(urlString)
            WebServices.sharedInstances.sendGetRequest(Url: urlString, successHandler: { (dict) in
                
                
                if let response = dict["response"]{
                    
                    statusString  = response["status"] as! String
                    let objectmsg = MessageCallServerModel()
                    
                    let msg = objectmsg.sealizeMessage(cell: response as! [String : AnyObject])
                    if(statusString == "success")
                    {
                        
                        obj.tableViewASUZ.isHidden = false
                        obj.noDataLabel(text: "" )
                        obj.label.isHidden = true
                        
                        if let data = dict["data"] as? [AnyObject]
                        {
                            
                            var dataArray : [DataListModel] = []
                            
                            for i in 0..<data.count
                            {
                                if let cell = data[i] as? [String:AnyObject]
                                {
                                    let object = DataListModel()
                                    
                                    if cell["ID"] is NSNull{
                                        object.id = ""
                                    }else{
                                        let idd : Int = (cell["ID"] as? Int)!
                                        object.id = String(idd)
                                    }
                                    
                                    if cell[keyString] is NSNull{
                                        object.name = ""
                                    }else{
                                        object.name = (cell[keyString] as? String)!
                                    }
                                    
                                    
                                    dataArray.append(object)
                                    
                                }
                            }
                            
                            obj.tableViewASUZ.reloadData()
                            success(dataArray as AnyObject)
                            
                            
                        }
                        
                        obj.refresh.endRefreshing()
                        obj.stopLoading(view: obj.tableViewASUZ)
                    }
                    else
                    {
                        statusString = "success"
                        obj.tableViewASUZ.reloadData()
                        obj.label.isHidden = false
                        obj.noDataLabel(text: msg )
                        print("DATA:fail")
                        //                        obj.tableView.isHidden = true
                        //                        obj.noDataLabel(text: "Sorry! No area manager found with name "+name)
                        obj.refresh.endRefreshing()
                        obj.stopLoading(view: obj.tableViewASUZ)
                        
                    }
                    
                    
                }
                
                
                
            }, failureHandler: { (error) in
                let errorStr : String = error.description
                statusString = "fail"
                print("DATA:error",errorStr)
                
                //  obj.errorChecking(error: error)
                
            })
            
            
        }
        else{
            obj.noDataLabel(text:  "No internet connection found. Check your internet connection and try again.")
            obj.tableViewASUZ.isHidden = true
            obj.label.isHidden = false
            obj.refresh.endRefreshing()
            obj.stopLoading()
            
            obj.tableViewASUZ.reloadData()
        }
        
        
    }
}





class MovieLocationListApi
{
    var reachablty = Reachability()!
    func serviceCalling(obj:MovieLocationViewController, success:@escaping (AnyObject)-> Void)
    {
        
        obj.startLoading(view: obj.tableViewASUZ)
        var statusString = String()
        
        if(reachablty.connection != .none)
        {
            
            var urlString = String()
            var keyString = String()
            
            if(FilterDataFromServer.filterType == "Movie") {
                urlString = Base_Url_News+"Movie_HZL_Location_GET/"
                keyString = "HZL_Location"
                
            }
            
            let parameter : [String: String] = [:]
               WebServices.sharedInstances.sendPostRequest(url: urlString, parameters: parameter, successHandler: { (dict) in
                
                
                if let response = dict["response"]{
                    
                    statusString  = response["status"] as! String
                    let objectmsg = MessageCallServerModel()
            let msg = objectmsg.sealizeMessage(cell: response as! [String : AnyObject])
                    print(response)
                    if(statusString == "success")
                    {
                        
                        obj.tableViewASUZ.isHidden = false
                        obj.noDataLabel(text: "" )
                        obj.label.isHidden = true
                        
                        if let data = dict["data"] as? [AnyObject]
                        {
                            
                            var dataArray : [DataListModel] = []
                            
                            for i in 0..<data.count
                            {
                                if let cell = data[i] as? [String:AnyObject]
                                {
                                    let object = DataListModel()
                                    
//                                    if cell["ID"] is NSNull{
//                                        object.id = ""
//                                    }else{
//                                        let idd : Int = (cell["ID"] as? Int)!
//                                        object.id = String(idd)
//                                    }
                                    
                                    if cell[keyString] is NSNull{
                                        object.name = ""
                                    }else{
                                        object.name = (cell[keyString] as? String)!
                                    }
                                    
                                    
                                    dataArray.append(object)
                                    
                                }
                            }
                            
                            obj.tableViewASUZ.reloadData()
                            success(dataArray as AnyObject)
                            
                            
                        }
                        
                        obj.refresh.endRefreshing()
                        obj.stopLoading(view: obj.tableViewASUZ)
                    }
                    else
                    {
                        statusString = "success"
                        obj.tableViewASUZ.reloadData()
                        obj.label.isHidden = false
                        obj.noDataLabel(text: msg )
                        print("DATA:fail")
                        //                        obj.tableView.isHidden = true
                        //                        obj.noDataLabel(text: "Sorry! No area manager found with name "+name)
                        obj.refresh.endRefreshing()
                        obj.stopLoading(view: obj.tableViewASUZ)
                        
                    }
                    
                    
                }
                
                
                
            }, failureHandler: { (error) in
                let errorStr : String = error.description
                statusString = "fail"
                print("DATA:error",errorStr)
                
                //  obj.errorChecking(error: error)
                
            })
            
            
        }
        else{
            obj.noDataLabel(text:  "No internet connection found. Check your internet connection and try again.")
            obj.tableViewASUZ.isHidden = true
            obj.label.isHidden = false
            obj.refresh.endRefreshing()
            obj.stopLoading()
            
            obj.tableViewASUZ.reloadData()
        }
        
        
    }
}
