//
//  ThemeDashboardViewController.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 12/02/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit
import Charts


import MobileCoreServices
import Alamofire
import CoreData
import CRToast
import IQKeyboardManagerSwift
import AVFoundation


class ThemeDashboardViewController: CommonVSClass,UINavigationBarDelegate,UINavigationControllerDelegate,UITableViewDelegate, UITableViewDataSource,ChartViewDelegate{
    
    
    
    @IBOutlet weak var homeTableView: UITableView!
    
    
    var applicationName = String()
    
    
    var themeStrNav = String()
    
    
    var indPath = IndexPath()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.frame.size.height = self.homeTableView.frame.size.height
        
        self.homeTableView.delegate = self
        self.homeTableView.dataSource = self
        
        self.homeTableView.sectionIndexBackgroundColor = UIColor.clear
        self.homeTableView.sectionIndexColor = UIColor.lightGray
        
        self.homeTableView.sectionIndexColor = UIColor.blue
        
        
        
        
        //        self.homeTableView.addGestureRecognizer(leftToRight)
        //        self.homeTableView.addGestureRecognizer(rightToLeft)
        //        self.view.addGestureRecognizer(leftToRight)
        //        self.view.addGestureRecognizer(rightToLeft)
        
        //        NotificationCenter.default.addObserver(self, selector: #selector(graphDataInsert), name: NSNotification.Name?.init(NSNotification.Name(rawValue: "graphDataUpdate")), object: nil)
        //        NotificationCenter.default.addObserver(self, selector: #selector(GetBusinessData), name: NSNotification.Name?.init(NSNotification.Name(rawValue: "mainLocationDataUpdate")), object: nil)
        //
        //
        //        graphDataInsert()
        
        
        
        
        
        getBannerImageData()
        if reachability.connection == .none{
            self.view.makeToast("Internet is not available, please check your internet connection try again." )
        }
        
        
    }
    
    
    var reachability = Reachability()!
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        // NotificationCenter.default.post(name: NSNotification.Name.init("sendReportFromHome"), object: nil)
        self.title = themeStrNav
        self.tabBarController?.tabBar.isHidden = true
        let nav = self.navigationController?.navigationBar
        nav?.tintColor = UIColor.white
        nav?.tintColor = UIColor.white
         nav?.barTintColor = UIColor(hexString: "2c3e50", alpha: 1.0)
        nav?.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        
        
        
    }
    @objc func getBannerImageData() {
        
        var imageDB = [BannerImage]()
        do {
            
            imageDB = try context.fetch(BannerImage.fetchRequest())
            //self.refresh.endRefreshing()
            if(imageDB.count > 0) {
                //BannerStruct.bannerPath = imageDB[0].text_Message!
            }
            self.homeTableView.reloadData()
            
        } catch {
            print("Fetching Failed")
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == 0{
            return 1
        }else{
            return 1
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 {
            let page = "page"
            
            
            let cell = tableView.dequeueReusableCell(withIdentifier: page) as! PageIdeaTableViewCell
            
            cell.contentView.frame.size.height = cell.imageCollectionView.frame.size.height
            cell.contentView.frame.size.width = cell.imageCollectionView.frame.size.width
            
            
            cell.pageView(ApplicationName: applicationName)
            tableView.isScrollEnabled = false
            return cell
            
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "buttons") as! UITableViewCell
            
            
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        
    
        
        let screenSize = UIScreen.main.bounds
        let screenHeight = screenSize.height - 70
        let screenWidth = screenSize.width
        if indexPath.section == 0{
            return (screenWidth/2) + 25
        }else{
            return screenHeight - ((screenWidth/2) + 25)
        }
        
    }
    
    
    @IBAction func RegisterHazardClicked(_ sender: UIButton) {
        
        FilterDataFromServer.filterType = String()
        FilterDataFromServer.unit_Id = Int()
        FilterDataFromServer.unit_Name = String()
        FilterDataFromServer.area_Id = Int()
        FilterDataFromServer.area_Name = String()
        FilterDataFromServer.sub_Area_Id = Int()
        FilterDataFromServer.sub_Area_Name = String()
        FilterDataFromServer.location_name = String()
        FilterDataFromServer.location_id = Int()
        FilterDataFromServer.theme = String()
        FilterDataFromServer.theme_Id = Int()
        FilterDataFromServer.hazard_Name = String()
        FilterDataFromServer.hazard_Id = Int()
     
        let storyBoard = UIStoryboard(name: "ThemeSafety", bundle: nil)
        let submitVC = storyBoard.instantiateViewController(withIdentifier: "ReportHazardThemeViewController") as! ReportHazardThemeViewController
        
        self.navigationController?.pushViewController(submitVC, animated: true)
        
    }
    @IBAction func btnSelfieClicked(_ sender: UIButton) {
        let storyBoard = UIStoryboard(name: "ThemeSafety", bundle: nil)
        let genVC = storyBoard.instantiateViewController(withIdentifier: "SelfieListViewController") as! SelfieListViewController
        
        self.navigationController?.pushViewController(genVC, animated: true)
    }
    @IBAction func btnLeaderClicked(_ sender: UIButton) {
        let storyBoard = UIStoryboard(name: "ThemeSafety", bundle: nil)
        let genVC = storyBoard.instantiateViewController(withIdentifier: "LeaderShipViewController") as! LeaderShipViewController
        
        self.navigationController?.pushViewController(genVC, animated: true)
    }
    @IBAction func btnScoreClicked(_ sender: UIButton) {
        let storyBoard = UIStoryboard(name: "ThemeSafety", bundle: nil)
        let genVC = storyBoard.instantiateViewController(withIdentifier: "ThemeScoreBoardViewController") as! ThemeScoreBoardViewController
        
        self.navigationController?.pushViewController(genVC, animated: true)
    }
    @IBAction func btnGenerateReportsClicked(_ sender: UIButton) {
         let storyBoard = UIStoryboard(name: "ThemeSafety", bundle: nil)
        let genVC = storyBoard.instantiateViewController(withIdentifier: "GenerateReportFrontViewController") as! GenerateReportFrontViewController
        
        self.navigationController?.pushViewController(genVC, animated: true)
    }
    @IBAction func btnActionsReuiredbyMe(_ sender: UIButton) {
        let storyBoard = UIStoryboard(name: "ThemeSafety", bundle: nil)
        UserDefaults.standard.set(2, forKey: "hazard")
        let displayhazardVC = storyBoard.instantiateViewController(withIdentifier: "DisplayHazardandActionViewController") as! DisplayHazardandActionViewController
        self.navigationController?.title = "Hazard Reported By Me"
        self.navigationController?.pushViewController(displayhazardVC, animated: true)
    }
    @IBAction func btnHazrdReportedbymeClicked(_ sender: UIButton) {
        UserDefaults.standard.set(1, forKey: "hazard")
        
        let displayhazardVC = self.storyboard?.instantiateViewController(withIdentifier: "DisplayHazardandActionViewController") as! DisplayHazardandActionViewController
        self.navigationController?.title = "Hazard Reported By Me"
        self.navigationController?.pushViewController(displayhazardVC, animated: true)
        
    }
    @IBAction func IdeaPointsClicked(_ sender: UIButton) {
        let storyBoard = UIStoryboard(name: "IdeaGeneration", bundle: nil)
        let IdeaClubVC = storyBoard.instantiateViewController(withIdentifier: "IdeaClubPointsViewController") as! IdeaClubPointsViewController
        
        self.navigationController?.pushViewController(IdeaClubVC, animated: true)
    }
    
    @IBAction func myInboxClicked(_ sender: UIButton) {
        let storyBoard = UIStoryboard(name: "IdeaGeneration", bundle: nil)
        UserDefaults.standard.set(1, forKey: "hazard")
        let InboxVC = storyBoard.instantiateViewController(withIdentifier: "InboxDisplayViewController") as! InboxDisplayViewController
        
        self.navigationController?.pushViewController(InboxVC, animated: true)
    }
    
    @IBAction func HODInboxClicked(_ sender: UIButton) {
        let storyBoard = UIStoryboard(name: "IdeaGeneration", bundle: nil)
        UserDefaults.standard.set(2, forKey: "hazard")
        let InboxVC = storyBoard.instantiateViewController(withIdentifier: "InboxDisplayViewController") as! InboxDisplayViewController
        
        self.navigationController?.pushViewController(InboxVC, animated: true)
    }
    
   
    @IBAction func btnHazardReportedbymeClicked(_ sender: UIButton) {
        let storyBoard = UIStoryboard(name: "ThemeSafety", bundle: nil)
        UserDefaults.standard.set(1, forKey: "hazard")
        let displayhazardVC = storyBoard.instantiateViewController(withIdentifier: "DisplayHazardandActionViewController") as! DisplayHazardandActionViewController
        self.navigationController?.title = "Hazard Reported By Me"
        self.navigationController?.pushViewController(displayhazardVC, animated: true)
    }
    
    //Core Data
    
    
    var reachablty = Reachability()!
    
    
    func getContext () -> NSManagedObjectContext {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        return appDelegate.persistentContainer.viewContext
    }
    
    
    
    
    
    
}
