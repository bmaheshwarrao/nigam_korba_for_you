//
//  SelfieListViewController.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 13/02/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit
import SDWebImage
class SelfieListViewController: CommonVSClass ,UITableViewDelegate, UITableViewDataSource{
    
    @IBOutlet weak var tableView: UITableView!
    
    var refreshControl = UIRefreshControl()
    
    var SelfieListDB:[SelfieListDataModel] = []
    var SelfieListAPI = SelfieListDataAPI()
    
    var SelfieListLoadMoreDB : [SelfieListDataModel] = []
    
    
    
    
    var SelfieListLoadMoreAPI = SelfieListDataLoadMoreAPI()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        getSelfieListData()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.estimatedRowHeight = 65
        tableView.rowHeight = UITableViewAutomaticDimension
        
        
        refresh.addTarget(self, action: #selector(getSelfieListData), for: .valueChanged)
        
        self.tableView.addSubview(refresh)
        let swipe = UISwipeGestureRecognizer(target: self, action: #selector(self.swipeRecognizer))
        swipe.direction = ([.down])
        self.view.addGestureRecognizer(swipe)
        
        tableView.tableFooterView = UIView()
        
        
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        if(MoveStruct.isMove == true){
            MoveStruct.isMove = false
            self.view.makeToast(MoveStruct.message)
        }
        self.tabBarController?.tabBar.isHidden = true
        self.title = "Selfie"
        let nav = self.navigationController?.navigationBar
        nav?.tintColor = UIColor.white
       nav?.barTintColor = UIColor(hexString: "2c3e50", alpha: 1.0)
        nav?.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        
    }
    var reachability = Reachability()!
    @objc func swipeRecognizer(_ sender: UISwipeGestureRecognizer) {
        if (sender.direction == .down) {
            
            if(self.reachability.connection != .none)
            {
                
                self.getSelfieListData()
            }
            else
            {
                self.startLoading(view: self.view)
                DispatchQueue.main.asyncAfter(deadline: .now() + 4.5) {
                    self.noDataLabel(text: "No internet connection found. Check your internet connection and try again.")
                    self.stopLoading(view: self.view)
                }
            }
            
        }
    }
    func imageResize (image:UIImage, sizeChange:CGSize)-> UIImage{
        
        let hasAlpha = true
        let scale: CGFloat = 0.0 // Use scale factor of main screen
        
        UIGraphicsBeginImageContextWithOptions(sizeChange, !hasAlpha, scale)
        image.draw(in: CGRect(origin: CGPoint.zero, size: sizeChange))
        
        let scaledImage = UIGraphicsGetImageFromCurrentImageContext()
        return scaledImage!
    }
    var data: String?
    var lastObject: String?
    var indPath = IndexPath()
    @IBOutlet var tapView: UITapGestureRecognizer!
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
  
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        
        
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.SelfieListDB.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! SelfieListTableViewCell
        
     
        if(self.SelfieListDB[indexPath.section].Date_Time != ""){
            let dateFormatter = DateFormatter()
            dateFormatter.timeZone = NSTimeZone.system
            
            dateFormatter.dateFormat = "MMM dd yyyy"
            let date : Date = dateFormatter.date(from: self.SelfieListDB[indexPath.section].Date_Time)!
            dateFormatter.string(from: date)
            let dateFormatter2 = DateFormatter()
            dateFormatter2.timeZone = NSTimeZone.system
            dateFormatter2.dateFormat = "dd MMM yyyy"
            let finalDate = dateFormatter2.string(from: date)
            cell.lblDate.text = finalDate
        }else{
            cell.lblDate.text = ""
        }
        cell.lblemp.text = self.SelfieListDB[indexPath.section].Employees_Name
        cell.textViewData.text = self.SelfieListDB[indexPath.section].ImageCaption
     
        let urlString = self.SelfieListDB[indexPath.section].ImageURL
        let imageView = UIImageView()
        
        let urlShow = urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
        if let url = NSURL(string: self.SelfieListDB[indexPath.section].ImageURL) {
            
            SDWebImageManager.shared().downloadImage(with: NSURL(string: urlShow!) as URL!, options: .continueInBackground, progress: {
                (receivedSize :Int, ExpectedSize :Int) in
                
            }, completed: {
                (image : UIImage?, error : Error?, cacheType : SDImageCacheType, finished : Bool, url : URL?) in
                if(image != nil) {
                    let size = CGSize(width: 500, height: 500)
                    let imageCell = self.imageResize(image: image!, sizeChange: size)
                    cell.imageViewSelfie.image = imageCell
                    //  cell.viewImageHold.backgroundColor = UIColor(patternImage: imageCell)
                    
                }else{
                    imageView.image =  UIImage(named: "placed")
                    let size = CGSize(width: 500, height: 500)
                    let imageCell = self.imageResize(image: imageView.image!, sizeChange: size)
                    cell.imageViewSelfie.image = imageCell
                    // cell.viewImageHold.backgroundColor = UIColor(patternImage: imageCell)
                }
            })
        }else{
            imageView.image =  UIImage(named: "placed")
            let size = CGSize(width: 500, height: 500)
            let imageCell = imageResize(image: imageView.image!, sizeChange: size)
            cell.imageViewSelfie.image = imageCell
            //cell.viewImageHold.backgroundColor = UIColor(patternImage: imageCell)
        }
        
        self.data = String(self.SelfieListDB[indexPath.section].ID)
        self.lastObject = String(self.SelfieListDB[indexPath.section].ID)
        
        if ( self.data ==  self.lastObject && indexPath.section == self.SelfieListDB.count - 1)
        {
            
             self.getSelfieListDataLoadMore( ID: self.SelfieListDB[indexPath.section].ID)
            
        }
        
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableViewAutomaticDimension
        
    }
    
    @objc func getSelfieListData(){
        var param = [String:String]()
        
        param =  [:]
        SelfieListAPI.serviceCalling(obj: self,  parameter: param ) { (dict) in
            
            self.SelfieListDB = dict as! [SelfieListDataModel]
            
            self.tableView.reloadData()
            
        }
    }
    
    @objc func getSelfieListDataLoadMore(ID : String){
        var param = [String:String]()
        
        param =  ["ID":ID]
        SelfieListLoadMoreAPI.serviceCalling(obj: self,  parameter: param ) { (dict) in
            
            self.SelfieListLoadMoreDB =  [SelfieListDataModel]()
            self.SelfieListLoadMoreDB = dict as! [SelfieListDataModel]
            
            switch dict.count {
            case 0:
                break;
            default:
                self.SelfieListDB.append(contentsOf: self.SelfieListLoadMoreDB)
                self.tableView.reloadData()
                break;
            }
            
        }
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
