//
//  LeaderShipViewController.swift
//  sesagoa
//
//  Created by SARVANG INFOTCH on 14/12/18.
//  Copyright © 2018 Bunga Maheshwar Rao. All rights reserved.
//

import UIKit
import SDWebImage
import DLRadioButton
class LeaderShipViewController: CommonVSClass,UITableViewDelegate,UITableViewDataSource {
    var LeaderListAPI = LeadershipData()
    var LeaderListDB:[Leadership] = []
    

    @IBOutlet weak var tableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        getLeaderListData()
        refresh.addTarget(self, action: #selector(getLeaderListData), for: .valueChanged)
        
        self.tableView.addSubview(refresh)
        let swipe = UISwipeGestureRecognizer(target: self, action: #selector(self.swipeRecognizer))
        swipe.direction = ([.down])
        self.view.addGestureRecognizer(swipe)
        
        tableView.tableFooterView = UIView()
        
        
        
    }
    
    var reachability = Reachability()!
    @objc func swipeRecognizer(_ sender: UISwipeGestureRecognizer) {
        if (sender.direction == .down) {
            
            if(self.reachability.connection != .none)
            {
                
                self.getLeaderListData()
            }
            else
            {
                self.startLoading(view: self.view)
                DispatchQueue.main.asyncAfter(deadline: .now() + 4.5) {
                    self.noDataLabel(text: "No internet connection found. Check your internet connection and try again.")
                    self.stopLoading(view: self.view)
                }
            }
            
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        
        self.title = "Leaderboard"
        self.tabBarController?.tabBar.isHidden = true
        let nav = self.navigationController?.navigationBar
        nav?.isTranslucent = false
        nav?.tintColor = UIColor.white
       nav?.barTintColor = UIColor(hexString: "2c3e50", alpha: 1.0)
        nav?.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        //  callMyReportedHazardData()
        
    }
    @objc func getLeaderListData() {
        //let empId : String = UserDefaults.standard.string(forKey: "EmployeeID")!
        var parameter : [String : String ] = [:]
        var  dictWithoutNilValues = [String:String]()
        
        
        dictWithoutNilValues = [:]
        parameter = dictWithoutNilValues.filter { $0.value != ""}
        LeaderListAPI.serviceCalling(obj: self, parameter:parameter ) { (dict) in
            
            self.LeaderListDB = [Leadership]()
            self.LeaderListDB = dict as! [Leadership]
            
            self.tableView.reloadData()
            
            
            
            
        }
    }
   
    var data: String?
    var lastObject: String?
    var indPath = IndexPath()
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.LeaderListDB.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        
        return 1
        
        
    }
    
    var LastSelected : IndexPath?
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! LeadershipTableViewCell
    
      //  cell.lblrewardDesc.text = self.LeaderListDB[indexPath.section].Description
        var sign = String()
        var  pointCr = Int()
        var  pointDr = Int()
        if(self.LeaderListDB[indexPath.section].PointsCr == nil || self.LeaderListDB[indexPath.section].PointsCr == "" || self.LeaderListDB[indexPath.section].PointsCr == "0" ){
            pointCr = 0
        }else {
            pointCr = Int(self.LeaderListDB[indexPath.section].PointsCr)!
            sign = "+ "
        }
        if(self.LeaderListDB[indexPath.section].PointsDr == nil || self.LeaderListDB[indexPath.section].PointsDr == "" || self.LeaderListDB[indexPath.section].PointsDr == "0"  ){
            pointDr = 0
        }else {
            pointDr = Int(self.LeaderListDB[indexPath.section].PointsDr)!
            sign = "- "
        }
        if(pointDr == 0){
             cell.lblPoints.text = sign  + String( pointCr)
        }else{
            cell.lblPoints.text = sign  + String( pointDr)
        }
        
        
        cell.textViewData.text = self.LeaderListDB[indexPath.section].Employee_Name
        
        
        
        return cell
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
