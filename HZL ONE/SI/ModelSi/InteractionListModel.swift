//
//  InteractionListModel.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 25/03/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import Foundation
import SwiftyJSON
class InteractionListReportedDataModel: NSObject {
    
    
    
    var InteractionCode: String?
    var InteractionDetailCode : String?
    var SubAreaId : String?
    var AreaLocation : String?
    var NoOfObservation : String?
    var Category : String?
    var CategoryId : String?
    var SubCategoryId : String?
    var SubCategory : String?
    var SubSubCategoryId : String?
    var SubSubCategory : String?
    
    var RiskPotentialId : String?
    var RiskPotential : String?
    var RiskSituationId : String?
    var RiskSituation : String?
    
    
    
    var ObservationDetails: String?
    var CorrectiveAction : String?
    var ForwardedEmployeeID : String?
    var ForwardedEmployeeName : String?
    var ForwardedCcEmployeeID : String?
    var ForwardedCcEmployeeName : String?
    var ForwardedRemark : String?
    var CanTakeAction : String?
    var CanForward : String?
    var ForwardedDate : String?
    var TargetDate : String?
    
    
    var Sequence : String?
    var Id : String?
    var CreatedBy : String?
    var CreatedOn : String?
    var ModifiedBy : String?
    var ModifiedOn : String?
    var Status : String?
    var UserTransactionCode : String?
    var ModuleId : String?
   
    
  
    
    
    
    func setDataInModel(cell:[String:AnyObject])
    {
       
        if cell["InteractionCode"] is NSNull || cell["InteractionCode"] == nil {
            self.InteractionCode = ""
        }else{
            let app = cell["InteractionCode"]
            self.InteractionCode = (app?.description)!
        }
        if cell["InteractionDetailCode"] is NSNull || cell["InteractionDetailCode"] == nil {
            self.InteractionDetailCode = ""
        }else{
            let app = cell["InteractionDetailCode"]
            self.InteractionDetailCode = (app?.description)!
        }
        if cell["SubAreaId"] is NSNull || cell["SubAreaId"] == nil {
            self.SubAreaId = ""
        }else{
            let app = cell["SubAreaId"]
            self.SubAreaId = (app?.description)!
        }
        
        
        if cell["AreaLocation"] is NSNull || cell["AreaLocation"] == nil {
            self.AreaLocation = ""
        }else{
            let app = cell["AreaLocation"]
            self.AreaLocation = (app?.description)!
        }
        if cell["NoOfObservation"] is NSNull || cell["NoOfObservation"] == nil {
            self.NoOfObservation = ""
        }else{
            let app = cell["NoOfObservation"]
            self.NoOfObservation = (app?.description)!
        }
        if cell["Category"] is NSNull || cell["Category"] == nil {
            self.Category = ""
        }else{
            let app = cell["Category"]
            self.Category = (app?.description)!
        }
        
        if cell["CategoryId"] is NSNull || cell["CategoryId"] == nil {
            self.CategoryId = ""
        }else{
            let app = cell["CategoryId"]
            self.CategoryId = (app?.description)!
        }
        
        if cell["SubCategoryId"] is NSNull || cell["SubCategoryId"] == nil {
            self.SubCategoryId = ""
        }else{
            let app = cell["SubCategoryId"]
            self.SubCategoryId = (app?.description)!
        }
        
        if cell["SubCategory"] is NSNull || cell["SubCategory"] == nil {
            self.SubCategory = ""
        }else{
            let app = cell["SubCategory"]
            self.SubCategory = (app?.description)!
        }
        
        if cell["SubSubCategoryId"] is NSNull || cell["SubSubCategoryId"] == nil {
            self.SubSubCategoryId = ""
        }else{
            let app = cell["SubSubCategoryId"]
            self.SubSubCategoryId = (app?.description)!
        }
        
        if cell["SubSubCategory"] is NSNull || cell["SubSubCategory"] == nil {
            self.SubSubCategory = ""
        }else{
            let app = cell["SubSubCategory"]
            self.SubSubCategory = (app?.description)!
        }
        
      
        if cell["RiskPotentialId"] is NSNull || cell["RiskPotentialId"] == nil {
            self.RiskPotentialId = ""
        }else{
            let app = cell["RiskPotentialId"]
            self.RiskPotentialId = (app?.description)!
        }
        if cell["RiskPotential"] is NSNull || cell["RiskPotential"] == nil {
            self.RiskPotential = ""
        }else{
            let app = cell["RiskPotential"]
            self.RiskPotential = (app?.description)!
        }
        if cell["RiskSituationId"] is NSNull || cell["RiskSituationId"] == nil {
            self.RiskSituationId = ""
        }else{
            let app = cell["RiskSituationId"]
            self.RiskSituationId = (app?.description)!
        }
        if cell["RiskSituation"] is NSNull || cell["RiskSituation"] == nil {
            self.RiskSituation = ""
        }else{
            let app = cell["RiskSituation"]
            self.RiskSituation = (app?.description)!
        }
        if cell["ObservationDetails"] is NSNull || cell["ObservationDetails"] == nil {
            self.ObservationDetails = ""
        }else{
            let app = cell["ObservationDetails"]
            self.ObservationDetails = (app?.description)!
        }
        if cell["CorrectiveAction"] is NSNull || cell["CorrectiveAction"] == nil {
            self.CorrectiveAction = ""
        }else{
            let app = cell["CorrectiveAction"]
            self.CorrectiveAction = (app?.description)!
        }
        if cell["ForwardedEmployeeID"] is NSNull || cell["ForwardedEmployeeID"] == nil {
            self.ForwardedEmployeeID = ""
        }else{
            let app = cell["ForwardedEmployeeID"]
            self.ForwardedEmployeeID = (app?.description)!
        }
        if cell["ForwardedEmployeeName"] is NSNull || cell["ForwardedEmployeeName"] == nil {
            self.ForwardedEmployeeName = ""
        }else{
            let app = cell["ForwardedEmployeeName"]
            self.ForwardedEmployeeName = (app?.description)!
        }
        if cell["ForwardedCcEmployeeID"] is NSNull || cell["ForwardedCcEmployeeID"] == nil {
            self.ForwardedCcEmployeeID = ""
        }else{
            let app = cell["ForwardedCcEmployeeID"]
            self.ForwardedCcEmployeeID = (app?.description)!
        }
        if cell["ForwardedCcEmployeeName"] is NSNull || cell["ForwardedCcEmployeeName"] == nil {
            self.ForwardedCcEmployeeName = ""
        }else{
            let app = cell["ForwardedCcEmployeeName"]
            self.ForwardedCcEmployeeName = (app?.description)!
        }
        if cell["ForwardedRemark"] is NSNull || cell["ForwardedRemark"] == nil {
            self.ForwardedRemark = ""
        }else{
            let app = cell["ForwardedRemark"]
            self.ForwardedRemark = (app?.description)!
        }
        if cell["CanTakeAction"] is NSNull || cell["CanTakeAction"] == nil {
            self.CanTakeAction = ""
        }else{
            let app = cell["CanTakeAction"]
            self.CanTakeAction = (app?.description)!
        }
        if cell["CanForward"] is NSNull || cell["CanForward"] == nil {
            self.CanForward = ""
        }else{
            let app = cell["CanForward"]
            self.CanForward = (app?.description)!
        }
        if cell["ForwardedDate"] is NSNull || cell["ForwardedDate"] == nil {
            self.ForwardedDate = ""
        }else{
            let app = cell["ForwardedDate"]
            self.ForwardedDate = (app?.description)!
        }
        if cell["TargetDate"] is NSNull || cell["TargetDate"] == nil {
            self.TargetDate = ""
        }else{
            let app = cell["TargetDate"]
            self.TargetDate = (app?.description)!
        }
        if cell["Sequence"] is NSNull || cell["Sequence"] == nil {
            self.Sequence = ""
        }else{
            let app = cell["Sequence"]
            self.Sequence = (app?.description)!
        }
        if cell["Id"] is NSNull || cell["Id"] == nil {
            self.Id = ""
        }else{
            let app = cell["Id"]
            self.Id = (app?.description)!
        }
        if cell["CreatedBy"] is NSNull || cell["CreatedBy"] == nil {
            self.CreatedBy = ""
        }else{
            let app = cell["CreatedBy"]
            self.CreatedBy = (app?.description)!
        }
        if cell["CreatedOn"] is NSNull || cell["CreatedOn"] == nil {
            self.CreatedOn = ""
        }else{
            let app = cell["CreatedOn"]
            self.CreatedOn = (app?.description)!
        }
        
        
        if cell["ModifiedBy"] is NSNull || cell["ModifiedBy"] == nil {
            self.ModifiedBy = ""
        }else{
            let app = cell["ModifiedBy"]
            self.ModifiedBy = (app?.description)!
        }
        if cell["ModifiedOn"] is NSNull || cell["ModifiedOn"] == nil {
            self.ModifiedOn = ""
        }else{
            let app = cell["ModifiedOn"]
            self.ModifiedOn = (app?.description)!
        }
        if cell["Status"] is NSNull || cell["Status"] == nil {
            self.Status = ""
        }else{
            let app = cell["Status"]
            self.Status = (app?.description)!
        }
        if cell["UserTransactionCode"] is NSNull || cell["UserTransactionCode"] == nil {
            self.UserTransactionCode = ""
        }else{
            let app = cell["UserTransactionCode"]
            self.UserTransactionCode = (app?.description)!
        }
        if cell["ModuleId"] is NSNull || cell["ModuleId"] == nil {
            self.ModuleId = ""
        }else{
            let app = cell["ModuleId"]
            self.ModuleId = (app?.description)!
        }
        
    }
}

class InteractionCategoryListReportedDataModel: NSObject {
    
   
    var InteractionCode : String?
    var FinancialYearId : String?
    var FinancialYear : String?
    var RequestorCode : String?
    var RequestorName : String?
    var ZoneId : String?
    var Zone : String?
    var UnitId : String?
    var Unit : String?
    var AreaId : String?
    var Area : String?
    var SIDate : String?
    var SIStartTime : String?
    var SIEndTime : String?
    var ShiftID : String?
    var SISchedulerID : String?
    var Shift : String?
    var SISubmitedDate : String?
   
    var InteractionDetailList : [InteractionListReportedDataModel]?
    func setDataInModel(cell:[String:AnyObject])
    {
       
        if cell["InteractionCode"] is NSNull || cell["InteractionCode"] == nil {
            self.InteractionCode = ""
        }else{
            let app = cell["InteractionCode"]
            self.InteractionCode = (app?.description)!
        }
        if cell["FinancialYearId"] is NSNull || cell["FinancialYearId"] == nil {
            self.FinancialYearId = ""
        }else{
            let app = cell["FinancialYearId"]
            self.FinancialYearId = (app?.description)!
        }
        if cell["FinancialYear"] is NSNull || cell["FinancialYear"] == nil {
            self.FinancialYear = ""
        }else{
            let app = cell["FinancialYear"]
            self.FinancialYear = (app?.description)!
        }
        if cell["RequestorCode"] is NSNull || cell["RequestorCode"] == nil {
            self.RequestorCode = ""
        }else{
            let app = cell["RequestorCode"]
            self.RequestorCode = (app?.description)!
        }
        
        if cell["RequestorName"] is NSNull || cell["RequestorName"] == nil {
            self.RequestorName = ""
        }else{
            let app = cell["RequestorName"]
            self.RequestorName = (app?.description)!
        }
        if cell["ZoneId"] is NSNull || cell["ZoneId"] == nil {
            self.ZoneId = ""
        }else{
            let app = cell["ZoneId"]
            self.ZoneId = (app?.description)!
        }
        if cell["Zone"] is NSNull || cell["Zone"] == nil {
            self.Zone = ""
        }else{
            let app = cell["Zone"]
            self.Zone = (app?.description)!
        }
        if cell["UnitId"] is NSNull || cell["UnitId"] == nil {
            self.UnitId = ""
        }else{
            let app = cell["UnitId"]
            self.UnitId = (app?.description)!
        }
        
        if cell["Unit"] is NSNull || cell["Unit"] == nil {
            self.Unit = ""
        }else{
            let app = cell["Unit"]
            self.Unit = (app?.description)!
        }
        
        if cell["AreaId"] is NSNull || cell["AreaId"] == nil {
            self.AreaId = ""
        }else{
            let app = cell["AreaId"]
            self.AreaId = (app?.description)!
        }
        
        if cell["Area"] is NSNull || cell["Area"] == nil {
            self.Area = ""
        }else{
            let app = cell["Area"]
            self.Area = (app?.description)!
        }
        
        if cell["SIDate"] is NSNull || cell["SIDate"] == nil {
            self.SIDate = ""
        }else{
            let app = cell["SIDate"]
            self.SIDate = (app?.description)!
        }
       
        if cell["SIStartTime"] is NSNull || cell["SIStartTime"] == nil {
            self.SIStartTime = ""
        }else{
            let app = cell["SIStartTime"]
            self.SIStartTime = (app?.description)!
        }
        if cell["SIEndTime"] is NSNull || cell["SIEndTime"] == nil {
            self.SIEndTime = ""
        }else{
            let app = cell["SIEndTime"]
            self.SIEndTime = (app?.description)!
        }
        if cell["ShiftID"] is NSNull || cell["ShiftID"] == nil {
            self.ShiftID = ""
        }else{
            let app = cell["ShiftID"]
            self.ShiftID = (app?.description)!
        }
        if cell["SISchedulerID"] is NSNull || cell["SISchedulerID"] == nil {
            self.SISchedulerID = ""
        }else{
            let app = cell["SISchedulerID"]
            self.SISchedulerID = (app?.description)!
        }
        if cell["Shift"] is NSNull || cell["Shift"] == nil {
            self.Shift = ""
        }else{
            let app = cell["Shift"]
            self.Shift = (app?.description)!
        }
        if cell["SISubmitedDate"] is NSNull || cell["SISubmitedDate"] == nil {
            self.SISubmitedDate = ""
        }else{
            let app = cell["SISubmitedDate"]
            self.SISubmitedDate = (app?.description)!
        }
        
        if let data = cell["InteractionDetailList"] as? [AnyObject]
        {
            InteractionDetailList = []
            var dataArray : [InteractionListReportedDataModel] = []
            
            for i in 0..<data.count
            {
                
                if let celll = data[i] as? [String:AnyObject]
                {
                    let object = InteractionListReportedDataModel()
                    object.setDataInModel(cell: celll)
                    dataArray.append(object)
                }
                
            }
            InteractionDetailList = dataArray
            print(InteractionDetailList?.count)
            
            
            
            
            
        }
        
        
    }
}


class IntercationDetailsListApi
{
    var reachablty = Reachability()!
    func serviceCalling(obj:ListInteractionViewController,  param: [String:String] ,success:@escaping (AnyObject)-> Void)
    {
        
      
        
        if(reachablty.connection != .none)
        {
            
            print(param)
            obj.startLoading()
         
          
            WebServices.sharedInstances.sendPostRequest(url: "https://aarohan.hzlmetals.com/siapi/api/Interaction/Get", parameters: param, successHandler: { (dict) in

                if let response = dict["ReponseCode"]{


                    if(response as! Int == 200)
                    {


                        obj.tableView.isHidden = false
                        obj.noDataLabel(text: "" )
                        obj.label.isHidden = true
                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                        if let data = dict["ResponseValue"] as? AnyObject
                        {

                            var dataArray : [InteractionCategoryListReportedDataModel] = []

                            

                                if let celll = data as? [String:AnyObject]
                                {
                                    let object = InteractionCategoryListReportedDataModel()
                                    object.setDataInModel(cell: celll)
                                    dataArray.append(object)
                                }

                            

                            success(dataArray as AnyObject)





                        }


                    }
                    else{
                        obj.label.isHidden = false
                        obj.tableView.isHidden = true
                        obj.noDataLabel(text: "No Data Found!" )
                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                    }


                }



            }, failureHandler: { (error) in
                let errorStr : String = error.description

                print("DATA:error",errorStr)
                obj.refresh.endRefreshing()
                obj.stopLoading()
                //  obj.errorChecking(error: error)

            })
            
            
    
            
        }
        else{
            
            obj.label.isHidden = false
            obj.tableView.isHidden = true
            obj.noDataLabel(text: "Internet is not available, please check your internet connection try again." )
            obj.stopLoading()
        }
    }
}
