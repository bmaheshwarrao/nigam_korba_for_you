//
//  InteractionDetailsModel.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 25/03/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import Foundation





class InteractionDetailsListReportedDataModel: NSObject {
    
    
    var InteractionCode : String?
    var InteractionDetailCode : String?
    var TargetDate : String?
    var Status : String?
    var Category : String?
    var SubCategory : String?
    var RiskPotential : String?
    var RiskSituation : String?
    var ObservationDetails : String?
    var CorrectiveAction : String?
    
    
   
    func setDataInModel(cell:[String:AnyObject])
    {
        
        if cell["InteractionCode"] is NSNull || cell["InteractionCode"] == nil {
            self.InteractionCode = ""
        }else{
            let app = cell["InteractionCode"]
            self.InteractionCode = (app?.description)!
        }
        if cell["InteractionDetailCode"] is NSNull || cell["InteractionDetailCode"] == nil {
            self.InteractionDetailCode = ""
        }else{
            let app = cell["InteractionDetailCode"]
            self.InteractionDetailCode = (app?.description)!
        }
        if cell["TargetDate"] is NSNull || cell["TargetDate"] == nil {
            self.TargetDate = ""
        }else{
            let app = cell["TargetDate"]
            self.TargetDate = (app?.description)!
        }
        if cell["Status"] is NSNull || cell["Status"] == nil {
            self.Status = ""
        }else{
            let app = cell["Status"]
            self.Status = (app?.description)!
        }
        
        if cell["Category"] is NSNull || cell["Category"] == nil {
            self.Category = ""
        }else{
            let app = cell["Category"]
            self.Category = (app?.description)!
        }
        if cell["SubCategory"] is NSNull || cell["SubCategory"] == nil {
            self.SubCategory = ""
        }else{
            let app = cell["SubCategory"]
            self.SubCategory = (app?.description)!
        }
        if cell["RiskPotential"] is NSNull || cell["RiskPotential"] == nil {
            self.RiskPotential = ""
        }else{
            let app = cell["RiskPotential"]
            self.RiskPotential = (app?.description)!
        }
        if cell["RiskSituation"] is NSNull || cell["RiskSituation"] == nil {
            self.RiskSituation = ""
        }else{
            let app = cell["RiskSituation"]
            self.RiskSituation = (app?.description)!
        }
        
        if cell["ObservationDetails"] is NSNull || cell["ObservationDetails"] == nil {
            self.ObservationDetails = ""
        }else{
            let app = cell["ObservationDetails"]
            self.ObservationDetails = (app?.description)!
        }
        
        if cell["CorrectiveAction"] is NSNull || cell["CorrectiveAction"] == nil {
            self.CorrectiveAction = ""
        }else{
            let app = cell["CorrectiveAction"]
            self.CorrectiveAction = (app?.description)!
        }
        
       
        
        
    }
}







class IntercationMyDetailsListApi
{
    var reachablty = Reachability()!
    func serviceCalling(obj:ListDetailsInteractionViewController,  param: [String:String] ,success:@escaping (AnyObject)-> Void)
    {
        
        
        
        if(reachablty.connection != .none)
        {
            
            
            obj.startLoading()
            
            WebServices.sharedInstances.sendPostRequest(url: URLConstants.InteractionsListDetailsInteractionDetails, parameters: param, successHandler: { (dict) in
                
                if let response = dict["ReponseCode"]{
                    
                    
                    if(response as! Int == 200)
                    {
                        
                        
                        obj.tableView.isHidden = false
                        obj.noDataLabel(text: "" )
                        obj.label.isHidden = true
                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                        if let data = dict["ResponseValue"] as? AnyObject
                        {
                            
                            var dataArray : [InteractionDetailsListReportedDataModel] = []
                            
                            
                                
                                if let celll = data["InteractionDetails"] as? [String:AnyObject]
                                {
                                    let object = InteractionDetailsListReportedDataModel()
                                    object.setDataInModel(cell: celll)
                                    dataArray.append(object)
                                }
                                
                            
                            
                            success(dataArray as AnyObject)
                            
                            
                            
                            
                            
                        }
                        
                        
                    }
                    else{
                        obj.label.isHidden = false
                        obj.tableView.isHidden = true
                        obj.noDataLabel(text: "No Data Found!" )
                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                    }
                    
                    
                }
                
                
                
            }, failureHandler: { (error) in
                let errorStr : String = error.description
                
                print("DATA:error",errorStr)
                obj.refresh.endRefreshing()
                obj.stopLoading()
                //  obj.errorChecking(error: error)
                
            })
            
        }
        else{
            
            obj.label.isHidden = false
            obj.tableView.isHidden = true
            obj.noDataLabel(text: "Internet is not available, please check your internet connection try again." )
            obj.stopLoading()
        }
    }
}
