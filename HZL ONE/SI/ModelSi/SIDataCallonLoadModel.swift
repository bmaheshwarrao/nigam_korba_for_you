//
//  SIDataCallonLoadModel.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 30/03/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import Foundation
class SiAppersessionListModel: NSObject {
    
   
    
    
    var UserSessionCode: String?
    
 
    func setDataInModel(cell:[String:AnyObject])
    {
     
        
        if cell["UserSessionCode"] is NSNull || cell["UserSessionCode"] == nil {
            self.UserSessionCode = ""
        }else{
            let app = cell["UserSessionCode"]
            self.UserSessionCode = (app?.description)!
        }
        
        
    
        
    }
}


class SiAppersessionListDataAPI
{
    
    var reachablty = Reachability()!
    
    
    func serviceCalling(obj:SIDashboardViewController,param : [String:String], success:@escaping (AnyObject)-> Void)
    {
        
        obj.startLoadingPK(view: obj.view)
        
        if(reachablty.connection != .none)
        {
            
            WebServices.sharedInstances.sendPostRequest(url: URLConstants.GetAppersession, parameters: param, successHandler: { (dict) in
                
                if let response = dict["ReponseCode"]{
                    
                 
                    
                    if(response as! Int == 200)
                    {
                        
                        
                        
                        if let data = dict["ResponseValue"] as? AnyObject
                        {
                            
                            var dataArray : [SiAppersessionListModel] = []
                            
                            
                                
                                if let celll = data as? [String:AnyObject]
                                {
                                    let object = SiAppersessionListModel()
                                    object.setDataInModel(cell: celll)
                                    dataArray.append(object)
                                }
                                
                            
                            
                            success(dataArray as AnyObject)
                            
                            
                        }
                        
                        obj.refresh.endRefreshing()
                        obj.stopLoadingPK(view: obj.view)
                    }
                    else
                    {
                        
                        
                        
                        obj.refresh.endRefreshing()
                        
                        obj.stopLoadingPK(view: obj.view)
                        
                    }
                    
                    
                }
                
                
                
            }, failureHandler: { (error) in
                let errorStr : String = error.description
                
                print("DATA:error",errorStr)
                
                // obj.errorChecking(error: error)
                obj.stopLoadingPK(view: obj.view)
            })
            
            
        }
        else{
            // obj.noDataLabel(text:  "No internet connection found. Check your internet connection and try again.")
            
            obj.refresh.endRefreshing()
            obj.stopLoadingPK(view: obj.view)
        }
        
        
    }
    
}
class UserTransactionListModel: NSObject {
    
    
    
    
    var ResponseValue: String?
    
    
    func setDataInModel(cell:[String:AnyObject])
    {
        
        
        if cell["ResponseValue"] is NSNull || cell["ResponseValue"] == nil {
            self.ResponseValue = ""
        }else{
            let app = cell["ResponseValue"]
            self.ResponseValue = (app?.description)!
        }
        
        
        
        
    }
}

class UserTransactionListDataAPI
{
    
    var reachablty = Reachability()!
    
    
    func serviceCalling(obj:SIDashboardViewController,param : [String:String], success:@escaping (AnyObject)-> Void)
    {
        
        obj.startLoadingPK(view: obj.view)
        
        if(reachablty.connection != .none)
        {
            
            WebServices.sharedInstances.sendPostRequest(url: URLConstants.GetUserTransaction, parameters: param, successHandler: { (dict) in
                
                if let response = dict["ReponseCode"]{
                    
                    
                    let message  = dict["ResponseMessage"] as! String
                    if(response as! Int == 200)
                    {
                        
                        
                        
                        if let data = dict["ResponseValue"] as? AnyObject
                        {
                            
                            var dataArray : [UserTransactionListModel] = []
                            
                           
                                
                                if let celll = data as? [String:AnyObject]
                                {
                                    let object = UserTransactionListModel()
                                    object.setDataInModel(cell: celll)
                                    dataArray.append(object)
                                }
                                
                            if dict["ResponseValue"] is NSNull || dict["ResponseValue"] == nil {
                                obj.ResponseValue = ""
                            }else{
                                let app = dict["ResponseValue"]
                                obj.ResponseValue = (app?.description)!
                            }
                            if(obj.StrResponseValue == "1"){
                                let storyboard1 = UIStoryboard(name: "SI", bundle: nil)
                                let auditVC = storyboard1.instantiateViewController(withIdentifier: "NewInteractionMainViewController") as! NewInteractionMainViewController
                                auditVC.ResponseValue = obj.ResponseValue
                                obj.navigationController?.pushViewController(auditVC, animated: true)
                            }
                            success(dataArray as AnyObject)
                            
                            
                        }
                        
                        obj.refresh.endRefreshing()
                        obj.stopLoadingPK(view: obj.view)
                    }
                    else
                    {
                        obj.ResponseValue = ""
                        obj.view.makeToast(message)
                        
                        obj.refresh.endRefreshing()
                        
                        obj.stopLoadingPK(view: obj.view)
                        
                    }
                    
                    
                }
                
                
                
            }, failureHandler: { (error) in
                let errorStr : String = error.description
                
                print("DATA:error",errorStr)
                
                // obj.errorChecking(error: error)
                obj.stopLoadingPK(view: obj.view)
            })
            
            
        }
        else{
            // obj.noDataLabel(text:  "No internet connection found. Check your internet connection and try again.")
            obj.ResponseValue = ""
            obj.refresh.endRefreshing()
            obj.stopLoadingPK(view: obj.view)
        }
        
        
    }
    
}
