//
//  SIDataListModel.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 23/03/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import Foundation
class SIDataListApi
{
    var reachablty = Reachability()!
    func serviceCalling(obj:NewInteractionMainViewController, success:@escaping (AnyObject)-> Void)
    {
        
       
        var statusString = String()
        
        if(reachablty.connection != .none)
        {
            
            var urlString = String()
            var keyString1 = String()
            var keyString2 = String()
            var param : [String:String] = [:]
            if(FilterDataFromServer.filterType == "Location") {
                urlString = Base_Url_SI+"Zone/Get"
                keyString2 = "ZoneName"
                keyString1 = "Id"
                let empId =  UserDefaults.standard.string(forKey: "EmployeeID")!
                param = ["ProcessID":"1","UserSessionCode":empId]
            } else if(FilterDataFromServer.filterType == "Unit"){
                
                
                urlString = Base_Url_SI+"Unit/GetUnitByZone/"
                keyString2 = "UnitName"
                keyString1 = "Id"
                 param = ["ZoneId":String(FilterDataFromServer.location_id)]
            } else if(FilterDataFromServer.filterType == "Area"){
                
                
                urlString = Base_Url_SI+"Area/GetAreaByUnit/"
                keyString2 = "AreaName"
                keyString1 = "Id"
                param = ["UnitId":String(FilterDataFromServer.unit_Id)]
            }
           
           
            
            print(urlString)
    

            WebServices.sharedInstances.sendPostRequest(url: urlString, parameters: param, successHandler: { (dict) in

                if let response = dict["ReponseCode"]{

                    //statusString  = dict["ResponseMessage"] as! String

                    if(response as! Int == 200)
                    {

                       
                     

                        if let data = dict["ResponseValue"] as? [AnyObject]
                        {

                            var dataArray : [DataListModel] = []

                            for i in 0..<data.count
                            {
                                if let cell = data[i] as? [String:AnyObject]
                                {
                                    let object = DataListModel()

                                    if cell[keyString1] is NSNull{
                                        object.id = ""
                                    }else{

                                        let idd  = cell[keyString1]
                                        object.id = (idd?.description)!
                                    }

                                    if cell[keyString2] is NSNull{
                                        object.name = ""
                                    }else{
                                        let idd  = cell[keyString2]
                                        object.name = (idd?.description)!

                                    }


                                    dataArray.append(object)

                                }
                            }

                            
                            success(dataArray as AnyObject)


                        }

                    
                    }
                    else
                    {
                        statusString = "success"
                        
                     
                        print("DATA:fail")
                        //                        obj.tableView.isHidden = true
                        //                        obj.noDataLabel(text: "Sorry! No area manager found with name "+name)
                        
                        

                    }


                }



            }, failureHandler: { (error) in
                let errorStr : String = error.description
                statusString = "fail"
                print("DATA:error",errorStr)

                //  obj.errorChecking(error: error)

            })

        }
        else{
        }
    }
}

class SIDataGetListApi
{
    var reachablty = Reachability()!
    func serviceCalling(obj:NewInteractionMainViewController, success:@escaping (AnyObject)-> Void)
    {
        var statusString = String()
        
        if(reachablty.connection != .none)
        {
            
            var urlString = String()
            var keyString1 = String()
            var keyString2 = String()
            var param : [String:String] = [:]
            if(FilterDataFromServer.filterType == "Location") {
                urlString = Base_Url_SI+"Zone/Get"
                keyString2 = "ZoneName"
                keyString1 = "Id"
                let empId =  UserDefaults.standard.string(forKey: "EmployeeID")!
                param = ["ProcessID":"1","UserSessionCode":empId]
            }
            
            
            
            print(urlString)
            
            
         
                WebServices.sharedInstances.sendGetRequest(Url: urlString, successHandler: { (dict) in
                if let response = dict["ReponseCode"]{
                    
                    //statusString  = dict["ResponseMessage"] as! String
                    
                    if(response as! Int == 200)
                    {
                        
                   
                 
                        if let data = dict["ResponseValue"] as? [AnyObject]
                        {
                           print(data)
                            var dataArray : [DataListModel] = []
                            
                            for i in 0..<data.count
                            {
                                
                                
                                if let cell = data[i] as? [String : AnyObject]
                                {
                                    
                                    let object = DataListModel()

                                    if cell[keyString1] is NSNull{
                                        object.id = ""
                                    }else{



                                        let idd  = cell[keyString1]
                                        object.id = (idd?.description)!
                                    }

                                    if cell[keyString2] is NSNull{
                                        object.name = ""
                                    }else{
                                        let idd  = cell[keyString2]
                                        object.name = (idd?.description)!

                                    }


                                    dataArray.append(object)

                                }
                            }
                            
                     
                            success(dataArray as AnyObject)
                            
                            
                        }
                        
                      
                       
                    }
                    else
                    {
                        statusString = "success"
                       
                     
                        print("DATA:fail")
                        //                        obj.tableView.isHidden = true
                        //                        obj.noDataLabel(text: "Sorry! No area manager found with name "+name)
                       
                   
                        
                    }
                    
                    
                }
                
                
                
            }, failureHandler: { (error) in
                let errorStr : String = error.description
                statusString = "fail"
                print("DATA:error",errorStr)
                
                //  obj.errorChecking(error: error)
                
            })
            
            
            
            
            
            
        }
        else{
            
         
        }
        
        
    }
}
