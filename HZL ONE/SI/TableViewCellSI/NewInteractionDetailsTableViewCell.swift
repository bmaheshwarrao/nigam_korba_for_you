//
//  NewInteractionDetailsTableViewCell.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 31/03/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit

class NewInteractionDetailsTableViewCell: UITableViewCell {
@IBOutlet weak var containerView: UIView!
    @IBOutlet weak var lblSituation: UILabel!
    @IBOutlet weak var lblRisk: UILabel!
    @IBOutlet weak var textViewCategory: UITextView!
    @IBOutlet weak var lblCategoryName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
