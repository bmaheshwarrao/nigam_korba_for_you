//
//  ListInteractionViewController.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 25/03/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit

class ListInteractionViewController: CommonVSClass ,UITableViewDelegate, UITableViewDataSource{
    
    @IBOutlet weak var tableView: UITableView!
    var InteractionCode  = String()
    var RequestorCode = String()
    var refreshControl = UIRefreshControl()
    
    var InteractionDB:[InteractionCategoryListReportedDataModel] = []
    var InteractionAPI = IntercationDetailsListApi()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.estimatedRowHeight = 65
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.tableFooterView = UIView()
        
        refreshControl.addTarget(self, action: #selector(getInteractionData), for: .valueChanged)
        
        self.tableView.addSubview(refreshControl)
        let swipe = UISwipeGestureRecognizer(target: self, action: #selector(self.swipeRecognizer))
        swipe.direction = ([.down])
        self.view.addGestureRecognizer(swipe)
        
        
        
        
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        if(MoveStruct.isMove == true){
            MoveStruct.isMove = false
            self.view.makeToast(MoveStruct.message)
        }
        self.tabBarController?.tabBar.isHidden = true
        self.title = "List Interaction"
        let nav = self.navigationController?.navigationBar
        nav?.tintColor = UIColor.white
        nav?.barTintColor = UIColor(hexString: "2c3e50", alpha: 1.0)
        nav?.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        getInteractionData()
    }
    var reachability = Reachability()!
    @objc func swipeRecognizer(_ sender: UISwipeGestureRecognizer) {
        if (sender.direction == .down) {
            
            if(self.reachability.connection != .none)
            {
                
                self.getInteractionData()
            }
            else
            {
                self.startLoading(view: self.view)
                DispatchQueue.main.asyncAfter(deadline: .now() + 4.5) {
                    self.noDataLabel(text: "No internet connection found. Check your internet connection and try again.")
                    self.stopLoading(view: self.view)
                }
            }
            
        }
    }
    @IBAction func tapViewClicked(_ sender: Any) {
        
        
        
        
        
        
        
    }
    @objc func tapViewTapClicked(_ sender: UITapGestureRecognizer) {
        let buttonPosition:CGPoint = sender.view!.convert(.zero, to:self.tableView)
        let indexPath = self.tableView.indexPathForRow(at: buttonPosition)
        
                let storyBoard = UIStoryboard(name: "SI", bundle: nil)
                let ZIVC = storyBoard.instantiateViewController(withIdentifier: "ListDetailsInteractionViewController") as! ListDetailsInteractionViewController
        ZIVC.InteractionDetailCode = self.InteractionDB[0].InteractionDetailList![(indexPath?.row)!].InteractionDetailCode!
        ZIVC.RequestorCode = self.InteractionDB[0].RequestorCode!
        
                self.navigationController?.pushViewController(ZIVC, animated: true)
        
        
    }
    @IBOutlet var tapView: UITapGestureRecognizer!
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    var data: String?
    var lastObject: String?
    var indPath = IndexPath()
 
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(section == 0){
            if(self.InteractionDB.count == 0){
                return 0
            }else{
            return 1
            }
        }else{
             if(self.InteractionDB.count == 0){
           return 0
             }else{
                return  (self.InteractionDB[0].InteractionDetailList?.count)!
            }
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
       

        
        if(indexPath.section == 0) {
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellDetails", for: indexPath) as! InteractionDetailsTableViewCell
        
            
        cell.lblRequestCode.text = self.InteractionDB[indexPath.section].InteractionCode!
        
        cell.lblEmpName.text = self.InteractionDB[indexPath.section].RequestorName!
        cell.lblFinYear.text = self.InteractionDB[indexPath.section].FinancialYear!
        cell.textViewZone.text = self.InteractionDB[indexPath.section].Zone! + " > " + self.InteractionDB[indexPath.section].Unit! + " > " + self.InteractionDB[indexPath.section].Area!
        
        
        
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = NSTimeZone.system
        dateFormatter.dateFormat = "yyyy-MM-dd'T'hh:mm:ss"
        let date = dateFormatter.date(from: self.InteractionDB[indexPath.section].SIDate!)
        
        let dateFormatter2 = DateFormatter()
        dateFormatter2.timeZone = NSTimeZone.system
        dateFormatter2.dateFormat = "dd MMM yyyy"
        
        switch date {
        case nil:
            let date_TimeStr = dateFormatter2.string(from: Date())
            cell.lblDate.text = date_TimeStr
            break;
        default:
            let date_TimeStr = dateFormatter2.string(from: date!)
            
            cell.lblDate.text = date_TimeStr
            break;
        }
        
        
        
        
        
        cell.separatorInset = UIEdgeInsetsMake(0, 10000, 0, 0);
        cell.selectionStyle = .none
        return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! InteractionListTableViewCell
           
            let TapGesture = UITapGestureRecognizer(target: self, action: #selector(self.tapViewTapClicked(_:)))
            cell.addGestureRecognizer(TapGesture)
            cell.isUserInteractionEnabled = true
            TapGesture.numberOfTapsRequired = 1
            cell.lblRequestId.text = "Id # " + self.InteractionDB[0].InteractionDetailList![indexPath.row].InteractionDetailCode!
            
            cell.lblLocation.text = self.InteractionDB[0].InteractionDetailList![indexPath.row].AreaLocation!
            cell.lblObservationDetails.text = self.InteractionDB[0].InteractionDetailList![indexPath.row].ObservationDetails!
            cell.lblStatus.text = self.InteractionDB[0].InteractionDetailList![indexPath.row].Status!
            
            cell.containerView.layer.cornerRadius = 10
            
            let dateFormatter = DateFormatter()
            dateFormatter.timeZone = NSTimeZone.system
            dateFormatter.dateFormat = "yyyy-MM-dd'T'hh:mm:ss"
            let date = dateFormatter.date(from: self.InteractionDB[0].InteractionDetailList![indexPath.row].TargetDate!)
            
            let dateFormatter2 = DateFormatter()
            dateFormatter2.timeZone = NSTimeZone.system
            dateFormatter2.dateFormat = "dd MMM yyyy"
            
            switch date {
            case nil:
                let date_TimeStr = dateFormatter2.string(from: Date())
                cell.lblDate.text = date_TimeStr
                break;
            default:
                let date_TimeStr = dateFormatter2.string(from: date!)
                
                cell.lblDate.text = date_TimeStr
                break;
            }
            
            
            
            
            
            cell.separatorInset = UIEdgeInsetsMake(0, 10000, 0, 0);
            cell.selectionStyle = .none
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableViewAutomaticDimension
        
    }
    
    @objc func getInteractionData(){
        var param = [String:String]()
      
        param = ["InteractionCode": InteractionCode ,"RequestorCode": RequestorCode   ]
        
 
        
        
        InteractionAPI.serviceCalling(obj: self,  param: param ) { (dict) in
            
            self.InteractionDB = dict as! [InteractionCategoryListReportedDataModel]
            
            self.tableView.reloadData()
            
        }
    }
    
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
