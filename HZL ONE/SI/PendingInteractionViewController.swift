//
//  PendingInteractionViewController.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 25/03/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit

class PendingInteractionViewController: CommonVSClass ,UITableViewDelegate, UITableViewDataSource{
    
    @IBOutlet weak var tableView: UITableView!
    
    var refreshControl = UIRefreshControl()
    
    var InteractionDB:[InteractionCategoryReportedDataModel] = []
    var InteractionAPI = PendingIntercationListApi()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        getInteractionData()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.estimatedRowHeight = 65
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.tableFooterView = UIView()
        
        refreshControl.addTarget(self, action: #selector(getInteractionData), for: .valueChanged)
        
        self.tableView.addSubview(refreshControl)
        let swipe = UISwipeGestureRecognizer(target: self, action: #selector(self.swipeRecognizer))
        swipe.direction = ([.down])
        self.view.addGestureRecognizer(swipe)
        
        
        
        
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.tabBarController?.tabBar.isHidden = true
        self.title = "Pending Interaction"
        let nav = self.navigationController?.navigationBar
        nav?.tintColor = UIColor.white
        nav?.barTintColor = UIColor(hexString: "2c3e50", alpha: 1.0)
        nav?.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        
    }
    var reachability = Reachability()!
    @objc func swipeRecognizer(_ sender: UISwipeGestureRecognizer) {
        if (sender.direction == .down) {
            
            if(self.reachability.connection != .none)
            {
                
                self.getInteractionData()
            }
            else
            {
                self.startLoading(view: self.view)
                DispatchQueue.main.asyncAfter(deadline: .now() + 4.5) {
                    self.noDataLabel(text: "No internet connection found. Check your internet connection and try again.")
                    self.stopLoading(view: self.view)
                }
            }
            
        }
    }
    @IBAction func tapViewClicked(_ sender: Any) {
        
        
        
        
        
        
        
    }
    @objc func tapViewTapClicked(_ sender: UITapGestureRecognizer) {
        let buttonPosition:CGPoint = sender.view!.convert(.zero, to:self.tableView)
        let indexPath = self.tableView.indexPathForRow(at: buttonPosition)
        
        let storyBoard = UIStoryboard(name: "SI", bundle: nil)
        let ZIVC = storyBoard.instantiateViewController(withIdentifier: "ListInteractionViewController") as! ListInteractionViewController
        ZIVC.InteractionCode = self.InteractionDB[(indexPath?.section)!].FilteredInteractionsList![(indexPath?.row)!].InteractionCode!
ZIVC.RequestorCode = self.InteractionDB[(indexPath?.section)!].FilteredInteractionsList![(indexPath?.row)!].RequestorCode!
        self.navigationController?.pushViewController(ZIVC, animated: true)
       
        
    }
    @IBOutlet var tapView: UITapGestureRecognizer!
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    var data: String?
    var lastObject: String?
    var indPath = IndexPath()
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let header = tableView.dequeueReusableCell(withIdentifier: "cellHeader") as! HeadereLearningTableViewCell
      
            header.lblHeader.text = self.InteractionDB[section].Accordion
       
        return header
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        
            return 50.0
        
        
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.InteractionDB.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (self.InteractionDB[section].FilteredInteractionsList?.count)!
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! PendindInteractionTableViewCell
        
        let TapGesture = UITapGestureRecognizer(target: self, action: #selector(self.tapViewTapClicked(_:)))
        cell.addGestureRecognizer(TapGesture)
        cell.isUserInteractionEnabled = true
        TapGesture.numberOfTapsRequired = 1
        cell.lblRequestId.text = "Req Code #" + self.InteractionDB[indexPath.section].FilteredInteractionsList![indexPath.row].RequestorCode!
        
        cell.lblEmpName.text = self.InteractionDB[indexPath.section].FilteredInteractionsList![indexPath.row].RequestorName!
        cell.lblLocation.text = self.InteractionDB[indexPath.section].FilteredInteractionsList![indexPath.row].Area!
        cell.lblStatus.text = self.InteractionDB[indexPath.section].FilteredInteractionsList![indexPath.row].Status!
        cell.containerView.layer.cornerRadius = 10
        

        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = NSTimeZone.system
        dateFormatter.dateFormat = "yyyy-MM-dd'T'hh:mm:ss"
        let date = dateFormatter.date(from: self.InteractionDB[indexPath.section].FilteredInteractionsList![indexPath.row].SIDate!)
       
        let dateFormatter2 = DateFormatter()
        dateFormatter2.timeZone = NSTimeZone.system
        dateFormatter2.dateFormat = "dd MMM yyyy"
        
        switch date {
        case nil:
            let date_TimeStr = dateFormatter2.string(from: Date())
            cell.lblDate.text = date_TimeStr
            break;
        default:
            let date_TimeStr = dateFormatter2.string(from: date!)
            
            cell.lblDate.text = date_TimeStr
            break;
        }
        
        
        
        
      
        cell.separatorInset = UIEdgeInsetsMake(0, 10000, 0, 0);
        cell.selectionStyle = .none
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableViewAutomaticDimension
        
    }
    
    @objc func getInteractionData(){
        var param = [String:String]()
        let pno : String = (UserDefaults.standard.string(forKey: "EmployeeID")!)
        let Profile_AuthKey = UserDefaults.standard.string(forKey: "Profile_AuthKey")!
        param = ["ModuleId":"1","RequestorCode":pno]
        
        InteractionAPI.serviceCalling(obj: self,  param: param ) { (dict) in
            
            self.InteractionDB = dict as! [InteractionCategoryReportedDataModel]
            
            self.tableView.reloadData()
            
        }
    }
   
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
