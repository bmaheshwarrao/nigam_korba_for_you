//
//  ViewController.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 17/11/18.
//  Copyright © 2018 SARVANG INFOTCH. All rights reserved.
//



import UIKit

import ViewPager_Swift
class ViewController: CommonVSClass {
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let myOptions = ViewPagerOptions(viewPagerWithFrame: CGRect(x: 0, y: self.view.frame.width/2, width: self.view.frame.width, height: 500))
        
        myOptions.tabType = ViewPagerTabType.basic
        
        
        
        
        
        
        myOptions.isTabHighlightAvailable = true
        // myOptions.is
        // If I want indicator bar to show below current page tab
        myOptions.isTabIndicatorAvailable = true
        myOptions.fitAllTabsInView = true;
        // Oh! and let's change color of tab to red
        myOptions.tabIndicatorViewBackgroundColor = UIColor.clear
        myOptions.tabViewBackgroundHighlightColor = UIColor.clear
        myOptions.tabViewBackgroundDefaultColor = UIColor.clear
        myOptions.tabViewTextDefaultColor = UIColor.clear
        myOptions.tabViewTextHighlightColor = UIColor.clear
        myOptions.tabViewTextFont = UIFont.systemFont(ofSize: 15.0, weight: .medium)
        let viewPager = ViewPagerController()
        // viewPager.view.backgroundColor = UIColor.red
        viewPager.options = myOptions
        viewPager.dataSource = self
        
        //Now let me add this to my viewcontroller
        self.addChildViewController(viewPager)
        
        
        
        self.view.addSubview(viewPager.view)
        viewPager.didMove(toParentViewController: self)
        // and many more...
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}
extension ViewController : ViewPagerControllerDataSource {
    func numberOfPages() -> Int {
        return 1
    }
    
    func viewControllerAtPosition(position: Int) -> UIViewController {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main",bundle : nil)
//        if(position == 0) {
        
            let empVC = storyBoard.instantiateViewController(withIdentifier: "EmployeeLoginViewController") as! EmployeeLoginViewController
            return  empVC as UIViewController
//        }else {
//            let nonEmpVC = storyBoard.instantiateViewController(withIdentifier: "NonEmployeeViewController") as! NonEmployeeViewController
//            return  nonEmpVC as UIViewController
//        }
    }
    
    func tabsForPages() -> [ViewPagerTab] {
        return [ViewPagerTab(title: "" , image: #imageLiteral(resourceName: "BackBlack")) ]
    }
    
    
}
extension ViewController : ViewPagerControllerDelegate {
    
}


