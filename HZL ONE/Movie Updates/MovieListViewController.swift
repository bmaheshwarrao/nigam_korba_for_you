//
//  MovieListViewController.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 20/02/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit
import SDWebImage
class MovieListViewController: CommonVSClass ,UITableViewDelegate, UITableViewDataSource{
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var lblLocation: UILabel!
    var movieStrNav = String()
    var refreshControl = UIRefreshControl()
    
    var movieDB:[MovieModel] = []
    var movieAPI = MovieDataAPI()
    
 
    @IBOutlet weak var heightLblLocation: NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
       
        
        getMovieData()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.estimatedRowHeight = 65
        tableView.rowHeight = UITableViewAutomaticDimension
        
        
        refreshControl.addTarget(self, action: #selector(getMovieData), for: .valueChanged)
        
        self.tableView.addSubview(refreshControl)
        let swipe = UISwipeGestureRecognizer(target: self, action: #selector(self.swipeRecognizer))
        swipe.direction = ([.down])
        self.view.addGestureRecognizer(swipe)
        
        tableView.tableFooterView = UIView()
        
        
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        let movieLoc = UserDefaults.standard.string(forKey: "movieLoc")
        if(movieLoc != nil){
            lblLocation.text = movieLoc
            heightLblLocation.constant = 21
        }else{
            lblLocation.text = ""
            heightLblLocation.constant = 0
            
        }
        self.tabBarController?.tabBar.isHidden = true
        self.title = movieStrNav
        let nav = self.navigationController?.navigationBar
        nav?.tintColor = UIColor.white
       nav?.barTintColor = UIColor(hexString: "2c3e50", alpha: 1.0)
        nav?.isTranslucent = false
        nav?.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        getMovieData()
        
    }
    var reachability = Reachability()!
    @objc func swipeRecognizer(_ sender: UISwipeGestureRecognizer) {
        if (sender.direction == .down) {
            
            if(self.reachability.connection != .none)
            {
                
                self.getMovieData()
            }
            else
            {
                self.startLoading(view: self.view)
                DispatchQueue.main.asyncAfter(deadline: .now() + 4.5) {
                    self.noDataLabel(text: "No internet connection found. Check your internet connection and try again.")
                    self.stopLoading(view: self.view)
                }
            }
            
        }
    }
    @IBAction func btnLocationClicked(_ sender: UIBarButtonItem) {
        
        FilterDataFromServer.location_Movie = String()
        FilterDataFromServer.filterType = "Movie"
        let storyBoard = UIStoryboard(name: "MovieUpdate", bundle: nil)
        let ZIVC = storyBoard.instantiateViewController(withIdentifier: "MovieLocationViewController") as! MovieLocationViewController
       ZIVC.titleStr = "Select Location"
        self.navigationController?.pushViewController(ZIVC, animated: true)
        
        
        
        
    }
//    @objc func updateData(){
//        if(FilterDataFromServer.location_Movie != "") {
//           lblLocation.text = FilterDataFromServer.location_Movie
//            lblLocation.textColor = UIColor.black
//        } else {
//            lblLocation.text = "Select Location"
//            lblLocation.textColor = UIColor.lightGray
//        }
//
//
//        // tableView.reloadData()
//    }
    func imageResize (image:UIImage, sizeChange:CGSize)-> UIImage{
        
        let hasAlpha = true
        let scale: CGFloat = 0.0 // Use scale factor of main screen
        
        UIGraphicsBeginImageContextWithOptions(sizeChange, !hasAlpha, scale)
        image.draw(in: CGRect(origin: CGPoint.zero, size: sizeChange))
        
        let scaledImage = UIGraphicsGetImageFromCurrentImageContext()
        return scaledImage!
    }
    @objc func tapViewTapClicked(_ sender: UITapGestureRecognizer) {
        let buttonPosition:CGPoint = sender.view!.convert(.zero, to:self.tableView)
        let indexPath = self.tableView.indexPathForRow(at: buttonPosition)
        
        let storyBoard = UIStoryboard(name: "MovieUpdate", bundle: nil)
        let ZIVC = storyBoard.instantiateViewController(withIdentifier: "MovieDetailsViewController") as! MovieDetailsViewController
        ZIVC.movieDB = self.movieDB[(indexPath?.section)!]
        self.navigationController?.pushViewController(ZIVC, animated: true)
        
        
    }
    @IBOutlet var tapView: UITapGestureRecognizer!
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    var data: String?
    var lastObject: String?
    var indPath = IndexPath()
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        
        
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.movieDB.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! MovieListTableViewCell
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tapViewTapClicked(_:)))
        cell.isUserInteractionEnabled = true;
        cell.addGestureRecognizer(tapGesture)
        let urlString = self.movieDB[indexPath.section].Image
        let imageView = UIImageView()
        
        let urlShow = urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
        if let url = NSURL(string: urlShow!) {
            
            SDWebImageManager.shared().downloadImage(with: NSURL(string: urlShow!) as URL!, options: .continueInBackground, progress: {
                (receivedSize :Int, ExpectedSize :Int) in
                
            }, completed: {
                (image : UIImage?, error : Error?, cacheType : SDImageCacheType, finished : Bool, url : URL?) in
                if(image != nil) {
                    let size = CGSize(width: 400, height: 200)
                    let imageCell = self.imageResize(image: image!, sizeChange: size)
                    cell.imageViewMovie.image = image
                    //  cell.viewImageHold.backgroundColor = UIColor(patternImage: imageCell)
                    
                }else{
                    imageView.image =  UIImage(named: "placed")
                    let size = CGSize(width: 400, height: 200)
                    let imageCell = self.imageResize(image: imageView.image!, sizeChange: size)
                    cell.imageViewMovie.image = UIImage(named: "placed")
                    // cell.viewImageHold.backgroundColor = UIColor(patternImage: imageCell)
                }
            })
        }else{
            imageView.image =  UIImage(named: "placed")
             let size = CGSize(width: 400, height: 200)
            let imageCell = imageResize(image: imageView.image!, sizeChange: size)
            cell.imageViewMovie.image = UIImage(named: "placed")
            //cell.viewImageHold.backgroundColor = UIColor(patternImage: imageCell)
        }
        cell.lblMovie.text = self.movieDB[indexPath.section].Movie_Name
         cell.lblLang.text = self.movieDB[indexPath.section].Language
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableViewAutomaticDimension
        
    }
    
    @objc func getMovieData(){
        
        var movieLoc = UserDefaults.standard.string(forKey: "movieLoc")
        if(movieLoc == nil){
            movieLoc = ""
        }
        if(movieLoc != ""){
        var param = [String:String]()
            var  dictWithoutNilValues = [String:String]()
        
        
        dictWithoutNilValues = ["Location" : movieLoc] as! [String : String]
        
        
        
        param = dictWithoutNilValues.filter { $0.value != ""}
        
        
        
      movieAPI.serviceCalling(obj: self,  param: param ) { (dict) in
            
            self.movieDB = dict as! [MovieModel]
            
            self.tableView.reloadData()
            
        }
        }else{
            tableView.isHidden = true
            noDataLabel(text: "Please Select Location")
            refresh.endRefreshing()
           
            label.isHidden = false
        }
    }
    
   
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
