//
//  MovieTalkiesTableViewCell.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 16/03/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit

class MovieTalkiesTableViewCell: UITableViewCell,UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

 
    @IBOutlet weak var heightCollectionCon: NSLayoutConstraint!
    @IBOutlet weak var collectionTiming: UICollectionView!
    
    @IBOutlet weak var textViewTalkies: UITextView!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.collectionTiming.delegate = self
        self.collectionTiming.dataSource = self
        
        // Initialization code
    }
    var dataArray : [String] = []
    func pageView(data : [String] )
    {
        
        dataArray = data
        self.collectionTiming.isScrollEnabled = false
       self.collectionTiming.reloadData()
        
                            
                            
        
        
    }
    
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return self.dataArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellTalk", for: indexPath) as! MovieTalkiesCollectionViewCell
        
     cell.btnTalk.setTitle(dataArray[indexPath.row], for: .normal)
        cell.btnTalk.setTitleColor(UIColor(hexString: "2c3e50", alpha: 1.0), for: .normal)
        cell.btnTalk.clipsToBounds = true
        cell.btnTalk.layer.borderWidth = 1.0
        cell.btnTalk.layer.borderColor = UIColor(hexString: "2c3e50", alpha: 1.0)?.cgColor
        return cell
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        return UIEdgeInsetsMake(0, 0, 0, 0)
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        return CGSize(width: 100, height: 40)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        
        //print(indexPath.item)
        
        
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
