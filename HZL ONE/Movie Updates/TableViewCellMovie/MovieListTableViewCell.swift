//
//  MovieListTableViewCell.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 20/02/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit

class MovieListTableViewCell: UITableViewCell {

    @IBOutlet weak var lblLang: UILabel!
    @IBOutlet weak var lblMovie: UILabel!
    @IBOutlet weak var imageViewMovie: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
