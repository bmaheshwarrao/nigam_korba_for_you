//
//  NewroomAlertPersonViewController.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 18/03/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit

class NewroomAlertPersonViewController: UIViewController ,UITextFieldDelegate{

    @IBOutlet weak var textViewAddress: UITextView!
    @IBOutlet weak var viewAddress: UIView!
    @IBOutlet weak var txtContact: UITextField!
    @IBOutlet weak var viewContact: UIView!
    @IBOutlet weak var txtName: UITextField!
    @IBOutlet weak var viewName: UIView!
    var name = String()
    var contact = String()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewContact.layer.borderWidth = 1.0
        viewContact.layer.borderColor = UIColor.black.cgColor
        viewContact.layer.cornerRadius = 10.0
        txtContact.keyboardType = UIKeyboardType.numberPad
        txtContact.delegate = self
        viewName.layer.borderWidth = 1.0
        viewName.layer.borderColor = UIColor.black.cgColor
        viewName.layer.cornerRadius = 10.0
        
        viewAddress.layer.borderWidth = 1.0
        viewAddress.layer.borderColor = UIColor.black.cgColor
        viewAddress.layer.cornerRadius = 10.0
     
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        if(name != ""){
            txtName.text = name
            txtName.isEnabled = false
        }else{
            txtName.text = name
            txtName.isEnabled = true
        }
        if(contact != ""){
            txtContact.text = contact
            txtContact.isEnabled = false
        }else{
            txtContact.text = contact
            txtContact.isEnabled = true
        }
    }
    static func instantiate() -> NewroomAlertPersonViewController? {
        return UIStoryboard(name: "GuestHouse", bundle: nil).instantiateViewController(withIdentifier: "\(NewroomAlertPersonViewController.self)") as? NewroomAlertPersonViewController
    }
    @IBAction func btnAddClicked(_ sender: UIButton) {
        
        
        if(txtName.text == ""){
            self.view.makeToast("Enter Name")
        }else if(txtContact.text == ""){
            self.view.makeToast("Enter Name")
        }else{
            print(lengthData)
           if(textViewAddress.text == ""){
                self.view.makeToast("Enter Address")
            }else{
        let object =  nameDataArray()
        object.name = txtName.text!
        object.Contact = txtContact.text!
        object.Address = textViewAddress.text!
        dataNameArray.append(object)
                txtContact.text = ""
                txtName.text = ""
                textViewAddress.text = ""
         NotificationCenter.default.post(name:NSNotification.Name.init("callRoomDataAdd"), object: nil)
         dismiss(animated: true, completion: nil)
            }
        }
    }
    @IBAction func btnCancelClicked(_ sender: UIButton) {
         dismiss(animated: true, completion: nil)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    var lengthData : Int = 0
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let maxLength = 10
        let currentString: NSString = textField.text! as NSString
        let newString: NSString =
            currentString.replacingCharacters(in: range, with: string) as NSString
        lengthData = newString.length
        return newString.length <= maxLength
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
