//
//  FoodDetailsViewController.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 23/02/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit
var dataListFood : [FoodDetailingModel] = []
class FoodDetailsViewController: CommonVSClass , WWCalendarTimeSelectorProtocol{
   @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
        getfoodDetailsList()
        // Do any additional setup after loading the view.
    }

    @IBAction func btnFoodTypeClicked(_ sender: UIButton) {
        
        DispatchQueue.main.async(execute: {() -> Void in
            let popup = PopUpTableViewController()
            
            popup.itemsArray = self.foodDetailsListArray
            
            popup.sourceView = self.cellData.lblFoodType
            popup.isScroll = true
            popup.backgroundColor = UIColor.white
            if popup.itemsArray.count > 5{
                popup.popUpHeight = 200
            }
            else{
                popup.popUpHeight = CGFloat(popup.itemsArray.count)*45
            }
            popup.popUpWidth = UIScreen.main.bounds.size.width-60
            popup.backgroundImage = nil
            popup.itemTitleColor = UIColor.white
            popup.itemSelectionColor = UIColor.lightGray
            popup.arrowDirections = .any
            popup.arrowColor = UIColor.white
            popup.popCellBlock = {(_ popupVC: PopUpTableViewController, _ popupCell: UITableViewCell, _ row: Int, _ section: Int) -> Void in

                self.cellData.lblFoodType.text = self.foodDetailsListArray[row]
                self.cellData.lblFoodType.textColor = UIColor.black
                self.idFood = self.foodDetailsIdArray[row]
                popupVC.dismiss(animated: false, completion: nil)
            }
            self.present(popup, animated: true, completion: {() -> Void in
            })
            
            
        })
        
    }
    
    var foodDetailsDB:[FoddTypeListModel] = []
    
    var foodDetailsAPI = FoodTypeDataAPI()
    var idFood = String()
    var foodDetailsListArray :[String] = []
    var foodDetailsIdArray :[String] = []
    @objc func getfoodDetailsList(){
        
        let empId = UserDefaults.standard.string(forKey: "EmployeeID")!
        let Profile_AuthKey = UserDefaults.standard.string(forKey: "Profile_AuthKey")!
        let paramm  : [String:String] = ["UserID": empId , "AuthKey" :Profile_AuthKey ]
        
        self.foodDetailsAPI.serviceCalling(obj: self , param : paramm) { (dict) in
            
            self.foodDetailsDB = [FoddTypeListModel]()
            self.foodDetailsDB = dict as! [FoddTypeListModel]
            if(self.foodDetailsDB.count > 0){
                for i in 0...self.foodDetailsDB.count - 1 {
                    self.foodDetailsListArray.append(self.foodDetailsDB[i].TypeName!)
                    self.foodDetailsIdArray.append(self.foodDetailsDB[i].ID!)
                }
            }
            
            
            
            
        }
    }
    fileprivate var singleDate: Date = Date()
    fileprivate var multipleDates: [Date] = []
     var selectdateVal = 0
    @IBAction func btnForDateeClicked(_ sender: UIButton) {
        let selector = UIStoryboard(name: "WWCalendarTimeSelector", bundle: nil).instantiateViewController(withIdentifier: "WWCalendarTimeSelector") as! WWCalendarTimeSelector
        
        selectdateVal = 1
        dateData = 0
        
        selector.delegate = self
        selector.optionCurrentDate = singleDate
        selector.optionCurrentDates = Set(multipleDates)
        selector.optionCurrentDateRange.setStartDate(multipleDates.first ?? singleDate)
        selector.optionCurrentDateRange.setEndDate(multipleDates.last ?? singleDate)
        
        
        selector.optionStyles.showDateMonth(true)
        selector.optionStyles.showMonth(false)
        selector.optionStyles.showYear(true)
        selector.optionStyles.showTime(false)
        
        
        present(selector, animated: true, completion: nil)
        
    }
    @IBAction func btnToDateClicked(_ sender: UIButton) {
        let selector = UIStoryboard(name: "WWCalendarTimeSelector", bundle: nil).instantiateViewController(withIdentifier: "WWCalendarTimeSelector") as! WWCalendarTimeSelector
        
        
        selectdateVal = 2
        
        selector.delegate = self
        selector.optionCurrentDate = singleDate
        selector.optionCurrentDates = Set(multipleDates)
        selector.optionCurrentDateRange.setStartDate(multipleDates.first ?? singleDate)
        selector.optionCurrentDateRange.setEndDate(multipleDates.last ?? singleDate)
        
        selector.optionStyles.showDateMonth(true)
        selector.optionStyles.showMonth(false)
        selector.optionStyles.showYear(true)
        selector.optionStyles.showTime(false)
        
        dateData = 0
        
        present(selector, animated: true, completion: nil)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        
        self.title = "Food Details"
        self.tabBarController?.tabBar.isHidden = true
        let nav = self.navigationController?.navigationBar
        nav?.tintColor = UIColor.white
       nav?.barTintColor = UIColor(hexString: "2c3e50", alpha: 1.0)
        nav?.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        
        
        
    }
    
    @IBAction func btnAddClicked(_ sender: UIButton) {
        if cellData.lblFoodType.text == "" || cellData.lblFoodType.text == "Select" {
            self.view.makeToast("Please select Food type")
        } else if cellData.txtMember.text == "" {
            self.view.makeToast("Please enter Member Count")
        }else if forDate == "" {
            self.view.makeToast("Please select For date")
        }else if toDate == "" {
            self.view.makeToast("Please select To date")
        }
       else{
        
        
        let obj = FoodDetailingModel()
        obj.count = cellData.txtMember.text!
        obj.foodType = cellData.lblFoodType.text!
        obj.forDate = forDate
        obj.toDate = toDate
        obj.toDateMilliSec = toDateMilliSec
            obj.forDateMilliSec = forDateMilliSec
        dataListFood.append(obj)
        self.navigationController?.popViewController(animated: true)
        }
    }
    var cellData : FoodDetailsTableViewCell!
    var forDate = String()
    var toDate = String()
    
    var forDateMilliSec = String()
    var toDateMilliSec = String()
    func getMilliseconds(date1 : Date) -> String{
        //let strDate = date1
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd MMM yyyy  hh:mma"
        //let date = dateFormatter.date(from: strDate)
        
        let millieseconds = self.getDiffernce(toTime: date1)
        print(millieseconds)
        return String(millieseconds)
    }
    func getDiffernce(toTime:Date) -> Int{
        let elapsed = toTime.timeIntervalSince1970
        return Int(elapsed * 1000)
    }
    func WWCalendarTimeSelectorDone(_ selector: WWCalendarTimeSelector, date: Date) {
        print("Selected \n\(date)\n---")
        singleDate = date
        
        
        
        if(selectdateVal == 1){
            cellData.lblforDate.text = date.stringFromFormat("d' 'MMM' 'yyyy'")
            forDate = date.stringFromFormat("d' 'MMM' 'yyyy'")
            forDateMilliSec = getMilliseconds(date1: date)
            DataDateStart = date
        }
        if(selectdateVal == 2){
            cellData.lblToDate.text = date.stringFromFormat("d' 'MMM' 'yyyy'")
             toDate = date.stringFromFormat("d' 'MMM' 'yyyy'")
              toDateMilliSec = getMilliseconds(date1: date)
            DataDateEnd = date
        }
     
        switch DataDateStart.compare(DataDateEnd) {
            
        case .orderedAscending:
            print("less")
        case .orderedSame:
            print("same")
        case .orderedDescending:
            toDateMilliSec = forDateMilliSec
            cellData.lblToDate.text = cellData.lblforDate.text
        }
    }
    
    func WWCalendarTimeSelectorDone(_ selector: WWCalendarTimeSelector, dates: [Date]) {
        print("Selected Multiple Dates \n\(dates)\n---")
        if let date = dates.first {
            singleDate = date
            // dateLabel.text = date.stringFromFormat("d' 'MMMM' 'yyyy', 'h':'mma")
        }
        else {
            //dateLabel.text = "No Date Selected"
        }
        multipleDates = dates
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
extension FoodDetailsViewController : UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 290
        
        
    }
}
extension FoodDetailsViewController : UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! FoodDetailsTableViewCell
        
        cellData = cell
        
        if(cell.lblFoodType.text == ""){
            cell.lblFoodType.text = "Select"
        }
        let dateFormatter2 = DateFormatter()
        dateFormatter2.timeZone = NSTimeZone.system
        dateFormatter2.dateFormat = "dd MMM yyyy"
        
        
        let date_TimeStr = dateFormatter2.string(from: Date())
        cell.lblforDate.text = date_TimeStr
        forDate = date_TimeStr
        toDate = date_TimeStr
        forDateMilliSec = getMilliseconds(date1: Date())
        cell.lblToDate.text = date_TimeStr
        toDateMilliSec = getMilliseconds(date1: Date())
        
        cell.separatorInset = UIEdgeInsetsMake(0, 10000, 0, 0);
        return cell
        
        
        
    }
}
