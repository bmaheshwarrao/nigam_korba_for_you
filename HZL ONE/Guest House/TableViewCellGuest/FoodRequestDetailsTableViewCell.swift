//
//  FoodRequestDetailsTableViewCell.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 23/02/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit

class FoodRequestDetailsTableViewCell: UITableViewCell {

    
    @IBOutlet weak var lblDay: UILabel!
    @IBOutlet weak var lblMonthTop: UILabel!
    @IBOutlet weak var lblDateTop: UILabel!
    @IBOutlet weak var lblNo: UILabel!
    @IBOutlet weak var lblName: UILabel!
   
     
   
    @IBOutlet weak var textViewGuestName: UITextView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
