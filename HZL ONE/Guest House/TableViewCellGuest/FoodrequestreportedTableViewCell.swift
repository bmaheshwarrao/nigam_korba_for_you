//
//  FoodrequestreportedTableViewCell.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 25/02/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit

class FoodrequestreportedTableViewCell: UITableViewCell {
@IBOutlet weak var containerView: UIViewX!
    @IBOutlet weak var lblDay: UILabel!
    @IBOutlet weak var lblMonth: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var lblContact: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var textViewGuestName: UITextView!
    
    @IBOutlet weak var textViewDesc: UITextView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
