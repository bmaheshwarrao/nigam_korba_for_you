//
//  NoofroomsTableViewCell.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 18/03/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit

class NoofroomsTableViewCell: UITableViewCell {

    @IBOutlet weak var txtNoofRooms: UITextField!
    @IBOutlet weak var viewNoofRooms: UIView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        viewNoofRooms.layer.borderWidth = 1.0
        viewNoofRooms.layer.borderColor = UIColor.black.cgColor
        viewNoofRooms.layer.cornerRadius = 10.0
        txtNoofRooms.keyboardType = UIKeyboardType.numberPad
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
