//
//  CheckStatusFoodRoomTableViewCell.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 19/04/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit

class CheckStatusFoodRoomTableViewCell:  UITableViewCell,UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    @IBOutlet weak var heightCollectionCon: NSLayoutConstraint!
    @IBOutlet weak var collectionRoomFood: UICollectionView!
    override func awakeFromNib() {
        super.awakeFromNib()
        collectionRoomFood.delegate = self
        collectionRoomFood.dataSource = self
        // Initialization code
    }
    var dataArray : [String] = []
    var dataArrayColor : [UIColor] = []
    func pageView(data : [String] ,color : [UIColor])
    {
        dataArrayColor = color
        dataArray = data
        self.collectionRoomFood.isScrollEnabled = false
        self.collectionRoomFood.reloadData()
        
        
        
        
        
    }
    
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return self.dataArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellStatusData", for: indexPath) as! RoomFoodStatusCollectionViewCell
        
        cell.btnRoomFoodStatus.setTitle(dataArray[indexPath.row], for: .normal)
        cell.btnRoomFoodStatus.setTitleColor(dataArrayColor[indexPath.row], for: .normal)
        cell.btnRoomFoodStatus.clipsToBounds = true
        cell.btnRoomFoodStatus.layer.borderWidth = 1.0
        cell.btnRoomFoodStatus.layer.borderColor = dataArrayColor[indexPath.row].cgColor
        return cell
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        return UIEdgeInsetsMake(0, 0, 0, 0)
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        return CGSize(width: 165, height: 44)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        
        //print(indexPath.item)
        
        
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
