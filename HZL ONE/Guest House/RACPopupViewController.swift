//
//  RACPopupViewController.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 20/03/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit
var textMsgAction = String()
class RACPopupViewController: CommonVSClass {

    var pendingApprovalDB = PendingApprovalGuestHouseDataModel()
    var typeString = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        viewProcess.layer.borderWidth = 1.0
        viewProcess.layer.borderColor = UIColor.black.cgColor
        viewProcess.layer.cornerRadius = 10.0
        // Do any additional setup after loading the view.
    }
//    static func instantiate() -> RACPopupViewController? {
//        return UIStoryboard(name: "GuestHouse", bundle: nil).instantiateViewController(withIdentifier: "\(RACPopupViewController.self)") as? RACPopupViewController
//    }
    
    @IBAction func btnCloseClicked(_ sender: UIButton) {
          dismiss(animated: true, completion: nil)
    }
  
   
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    var popUpActionDB : [actionTaken] = []
    
    
    
    @IBAction func btnProcessClicked(_ sender: UIButton) {
        if(textViewProcess.text == ""){
            self.view.makeToast("Enter Remark")
        }else{
       textMsgAction = textViewProcess.text!
        
        
        
        if(typeString == "Food" ){
           
            NotificationCenter.default.post(name:NSNotification.Name.init("FoodGuestHouse"), object: nil)
        }else if(typeString == "Room" ){
            NotificationCenter.default.post(name:NSNotification.Name.init("RoomGuestHouse"), object: nil)
        }
        
        
        
         dismiss(animated: true, completion: nil)
        }
    }
    @IBAction func btnCancelClicked(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
   
    
    
    
    @IBOutlet weak var textViewProcess: UITextView!
    
    @IBOutlet weak var viewProcess: UIView!
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
   

}

class actionTaken : NSObject{
    var title : String?
    var desc : String?
}
