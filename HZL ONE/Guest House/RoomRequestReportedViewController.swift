//
//  RoomRequestReportedViewController.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 23/02/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit

class RoomRequestReportedViewController: CommonVSClass ,UITableViewDelegate, UITableViewDataSource{
    
    @IBOutlet weak var tableView: UITableView!
    
    var refreshControl = UIRefreshControl()
    var typeData = String()
    var valueData = String()
    var showUrlData = String()
    var RoomDB:[RoomRequestReportedDataModel] = []
    var roomAPI = RoomRequestReportedDataAPI()
    
    var roomLoadMoreDB : [RoomRequestReportedDataModel] = []
    
    
    
    
    var roomLoadMoreAPI = RoomRequestReportedDataAPILoadMore()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        getRoomData()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.estimatedRowHeight = 65
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.tableFooterView = UIView()
        
        refreshControl.addTarget(self, action: #selector(getRoomData), for: .valueChanged)
        
        self.tableView.addSubview(refreshControl)
        let swipe = UISwipeGestureRecognizer(target: self, action: #selector(self.swipeRecognizer))
        swipe.direction = ([.down])
        self.view.addGestureRecognizer(swipe)
        
        
        
        
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        if(MoveStruct.isMove == true){
            MoveStruct.isMove = false
            //self.view.makeToast(MoveStruct.message)
            
            
            self.showSingleButtonAlertWithoutAction(title: MoveStruct.message)
        }
        self.tabBarController?.tabBar.isHidden = true
        self.title = "Room Request"
        let nav = self.navigationController?.navigationBar
        nav?.tintColor = UIColor.white
       nav?.barTintColor = UIColor(hexString: "2c3e50", alpha: 1.0)
        nav?.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        
    }
    var reachability = Reachability()!
    @objc func swipeRecognizer(_ sender: UISwipeGestureRecognizer) {
        if (sender.direction == .down) {
            
            if(self.reachability.connection != .none)
            {
                
                self.getRoomData()
            }
            else
            {
                self.startLoading(view: self.view)
                DispatchQueue.main.asyncAfter(deadline: .now() + 4.5) {
                    self.noDataLabel(text: "No internet connection found. Check your internet connection and try again.")
                    self.stopLoading(view: self.view)
                }
            }
            
        }
    }
    @IBAction func tapViewClicked(_ sender: Any) {
        
        
        
        
        
        
        
    }
    @objc func tapViewTapClicked(_ sender: UITapGestureRecognizer) {
        let buttonPosition:CGPoint = sender.view!.convert(.zero, to:self.tableView)
        let indexPath = self.tableView.indexPathForRow(at: buttonPosition)
        
        let storyBoard = UIStoryboard(name: "GuestHouse", bundle: nil)
        let ZIVC = storyBoard.instantiateViewController(withIdentifier: "RoomRequestDetailsViewController") as! RoomRequestDetailsViewController
        ZIVC.RoomRequestDB = self.RoomDB[(indexPath?.row)!]
        ZIVC.statusData = valueData
        ZIVC.typeDataVal = typeData
        self.navigationController?.pushViewController(ZIVC, animated: true)
//
        
    }
    @IBOutlet var tapView: UITapGestureRecognizer!
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    var data: String?
    var lastObject: String?
    var indPath = IndexPath()
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        
        
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if(section == 0) {
            return 10.0
        } else {
            return 20.0
        }
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(self.RoomDB.count > 0){
        return self.RoomDB.count
        }else{
            return 0
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! RoomRequestReportedTableViewCell
        
        let TapGesture = UITapGestureRecognizer(target: self, action: #selector(self.tapViewTapClicked(_:)))
        cell.addGestureRecognizer(TapGesture)
        cell.isUserInteractionEnabled = true
        TapGesture.numberOfTapsRequired = 1
        cell.textViewGuestHouse.text = self.RoomDB[indexPath.row].HouseName
        cell.textViewName.text = self.RoomDB[indexPath.row].Name
        if(self.RoomDB[indexPath.row].Contact == ""){
            cell.textViewMobileHeight.constant = 0
        }else{
            cell.textViewMobileHeight.constant = 30
        }
        cell.textViewMobile.text = self.RoomDB[indexPath.row].Contact
        print(self.RoomDB[indexPath.row].Purpose)
cell.textViewDesc.text = self.RoomDB[indexPath.row].Purpose
//        cell.lblNameNo.text = self.RoomDB[indexPath.row].Name
//        cell.lblContact.text = self.RoomDB[indexPath.row].Contact
        cell.containerView.layer.cornerRadius = 10
        
        let milisecondDate = self.RoomDB[indexPath.row].ReqDate
        let dateVarDate = Date.init(timeIntervalSince1970: TimeInterval(milisecondDate)!/1000)
        let dateFormatterday = DateFormatter()
        dateFormatterday.dateFormat = "dd"
        let valDate =  dateFormatterday.string(from: dateVarDate)
        cell.lblDateTop.text = valDate
        let dateFormattermon = DateFormatter()
        dateFormattermon.dateFormat = "MMM"
        let valDateMon =  dateFormattermon.string(from: dateVarDate)
        cell.lblMonthTop.text = valDateMon
        
        let dateFormatterWeekDay = DateFormatter()
        dateFormatterWeekDay.dateFormat = "EEEE"
        let valDateWeekDay =  dateFormatterWeekDay.string(from: dateVarDate)
        cell.lblDay.text = valDateWeekDay
        
        
        
        
        
        let milisecondCheckIn = self.RoomDB[indexPath.row].CheckInDate
        let dateVarCheckIn = Date.init(timeIntervalSince1970: TimeInterval(milisecondCheckIn)!/1000)
        let dateFormatterdayCheckIn = DateFormatter()
        dateFormatterdayCheckIn.dateFormat = "dd"
        let valDateCheckIn =  dateFormatterdayCheckIn.string(from: dateVarCheckIn)
        cell.lblDateFor.text = valDateCheckIn
        let dateFormattermonCheckIn = DateFormatter()
        dateFormattermonCheckIn.dateFormat = "MMM"
        let valDateMonCheckIn =  dateFormattermonCheckIn.string(from: dateVarCheckIn)
        cell.lblMonthFor.text = valDateMonCheckIn
        
  
        
        
     
        
        
        
        
        
        
        let milisecondCheckOut = self.RoomDB[indexPath.row].CheckOutDate
        let dateVarCheckOut = Date.init(timeIntervalSince1970: TimeInterval(milisecondCheckOut)!/1000)
        let dateFormatterdayCheckOut = DateFormatter()
        dateFormatterdayCheckOut.dateFormat = "dd"
        let valDateCheckOut =  dateFormatterdayCheckOut.string(from: dateVarCheckOut)
        cell.lblDateTo.text = valDateCheckOut
        let dateFormattermonCheckOut = DateFormatter()
        dateFormattermonCheckOut.dateFormat = "MMM"
        let valDateMonCheckOut =  dateFormattermonCheckOut.string(from: dateVarCheckOut)
        cell.lblMonthTo.text = valDateMonCheckOut
   
        
        
       
        
        
        self.data = String(self.RoomDB[indexPath.row].ID)
        self.lastObject = String(self.RoomDB[indexPath.row].ID)
        
        if ( self.data ==  self.lastObject && indexPath.section == self.RoomDB.count - 1)
        {
            
            self.getRoomDataLoadMore( ID: String(Int(self.RoomDB[indexPath.row].ID)))
            
        }
        cell.separatorInset = UIEdgeInsetsMake(0, 10000, 0, 0);
        cell.selectionStyle = .none
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableViewAutomaticDimension
        
    }
    
    @objc func getRoomData(){
        var param = [String:String]()
        let pno : String = (UserDefaults.standard.string(forKey: "EmployeeID")!)
        let Profile_AuthKey = UserDefaults.standard.string(forKey: "Profile_AuthKey")!
        param = ["UserID": pno ,"AuthKey": Profile_AuthKey , "Type" : typeData  ]
      
        roomAPI.serviceCalling(obj: self, urlStr: showUrlData,  param: param ) { (dict) in
            
            self.RoomDB = dict as! [RoomRequestReportedDataModel]
            
            self.tableView.reloadData()
            
        }
    }
    var countData = 1
    @objc func getRoomDataLoadMore(ID : String){
        var param = [String:String]()
        let pno : String = (UserDefaults.standard.string(forKey: "EmployeeID")!)
        let Profile_AuthKey = UserDefaults.standard.string(forKey: "Profile_AuthKey")!
        param = ["UserID": pno ,"AuthKey": Profile_AuthKey , "Type" : typeData ,"pn":String(countData) ]
       
        roomLoadMoreAPI.serviceCalling(obj: self, urlStr: showUrlData ,  param: param ) { (dict) in
            
            self.roomLoadMoreDB =  [RoomRequestReportedDataModel]()
            self.roomLoadMoreDB = dict as! [RoomRequestReportedDataModel]
            
            switch dict.count {
            case 0:
                //self.countData = self.countData + 1
                break;
            default:
                self.RoomDB.append(contentsOf: self.roomLoadMoreDB)
                self.tableView.reloadData()
                self.countData = self.countData + 1
                break;
            }
            
        }
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
