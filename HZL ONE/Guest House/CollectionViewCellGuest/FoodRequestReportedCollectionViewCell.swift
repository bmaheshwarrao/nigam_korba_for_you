//
//  FoodRequestReportedCollectionViewCell.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 23/02/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit

class FoodRequestReportedCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var textViewNameNo: UITextView!
    @IBOutlet weak var textViewGuest: UITextView!
    @IBOutlet weak var containerView: UIViewX!
}
