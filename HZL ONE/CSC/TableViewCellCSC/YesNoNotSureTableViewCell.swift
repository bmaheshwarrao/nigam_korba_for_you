//
//  YesNoNotSureTableViewCell.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 20/02/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit
import DLRadioButton
class YesNoNotSureTableViewCell: UITableViewCell {
    @IBOutlet weak var btnNo: DLRadioButton!
    @IBOutlet weak var btnYes: DLRadioButton!
    @IBOutlet weak var btnNotSure: DLRadioButton!
    @IBOutlet weak var lblNumber: UILabel!
    @IBOutlet weak var textViewQuestion: UITextView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
