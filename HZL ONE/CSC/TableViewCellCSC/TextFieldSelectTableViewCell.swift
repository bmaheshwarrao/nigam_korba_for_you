//
//  TextFieldSelectTableViewCell.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 07/03/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit

class TextFieldSelectTableViewCell: UITableViewCell,UITextFieldDelegate {
    
    @IBOutlet weak var lblNumber: UILabel!
    @IBOutlet weak var lblLine: UILabel!
    @IBOutlet weak var textFieldAnswer: UITextField!
    @IBOutlet weak var textViewQuestions: UITextView!
    override func awakeFromNib() {
        super.awakeFromNib()
        textFieldAnswer.delegate = self;
        textFieldAnswer.keyboardType = UIKeyboardType.numberPad
        // Initialization code
    }
   
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
