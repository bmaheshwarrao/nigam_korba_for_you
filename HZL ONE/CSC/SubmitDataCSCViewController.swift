//
//  SubmitDataCSCViewController.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 20/02/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit

class SubmitDataCSCViewController: CommonVSClass ,UITextFieldDelegate{

    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func btnAreaClicked(_ sender: UIButton) {
        if(FilterDataFromServer.unit_Name != "") {
            
            
            FilterDataFromServer.filterType = "Area"
            FilterDataFromServer.area_Id = Int()
            FilterDataFromServer.area_Name = String()
            
            FilterDataFromServer.sub_Area_Id = Int()
            FilterDataFromServer.sub_Area_Name = String()
            updateData()
            
            let storyBoard = UIStoryboard(name: "ThemeSafety", bundle: nil)
            let ZIVC = storyBoard.instantiateViewController(withIdentifier: "ThemeBasedASUZViewController") as! ThemeBasedASUZViewController
            ZIVC.titleStr = "Select Area"
            self.navigationController?.pushViewController(ZIVC, animated: true)
            
            
            
            
        }
    }
    @IBAction func btnUnitClicked(_ sender: UIButton) {
       
            FilterDataFromServer.filterType = "Unit"
            FilterDataFromServer.unit_Id = Int()
            FilterDataFromServer.unit_Name = String()
            FilterDataFromServer.area_Id = Int()
            FilterDataFromServer.area_Name = String()
            FilterDataFromServer.sub_Area_Id = Int()
            FilterDataFromServer.sub_Area_Name = String()
            updateData()
            
            
            let storyBoard = UIStoryboard(name: "ThemeSafety", bundle: nil)
            let ZIVC = storyBoard.instantiateViewController(withIdentifier: "ThemeBasedASUZViewController") as! ThemeBasedASUZViewController
            ZIVC.titleStr = "Select Unit"
            self.navigationController?.pushViewController(ZIVC, animated: true)
        
    }
    @IBAction func btnDepartmentClicked(_ sender: UIButton) {
        if(FilterDataFromServer.unit_Name != "" && FilterDataFromServer.area_Name != ""  ) {
            FilterDataFromServer.filterType = "Sub Area"
            
            
            FilterDataFromServer.sub_Area_Id = Int()
            FilterDataFromServer.sub_Area_Name = String()
            updateData()
            let storyBoard = UIStoryboard(name: "ThemeSafety", bundle: nil)
            let ZIVC = storyBoard.instantiateViewController(withIdentifier: "ThemeBasedASUZViewController") as! ThemeBasedASUZViewController
            ZIVC.titleStr = "Select Area Location"
            self.navigationController?.pushViewController(ZIVC, animated: true)
        }
    }
    
    
     @IBAction func btnSubmitClicked(_ sender: UIButton) {
        
        if cellData.textVisitDate.text == ""
        {
            self.view.makeToast("Please Select Visit date.")
           
        } else if cellData.lblUnit.text == ""
        {
            self.view.makeToast("Please Select Unit.")
          
        }  else if cellData.lblArea.text == ""
        {
            self.view.makeToast("Please Select Area.")
            
        } else if cellData.lblDept.text == ""
        {
            self.view.makeToast("Please Select Area Location.")
            
        }  else if cellData.textCompany.text == ""
        {
            self.view.makeToast("Please Enter Person a Company.")
            
        } else {
            
            
            
            
            
            
            
            SubmitMethod()
            
            
        }
        
        
    }
    var reachability = Reachability()!
    func SubmitMethod() {
        if(self.reachability.connection != .none) {
            
         
            let zoneId  = UserDefaults.standard.string(forKey: "zoneId")
            let visitId  = UserDefaults.standard.string(forKey: "visitId")
            
            
            let parametersToSet = ["No_Safe_acts":QuestionsData.text1,"Level_housekeeping_observed":QuestionsData.Rate1,"Rate_structural_and_equipment":QuestionsData.Rate2,"No_UA":QuestionsData.text2,"No_UC":QuestionsData.text3,"Safety_issues_identified_with_potential_Fatalities":QuestionsData.text4,"Safety_issues_with_potential_with_serious_injuries":QuestionsData.text5,"Rate_overall_awareness_people":QuestionsData.Rate3,"Aware_recent_incidents_happened":QuestionsData.conditionYN1,"updated_passport_was_available_with_them":QuestionsData.conditionYN2 , "Impact_workers_safe_behaviour":QuestionsData.conditionYN3 ,"Improvement_observed_with_respect_last_visit": QuestionsData.conditionYN4 ,"LSR_Violation_observed":QuestionsData.text6 ,"Ramp_condition":QuestionsData.conditionGAB1 , "Ventilation":QuestionsData.conditionGAB2 ,"GCPM_Compliance":QuestionsData.conditionGAB3 ,"Competency_MM_OPERATORS":QuestionsData.conditionGAB4 ,"Unsafe_Acts_observed":QuestionsData.popVal ,"Date_Time_Visit":FilterGraphStruct.visitDate ,"Site_Name":"NA","Name_Area_Incharge":cellData.textCompany.text! ,"Unit_ID": String(FilterDataFromServer.area_Id),"Area_ID": String(FilterDataFromServer.unit_Id) , "Area_Location_ID": String(FilterDataFromServer.sub_Area_Id) , "Zone_ID": zoneId , "Visit_ID": visitId
                ] as! [String : String]
            print(parametersToSet)
            
            self.startLoadingPK(view: self.view)
            
            WebServices.sharedInstances.sendPostRequest(url: URLConstants.visitSubmit,parameters: parametersToSet, successHandler: { (dict) in
                
                self.stopLoadingPK(view: self.view)
                
                print("",dict)
                
                if let response = dict["response"] {
                    
                    let statusString : String = response["status"] as! String
                    let objectmsg = MessageCallServerModel()
            let msg = objectmsg.sealizeMessage(cell: response as! [String : AnyObject])
                    
                    if(statusString == "success")
                    {
                          self.stopLoadingPK(view: self.view)
                        MoveStruct.isMove = true
                        MoveStruct.message = msg
                        self.navigationController?.popViewController(animated: false)
                    }
                    else
                    {
                         self.stopLoadingPK(view: self.view)
                       self.view.makeToast("Data not saved")
                    }
                }
                
                
                
                
            }, failureHandler: { (error) in
                
                
                 self.stopLoadingPK(view: self.view)
                
                self.errorChecking(error: error[0])
                
                
                
            })
            
            
            
        }
        else {
            
            self.showSingleButtonWithMessage(title: "No Network!", message: "No internet connection found. Check your internet connection or try again.", buttonName: "OK")
            
            
            
        }
        
    }
    
    
    
    
    
    @objc func updateData(){
        if(FilterDataFromServer.sub_Area_Name != "") {
            cellData.lblDept.text = FilterDataFromServer.sub_Area_Name
            cellData.lblDept.textColor = UIColor.black
        } else {
            cellData.lblDept.text = "Select Area Location"
            cellData.lblDept.textColor = UIColor.lightGray
        }
        if(FilterDataFromServer.unit_Name != "") {
            cellData.lblUnit.text = FilterDataFromServer.unit_Name
            cellData.lblUnit.textColor = UIColor.black
        } else {
            cellData.lblUnit.text = "Select Unit"
            cellData.lblUnit.textColor = UIColor.lightGray
        }
        if(FilterDataFromServer.area_Name != "") {
            cellData.lblArea.text = FilterDataFromServer.area_Name
            cellData.lblArea.textColor = UIColor.black
        } else {
            cellData.lblArea.text = "Select Area"
            cellData.lblArea.textColor = UIColor.lightGray
        }
        
        // tableView.reloadData()
    }
    var cellData : SubmitDataTableViewCell!
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.title = "Submit"
        
        let nav = self.navigationController?.navigationBar
        nav?.tintColor = UIColor.white
       nav?.barTintColor = UIColor(hexString: "2c3e50", alpha: 1.0)
        nav?.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
         if(FilterDataFromServer.sub_Area_Name != "" || FilterDataFromServer.unit_Name != "" || FilterDataFromServer.area_Name != "") {
            updateData()
        }
        
        
        
        
    }
    
    
    func pickUpFromDate(_ textField : UITextField){
        
        // DatePicker
        self.datePicker = UIDatePicker(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
        self.datePicker.backgroundColor = UIColor.white
        self.datePicker.datePickerMode = UIDatePickerMode.date
//        if FilterGraphStruct.toDate.isEmpty == false{
//            dateValidation(datePicker: self.datePicker, date: FilterGraphStruct.toDate, type: "from")
//        }
        textField.inputView = self.datePicker
        
        // ToolBar
        let toolBar = UIToolbar()
        toolBar.barStyle = .default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
        toolBar.sizeToFit()
        
        // Adding Button ToolBar
        
        let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(self.doneClickFromTF))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(self.cancelClick))
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        textField.inputAccessoryView = toolBar
        
    }
    
   
    
    func dateValidation(datePicker:UIDatePicker, date:String, type:String) {
        
        let dateFormatterFinal = DateFormatter()
        dateFormatterFinal.dateFormat = "yyyy/MM/dd"
        let minDate: Date =  dateFormatterFinal.date(from: date)!
        switch type {
        case "to":
            datePicker.minimumDate = minDate
            break;
        default:
            datePicker.maximumDate = minDate
            break;
        }
        
    }
    var datePicker : UIDatePicker!
    @objc func doneClickFromTF() {
        
        let dateFormatter1 = DateFormatter()
        dateFormatter1.dateStyle = .medium
        dateFormatter1.timeStyle = .none
        cellData.textVisitDate.text = dateFormatter1.string(from: datePicker.date)
        let dateFormatterFinal = DateFormatter()
        dateFormatterFinal.timeZone = NSTimeZone.system
        dateFormatterFinal.dateFormat = "yyyy/MM/dd"
        FilterGraphStruct.visitDate = dateFormatterFinal.string(from: datePicker.date)
        
        cellData.textVisitDate.resignFirstResponder()
        
    }
    
  
    
    @objc func cancelClick() {
        
        cellData.textVisitDate.resignFirstResponder()
        //
       
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if textField == cellData.textVisitDate{
            self.pickUpFromDate(cellData.textVisitDate)
        }else
        {
            //self.toDateTF.resignFirstResponder()
        }
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
extension SubmitDataCSCViewController : UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 619
        
    }
}
extension SubmitDataCSCViewController : UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! SubmitDataTableViewCell
        
        cellData = cell
      
       
        let dateFormatter2 = DateFormatter()
        dateFormatter2.timeZone = NSTimeZone.system
        dateFormatter2.dateFormat = "dd-MMM-yyyy"
        
        
            let date_TimeStr = dateFormatter2.string(from: Date())
            
            cell.textVisitDate.text = date_TimeStr

        updateData()
    cell.textVisitDate.delegate = self
        cell.separatorInset = UIEdgeInsetsMake(0, 10000, 0, 0);
        return cell
        
        
        
    }
}
