//
//  CSCFirstViewController.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 19/02/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit
var moveLight : Int = 0
class CSCFirstViewController: CommonVSClass {
var StrNav = String()
    @IBOutlet weak var textVieewMsg: UITextView!
    var reachablity = Reachability()!
    override func viewDidLoad() {
        super.viewDidLoad()
        if(reachablity.connection != .none){
        MemberResult()
            btnValidate.isHidden = true
            textVieewMsg.isHidden = true
        }else{
            btnValidate.isHidden = false
            textVieewMsg.isHidden = false
            textVieewMsg.text = "Internet is not available , Please check you internet connection and try again."
        }

        
        QuestionsData.text1 = String()
        QuestionsData.text2 = String()
        QuestionsData.text3 = String()
        QuestionsData.text4 = String()
        QuestionsData.text5 = String()
        QuestionsData.text6 = String()
        
        QuestionsData.conditionYN1 = String()
        QuestionsData.conditionYN2 = String()
        QuestionsData.conditionYN3 = String()
        QuestionsData.conditionYN4 = String()
        QuestionsData.Rate1 = String()
        QuestionsData.Rate2 = String()
        QuestionsData.Rate3 = String()
        QuestionsData.popVal = String()
        
        QuestionsData.conditionGAB1 = String()
        QuestionsData.conditionGAB2 = String()
        QuestionsData.conditionGAB3 = String()
        QuestionsData.conditionGAB4 = String()
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        if(moveLight == 1){
            self.navigationController?.popViewController(animated: false)
        }
        
        self.tabBarController?.tabBar.isHidden = true
        self.title = StrNav
        let nav = self.navigationController?.navigationBar
        nav?.tintColor = UIColor.white
       nav?.barTintColor = UIColor(hexString: "2c3e50", alpha: 1.0)
        nav?.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
    }
    @IBOutlet weak var btnValidate: UIButton!
    @IBAction func btnValidateUserClicked(_ sender: UIButton) {
        MemberResult()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    var memberDB:[MemberModel] = []
  
    var MemberAPI = MemberDataAPI()
    
    var loginVisitDB:[LoginVisitCscModel] = []
    
    var loginVisitAPI = LoginVisitDataAPI()
    @objc func MemberResult() {
        var para = [String:String]()
        let parameter = ["Employee_ID":UserDefaults.standard.string(forKey: "EmployeeID")!]
        
        para = parameter.filter { $0.value != ""}
        print("para",para)
        self.MemberAPI.serviceCalling(obj: self, parameter: para) { (dict) in
            
            self.memberDB = [MemberModel]()
            self.memberDB = dict as! [MemberModel]
            
            if(self.memberDB.count > 0){
            if(self.memberDB[0].IS_App_Member == "Yes"){
                
                
                self.loginVisitAPI.serviceCalling(obj: self) { (dict) in
                    
                    self.loginVisitDB = [LoginVisitCscModel]()
                    self.loginVisitDB = dict as! [LoginVisitCscModel]
                    if(self.loginVisitDB.count > 0){
                        if(self.loginVisitDB[0].Visit_Area_Name != ""){
                            UserDefaults.standard.set(self.loginVisitDB[0].Visit_Area_Name, forKey: "visitArea")
                             UserDefaults.standard.set(self.loginVisitDB[0].Zone_ID, forKey: "zoneId")
                            UserDefaults.standard.set(self.loginVisitDB[0].ID, forKey: "visitId")
                            FilterDataFromServer.location_id = Int(self.loginVisitDB[0].Zone_ID!)!
                            let storyBoard : UIStoryboard = UIStoryboard(name: "CSC",bundle : nil)
                            let questionVC = storyBoard.instantiateViewController(withIdentifier: "QuestionAnswerViewController") as! QuestionAnswerViewController
                            self.navigationController?.pushViewController(questionVC, animated: true)
                        }
                }
                
                
                
                
                
            }
            }
          
        }
        
    }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
