//
//  AddObservationViewController.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 06/02/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit
import Alamofire

class AddObservationViewController: CommonVSClass, UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    

    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        
        
     
        // Do any additional setup after loading the view.
    }
var statusType = ["Open", "Close"]
    @IBAction func btnStatusClicked(_ sender: UIButton) {
        
        DispatchQueue.main.async(execute: {() -> Void in
            let popup = PopUpTableViewController()
            
            popup.itemsArray = self.statusType
            
            popup.sourceView = self.cellData.lblStatus
            popup.isScroll = true
            popup.backgroundColor = UIColor.white
            if popup.itemsArray.count > 5{
                popup.popUpHeight = 200
            }
            else{
                popup.popUpHeight = CGFloat(popup.itemsArray.count)*45
            }
            popup.popUpWidth = UIScreen.main.bounds.size.width-60
            popup.backgroundImage = nil
            popup.itemTitleColor = UIColor.white
            popup.itemSelectionColor = UIColor.lightGray
            popup.arrowDirections = .any
            popup.arrowColor = UIColor.white
            popup.popCellBlock = {(_ popupVC: PopUpTableViewController, _ popupCell: UITableViewCell, _ row: Int, _ section: Int) -> Void in
                
                
                
                
                
                self.cellData.lblStatus.text = self.statusType[row]
                self.cellData.lblStatus.textColor = UIColor.black
                self.updateConst()
                
                popupVC.dismiss(animated: false, completion: nil)
                
            }
            self.present(popup, animated: true, completion: {() -> Void in
            })
            
            
        })
        
    }
     let reachablty = Reachability()!
    @IBAction func btnAddObClicked(_ sender: UIButton) {

      
        if reachablty.connection == .none{
            
            self.view.makeToast("Internet is not available, please check your internet connection try again.")
            
        }
        
       else if (cellData.textViewObservation.text == "") {
            
            self.view.makeToast("Please Write Observation")
        }
        else if (cellData.textViewSuggestion.text == "") {
            
            self.view.makeToast("Please Write Suggestion")
        }else if (cellData.lblStatus.text == "") {
            
            self.view.makeToast("Please Select Status")
        }else if (cellData.lblStatus.text == "Open" && FilterDataFromServer.dept_Name == String()) {
            
            self.view.makeToast("Please Select Department")
        }else{
            
            
            
            self.startLoadingPK(view: self.view)
            
            
            
      
            
            let parameter = ["Observation":cellData.textViewObservation.text,
                             "Suggestion":cellData.textViewSuggestion.text ,
                             "Status":cellData.lblStatus.text!,
                             "Responsible_Department_ID": String(FilterDataFromServer.dept_Id),
                             "Temp_ID":FilterDataFromServer.uniqueId
                ] as [String:String]
            
            print(parameter)
            let jsonData: Data? = try? JSONSerialization.data(withJSONObject: parameter, options: [])
            let para: [String: Data] = ["data": jsonData!]
            
            Alamofire.upload(multipartFormData: { multipartFormData in
                
                if !(self.imagedata == nil) {
                    var res : UIImage = UIImage()
                    // res =  (self.imgViewDrop.image?.resizedTo1MB())!
                    let size = CGSize(width: 150, height: 150)
                    res = self.imageResize(image: self.cellData.imageViewPhoto.image!,sizeChange: size)
                    
                    self.imagedata = UIImageJPEGRepresentation(res, 1.0)!
                    multipartFormData.append(self.imagedata!, withName: "file", fileName: "file.jpg", mimeType: "image/jpeg")
                }
                
                for (key, value) in para {
                    multipartFormData.append(value, withName: key)
                    
                    
                }
                
            },
                             to:URLConstants.Observations_Add)
            { (result) in
                switch result {
                case .success(let upload, _, _):
                    
                    upload.uploadProgress(closure: { (progress) in
                        
                    })
                    
                    
                    upload.responseJSON { response in
                        
                        
                        if let json = response.result.value
                        {
                            var dict = json as! [String: AnyObject]
                            
                            if let response = dict["response"]{
                                print(response)
                                let statusString = response["status"] as! String
                                let objectmsg = MessageCallServerModel()
                    let msg = objectmsg.sealizeMessage(cell: response as! [String : AnyObject])
                                print("HZl Dict :",dict)
                                if(statusString == "success")
                                {
                                    
                                    
                                    self.stopLoadingPK(view: self.view)
                                    MoveStruct.isMove = true
                                   
                                        MoveStruct.message = "Successfully Added Observation."
                                    let obData = SubmitObNDSO()
                                    if(self.imagedata == nil){
                                        obData.image = UIImage(named: "placed")!
                                    }else{
                                        obData.image = self.cellData.imageViewPhoto.image!
                                    }
                                    
                                    obData.observation = self.cellData.textViewObservation.text
                                    obData.suggestion = self.cellData.textViewSuggestion.text
                                    obData.status = self.cellData.lblStatus.text!
                                    dataArrayObNDSO.append(obData)
                                    self.navigationController?.popViewController(animated: false)
                                  
                                    
                                    
                                    
                                }
                                else{
                                    
                                    self.stopLoadingPK(view: self.view)
                                    self.view.makeToast(msg)
                                    
                                }
                                
                                
                                
                            }
                            
                            
                        }
                        
                        
                    }
                    
                case .failure(let encodingError):
                    
                    self.stopLoadingPK(view: self.view)
                    self.errorChecking(error: encodingError)
                    
                    print(encodingError.localizedDescription)
                }
            }
        }

        
        
    }
    
    
    @IBAction func btnDepartmentClicked(_ sender: UIButton) {
        FilterDataFromServer.filterType = "Department"
      
        
        updateData()
        
        
        let storyBoard = UIStoryboard(name: "NDSO", bundle: nil)
        let ZIVC = storyBoard.instantiateViewController(withIdentifier: "NDSOASUZViewController") as! NDSOASUZViewController
        ZIVC.titleStr = "Select Department"
        self.navigationController?.pushViewController(ZIVC, animated: true)
    }
    
    @objc func updateData(){
        if(FilterDataFromServer.dept_Name != "") {
            cellData.lblDept.text = FilterDataFromServer.dept_Name
            cellData.lblDept.textColor = UIColor.black
        } else {
            cellData.lblDept.text = "Select Department"
            cellData.lblDept.textColor = UIColor.lightGray
        }
       
        
        // tableView.reloadData()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.title = "Add Observation"
        
        let nav = self.navigationController?.navigationBar
        nav?.tintColor = UIColor.white
       nav?.barTintColor = UIColor(hexString: "2c3e50", alpha: 1.0)
        nav?.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        if(FilterDataFromServer.dept_Name != ""){
            updateData()
        }
        //updateData()
        
        
    }
    
   
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    @IBAction func btnImageButtonClicked(_ sender: UIButton) {
        
        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
            self.openCamera()
        }))
        
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
            self.openGallary()
        }))
        
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
        
    }
    
    
    @objc func pressCamera(_ sender : UITapGestureRecognizer) {
       
    }
     let imagePicker = UIImagePickerController()
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera))
        {
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func openGallary()
    {
        imagePicker.delegate = self
        imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        imagePicker.allowsEditing = true
        self.present(imagePicker, animated: true, completion: nil)
    }
    internal func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            cellData.imageViewPhoto.image = image
            imagedata = UIImageJPEGRepresentation(image, 1.0)!
            cellData.btnCamera.isHidden = true
            cellData.lblOptional.isHidden = true
            cellData.btnClose.isHidden = false
            cellData.imageViewPhoto.isHidden = false
            updateConst()
            
            
        }else{
            print("Something went wrong")
        }
        self.dismiss(animated: true, completion: nil)
    }
    
   // var reachability = Reachability()!
      var imagedata: Data? = nil
    @IBAction func btnCloseButtonClicked(_ sender: UIButton) {
       
        self.imagedata = nil
        self.cellData.btnClose.isHidden = true
        cellData.btnCamera.isHidden = false
        cellData.lblOptional.isHidden = false
       updateConst()
    }
    func updateConst(){
        print(self.cellData.lblStatus.text)
        if(self.cellData.lblStatus.text == "Close" || self.cellData.lblStatus.text == ""){
            self.cellData.heightRDept.constant = 0
            self.cellData.heightLblRDept.constant = 0
            self.cellData.heightImageRDept.constant = 0
            self.cellData.topViewConstraint.constant = 30
            self.cellData.lblRDept.isHidden = true
             self.cellData.lblDept.isHidden = true
            self.cellData.imgDept.isHidden = true
            
            if(self.cellData.btnClose.isHidden == true){
                self.cellData.heightViewofClose.constant = 0
                self.cellData.heightImageselected.constant = 0
                self.cellData.heightViewImage.constant = 40
            }else{
                self.cellData.heightViewofClose.constant = 20
                self.cellData.heightImageselected.constant = 100
                self.cellData.heightViewImage.constant = 120
            }
            
        }else{
            self.cellData.heightRDept.constant = 40
            self.cellData.heightLblRDept.constant = 21
            self.cellData.heightImageRDept.constant = 20
            self.cellData.topViewConstraint.constant = 88
            self.cellData.lblRDept.isHidden = false
            self.cellData.lblDept.isHidden = false
            self.cellData.imgDept.isHidden = false
            if(self.cellData.btnClose.isHidden == true){
                self.cellData.heightViewofClose.constant = 0
                self.cellData.heightImageselected.constant = 0
                self.cellData.heightViewImage.constant = 40
            }else{
                self.cellData.heightViewofClose.constant = 20
                self.cellData.heightImageselected.constant = 100
                self.cellData.heightViewImage.constant = 120
            }
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /// Fusuma
    
  
    func imageResize (image:UIImage, sizeChange:CGSize)-> UIImage{
        
        let hasAlpha = true
        let scale: CGFloat = 0.0 // Use scale factor of main screen
        
        UIGraphicsBeginImageContextWithOptions(sizeChange, !hasAlpha, scale)
        image.draw(in: CGRect(origin: CGPoint.zero, size: sizeChange))
        
        let scaledImage = UIGraphicsGetImageFromCurrentImageContext()
        return scaledImage!
    }
    
    var indexing : Int = 1
var cellData : AddObservationTableViewCell!
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
extension AddObservationViewController : UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 619
        
    }
}
extension AddObservationViewController : UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! AddObservationTableViewCell
       
        cellData = cell
        
        if(indexing == 1){
            cellData.btnClose.isHidden = true
            indexing = indexing + 1
        }
          updateConst()
        cell.separatorInset = UIEdgeInsetsMake(0, 10000, 0, 0);
        return cell
        
        
        
    }
}
