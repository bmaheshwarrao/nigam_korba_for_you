//
//  NDSOReportedMenuViewController.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 08/02/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit
import Parchment
import ViewPager_Swift
class NDSOReportedMenuViewController: CommonVSClass {

    var views = [UIViewController]()
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
  
        createDesign()
        
        
    }
    
    
    func createDesign() {
        
        
        
        
        let  myOptions = ViewPagerOptions(viewPagerWithFrame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height))
        
        myOptions.tabType = ViewPagerTabType.basic
        
        
        
        
        
        
        myOptions.isTabHighlightAvailable = false
        
        // If I want indicator bar to show below current page tab
        myOptions.isTabIndicatorAvailable = true
        myOptions.fitAllTabsInView = true;
        // Oh! and let's change color of tab to red
        myOptions.tabIndicatorViewBackgroundColor = UIColor.white
        myOptions.tabViewBackgroundDefaultColor = UIColor.colorwithHexString("2c3e50", alpha: 1.0)
        
        myOptions.tabViewTextDefaultColor = UIColor.white
        // myOptions.tabViewTextHighlightColor = UIColor.yellow
        let viewPager = ViewPagerController()
        // viewPager.view.backgroundColor = UIColor.red
        viewPager.options = myOptions
        viewPager.dataSource = self
        
        //Now let me add this to my viewcontroller
        self.addChildViewController(viewPager)
        
        self.view.addSubview(viewPager.view)
       
        
        // self.view.addSubview(viewPager.view)
        viewPager.didMove(toParentViewController: self)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
       
        let status : String = UserDefaults.standard.value(forKey: "Status") as! String
        self.title = status
        let nav = self.navigationController?.navigationBar
        nav?.tintColor = UIColor.white
       nav?.barTintColor = UIColor(hexString: "2c3e50", alpha: 1.0)
        nav?.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
    }
    
   
    
}

extension NDSOReportedMenuViewController : ViewPagerControllerDataSource {
    func numberOfPages() -> Int {
        return 2
    }
    
    func viewControllerAtPosition(position: Int) -> UIViewController {
        let storyBoard : UIStoryboard = UIStoryboard(name: "NDSO",bundle : nil)
        if(position == 0) {
            
            let openVC = storyBoard.instantiateViewController(withIdentifier: "NDSOReportedDataViewController") as! NDSOReportedDataViewController
             UserDefaults.standard.set("Open", forKey: "staOpenClose")
            return  openVC as CommonVSClass
        }else {
            let closeVC = storyBoard.instantiateViewController(withIdentifier: "NDSOReportedDataViewController") as! NDSOReportedDataViewController
            UserDefaults.standard.set("Close", forKey: "staOpenClose")
            return  closeVC as CommonVSClass
        }
    }
    
    func tabsForPages() -> [ViewPagerTab] {
        return [ViewPagerTab(title: "OPEN" , image: #imageLiteral(resourceName: "BackBlack")) , ViewPagerTab(title: "CLOSED" , image: #imageLiteral(resourceName: "BackBlack"))]
    }
    
    
}
extension NDSOReportedMenuViewController : ViewPagerControllerDelegate {
    
}
