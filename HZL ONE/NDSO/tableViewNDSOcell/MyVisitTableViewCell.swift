//
//  MyVisitTableViewCell.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 05/02/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit

class MyVisitTableViewCell: UITableViewCell {

    @IBOutlet weak var zoneLbl: UILabel!
    @IBOutlet weak var nameEmp2Lbl: UILabel!
    @IBOutlet weak var nameEmpLbl: UILabel!
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var visitIdLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
