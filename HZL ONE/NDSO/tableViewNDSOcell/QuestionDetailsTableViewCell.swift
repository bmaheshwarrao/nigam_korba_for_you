//
//  QuestionDetailsTableViewCell.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 06/02/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit

class QuestionDetailsTableViewCell: UITableViewCell {
  @IBOutlet weak var lblUnit: UILabel!
    @IBOutlet weak var btnUnit: UIButton!
    @IBOutlet weak var viewUnit: UIView!
    
    @IBOutlet weak var viewGatepass: UIView!
    @IBOutlet weak var textGatepass: UITextField!
    
    
  
    @IBOutlet weak var textViewRemark: UITextView!
    @IBOutlet weak var lblArea: UILabel!
    @IBOutlet weak var btnArea: UIButton!
    @IBOutlet weak var viewArea: UIView!
    @IBOutlet weak var lblEmployee: UILabel!
    @IBOutlet weak var btnEmp: UIButton!
    @IBOutlet weak var viewEmp: UIView!
    @IBOutlet weak var textViewQuestion: UITextView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        
        viewEmp.layer.borderWidth = 1.0
        viewEmp.layer.borderColor = UIColor.black.cgColor
        viewEmp.layer.cornerRadius = 10.0
        
        
        viewArea.layer.borderWidth = 1.0
        viewArea.layer.borderColor = UIColor.black.cgColor
        viewArea.layer.cornerRadius = 10.0
        viewUnit.layer.borderWidth = 1.0
        viewUnit.layer.borderColor = UIColor.black.cgColor
        viewUnit.layer.cornerRadius = 10.0
        viewGatepass.layer.borderWidth = 1.0
        viewGatepass.layer.borderColor = UIColor.black.cgColor
        viewGatepass.layer.cornerRadius = 10.0
        textViewRemark.layer.borderWidth = 1.0
        textViewRemark.layer.borderColor = UIColor.black.cgColor
        textViewRemark.layer.cornerRadius = 10.0
        textGatepass.keyboardType = UIKeyboardType.numberPad
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
