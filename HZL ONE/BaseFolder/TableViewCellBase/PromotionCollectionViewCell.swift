//
//  PromotionCollectionViewCell.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 20/11/18.
//  Copyright © 2018 SARVANG INFOTCH. All rights reserved.
//

import UIKit

class PromotionCollectionViewCell:  UICollectionViewCell {
    @IBOutlet weak var imageSlide: UIImageView!
    @IBOutlet weak var imagButton: UIButton!
    
  
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        imageSlide.layer.cornerRadius = 10
        
        
    }
}
