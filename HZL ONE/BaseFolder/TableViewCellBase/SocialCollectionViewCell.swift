//
//  SocialCollectionViewCell.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 25/01/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit

class SocialCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var imageSlideData: UIImageView!
   
    
    @IBOutlet weak var btnSocial: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        imageSlideData.layer.cornerRadius = 10
        
        
    }
}
