//
//  ImageSlideCollectionViewCell.swift
//  VallSafety
//
//  Created by SARVANG INFOTCH on 18/10/18.
//  Copyright © 2018 Bunga Maheshwar Rao. All rights reserved.
//

import UIKit

class ImageSlideCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var imageSlide: UIImageView!
    @IBOutlet weak var imagButton: UIButton!
    
    @IBOutlet weak var pageControl: UIPageControl!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        
        pageControl.currentPage = 0
        
    }
}
