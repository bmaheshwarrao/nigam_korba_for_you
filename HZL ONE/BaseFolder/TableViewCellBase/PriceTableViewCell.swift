//
//  PriceTableViewCell.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 20/11/18.
//  Copyright © 2018 SARVANG INFOTCH. All rights reserved.
//

import UIKit

class PriceTableViewCell: UITableViewCell,UICollectionViewDelegate, UICollectionViewDataSource ,UICollectionViewDelegateFlowLayout{
    
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    var view = UIView()
    var timer = Timer()
    var lastXAxis = CGFloat()
    var contentOffset = CGPoint()
    var i = CGFloat()
    var shareDB :[HomeSharePageDataModel] = []
    
    var scrollingTimer = Timer()
    
    @IBOutlet weak var imageCollectionView: UICollectionView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        
        self.imageCollectionView.delegate = self
        self.imageCollectionView.dataSource = self
        
      
        
        // Initialization code
    }
    
 
    
    
    
    func pageView()
    {
        var reachablty = Reachability()!
        if(reachablty.connection != .none)
        {
            
            let param : [String:String] = [:]
            WebServices.sharedInstances.sendPostRequest(url: URLConstants.Share_Market, parameters: param , successHandler: { (dict) in
                
                if let response = dict["response"]{
                    
                    let statusString : String = response["status"] as! String
                    
                    if(statusString == "success")
                    {
                        
                        if let data = dict["data"] as? [AnyObject]
                        {
                            self.isHidden = false
                            self.shareDB = []
                            
                            for i in 0..<data.count
                            {
                                
                                if let cell = data[i] as? [String:AnyObject]
                                {
                                    let object = HomeSharePageDataModel()
                                    object.setDataInModel(str: cell)
                                    self.shareDB.append(object)
                                }
                                
                            }
                            
                            self.imageCollectionView.reloadData()
                            
                            
                        }
                        
                        
                    }
                    else
                    {
                        
                        self.isHidden = true
                    }
                    
                    
                }
                
                
                
                
            })
            { (error) in
                
                let errorStr : String = error.description
                
                print("DATA:error",errorStr)
                
                //                obj.errorChecking(error: error)
                
            }
            
            
        }
        else{
            self.isHidden = true
        }
        
        self.imageCollectionView.reloadData()
    }
    
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return shareDB.count
    }
   
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "priceShare", for: indexPath) as! PriceCollectionViewCell
       
        
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = NSTimeZone.system
        dateFormatter.dateFormat = "MMM dd yyyy"
        let date = dateFormatter.date(from: self.shareDB[indexPath.row].Date_Time)
        
        let dateFormatter2 = DateFormatter()
        dateFormatter2.timeZone = NSTimeZone.system
        dateFormatter2.dateFormat = "dd MMM yyyy"
        
        switch date {
        case nil:
            let date_TimeStr = dateFormatter2.string(from: Date())
            cell.lblDate.text = date_TimeStr
            break;
        default:
            let date_TimeStr = dateFormatter2.string(from: date!)
            
            cell.lblDate.text = date_TimeStr
            break;
        }
        
        
       
     print(shareDB[indexPath.row].Price)
         print(shareDB[indexPath.row].Unit)
         print(shareDB[indexPath.row].Name)
            cell.CompanyLbl.text = shareDB[indexPath.row].Name
            cell.PriceLbl.text = shareDB[indexPath.row].Price
cell.PriceCurrencyLbl.text = shareDB[indexPath.row].Unit
        
        return cell
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {

        return UIEdgeInsetsMake(0, 0, 0, 0)


    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
          // let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "priceShare", for: indexPath) as! PriceCollectionViewCell
        var sizeWidth = 150.0
//        if(cell.PriceLbl.intrinsicContentSize.width > cell.CompanyLbl.intrinsicContentSize.width){
//            sizeWidth = cell.PriceLbl.intrinsicContentSize.width + 30
//        }else{
//            sizeWidth = cell.CompanyLbl.intrinsicContentSize.width + 30
//        }
        return CGSize(width: sizeWidth, height: 100.0)
    }

    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        
        //print(indexPath.item)
        
        
    }
    

    
    
    
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
