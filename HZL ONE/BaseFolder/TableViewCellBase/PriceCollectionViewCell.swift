//
//  PriceCollectionViewCell.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 20/11/18.
//  Copyright © 2018 SARVANG INFOTCH. All rights reserved.
//

import UIKit

class PriceCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var PriceCurrencyLbl: UILabel!
    @IBOutlet weak var PriceLbl: UILabel!
    @IBOutlet weak var CompanyLbl: UILabel!
    @IBOutlet weak var lblDate: UILabel!
}
