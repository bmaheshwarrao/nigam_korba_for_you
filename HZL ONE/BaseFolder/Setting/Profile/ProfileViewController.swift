//
//  ProfileViewController.swift
//  Balco CRP
//
//  Created by sudheer-kumar on 12/10/17.
//  Copyright © 2017 safiqul islam. All rights reserved.
//

import UIKit
import Reachability
import CoreData
import MobileCoreServices
import Alamofire
import CropViewController

class ProfileViewController: CommonVSClass, UITableViewDelegate, UITableViewDataSource,UIImagePickerControllerDelegate,UINavigationControllerDelegate,CropViewControllerDelegate {

    @IBOutlet weak var tableView: UITableView!
    var reachablty = Reachability()!
    var imagedata: Data? = nil
    let pickerr = UIImagePickerController()
    let pickerImage = UIImage()
    var profileDB : [Profile] = []
    var indPath = IndexPath()
    
    //let ToCo = CropViewController()
    
    var Profile_AuthKey = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.tableView.delegate = self
        self.tableView.dataSource = self
        
        
        
        self.tableView.tableFooterView = UIView()
        
        
        self.Profile_AuthKey = UserDefaults.standard.value(forKey: "Profile_AuthKey") as! String
        NotificationCenter.default.addObserver(self, selector: #selector(self.profileDetailsData), name: NSNotification.Name(rawValue: "SettingsUpdate"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.getProfileDetailsData), name: NSNotification.Name(rawValue: "SettingsUpdate"), object: nil)
        
        pickerr.delegate = self
        
      
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        profileDetailsData()
        getProfileDetailsData()
        
        self.tabBarController?.tabBar.isHidden = true
        
        self.title = "Profile"
        let nav = self.navigationController?.navigationBar
        nav?.tintColor = UIColor.white
       nav?.barTintColor = UIColor(hexString: "2c3e50", alpha: 1.0)
        nav?.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       
       
            if(profileDB.count == 0){
                return 1
            }else{
               return profileDB.count
            }
          
       
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0{
            
           let cell = tableView.dequeueReusableCell(withIdentifier: "profileDetails") as! ProfileDetailsTableViewCell
            
            indPath = indexPath
            
            
            
            switch self.profileDB.count {
            case 1:
                
                if let url = NSURL(string: profileDB[indexPath.row].profile_Pic_Path!) {
                    cell.userImage.sd_setImage(with: url as URL!, placeholderImage: UIImage.init(named: "profile"))
                }
                
                
                break;
            default:
                if profileDB.count>0{
                if let url = NSURL(string: profileDB[0].profile_Pic_Path!) {
                    cell.userImage.sd_setImage(with: url as URL!, placeholderImage: UIImage.init(named: "profile"))
                }else{
                cell.userImage.image = UIImage.init(named: "profile")
            }
                }
                break;
            }
            if(self.profileDB.count > 0){
            cell.userName.text = self.profileDB[indexPath.row].employee_Name
            }else{
                cell.userName.text = "Employee Name"
            }
            cell.profileWidth.constant = 80
            cell.profileHeight.constant = 80
           
            return cell
            
        }else if indexPath.section == 1{
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "mobileEdit") as! MobileEditTableViewCell
             if(self.profileDB.count > 0){
            cell.userName.text = self.profileDB[indexPath.row].employeeId
            cell.mobileNo.text = self.profileDB[indexPath.row].mobile_Number
            cell.personalNo.text = "Employee Id"
            cell.gender.text = self.profileDB[indexPath.row].gender
        }else{
                cell.userName.text = "***"
                cell.mobileNo.text = "***"
                cell.personalNo.text = "Employee Id"
                cell.gender.text = "***"
        }
            return cell
            
        }else{
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "personalDetailsNew") as! PersonalDetailsTableViewCell
            
            if(self.profileDB.count > 0){
            cell.emailId.text = self.profileDB[indexPath.row].email_ID_Official!
                if(self.profileDB[indexPath.row].unit_Name! == ""){
                     cell.lblUnit.text = "***"
                }else{
                   cell.lblUnit.text = self.profileDB[indexPath.row].unit_Name!
                }
                if(self.profileDB[indexPath.row].area_Name! == ""){
                     cell.lblArea.text = "***"
                }else{
                    cell.lblArea.text = self.profileDB[indexPath.row].area_Name!
                }
                if(self.profileDB[indexPath.row].zone_Name! == ""){
                     cell.lblZone.text = "***"
                }else{
                      cell.lblZone.text = self.profileDB[indexPath.row].zone_Name!
                }
                if(self.profileDB[indexPath.row].designation_Name! == ""){
                     cell.lblDesignation.text = "***"
                }else{
                    cell.lblDesignation.text = self.profileDB[indexPath.row].designation_Name!
                }
            
            
          
            
          //  cell.emailid.text = self.profileDB[indexPath.row].email_ID_Official
//            cell.SBUName.text = self.profileDB[indexPath.row].sBU_Name
//            cell.department.text = self.profileDB[indexPath.row].department_Name
//            cell.designation.text = self.profileDB[indexPath.row].designation_Name
        }else{
            cell.emailId.text = "***"
            cell.lblUnit.text = "***"
            cell.lblArea.text = "***"
            cell.lblZone.text = "***"
                cell.lblDesignation.text = "***"
        }
         
            return cell
            
        }
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0{
           return 150
        }else if indexPath.section == 1{
            return 212
        }else{
           return 330
            
        }
    }

    @IBAction func editBtn(_ sender: Any) {
        
        let mainVC = self.storyboard?.instantiateViewController(withIdentifier: "RegisterViewController") as! RegisterViewController
        mainVC.strRegister = "Update"
        self.navigationController?.pushViewController(mainVC, animated: true)
        
    }
    
    @objc func profileDetailsData(){
      
        let parameters = ["Employee_ID":UserDefaults.standard.string(forKey: "EmployeeID")!,
              "Profile_AuthKey":self.Profile_AuthKey,
        "AppName":URLConstants.appName]
        
        WebServices.sharedInstances.sendPostRequest(url: URLConstants.GetProfile, parameters: parameters, successHandler: { (response:[String : AnyObject]) in
            
             DispatchQueue.main.asyncAfter(deadline: .now() + 0.0) {
                
             let respon = response["response"] as! [String:AnyObject]
            
             if self.reachablty.connection != .none{
             
           print(respon)
                if respon["status"] as! String == "success" {
                    
                    self.deleteProfileData()
               
                    let dict = response["data"] as! [String:AnyObject]
                    var Employee_Name = String()
                 var Employee_ID = String()
                    var Gender = String()
                    var Email_ID_Official = String()
                    var Profile_Pic_Path = String()
                    var Mobile_Number = String()
                    
                    var Zone_Name = String()
                    var Unit_Name = String()
                    var Area_Name = String()
                    var Designation = String()
                     var Designation_Name = String()
                  
                    
                    
//                    if dict["SBU_ID"] is NSNull{
//                        SBU_ID = 0
//                    }else{
//                        SBU_ID = (dict["SBU_ID"] as? Int)!
//                    }
                    if dict["Employee_ID"] is NSNull || dict["Employee_ID"] == nil{
                        Employee_ID = ""
                    }else{
                        Employee_ID = (dict["Employee_ID"] as? String)!
                    }
                    if dict["Employee_Name"] is NSNull || dict["Employee_Name"] == nil{
                        Employee_Name = ""
                    }else{
                        Employee_Name = (dict["Employee_Name"] as? String)!
                    }

                    if dict["Gender"] is NSNull || dict["Gender"] == nil{
                        Gender = ""
                    }else{
                        Gender = (dict["Gender"] as? String)!
                    }
                    if dict["Email_ID"] is NSNull || dict["Email_ID"] == nil{
                        Email_ID_Official = ""
                    }else{
                        Email_ID_Official = (dict["Email_ID"] as? String)!
                    }
                    if dict["Profile_Pic_Path"] is NSNull || dict["Profile_Pic_Path"] == nil{
                        Profile_Pic_Path = ""
                    }else{
                        Profile_Pic_Path = (dict["Profile_Pic_Path"] as? String)!
                    }
                    if dict["Mobile_Number"] is NSNull || dict["Mobile_Number"] == nil{
                        Mobile_Number = ""
                    }else{
                        Mobile_Number = (dict["Mobile_Number"] as? String)!
                    }
                    if dict["Designation"] is NSNull || dict["Designation"] == nil{
                        Designation = ""
                    }else{
                        Designation = dict["Designation"] as! String
                    }
                    if dict["Designation_Name"] is NSNull || dict["Designation_Name"] == nil{
                        Designation_Name = ""
                    }else{
                        Designation_Name = dict["Designation_Name"] as! String
                    }
                    
                    if dict["Area_Name"] is NSNull || dict["Area_Name"] == nil{
                        Area_Name = ""
                    }else{
                        Area_Name = dict["Area_Name"] as! String
                    }
                    if dict["Zone_Name"] is NSNull || dict["Zone_Name"] == nil{
                        Zone_Name = ""
                    }else{
                        Zone_Name = dict["Zone_Name"] as! String
                    }
                    
                    if dict["Unit_Name"] is NSNull || dict["Unit_Name"] == nil{
                        Unit_Name = ""
                    }else{
                        Unit_Name = dict["Unit_Name"] as! String
                    }
                    
                    print(Mobile_Number)
                    
                    self.saveProfileData(Employee_Name: Employee_Name as! String, Gender: Gender as! String, Email_ID_Official: Email_ID_Official as! String, Profile_Pic_Path: Profile_Pic_Path as! String, Mobile_Number: String(Mobile_Number), Zone_Name: Zone_Name, Unit_Name: Unit_Name, Area_Name: Area_Name, Designation: Designation,Employee_Id :Employee_ID, Designation_Name : Designation_Name)
                    
                  //self.saveProfileData(Employee_Name: Employee_Name as! String, SBU_ID: String(SBU_ID), SBU_Name: SBU_Name as! String, Department: String(Department), Department_Name: Department_Name as! String, Designation:  String(Designation), Gender: Gender as! String, Email_ID_Official: Email_ID_Official as! String, Profile_Pic_Path: Profile_Pic_Path as! String, Mobile_Number: String(Mobile_Number), Designation_Name: Designation_Name as! String)
                
                    
                    
                    
                    
                    
                    
                    
                    
                    
                }
            }
        }
        
        }) { (err) in
            print(err.description)
        }
        
    }
    
    
    @objc func getProfileDetailsData() {
        
        self.profileDB = [Profile]()
        
        do {
            
            self.profileDB = try context.fetch(Profile.fetchRequest())
            
            self.tableView.reloadData()
            
        } catch {
            print("Fetching Failed")
        }
    }

    //Core Data
    func saveProfileData (Employee_Name:String,Gender:String,Email_ID_Official:String,Profile_Pic_Path:String,Mobile_Number:String,Zone_Name:String,Unit_Name:String,Area_Name:String,Designation:String,Employee_Id:String,Designation_Name:String){
        
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        
        let tasks = Profile(context: context)
        
         tasks.employee_Name = Employee_Name
//         tasks.sBU_ID = SBU_ID
//         tasks.sBU_Name = SBU_Name
//         tasks.department = Department
//         tasks.department_Name = Department_Name
//         tasks.designation = Designation
         tasks.gender = Gender
         tasks.mobile_Number = Mobile_Number
         tasks.email_ID_Official = Email_ID_Official
         tasks.profile_Pic_Path = Profile_Pic_Path
         tasks.employeeId = Employee_Id
        
        tasks.zone_Name = Zone_Name
        tasks.unit_Name = Unit_Name
        tasks.area_Name = Area_Name
        tasks.designation = Designation
        tasks.designation_Name = Designation_Name
       
        (UIApplication.shared.delegate as! AppDelegate).saveContext()
        
        getProfileDetailsData()
        
        
    }
    
    func deleteProfileData()
    {
        
        let req = NSFetchRequest<NSFetchRequestResult>.init(entityName: "Profile")
        let batchDeleteRequest = NSBatchDeleteRequest(fetchRequest: req)
        
        do {
            try context.execute(batchDeleteRequest)
            
        } catch {
            // Error Handling
        }
    }
    
    func selectImage(){
        
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.0) {
            
            let actionSheetController: UIAlertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
            
            let camera = UIAlertAction(title: "Take Photo", style: .default, handler: { (alert) in
                
                self.camera()
                
            })
            let Library = UIAlertAction(title: "Choose From Library", style: .default, handler: { (alert) in
                
                self.gallery()
                
            })
            
            
            
            let cancel = UIAlertAction(title: "Cancel", style: .cancel, handler: { (alert) in
                
            })
            
            actionSheetController.addAction(camera)
            actionSheetController.addAction(Library)
            actionSheetController.addAction(cancel)
            self.present(actionSheetController, animated: true, completion: nil)
            
            
        }
        
        
    }
    
     private var croppingStyle = CropViewCroppingStyle.default
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        if let mediaType = info[UIImagePickerControllerMediaType] as? String {
            
            if mediaType == (kUTTypeImage as String)
            {
                
                //let cell = tableView.cellForRow(at: indPath) as! ProfileDetailsTableViewCell
//
              let image = info[UIImagePickerControllerOriginalImage] as! UIImage
//
//                let cropViewController = CropViewController(image:image)
//                cropViewController.delegate = self
//                cropViewController.showActivitySheetOnDone = false
//                navigationController?.pushViewController(cropViewController, animated: true)
//
//
//                dismiss(animated:true, completion: nil)
                
                
                
                let cropController = CropViewController(croppingStyle: croppingStyle, image: image)
                cropController.delegate = self
                
                // Uncomment this if you wish to provide extra instructions via a title label
                //cropController.title = "Crop Image"
                
                // -- Uncomment these if you want to test out restoring to a previous crop setting --
                //cropController.angle = 90 // The initial angle in which the image will be rotated
                //cropController.imageCropFrame = CGRect(x: 0, y: 0, width: 2848, height: 4288) //The initial frame that the crop controller will have visible.
                
                // -- Uncomment the following lines of code to test out the aspect ratio features --
                //cropController.aspectRatioPreset = .presetSquare; //Set the initial aspect ratio as a square
                //cropController.aspectRatioLockEnabled = true // The crop box is locked to the aspect ratio and can't be resized away from it
                //cropController.resetAspectRatioEnabled = false // When tapping 'reset', the aspect ratio will NOT be reset back to default
                //cropController.aspectRatioPickerButtonHidden = true
                
                // -- Uncomment this line of code to place the toolbar at the top of the view controller --
                //cropController.toolbarPosition = .top
                
                //cropController.rotateButtonsHidden = true
                //cropController.rotateClockwiseButtonHidden = true
                
                //cropController.doneButtonTitle = "Title"
                //cropController.cancelButtonTitle = "Title"
                
               // self.image = image
                
                //If profile picture, push onto the same navigation stack
                if croppingStyle == .circular {
                    if picker.sourceType == .camera {
                        picker.dismiss(animated: true, completion: {
                            self.present(cropController, animated: true, completion: nil)
                        })
                    } else {
                        picker.pushViewController(cropController, animated: true)
                    }
                }
                else { //otherwise dismiss, and then present from the main controller
                    picker.dismiss(animated: true, completion: {
                        self.present(cropController, animated: true, completion: nil)
                        //self.navigationController!.pushViewController(cropController, animated: true)
                    })
                }
                
                
                
                
            }
            else
            {
                self.dismiss(animated: true, completion: nil)
            }
        }
        
        
    }
    
    public func cropViewController(_ cropViewController: CropViewController, didCropToImage image: UIImage, withRect cropRect: CGRect, angle: Int) {

        let cell = tableView.cellForRow(at: indPath) as! ProfileDetailsTableViewCell
        cell.userImage.image = image
//        let resizedImage = image.resizedTo1MB()
//        cell.userImage.contentMode = .scaleAspectFill
//        cell.userImage.image = resizedImage
        imagedata = UIImageJPEGRepresentation(image, 1.0)! as Data
        
        dismiss(animated: true, completion: nil)
        self.updateProfilePic()
    }
    public func cropViewController(_ cropViewController: CropViewController, didCropToCircularImage image: UIImage, withRect cropRect: CGRect, angle: Int) {
        //self.croppedRect = cropRect
       // self.croppedAngle = angle
       // updateImageViewWithImage(image, fromCropViewController: cropViewController)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
        
    }
    
    func camera()
    {
        pickerr.allowsEditing = false
        pickerr.sourceType = UIImagePickerControllerSourceType.camera
        pickerr.cameraCaptureMode = .photo
        pickerr.modalPresentationStyle = .fullScreen
        present(pickerr,animated: true,completion: nil)
        
    }
    
    func gallery()
    {
        pickerr.allowsEditing = false
        pickerr.sourceType = .photoLibrary
        present(pickerr, animated: true, completion: nil)
    }
    

    @IBAction func profileEditAction(_ sender: Any) {
        
        selectImage()
    }
    
    func updateProfilePic(){
        
        let cell = tableView.cellForRow(at: indPath) as! ProfileDetailsTableViewCell
        if reachablty.connection == .none{
            
            self.view.makeToast("Internet is not available, please check your internet connection try again.")
            
        }
        else{
            
            self.startLoading()
           
            let parameter = [
                "Personnel_Number": UserDefaults.standard.string(forKey: "EmployeeID")! ,
                "Profile_AuthKey":Profile_AuthKey,
                "AppName": URLConstants.appName
                ]
          
           
        
            let jsonData: Data? = try? JSONSerialization.data(withJSONObject: parameter, options: [])
            let para: [String: Data] = ["data": jsonData!]
          
            Alamofire.upload(multipartFormData: { multipartFormData in
                
                if !(self.imagedata == nil) {
                    
                  
                    
                    multipartFormData.append(self.imagedata!, withName: "file", fileName: "file.jpg", mimeType: "image/jpeg")
                }
                
                for (key, value) in para {
                    multipartFormData.append(value, withName: key)
                    
                    
                }
                
            },
                             to:URLConstants.Update_Profile_Pic)
            { (result) in
                switch result {
                case .success(let upload, _, _):
                    
                    upload.uploadProgress(closure: { (progress) in
                        
                    })
                    
                    
                    upload.responseJSON { response in
                        
                        
                        if let json = response.result.value
                        {
                            var dict = json as! [String: AnyObject]
                            
                            if let response = dict["response"]{
                                print(response)
                                let statusString = response["status"] as! String
                                let objectmsg = MessageCallServerModel()
                    let msg = objectmsg.sealizeMessage(cell: response as! [String : AnyObject])
                               
                                if(statusString == "success")
                                {
                                    
//                                    let alertController = UIAlertController(title: nil, message: msg, preferredStyle: .alert)
//                                    alertController.view.tintColor = UIColor.black
//                                    self.present(alertController, animated: true, completion: nil)
//
//                                        alertController.dismiss(animated: true, completion: {() -> Void in
                                            self.stopLoading()
                                            
                                            cell.userImage.image = UIImage.init(data: self.imagedata!)
                                            
                                            cell.profileWidth.constant = 80
                                            cell.profileHeight.constant = 80
                                            
                                            self.profileDetailsData()
                                            //self.getProfileDetailsData()
                                            
                                    //    })
                                     self.dismiss(animated: true, completion: nil)
                                    
                                    
                                }
                                else{
                                    
                                    self.stopLoading()
                                    self.tableView.reloadData()
                                    self.view.makeToast(msg)
                                    
                                }
                                
                                
                                
                            }
                            
                            
                        }
                        
                        
                    }
                    
                case .failure(let encodingError):
                    
                    self.errorChecking(error: encodingError)
                    self.stopLoading()
                    print(encodingError.localizedDescription)
                }
            }
            }
        
    }
    

}
