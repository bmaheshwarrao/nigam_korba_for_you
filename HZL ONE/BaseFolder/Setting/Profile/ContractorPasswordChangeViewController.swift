//
//  ContractorPasswordChangeViewController.swift
//  Balco Sustainability Plus
//
//  Created by sudheer-kumar on 17/04/18.
//  Copyright © 2018 safiqul islam. All rights reserved.
//

import UIKit

class ContractorPasswordChangeViewController: CommonVSClass {
    
    @IBOutlet weak var oldPTF: UITextField!
    @IBOutlet weak var newPTF: UITextField!
    @IBOutlet weak var confirmPTF: UITextField!
    var personalNo = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "Change Password"
        
        
       
       
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
var reachability = Reachability()!
    @IBAction func ChangePasswordAction(_ sender: Any) {
        
        oldPTF.resignFirstResponder()
        newPTF.resignFirstResponder()
        confirmPTF.resignFirstResponder()
        
        guard reachability.connection != .none else {
            self.view.makeToast("Internet is not available, Please check.")
            return
        }
        guard let old = self.oldPTF.text, old != "" else {
            self.view.makeToast("Please enter old password")
            return
        }
        guard let new = self.newPTF.text, new != "" else {
            self.view.makeToast("Please enter new password")
            return
        }
        guard let confirm = self.confirmPTF.text, confirm != "" else {
            self.view.makeToast("Please enter confirm password")
            return
        }
        guard new == confirm else {
            self.view.makeToast("Your new and confirm password not matched")
            return
        }
        self.loginData()
        
    }
 
    func loginData()
    {
        
        self.startLoading()
        let valueNop : Int = UserDefaults.standard.value(forKey: "EmployeeID")! as! Int
        let strVal : String = String(valueNop)
        let parameters = ["Personnel_Number":strVal ,
                          "password":self.confirmPTF.text!,
                          "old_password":self.oldPTF.text!]
        
        print("parameters:",parameters)
        
        
        WebServices.sharedInstances.sendPostRequest(url: URLConstants.ContractPasswordChange, parameters: parameters, successHandler: { (dict) in
            
           
            
            if let response = dict["response"]{
                
                
                self.startLoading()
                let statusString: String = response["status"] as! String
                let msgString: String = response["msg"] as! String
                
                if(statusString == "success")
                {
                     self.stopLoading()
                    self.alert(title: msgString)
                    self.navigationController?.popViewController(animated: true)
                }
                else{
                    
                    self.stopLoading()
                    self.view.makeToast(msgString)
                    
                    
                }
                
            }
            
        }) { (error) in
            self.stopLoading()
            
            self.errorChecking(error: error as! Error)
            
            print("Error")
            
        }
        
        
        
        
    }
    
}
