//
//  ServiceUrl.swift
//  HZL CSC
//
//  Created by SARVANG INFOTCH on 25/09/18.
//  Copyright © 2018 SARVANG INFOTCH. All rights reserved.
//

import Foundation

var BASE = "http://hzlsafety.dev.app6.in/"
var BASE_URL = BASE + "API/"

var Base_Url_Quiz = "http://equiz.dev.app6.in/API/"
var Base_Url_News = "http://hzlone.dev.app6.in/API/"
var Base_Url_Health_Tips = "http://hzlone.dev.app6.in/API/"
var Base_Url_NDSO = "http://hzlndsoapi.dev.app6.in/API/"
var Base_Url_ODSO = "http://hzlodsoapi.dev.app6.in/API/"
var Base_Url_TBS = "http://hzltbs.dev.app6.in/API/"
var Base_Url_SI = "https://aarohan.hzlmetals.com/siapi/api/"
var Base_Url_KPI = "http://hzlkpi.dev.app6.in/API/"

var  Base_Url_Idea = "http://hlzidea.dev.app6.in/Api/"  
var Base_Url_CSC = "http://vfeedback.dev.app6.in/API/"
var Base_Url_Lsr = "http://hzllsr.dev.app6.in/API/"
var Base_Url_WellDone = "http://hzlwelldone.dev.app6.in/API/"
var Base_Url_GuestHouse = "http://ghouse.dev.app6.in/GHouseApi/"
var Base_Url_Vehicle = "http://hzltravel.dev.app6.in/TravelApi/"
var Base_Url_elearning = "http://elearning.dev.app6.in/API/"
var Base_Url_township = "http://hzltownship.dev.app6.in/Api/"
var Base_Url_MOM = "http://mmp.dev.app6.in/API/"
var Base_Url_Ghosna = "http://ghoshna.hzlmetals.com/API/"


var Base_Url_fives = "http://hzl5s.dev.app6.in/API/_5S/"

let Company_Url = "http://sarvang-apps.app6.in/API/"

class URLConstants: NSObject
{
    /// Home
    
    static let AppMarket = Base_Url_News+"AppMarket/"
      static let Dashboard_ShowCase = Base_Url_News+"Dashboard_ShowCase/"
  
    static let Social_Media_Dashboard = Base_Url_News+"Social_Media_Dashboard/"
     static let Share_Market = Base_Url_News+"Share_Market/"
     static let CEO_Message = Base_Url_News+"CEO_Message/"
static let About_HZL = Base_Url_News+"About_HZL/"
    static let Company_Policies_Category = Base_Url_News+"Company_Policies_Category/"
    static let Company_Policies = Base_Url_News+"Company_Policies/"
      static let Notification_List = Base_Url_News+"Notification_List/"
    static let Feedback_Post = Base_Url_News+"Feedback_Post/"
    /// Inbox
    /// in Use Api
    static let HZL_ONE_MY_Inbox_Action = Base_Url_News+"MY_Inbox/?Employee_ID=\(UserDefaults.standard.string(forKey: "EmployeeID")!)"
    
    /// not in use
    static let HZL_ONE_MY_Inbox_Action_TBS = Base_Url_TBS+"HZL_ONE_MY_Inbox_Action_TBS/?Employee_ID=\(UserDefaults.standard.string(forKey: "EmployeeID")!)"
    static let HZL_ONE_MY_Inbox_Action_EQ = Base_Url_Quiz+"HZL_ONE_MY_Inbox_Action_EQ/?Employee_ID=\(UserDefaults.standard.string(forKey: "EmployeeID")!)"
    static let HZL_ONE_MY_Inbox_Action_NDSO = Base_Url_NDSO+"HZL_ONE_MY_Inbox_Action_NDSO/?Employee_ID=\(UserDefaults.standard.string(forKey: "EmployeeID")!)"
    static let HZL_ONE_MY_Inbox_Action_ODSO = Base_Url_ODSO+"HZL_ONE_MY_Inbox_Action_ODSO/?Employee_ID=\(UserDefaults.standard.string(forKey: "EmployeeID")!)"
    static let HZL_ONE_MY_Inbox_Action_IDEA = Base_Url_Idea+"HZL_ONE_MY_Inbox_Action_IDEA/?Employee_ID=\(UserDefaults.standard.string(forKey: "EmployeeID")!)"


     static let Post_Gcm = Base_Url_News+"GCM_Post/"
    static let Logout = Base_Url_News+"GCM_Post_Delete/"
    static let Hazard_Type_List = BASE_URL+"Hazard/HazardType_List/"
    
    /// Settings
    static let GetProfile = BASE_URL+"iProfile/GetProfile/"
    static let Update_Profile_Pic = BASE_URL+"iProfile/Update_Profile_Pic/"
    static let Application_Banner = Base_Url_News+"Application_Banner/"
    /// Township
    static let Complaint_Category = Base_Url_township+"Complaint_Category/"
    static let Complaint_Subject = Base_Url_township+"Complaint_Subject/"
    static let Complaint_Register = Base_Url_township+"Complaint_Register/"
      static let Complaint_List = Base_Url_township+"Complaint_List/"
 static let Feedback_Data = Base_Url_township+"Feedback_Data/"
    static let Reopen_Complaint = Base_Url_township+"Reopen_Complaint/"
    static let Insert_Feedback = Base_Url_township+"Insert_Feedback/"
 static let User_Details = Base_Url_township+"User_Details/"
static let QuarterRequest_List = Base_Url_township+"QuarterRequest_List/"
    static let Insert_QuarterRequest = Base_Url_township+"Insert_QuarterRequest/"
/// Ghosna
    static let Ghoshna_data = Base_Url_Ghosna+"Ghoshna_data.aspx?EmpId="
    

    /// 5s
    
    static let Get_Scoreboard_List = Base_Url_fives+"Audit_Master_Title/Default.aspx"
    static let Get_Scoreboard_Audit_List = Base_Url_fives+"Audit_Score_Card/Default.aspx"
    static let Get_AuditPending_List = Base_Url_fives+"Pending_Audit_List/Default.aspx"
    static let Audit_AuditPointListStatus = Base_Url_fives+"Audit_AuditPointListStatus/Default.aspx"
    
    
    static let Audit_CheckList = Base_Url_fives+"Audit_CheckList/Default.aspx"
    static let Pending_Observations = Base_Url_fives+"Pending_Observations/Default.aspx"
    static let Pending_ObservationsAuditList = Base_Url_fives+"Pending_Observations_List/Default.aspx"
    static let Submit_Action_on_Observation = Base_Url_fives+"Submit_Action_on_Observation/Default.aspx"
    static let Submit_Action_on_Observation_HOD = Base_Url_fives+"Submit_Action_on_Observation_HOD/Default.aspx"
    
    static let Banner_Images = Base_Url_fives+"Banner/Default.aspx"
    static let Submit_Observation = Base_Url_fives+"Submit_Observation/Default.aspx"
    static let Action_Taken_On_Open_Observation = Base_Url_fives+"Action_Taken_On_Open_Observation/Default.aspx"
    
    static let Upcoming_Audits = Base_Url_fives+"Upcoming_Audits/Default.aspx"
    static let Completed_Audits = Base_Url_fives+"Completed_Audits/Default.aspx"
    static let Missed_Audit = Base_Url_fives+"Missed_Audit/Default.aspx"
    static let Delete_Image = Base_Url_fives+"Delete_Image/Default.aspx"
    static let Submit_Final_Audit = Base_Url_fives+"Submit_Final_Audit/Default.aspx"
    
    /// SI
    static let PendingInteractionsList = Base_Url_SI+"InteractionsList/Get"
static let InteractionsListDetails = Base_Url_SI+"Interaction/Get"
    static let InteractionsListDetailsInteractionDetails = Base_Url_SI+"InteractionDetails/Get"
      static let GetAppersession = Base_Url_SI+"Common/GetAppersession"
    static let GetUserTransaction = Base_Url_SI+"Common/GetUserTransaction"
    static let GetPartnerList = Base_Url_SI+"Partner/Get"
    static let GetInteractionPost = Base_Url_SI+"Interaction/Post"
  static let PostApproverAction = Base_Url_SI+"Approver/PostApproverAction"

    /// Safety KPI
    static let Zone_Dashboard_KPI = Base_Url_KPI+"Zone_Dashboard/"

static let KPI_Overview_Sheet = Base_Url_KPI+"KPI_Overview_Sheet/"
   
    /// Vehicle
    static let GetPurposeOfJourney = Base_Url_Vehicle+"GetPurposeOfJourney/"
       static let GetApproverListAuth = Base_Url_Vehicle+"GetApproverList/"
    static let GetTravelType = Base_Url_Vehicle+"GetTravelType/"
    static let MyRequestCount = Base_Url_Vehicle+"MyRequestCount/"
    static let MyRequestVehicle = Base_Url_Vehicle+"MyRequest/"
    static let LocalRequest = Base_Url_Vehicle+"LocalRequest/"
    static let OutstationRequest = Base_Url_Vehicle+"OutstationRequest/"
 static let TourRequest = Base_Url_Vehicle+"TourRequest/"
 static let MyApprovalCountVehicle = Base_Url_Vehicle+"MyApprovalCount/"
     static let GetPlaces = Base_Url_Vehicle+"GetPlaces/"
/// Elearning
  
    static let Dashboad_Elibrary_File = Base_Url_elearning+"Dashboad_Elibrary_File/"
     static let Dashboad_E_Learning_Course = Base_Url_elearning+"Dashboad_E_Learning_Course/"
 static let E_Learning_Category = Base_Url_elearning+"E_Learning_Category/"
    static let Quiz_ListmLearning = Base_Url_elearning+"Quiz_List/"
  
   
    static let E_Learning_Files = Base_Url_elearning+"E_Learning_Files/"
    static let E_Learning_Course_Completed = Base_Url_elearning+"E_Learning_Course_Completed/"
 
  static let Temp_Question_ListElearning = Base_Url_elearning+"Temp_Question_List/"
  static let Preview_questions_with_answersElearning = Base_Url_elearning+"Preview_questions_with_answers/"
    static let Quiz_HistoryElearning = Base_Url_elearning+"Quiz_History/"
     static let Answered_Questions_HistoryElearning = Base_Url_elearning+"Answered_Questions_History/"
static let My_Course_Attempte_List = Base_Url_elearning+"My_Course_Attempte_List/"
    static let ELibrary_History_Write = Base_Url_elearning+"ELibrary_History_Write/"
     static let Temp_Final_Question_SubmitELearning = Base_Url_elearning+"Temp_Final_Question_Submit/"


    // mLibrary (part of Elearning)
    static let Favourite_List_Remove = Base_Url_elearning+"Favourite_List_Remove/"
    static let Add_Favourite = Base_Url_elearning+"Add_Favourite/"
    static let Add_Watchlist = Base_Url_elearning+"Add_Watchlist/"
    static let Watchlist_List_Remove = Base_Url_elearning+"Watchlist_List_Remove/"
    static let Favourite_List = Base_Url_elearning+"Favourite_List/"
    static let Watchlist_List = Base_Url_elearning+"Watchlist_List/"
static let Submit_Quizelearning = Base_Url_elearning+"Temp_Next_Question_Submit/"
static let E_Learning = Base_Url_elearning+"E_library_Data/"
    /// WellDone
    static let Employee_List_By_ID_WellDone = Base_Url_WellDone+"Employee_List_By_ID/"
     static let Issue_Card_Create = Base_Url_WellDone+"Issue_Card_Create/"
    static let My_Card = Base_Url_WellDone+"My_Card/"
    static let My_Redeems = Base_Url_WellDone+"My_Redeems/"
 

    /// LSR
    static let Employee_List_By_ID = Base_Url_Lsr+"Employee_List_By_ID/"
    static let Check_Location_Admin = Base_Url_Lsr+"Check_Location_Admin/"

      static let Rules_List = Base_Url_Lsr+"Rules_List/"
 static let Register_Violation = Base_Url_Lsr+"Register_Violation/"
    
    
 
    
    /// CSC
     static let visitSubmit = Base_Url_CSC+"Visit_Data_Insert/"
     static let unsafeActsObservedCSC = Base_Url_CSC+"Unsafe_Acts_observed/"
static let Check_Member = Base_Url_CSC+"Check_Member/"
    static let User_Login_Visit_Location = Base_Url_CSC+"User_Login_Visit_Location/"
  
    /// MOM

     static let My_Task_MOM = Base_Url_MOM+"My_Task/"
    static let Manage_Meeting_MOM = Base_Url_MOM+"Manage_Meeting/"
    static let My_Meeting_MOM = Base_Url_MOM+"My_Meeting/"
     static let EmployeeDetails_MOM = Base_Url_MOM+"EmployeeDetails/"
static let CreateMeeting_MOM = Base_Url_MOM+"CreateMeeting/"
    static let View_Meeting_MOM = Base_Url_MOM+"View_Meeting/"
    static let Task_List_MOM = Base_Url_MOM+"Task_List/"
static let Update_Task_MOM = Base_Url_MOM+"Update_Task/"
static let Temp_Task_Insert_MOM = Base_Url_MOM+"Temp_Task_Insert/"
static let Temp_Task_List_MOM = Base_Url_MOM+"Temp_Task_List/"
  static let Process_Meeting_MOM = Base_Url_MOM+"Process_Meeting/"
    static let Completed_Meeting_MOM = Base_Url_MOM+"Completed_Meeting/"

    
    


    /// GUEST HOUSE
    static let GetHouseList = Base_Url_GuestHouse+"GetHouseList/"
    static let GetApproverList = Base_Url_GuestHouse+"GetApproverList/"
    static let FoodType = Base_Url_GuestHouse+"FoodType/"
    static let MyRequest = Base_Url_GuestHouse+"MyRequest/"
 static let MyRequestList = Base_Url_GuestHouse+"MyRequestList/"
     static let FoodRequest = Base_Url_GuestHouse+"FoodRequest/"
 static let RoomType = Base_Url_GuestHouse+"RoomType/"
   static let RoomRequest = Base_Url_GuestHouse+"RoomRequest/"
static let MyApprovalsGuestHouse = Base_Url_GuestHouse+"MyApprovals/"
 static let MyApprovalListGuestHouse = Base_Url_GuestHouse+"MyApprovalList/"
static let RoomApproverApprove = Base_Url_GuestHouse+"RoomApproverApprove/"
static let RoomAdminApprove = Base_Url_GuestHouse+"RoomAdminApprove/"
    
    static let FoodApproverApprove = Base_Url_GuestHouse+"FoodApproverApprove/"
    static let FoodAdminApprove = Base_Url_GuestHouse+"FoodAdminApprove/"
    static let FoodCancel = Base_Url_GuestHouse+"FoodCancel/"
      static let RoomCancel = Base_Url_GuestHouse+"RoomCancel/"

    /// Hazards

    
   
    static let ContractPasswordChange = BASE_URL+"iProfile/Contract_Password_Change/" 
    
    
    /// Idea
    
        static let Submit_Idea = Base_Url_Idea+"Submit_Idea/"
        static let My_Points_Ledger = Base_Url_Idea+"My_Points_Ledger/"
    static let My_Inbox_Count = Base_Url_Idea+"My_Inbox_Count/"
    
    static let HOD_Inbox_Count = Base_Url_Idea+"HOD_Inbox_Count/"
    static let My_Inbox = Base_Url_Idea+"My_Inbox/"
    
    static let HOD_Inbox = Base_Url_Idea+"HOD_Inbox/"
     static let Serach_Employee = Base_Url_Idea+"Serach_Employee/"
     static let Area_Managers_List = Base_Url_Idea+"Idea_Assign_List/"
    static let Assign_Idea = Base_Url_Idea+"Assign_Idea/"
      static let Accepct_Idea = Base_Url_Idea+"Accepct_Idea/"
    static let Process_Idea = Base_Url_Idea+"Process_Idea/"
    static let Reject_Idea = Base_Url_Idea+"Reject_Idea/"
    ///     News Update
    
    static let Tips_News_And_Update = Base_Url_News+"Tips_News_And_Update/"
  
    ///     Organization Tips
    static let Tips_Organization_Advice = Base_Url_News+"Tips_Organization_Advice/"
    
   
    ///     Safety whistle
    static let Safety_whistle_blower = Base_Url_News+"Safety_whistle_blower/"
    
    ///     Health Tips
     static let Tips_Health_Tips = Base_Url_News+"Tips_Health_Tips/"
    
    ///     Security Tips
    static let Tips_ITsecurityTips = Base_Url_News+"Tips_ITsecurityTips/"
    
    
    ///     Theme Safety
    static let themeSafetyNewHazard = Base_Url_TBS+"Hazard/New_Hazard/"
    static let GraphOverall = Base_Url_TBS + "Hazard/Hazard_Graph/"
    
    static let Hazard_Graph = Base_Url_TBS+"Hazard/Hazard_Graph_Dashboard_New/"

 static let Selfi_Time_Line = Base_Url_TBS+"Hazard/Selfi_Time_Line/"
     static let Point_List = Base_Url_TBS+"Hazard/Point_List/"
   static let Leader_Board = Base_Url_TBS+"Hazard/Leader_Board/"
    static let Leadership_Compliance_Add_SI_Delete = Base_Url_TBS+"SI/Leadership_Compliance_Add_SI_Delete/"
    static let Create_Leadership_Compliance_Submit = Base_Url_TBS+"SI/Create_Leadership_Compliance_Submit/"
    static let Create_Leadership_Compliance = Base_Url_TBS+"SI/Create_Leadership_Compliance/"
    static let Leadership_Compliance_Add_List = Base_Url_TBS+"SI/Leadership_Compliance_Add_List/"

    
    static let Delete_Gcm = Base_Url_TBS+"Hazard/GCM_Post_Delete/"
    static let Hazard_Process_Details = Base_Url_TBS+"Hazard/Hazard_Process_Details/"
    static let Hazard_Report_Dashboard_Details = Base_Url_TBS+"Hazard/Hazard_Report_Dashboard_Details/"
    static let Dashbord_List_Pending_For_Closer = Base_Url_TBS+"Hazard/Dashbord_List_Pending_For_Closer/"
    static let Dashbord_List_Assign_For_Pending = Base_Url_TBS+"Hazard/Dashbord_List_Assign_For_Pending/"
    static let Dashbord_List_HAZARD_CLOSER = Base_Url_TBS+"Hazard/Dashbord_List_HAZARD_CLOSER/"
    static let Dashbord_List_HAZARD_REPORTER = Base_Url_TBS+"Hazard/Dashbord_List_HAZARD_REPORTER/"
    static let Emergency_Contact = Base_Url_TBS+"Hazard/Emergency_Contact/"
    static let Hazard_Process_Submit = Base_Url_TBS+"Hazard/Hazard_Process_Submit/"
    static let Get_Library_File = Base_Url_TBS+"Hazard/ELibrary_File/"
    static let Dashbord_List_New_Hazard = Base_Url_TBS+"Hazard/Dashbord_List_New_Hazard/"
    static let Get_File_Categories = Base_Url_TBS+"Hazard/ELibrary_File_Category/"
    static let Remainder_Assign_But_User_Side_Pending = Base_Url_TBS+"Hazard/Remainder_Assign_But_User_Side_Pending/"
    static let Reject_Hazard = Base_Url_TBS+"Hazard/Hazard_Reject/"
    
    static let Forward_Hazard = Base_Url_TBS+"Hazard/Hazard_Forword/"
    static let Assign_Hazard = Base_Url_TBS+"Hazard/Assign_Hazard/"
    static let List_New_Hazard = Base_Url_TBS+"Hazard/Hazard_List/"
    
    
    
    static let Hazard_Report_Dashboard = Base_Url_TBS+"Hazard/Hazard_Report_Dashboard/"
    
    static let search_hazards = Base_Url_TBS+"Hazard/Search_Hazard/"
    static let hazard_by_actions_change = Base_Url_TBS+"Hazard/Hazard_List/"
    static let hazard_by_actions = Base_Url_TBS+"Hazard/My_Hazard/"
    static let Area_Managers_ListTBS = Base_Url_TBS+"Hazard/Area_Manager_List/"
    static let Hazard_Details = Base_Url_TBS+"Hazard/Hazard_Details_ID/"
    
    static let hazard_by_status = Base_Url_TBS+"Hazard/Report_My_Hazard/"
    static let New_hazard = Base_Url_TBS+"Hazard/New_Hazard/"
    static let Hazard_Count = Base_Url_TBS+"Hazard/Total_Count_Reported_and_Assign/"
    
    static let submittedURL = Base_Url_TBS+"get-submitted-ideas/"
    
    static let assignedURL = Base_Url_TBS+"dpartmentRepresentative/get-assigned-ideas/"
    
    static let assignedDetailURL = Base_Url_TBS+"ideas/get-implementation-process-list/"
    
    static let ideaClubURL = Base_Url_TBS+"ideaClubPoints/emp-points/"
    
    static let HODURL = Base_Url_TBS+"hod/get-idea-list/"
    static let assignIdea = Base_Url_TBS+"dpartmentRepresentative/get-dr-list/"
    static let depListURL = Base_Url_TBS+"get-department/"
    static let nonEmpReg = Base_Url_TBS+"registration/non-employee/"
    static let nonEmpRegConfirm = Base_Url_TBS+"registration/non-employee/confirm/"
    static let verifyMobile = Base_Url_TBS+"login/login/"
    
    static let checkLogin = Base_Url_TBS+"login/check-login/"
    static let submitIdea = Base_Url_TBS+"ideas/submit-idea/"
    static let commentURL = Base_Url_TBS+"dpartmentRepresentative/post-implementation-process/"
    static let hodAllShowProcess = Base_Url_TBS+"hod/process-idea/"
    static let hodAssignIdea = Base_Url_TBS+"hod/assign-idea/"
    
    
 
    
    
    
    
    
    
    /// get Base Url
    
 
   static let Get_App_Data = Company_Url+"Get_App_Data/"  
    
    
    ///  Movie update
    static let Movie_Details = Base_Url_News+"Movie_Details_Json/"
    


    
    ///  mQuiz Real Url Start
 static let Final_Submit_Quiz = Base_Url_Quiz+"Temp_Final_Question_Submit/"
    static let Submit_Quiz = Base_Url_Quiz+"Temp_Next_Question_Submit/"
    static let List_Study_Material = Base_Url_Quiz+"List_Study_Material/"
    static let Quiz_List = Base_Url_Quiz+"Quiz_List/"
    static let Quiz_History = Base_Url_Quiz+"Quiz_History/"
    static let Question_List = Base_Url_Quiz+"Temp_Question_List/"
    static let Answered_Questions_History = Base_Url_Quiz+"Answered_Questions_History/"
    static let Preview_questions_with_answers = Base_Url_Quiz+"Preview_questions_with_answers/"
     static let Temp_Timing_Updates = Base_Url_Quiz+"Temp_Timing_Updates/"

    /// End

 
   ///  NDSO
      static let My_Visits_ListNSDO =  Base_Url_NDSO + "My_Visits_List/"
    static let Question_ListNSDO =  Base_Url_NDSO + "Question_List/"
    static let Observations_DetailsNSDO =  Base_Url_NDSO + "Observations_Details/"
  static let G_Location_List =  Base_Url_NDSO + "G_Location_List/"
  static let Observations_Add =  Base_Url_NDSO + "Observations_Add/"
 static let Observations_Submit =  Base_Url_NDSO + "Observations_Submit/"
     static let Specific_Obs_Type_One =  Base_Url_NDSO + "Specific_Obs_Type_One/"
    static let Submit_Reward =  Base_Url_NDSO + "Submit_Reward/"
    static let Submit_Final =  Base_Url_NDSO + "Observation_Submit_Final/"
    static let Observations_Details_Count =  Base_Url_NDSO + "Observations_Details_Count/"
   static let Pending_to_Assign_Observation =  Base_Url_NDSO + "Pending_to_Assign_Observation/"
     static let Forward_Observation =  Base_Url_NDSO + "Forward_Observation/"
    static let Assigned_Observation_MY =  Base_Url_NDSO + "Assigned_Observation_MY/"
     static let Assign_Observation =  Base_Url_NDSO + "Assign_Observation/"
  static let Observation_Process_Submit_CloseNDSO =  Base_Url_NDSO + "Observation_Process_Submit_Close/"
    

///   ODSO
    
   
      static let My_Visits_ListODSO =  Base_Url_ODSO + "My_Visits_List/"
      static let Observation_Datails_Team_ID =  Base_Url_ODSO + "Observation_Datails_Team_ID/"
      static let Add_Work_Permit =  Base_Url_ODSO + "Add_Work_Permit/"
      static let Add_ObservationODSO =  Base_Url_ODSO + "Add_Observation/"
      static let Add_Add_Tool_Box =  Base_Url_ODSO + "Add_Add_Tool_Box/"
   static let Observations_Details_CountODSO =  Base_Url_ODSO + "Observations_Details_Count/"
static let Forward_ObservationODSO =  Base_Url_ODSO + "Forward_Observation/"
  static let Assigned_Observation_MYODSO =  Base_Url_ODSO + "Assigned_Observation_MY/"
static let Pending_to_Assign_ObservationODSO =  Base_Url_ODSO + "Pending_to_Assign_Observation/"
static let Assign_ObservationODSO =  Base_Url_ODSO + "Assign_Observation/"
    static let Observation_Submit_Final =  Base_Url_ODSO + "Observation_Submit_Final/"
static let Observation_Process_Submit_CloseODSO =  Base_Url_ODSO + "Observation_Process_Submit_Close/"
    
    
    
    /// iProfile
    static let Post_UpdateData = BASE_URL+"iProfile/Post_UpdateData/"
    static let setLoginDataEmployee = BASE_URL+"iProfile/Get_Login/"
    static let setLoginDataNonEmployee = BASE_URL+"iProfile/Login_Contract/"
    static let setDataByLocation = BASE_URL+"Hazard/GET_Location_Data/"
    
    static let verifyOTP = BASE_URL+"iProfile/Get_OTP/"
    
    /// Settings

     static let appName = "HZLOne"
     static let appNameInner = "HZL_ONE"
 static let platform = "iOS"
    
    //static let platform = "Android"
}

class Fontconstant: NSObject
{
    static let FontData = ""
}
