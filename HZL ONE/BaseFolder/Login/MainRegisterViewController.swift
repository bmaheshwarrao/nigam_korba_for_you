//
//  MainRegisterViewController.swift
// RAS
//
//  Created by SARVANG INFOTCH on 26/10/18.
//  Copyright © 2018 Bunga Maheshwar Rao. All rights reserved.
//

import UIKit

class MainRegisterViewController: CommonVSClass {

    @IBOutlet weak var submitBtn: UIButton!
    @IBOutlet weak var txtCnfPassword: MytextField!
    @IBOutlet weak var txtPassword: MytextField!
    @IBOutlet weak var txtEmail: MytextField!
    @IBOutlet weak var txtFirstName: MytextField!
    @IBOutlet weak var txtLastName: MytextField!
    @IBOutlet weak var txtMobile: MytextField!
    override func viewDidLoad() {
        super.viewDidLoad()
txtFirstName.keyboardType = UIKeyboardType.numberPad
        txtFirstName.clipsToBounds = true;
        txtFirstName.layer.cornerRadius = 10;
        txtFirstName.layer.borderWidth = 1.0
        txtFirstName.layer.borderColor = UIColor.black.cgColor
      
        txtPassword.clipsToBounds = true;
        txtPassword.layer.cornerRadius = 10;
        txtPassword.layer.borderWidth = 1.0
        txtPassword.layer.borderColor = UIColor.black.cgColor
        
        txtLastName.clipsToBounds = true;
        txtLastName.layer.cornerRadius = 10;
        txtLastName.layer.borderWidth = 1.0
        txtLastName.layer.borderColor = UIColor.black.cgColor
        
        txtEmail.clipsToBounds = true;
        txtEmail.layer.cornerRadius = 10;
        txtEmail.layer.borderWidth = 1.0
        txtEmail.layer.borderColor = UIColor.black.cgColor
        
        txtMobile.clipsToBounds = true;
        txtMobile.layer.cornerRadius = 10;
         txtMobile.keyboardType = UIKeyboardType.numberPad
        txtMobile.layer.borderWidth = 1.0
        txtMobile.layer.borderColor = UIColor.black.cgColor
        txtPassword.clipsToBounds = true;
        txtPassword.layer.cornerRadius = 10;
        txtPassword.layer.borderWidth = 1.0
        txtPassword.layer.borderColor = UIColor.black.cgColor
        
        txtCnfPassword.clipsToBounds = true;
        txtCnfPassword.layer.cornerRadius = 10;
        txtCnfPassword.layer.borderWidth = 1.0
        txtCnfPassword.layer.borderColor = UIColor.black.cgColor
        
        
        
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.title = "Register"
        self.tabBarController?.tabBar.isHidden = true
        let nav = self.navigationController?.navigationBar
        nav?.tintColor = UIColor.white
       nav?.barTintColor = UIColor(hexString: "2c3e50", alpha: 1.0)
        nav?.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
       
        
    }
    @IBAction func backButton(_ sender: UIBarButtonItem) {
        dismiss(animated: true, completion: nil)
    }
    @IBAction func submitBtnClicked(_ sender: UIButton) {
        
        
        if self.checkTextIsEmpty(text: txtFirstName.text!) == true
        {
            
            self.showSingleButtonWithMessage(title: "Empty!", message: "Please Enter User Name.", buttonName: "OK")
        } else if self.checkTextIsEmpty(text: txtLastName.text!) == true
        {
            
            self.showSingleButtonWithMessage(title: "Empty!", message: "Please Enter Full Name.", buttonName: "OK")
        }
        else if self.checkTextIsEmpty(text: txtMobile.text!) == true
        {
            
            self.showSingleButtonWithMessage(title: "Empty!", message: "Please Enter Mobile No.", buttonName: "OK")
        }else if self.checkTextIsEmpty(text: txtEmail.text!) == true
        {
            
            self.showSingleButtonWithMessage(title: "Empty!", message: "Please Enter Email Id.", buttonName: "OK")
        }  else if self.checkTextIsEmpty(text: txtPassword.text!) == true
        {
            
            self.showSingleButtonWithMessage(title: "Empty!", message: "Please Enter Password.", buttonName: "OK")
        } else {
            if(txtPassword.text! != txtCnfPassword.text!){
                self.showSingleButtonWithMessage(title: "Mismatch!", message: "Please Confirm Correct Password.", buttonName: "OK")
            }else{
            
            
            SunbitMethod()
                
            }
            
        }
        
    }
    var reachability = Reachability()!
    var window: UIWindow?
    func SunbitMethod() {
        if(self.reachability.connection != .none) {
            
                 let visitRegister = "http://vfeedback.dev.app6.in/API/Registration/"
            var parametersToSet = ["User_ID":txtFirstName.text!,"Password":txtPassword.text!]
            
            self.startLoading()
            
            WebServices.sharedInstances.sendPostRequest(url: visitRegister,parameters: parametersToSet, successHandler: { (dict) in
                
                self.stopLoading()
                
                print("",dict)
                
                if let response = dict["response"] {
                    
                    let statusString : String = response["status"] as! String
                    let objectmsg = MessageCallServerModel()
            let msg = objectmsg.sealizeMessage(cell: response as! [String : AnyObject])
                    self.cnt = 1
                    if(statusString == "success")
                    {
                        
                        
                        
                        self.showSingleButtonAlertWithActionMessage(title: "Successfully Registered!", message: "Your Account is under review.", buttonTitle: "OK", completionHandler: {
                            let storyBoard : UIStoryboard = UIStoryboard(name: "Main",bundle : nil)
                            let loginVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                            self.present(loginVC, animated: true, completion: nil)
                        })
                           // self.dismiss(animated: true, completion: nil)
                        
            
                        
                        
                        
                        
                        
                        
                        
                        
                    } else if(msg == "Already Present")
                    {
                        self.showSingleButtonAlertWithActionMessage(title: "Successfully Registered!", message: "Your Account is under review.", buttonTitle: "OK", completionHandler: {
                            let storyBoard : UIStoryboard = UIStoryboard(name: "Main",bundle : nil)
                            let loginVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                            self.present(loginVC, animated: true, completion: nil)
                        })
                           // self.dismiss(animated: true, completion: nil)
                        
                    }
                    else
                    {
                        self.showSingleButtonAlertWithActionMessage(title: "Successfully Registered!", message: "Your Account is under review.", buttonTitle: "OK", completionHandler: {
                            let storyBoard : UIStoryboard = UIStoryboard(name: "Main",bundle : nil)
                            let loginVC = storyBoard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                            self.present(loginVC, animated: true, completion: nil)
                        })
                    }
                }
                
                
                
                
            }, failureHandler: { (error) in
                
                
                self.stopLoading()
                
                self.errorChecking(error: error[0])
                
                
                
            })
            
           
            
        }
        else {
            
            self.showSingleButtonWithMessage(title: "No Network!", message: "No internet connection found. Check your internet connection or try again.", buttonName: "OK")
            
            
            
        }
        
    }
    func close(){
         dismiss(animated: true, completion: nil)
    }
    var cnt = 0
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
