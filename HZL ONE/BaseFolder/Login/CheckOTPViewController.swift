//
//  CheckOTPViewController.swift
// RAS
//
//  Created by SARVANG INFOTCH on 04/10/18.
//  Copyright © 2018 Bunga Maheshwar Rao. All rights reserved.
//

import UIKit

class CheckOTPViewController: CommonVSClass {

    var strRegister = String()
    var mobileNo = String()
    @IBOutlet weak var btnOtp: UIButton!
    @IBOutlet weak var txtOTP: MytextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        txtOTP.clipsToBounds = true;
        txtOTP.layer.cornerRadius = 10;
        txtOTP.layer.borderWidth = 1.0
        txtOTP.layer.borderColor = UIColor.black.cgColor
        txtOTP.keyboardType = UIKeyboardType.numberPad
self.title = "Verify OTP"
        // Do any additional setup after loading the view.
    }
    @IBAction func btnOTPCLICKED(_ sender: UIButton) {
        
        if(txtOTP.text != "") {
            let otp : String = UserDefaults.standard.value(forKey: "OTP") as! String
            if(otp == txtOTP.text) {
                if(strRegister == "Register") {
                     self.view.makeToast("Successfully Registered.")
                 let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let loginVC  =  storyboard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
                
                self.present(loginVC, animated: true, completion: nil)
                } else {
                    let Profile_AuthKey = UserDefaults.standard.value(forKey: "Profile_AuthKey") as! String
                    let mobile = UserDefaults.standard.value(forKey: "mobile") as! String
                    self.startLoadingPK(view: self.view)
                  
                    let parameter = ["Mobile_No":mobile,
                                     "Profile_AuthKey":Profile_AuthKey ,
                                     "AppName":URLConstants.appName,
                                     "Employee_ID": UserDefaults.standard.string(forKey: "EmployeeID")!
                        
                        ] as [String:String]
                    
                    
                    
                    print("parameter",parameter)
                    
                    WebServices.sharedInstances.sendPostRequest(url: URLConstants.Post_UpdateData, parameters: parameter, successHandler: { (response:[String : AnyObject]) in
                        
                        let respon = response["response"] as! [String:AnyObject]
                        self.stopLoadingPK(view: self.view)
                        let objectmsg = MessageCallServerModel()
                        let message = objectmsg.sealizeMessage(cell: respon as! [String : AnyObject])
                        if respon["status"] as! String == "success" {
                            self.view.makeToast(message)
                            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                                
                                MoveStruct.isMove = true
                                self.navigationController?.popViewController(animated: false)
                            }
                            
                        }else{
                            self.stopLoadingPK(view: self.view)
                            self.view.makeToast(message)
                        }
                        
                    }) { (err) in
                        self.stopLoadingPK(view: self.view)
                        print(err.description)
                    }
                }
            } else {
                 self.view.makeToast("Enter Correct OTP.")
           
            }
        } else {
            self.view.makeToast("Please Enter OTP.")
           
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func barSetup() {
        let view = UIView(frame: CGRect(x: 0, y: 0, width: self.view.bounds.width , height: 44))
        view.backgroundColor = UIColor.clear
        let imgView = UIImageView(frame: CGRect(x: 0, y: 5, width: 25, height: 25))
        imgView.image = UIImage(named: "leftArrowWhite")
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tapBack(_:)))
        imgView.isUserInteractionEnabled = true;
        imgView.addGestureRecognizer(tapGesture)
        let txtBarRight = UITextField(frame: CGRect(x: 100, y: 5, width: self.view.bounds.width , height: 30))
        txtBarRight.text = "Verify OTP"
        txtBarRight.textColor = UIColor.white
        
        txtBarRight.font = UIFont(name: "Georgia", size: 17.0)
        view.addSubview(txtBarRight)
        view.addSubview(imgView)
        let barButtonItem = UIBarButtonItem(customView: view)
        self.navigationItem.leftBarButtonItem = barButtonItem
        
        
    }
    @objc func tapBack(_ sender : UIButton) {
        dismiss(animated: true, completion: nil)
    }
 
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
