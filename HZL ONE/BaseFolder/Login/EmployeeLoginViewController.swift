//
//  EmployeeLoginViewController.swift
// RAS
//
//  Created by Bunga Maheshwar Rao on 01/10/18.
//  Copyright © 2018 Bunga Maheshwar Rao. All rights reserved.
//

import UIKit

class EmployeeLoginViewController: CommonVSClass {

    override func viewDidLoad() {
        super.viewDidLoad()
       
        
        // Do any additional setup after loading the view.
        txtEmployeePersonalNo.clipsToBounds = true;
        txtEmployeePersonalNo.layer.cornerRadius = 10;
        txtEmployeePersonalNo.keyboardType = UIKeyboardType.numberPad
        txtPassword.clipsToBounds = true;
        txtPassword.layer.cornerRadius = 10;
        
        
       // self.showSingleButtonWithMessage(title: "Mahi", message: "Hello", buttonName: "OK")
        
    }
    override func viewDidAppear(_ animated: Bool) {
        
    }
    func alert(){
        let actionSheetController: UIAlertController = UIAlertController(title: "Alert", message: "Swiftly Now! Choose an option!", preferredStyle: .alert)
        
        //Create and add the Cancel action
        let cancelAction: UIAlertAction = UIAlertAction(title: "Cancel", style: .cancel) { action -> Void in
            //Do some stuff
        }
        actionSheetController.addAction(cancelAction)
        //Create and an option action
        let nextAction: UIAlertAction = UIAlertAction(title: "Next", style: .default) { action -> Void in
            //Do some other stuff
        }
        actionSheetController.addAction(nextAction)
        //Add a text field
        
        
        //Present the AlertController
        self.present(actionSheetController, animated: true, completion: nil)
    }
let reachability = Reachability()!
    @IBOutlet weak var btnLogin: UIButton!
    @IBOutlet weak var txtEmployeePersonalNo: MytextField!
    @IBOutlet weak var txtPassword: MytextField!
    @IBAction func btnLoginClicked(_ sender: UIButton) {
        
       loginMethod() 
    }
   
    @IBAction func btnRegisreClicked(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let loginVC  =  storyboard.instantiateViewController(withIdentifier: "MainRegisterViewController") as! MainRegisterViewController
        
        let navigationVC = UINavigationController(rootViewController: loginVC)
        navigationVC.navigationBar.barTintColor =  UIColor(hexString: "2c3e50", alpha: 1.0)
        navigationVC.navigationBar.isTranslucent = false;
      
        navigationVC.navigationBar.tintColor =  UIColor.white
       
        self.present(navigationVC, animated: true, completion: nil)
        
        
        
    }
    var app = AppDelegate()
    func loginMethod() {
        if(self.reachability.connection != .none) {
            
  
            let parameters = ["Email_ID":self.txtEmployeePersonalNo.text!,
                              "Password":self.txtPassword.text! , "AppName" : URLConstants.appName]
            
            self.startLoadingPK(view: self.view)
            
            WebServices.sharedInstances.sendPostRequest(url: URLConstants.setLoginDataEmployee,parameters: parameters, successHandler: { (dict) in
                
                self.stopLoadingPK(view: self.view)
                
                print("",dict)
                
                if let response = dict["response"] {
                    
                    let statusString : String = response["status"] as! String
                    let objectmsg = MessageCallServerModel()
            let msg = objectmsg.sealizeMessage(cell: response as! [String : AnyObject])
                    print(msg)
                    print(response)
                    if(statusString == "success")
                    {
                        
                        
                        
                        if let data = dict["data"]
                        {
                            var EmployeeID = String()
                            var Username = String()
                            
                            var Profile_AuthKey = String()
                            var Email = String()
                            var mobileno = String()
                            var FirstName = String()
                            var LastName = String()
                            var MiddleName = String()
                            var ID = String()
                            var Zone_Name = String()
                            var Unit_Name = String()
                            var Area_Name = String()
                            var Designation = String()
                            var location_Id = String()
                            var location_Name = String()
                    
                           
                            if data["ID"] is NSNull  || data["ID"] == nil{
                                ID = ""
                            }else{
                                let idd = data["ID"] as! Int
                                ID = String(idd)
                            }
                            
                            if data["Employee_ID"] is NSNull  || data["Employee_ID"] == nil{
                                EmployeeID = ""
                            }else{
                                EmployeeID = data["Employee_ID"] as! String
                            }
                            
                            if data["Profile_AuthKey"] is NSNull  || data["Profile_AuthKey"] == nil{
                                Profile_AuthKey = ""
                            }else{
                                Profile_AuthKey = data["Profile_AuthKey"] as! String
                            }
                            
                            if data["Email_ID"] is NSNull  || data["Email_ID"] == nil{
                                Email = ""
                            }else{
                                Email = data["Email_ID"] as! String
                            }
                            if data["Last_Name"] is NSNull || data["Last_Name"] == nil{
                                LastName = ""
                            }else{
                                LastName = data["Last_Name"] as! String
                            }
                            if data["First_Name"] is NSNull || data["First_Name"] == nil{
                                FirstName = ""
                            }else{
                                FirstName = data["First_Name"] as! String
                            }
                            if data["Middle_Name"] is NSNull || data["Middle_Name"] == nil{
                                MiddleName = ""
                            }else{
                                MiddleName = data["Middle_Name"] as! String
                            }
                            if data["Designation"] is NSNull || data["Designation"] == nil{
                                Designation = ""
                            }else{
                                Designation = data["Designation"] as! String
                            }
                            
                            if data["Area_Name"] is NSNull || data["Area_Name"] == nil{
                                Area_Name = ""
                            }else{
                                Area_Name = data["Area_Name"] as! String
                            }
                            if data["Zone_Name"] is NSNull || data["Zone_Name"] == nil{
                                Zone_Name = ""
                            }else{
                                Zone_Name = data["Zone_Name"] as! String
                            }
                            
                            if data["Unit_Name"] is NSNull || data["Unit_Name"] == nil{
                                Unit_Name = ""
                            }else{
                                Unit_Name = data["Unit_Name"] as! String
                            }
                            if data["Location"] is NSNull || data["Location"] == nil{
                                location_Id =  ""
                            }else{
                                let idd = data["Location"] as! Int
                                location_Id = String(idd)
                               
                               
                            }
                            if data["Location_Name"] is NSNull || data["Location_Name"] == nil{
                                location_Name =  ""
                            }else{
                                
                                location_Name = data["Location_Name"] as! String
                            }
                         
                            UserDefaults.standard.set(ID, forKey: "IDData")
                            UserDefaults.standard.set(location_Id, forKey: "LocationId")
                            
                            UserDefaults.standard.set(location_Name, forKey: "LocationName")
                            UserDefaults.standard.set(Unit_Name, forKey: "Unit_Name")
                            UserDefaults.standard.set(Zone_Name, forKey: "Zone_Name")
                            UserDefaults.standard.set(Area_Name, forKey: "Area_Name")
                            UserDefaults.standard.set(Designation, forKey: "Designation")
                            
                            UserDefaults.standard.set("", forKey: "UserCode")
                            
                            
                            
                            
                            
                            
                            
                            self.stopLoadingPK(view: self.view)
                            print(Profile_AuthKey)
                            
                           let mobile = data["Mobile_Number"]
                            UserDefaults.standard.set(Username, forKey: "Username")
                            UserDefaults.standard.set(String(EmployeeID), forKey: "EmployeeID")
                            UserDefaults.standard.set(Profile_AuthKey, forKey: "Profile_AuthKey")
                            UserDefaults.standard.set(Email, forKey: "Email")
                            UserDefaults.standard.set(FirstName, forKey: "FirstName")
                            UserDefaults.standard.set(MiddleName, forKey: "MiddleName")
                             UserDefaults.standard.set(LastName, forKey: "Lastname")
                            UserDefaults.standard.set("E", forKey: "LoginType")
                            if mobile is NSNull || mobile == nil {
                                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                let loginVC  =  storyboard.instantiateViewController(withIdentifier: "RegisterLoginViewController") as! RegisterLoginViewController
                          
                                let navigationVC = UINavigationController(rootViewController: loginVC)
                                navigationVC.navigationBar.barTintColor =  UIColor(hexString: "2c3e50", alpha: 1.0)
                                navigationVC.navigationBar.isTranslucent = false;
                                navigationVC.navigationBar.isTranslucent = false;
                                navigationVC.navigationBar.tintColor =  UIColor.white
                                self.present(navigationVC, animated: true, completion: nil)
                            } else {
                                 mobileno = data["Mobile_Number"] as! String
                                if (mobileno == "") {
                                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                    let loginVC  =  storyboard.instantiateViewController(withIdentifier: "RegisterLoginViewController") as! RegisterLoginViewController
                                    
                                    let navigationVC = UINavigationController(rootViewController: loginVC)
                                    navigationVC.navigationBar.barTintColor =  UIColor(hexString: "2c3e50", alpha: 1.0)
                                    navigationVC.navigationBar.isTranslucent = false;
                                    
                                    navigationVC.navigationBar.tintColor =  UIColor.white
                                    self.present(navigationVC, animated: true, completion: nil)
                                } else {
                                UserDefaults.standard.set(mobileno, forKey: "mobileno")
                            UserDefaults.standard.set(true, forKey: "isLogin")
                           UserDefaults.standard.set(true, forKey: "isWorkmanLogin")
                            
                            let storyboard = UIStoryboard(name: "HZLONE", bundle: nil)
                            let loginVC  =  ExampleProvider.customBouncesStyle()
                                
             
                                   
                                    
//                                    self.app.window?.rootViewController = ExampleProvider.customBouncesStyle()
//                                    self.app.window?.makeKeyAndVisible()
                            self.present(loginVC, animated: true, completion: nil)
                                }
                            }
                            
                            
                            
                            
                        }
                        
                        
                    }
                    else
                    {
                        self.stopLoadingPK(view: self.view)
                        self.showSingleButtonWithMessage(title: statusString, message: msg, buttonName: "OK")
                    }
                }
                
                
                
                
            }, failureHandler: { (error) in
                
              
                 self.stopLoadingPK(view: self.view)
                
                self.errorChecking(error: error[0])
                
                
                
            })
            
            
            
        }
        else {
             self.stopLoadingPK(view: self.view)
            self.showSingleButtonWithMessage(title: "No Network!", message: "No internet connection found. Check your internet connection or try again.", buttonName: "OK")
            
            
            
        }
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
public extension UIAlertController {
    func show() {
        let win = UIWindow(frame: UIScreen.main.bounds)
        let vc = UIViewController()
        vc.view.backgroundColor = .clear
        win.rootViewController = vc
        win.windowLevel = UIWindowLevelAlert + 1
        win.makeKeyAndVisible()
        vc.present(self, animated: true, completion: nil)
    }
}
