//
//  RegisterLoginViewController.swift
//  VallSafety
//
//  Created by SARVANG INFOTCH on 15/11/18.
//  Copyright © 2018 Bunga Maheshwar Rao. All rights reserved.
//

import UIKit

class RegisterLoginViewController: CommonVSClass ,UITextFieldDelegate{
    var strRegister1 = String()
    @IBOutlet weak var txtMobile: MytextField!
    @IBOutlet weak var btnRegister: UIButton!
    @IBAction func btnRegisterClicked(_ sender: UIButton) {
        checkOTP()
        
    }
    
    @objc func tapBack(_ sender : UIButton) {
        dismiss(animated: true, completion: nil)
    }
        func barSetup() {
            let view = UIView(frame: CGRect(x: 0, y: 0, width: 25 , height: 25))
            view.backgroundColor = UIColor.clear
            let imgView = UIImageView(frame: CGRect(x: 0, y: 0, width: 25, height: 25))
            imgView.image = UIImage(named: "backWhite")
            let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tapBack(_:)))
            imgView.isUserInteractionEnabled = true;
            imgView.addGestureRecognizer(tapGesture)
            
           view.addSubview(imgView)
            let barButtonItem = UIBarButtonItem(customView: view)
            self.navigationItem.leftBarButtonItem = barButtonItem
    
    //
        }
    @IBAction func back(_ sender: UIBarButtonItem) {
         dismiss(animated: true, completion: nil)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        barSetup()
        btnRegister.setTitle("Send OTP", for: .normal)
        
        txtMobile.clipsToBounds = true;
        txtMobile.layer.cornerRadius = 10;
        txtMobile.delegate = self
        txtMobile.layer.borderWidth = 1.0
        txtMobile.keyboardType = UIKeyboardType.numberPad
        txtMobile.layer.borderColor = UIColor.black.cgColor
        // Do any additional setup after loading the view.
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return true }
        let newLength = text.count + string.count - range.length
        return newLength <= 10
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.title = "Register Mobile"
        self.tabBarController?.tabBar.isHidden = true
        let nav = self.navigationController?.navigationBar
        nav?.barStyle = UIBarStyle.black
        nav?.tintColor = UIColor.white
        nav?.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    let reachability = Reachability()!
    func checkOTP() {
        if (txtMobile.text!.characters.count != 10) {
            
            self.view.makeToast("Please Enter Correct Mobile No.")
        } else{
            
            
            if(self.reachability.connection != .none) {
                
                let EmpID : String = UserDefaults.standard.value(forKey: "EmployeeID") as! String
                let parameters = ["Personnel_Number":EmpID,
                                  "Mobile_No":self.txtMobile.text! , "AppName" : URLConstants.appName] as [String : Any]
                
                self.startLoadingPK(view: self.view)
                
                WebServices.sharedInstances.sendPostRequest(url: URLConstants.verifyOTP,parameters: parameters, successHandler: { (dict) in
                    
                    
                    
                    print("",dict)
                    
                    if let response = dict["response"] {
                        
                        let statusString : String = response["status"] as! String
                        let objectmsg = MessageCallServerModel()
            let msg = objectmsg.sealizeMessage(cell: response as! [String : AnyObject])
                        print(msg)
                        if(statusString == "success")
                        {
                            
                            
                            self.stopLoadingPK(view: self.view)
                            if let data = dict["data"]
                            {
                                
                                
                                let OTP = data["OTP"] as! String
                                
                                
                                UserDefaults.standard.set(OTP, forKey: "OTP")
                                if(self.strRegister1 == "Register") {
                                    if(self.txtMobile.text != "") {
                                        UserDefaults.standard.set(self.txtMobile.text, forKey: "mobile")
                                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                        
                                        let loginVC  =  storyboard.instantiateViewController(withIdentifier: "CheckOTPViewController") as! CheckOTPViewController
                                        loginVC.strRegister = self.strRegister1
                                        self.navigationController?.pushViewController(loginVC, animated: true)
                                    }
                                } else {
                                    UserDefaults.standard.set(self.txtMobile.text, forKey: "mobile")
                                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                    let loginVC  =  storyboard.instantiateViewController(withIdentifier: "CheckOTPViewController") as! CheckOTPViewController
                                    loginVC.strRegister = self.strRegister1
                                    self.navigationController?.pushViewController(loginVC, animated: true)
                                }
                                
                                
                                
                                
                                
                                
                            }
                            
                            
                        }
                        else
                        {  self.stopLoadingPK(view: self.view)
                            self.showSingleButtonWithMessage(title: statusString, message: msg, buttonName: "OK")
                        }
                    }
                    
                    
                    
                    
                }, failureHandler: { (error) in
                    
                    
                    self.stopLoadingPK(view: self.view)
                    
                    self.errorChecking(error: error[0])
                    
                    
                    
                })
                
                
                
            }
            else {
                self.stopLoadingPK(view: self.view)
                self.showSingleButtonWithMessage(title: "No Network!", message: "No internet connection found. Check your internet connection or try again.", buttonName: "OK")
                
                
                
            }
        }
        
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
