//
//  IdeaClubPointsViewController.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 11/02/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

//
//  IdeaClubPointsViewController.swift
//  Sesa Goa Idea App
//
//  Created by SARVANG INFOTCH on 06/11/18.
//  Copyright © 2018 SARVANG INFOTCH. All rights reserved.
//

import UIKit

class IdeaClubPointsViewController: CommonVSClass,UITableViewDelegate,UITableViewDataSource {
    var ClubPointsListAPI = ClubPointsListDataAPI()
    var ClubPointsListDB:[ClubPointsListDataModel] = []
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var lblPointsTotal: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        getClubPointsListData()
        refresh.addTarget(self, action: #selector(getClubPointsListData), for: .valueChanged)
        
        self.tableView.addSubview(refresh)
        let swipe = UISwipeGestureRecognizer(target: self, action: #selector(self.swipeRecognizer))
        swipe.direction = ([.down])
        self.view.addGestureRecognizer(swipe)
        
        tableView.tableFooterView = UIView()
        
        
        
    }
    
    var reachability = Reachability()!
    @objc func swipeRecognizer(_ sender: UISwipeGestureRecognizer) {
        if (sender.direction == .down) {
            
            if(self.reachability.connection != .none)
            {
                
                self.getClubPointsListData()
            }
            else
            {
                self.startLoading(view: self.view)
                DispatchQueue.main.asyncAfter(deadline: .now() + 4.5) {
                    self.noDataLabel(text: "No internet connection found. Check your internet connection and try again.")
                    self.stopLoading(view: self.view)
                }
            }
            
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        
        self.title = "Idea Club Point"
        self.tabBarController?.tabBar.isHidden = true
        let nav = self.navigationController?.navigationBar
        nav?.isTranslucent = false
        nav?.tintColor = UIColor.white
       nav?.barTintColor = UIColor(hexString: "2c3e50", alpha: 1.0)
        nav?.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        //  callMyReportedHazardData()
        
    }
    @objc func getClubPointsListData() {
        let empId : String = UserDefaults.standard.string(forKey: "EmployeeID")!
        var parameter : [String : String ] = [:]
        var  dictWithoutNilValues = [String:String]()
        
        
        dictWithoutNilValues = ["Employee_ID":empId
            
        ]
        parameter = dictWithoutNilValues.filter { $0.value != ""}
        ClubPointsListAPI.serviceCalling(obj: self, parameter:parameter ) { (dict) in
            
            self.ClubPointsListDB = [ClubPointsListDataModel]()
            self.ClubPointsListDB = dict as! [ClubPointsListDataModel]
            
            self.tableView.reloadData()
            
            
            
            
        }
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.ClubPointsListDB.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        
        return 1
        
        
    }
    
    var LastSelected : IndexPath?
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! ClubPointsTableViewCell
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = NSTimeZone.system
        dateFormatter.dateFormat = "MMM dd yyyy HH:mm:ss:SSSa"
        let date = dateFormatter.date(from: self.ClubPointsListDB[indexPath.section].DateTime)
        
        let dateFormatter2 = DateFormatter()
        dateFormatter2.timeZone = NSTimeZone.system
        dateFormatter2.dateFormat = "dd MMM yyyy"
        
        
        let dateFormatterTime = DateFormatter()
        dateFormatterTime.timeZone = NSTimeZone.system
        dateFormatterTime.dateFormat = "HH:mm a"
        var dateStr = String()
        switch date {
        case nil:
            let date_TimeStr = dateFormatter2.string(from: Date())
            //  let time_TimeStr = dateFormatterTime.string(from: Date())
            dateStr = date_TimeStr
            break;
        default:
            let date_TimeStr = dateFormatter2.string(from: date!)
            
            // let time_TimeStr = dateFormatterTime.string(from: date!)
            dateStr = date_TimeStr
            break;
        }
        cell.lblDate.text = dateStr
        cell.lblrewardDesc.text = self.ClubPointsListDB[indexPath.section].Description
        
        var  pointCr = Int()
        var  pointDr = Int()
        var sign = String()
        if(self.ClubPointsListDB[indexPath.section].PointsCr == nil || self.ClubPointsListDB[indexPath.section].PointsCr == "" || self.ClubPointsListDB[indexPath.section].PointsCr == "0" ){
            pointCr = 0
        }else {
            pointCr = Int(self.ClubPointsListDB[indexPath.section].PointsCr)!
            sign = "+ "
        }
        if(self.ClubPointsListDB[indexPath.section].PointsDr == nil || self.ClubPointsListDB[indexPath.section].PointsDr == "" || self.ClubPointsListDB[indexPath.section].PointsDr == "0"  ){
            pointDr = 0
        }else {
            pointDr = Int(self.ClubPointsListDB[indexPath.section].PointsDr)!
            sign = "- "
        }
        
        if(pointDr == 0){
            cell.lblRewardPoints.text = sign  + String( pointCr)
        }else{
            cell.lblRewardPoints.text = sign  + String( pointDr)
        }
        
        lblPointsTotal.text = self.ClubPointsListDB[indexPath.section].Total_Balance
        
        
        
        return cell
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
