//
//  InboxReportedViewController.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 11/02/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit
import SDWebImage

class InboxReportedViewController: CommonVSClass {
    
    //    var ID : [String] = ["16" ,"17"]
    //var Date : [String] = ["27-Sep-2018","27-Sep-2018"]
    
    var imagedata : [String] = []
    var hazarddisplay = Int()
    
    
    
    var DataAPI = ReportedInboxForStatus()
    var hazardDB : [ReportedInboxStatus] = []
    var data: String?
    var lastObject: String?
    var indPath = IndexPath()
    var hazardLoadMoreDB : [ReportedInboxStatus] = []
    
    
    @IBOutlet weak var tableViewReported: UITableView!
    
    
    var DataLoadMoreAPI = ReportedInboxForStatusLoadMore()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableViewReported.contentInset = UIEdgeInsetsMake(0.0, 0.0, 0.0, 0.0)
        tableViewReported.delegate = self;
        tableViewReported.dataSource = self;
        tableViewReported.estimatedRowHeight = 290
        tableViewReported.rowHeight = UITableViewAutomaticDimension
        
        
        refresh.addTarget(self, action: #selector(ReportdatabyStatus), for: .valueChanged)
        
        self.tableViewReported.addSubview(refresh)
        let swipe = UISwipeGestureRecognizer(target: self, action: #selector(self.swipeRecognizer))
        swipe.direction = ([.down])
        self.view.addGestureRecognizer(swipe)
        tableViewReported.tableFooterView = UIView()
        
        
        // ReportdatabyStatus()
        
        
        //barSetup()
        
        
        // Do any additional setup after loading the view.
    }
    var reachability = Reachability()!
    @objc func swipeRecognizer(_ sender: UISwipeGestureRecognizer) {
        if (sender.direction == .down) {
            
            if(self.reachability.connection != .none)
            {
                
                self.ReportdatabyStatus()
            }
            else
            {
                self.startLoading(view: self.view)
                DispatchQueue.main.asyncAfter(deadline: .now() + 4.5) {
                    self.noDataLabel(text: "No internet connection found. Check your internet connection and try again.")
                    self.stopLoading(view: self.view)
                }
                
            }
            
        }
    }
    func setUp() {
        if(MoveStruct.isMove == true){
            MoveStruct.isMove = false
            self.view.makeToast(MoveStruct.message)
        }
        hazarddisplay = UserDefaults.standard.integer(forKey: "hazard")
        print(hazarddisplay)
        self.title = UserDefaults.standard.string(forKey: "StatusTitle")
        self.tabBarController?.tabBar.isHidden = true
        let nav = self.navigationController?.navigationBar
        nav?.tintColor = UIColor.white
       nav?.barTintColor = UIColor(hexString: "2c3e50", alpha: 1.0)
        nav?.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        ReportdatabyStatus()
    }
    override func viewDidAppear(_ animated: Bool) {
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        setUp()
    }
    override func viewWillDisappear(_ animated: Bool) {
        
    }
    
    
    @objc func tapBack(_ sender : UIButton) {
        dismiss(animated: true, completion: nil)
    }
    
    @objc func ImageViewTapped(_ sender : UITapGestureRecognizer) {
        let buttonPosition:CGPoint = sender.view!.convert(.zero, to:self.tableViewReported)
        let indexPath = self.tableViewReported.indexPathForRow(at: buttonPosition)
        
        if hazardDB[(indexPath?.row)!].Image_URL != "" {
            
            let ZIVC = self.storyboard?.instantiateViewController(withIdentifier: "ImageZoom") as! ImageZoomViewController
            ZIVC.zoomImageUrl = hazardDB[(indexPath?.row)!].Image_URL!
            self.navigationController?.pushViewController(ZIVC, animated: true)
            
        }
    }
    
    @objc func tapCell(_ sender : UITapGestureRecognizer) {
        
        
        let buttonPosition:CGPoint = sender.view!.convert(.zero, to:self.tableViewReported)
        let indexPath = self.tableViewReported.indexPathForRow(at: buttonPosition)
        
        let storyBoard = UIStoryboard(name: "IdeaGeneration", bundle: nil)
        let ZIVC = storyBoard.instantiateViewController(withIdentifier: "InboxIdeaDetailsViewController") as! InboxIdeaDetailsViewController
        ZIVC.MyReportedHazardDB = hazardDB
        ZIVC.row = (indexPath?.row)!
        ZIVC.IdeaID = String(describing: hazardDB[(indexPath?.row)!].ID)
        ZIVC.empId = hazardDB[(indexPath?.row)!].Employee_ID!
        self.navigationController?.pushViewController(ZIVC, animated: true)
        
    }
    
    var countID : Int = 0
    
    @objc func ReportdatabyStatus() {
        
        
        
        
        let status : String = UserDefaults.standard.value(forKey: "Status") as! String
        let empId : String = UserDefaults.standard.string(forKey: "EmployeeID")!
        
        print(status)
        var parameter : [String : String ] = [:]
        var  dictWithoutNilValues = [String:String]()
        
        
        dictWithoutNilValues = ["Employee_ID":empId,
                                
                                "Flag" : status]
        parameter = dictWithoutNilValues.filter { $0.value != ""}
        
        print(parameter)
        //para = parameter.filter { $0.value != ""}
        var url : String = ""
        if(hazarddisplay == 1) {
            url = URLConstants.My_Inbox
        }
        else {
            
            url = URLConstants.HOD_Inbox
        }
        
        
        
        self.DataAPI.serviceCalling(obj: self, parameter: parameter , status: status, statusData: self.title!, url: url) { (dict) in
            
            self.hazardDB = [ReportedInboxStatus]()
            self.hazardDB = dict as! [ReportedInboxStatus]
            self.tableViewReported.reloadData()
        }
        
    }
    func callMyReportedReportedHazardLoadMore(Id:String) {
        
        let status : String = UserDefaults.standard.value(forKey: "Status") as! String
        let empId : String = UserDefaults.standard.string(forKey: "EmployeeID")!
        var url : String = ""
        var  dictWithoutNilValues = [String:String]()
        if(hazarddisplay == 1) {
            url = URLConstants.My_Inbox
        }
        else {
            
            url = URLConstants.HOD_Inbox
        }
        var parameter : [String : String ] = [:]
        
        
        
        
        
        
        
        dictWithoutNilValues = ["Employee_ID":empId,
                                "ID" : Id ,
                                "Flag" : status]
        
        
        parameter = dictWithoutNilValues.filter { $0.value != ""}
        
        print(parameter)
        
        DataLoadMoreAPI.serviceCalling(obj: self, parameter: parameter , url : url) { (dict) in
            
            self.hazardLoadMoreDB = [ReportedInboxStatus]()
            self.hazardLoadMoreDB = dict as! [ReportedInboxStatus]
            
            switch dict.count {
            case 0:
                break;
            default:
                self.hazardDB.append(contentsOf: self.hazardLoadMoreDB)
                self.tableViewReported.reloadData()
                break;
            }
            
        }
        
    }
    func imageResize (image:UIImage, sizeChange:CGSize)-> UIImage{
        
        let hasAlpha = true
        let scale: CGFloat = 0.0 // Use scale factor of main screen
        
        UIGraphicsBeginImageContextWithOptions(sizeChange, !hasAlpha, scale)
        image.draw(in: CGRect(origin: CGPoint.zero, size: sizeChange))
        
        let scaledImage = UIGraphicsGetImageFromCurrentImageContext()
        return scaledImage!
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    var imageData : Data? = nil
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
extension InboxReportedViewController : UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if(section == 0) {
            return 10.0
        } else {
            return 0.0
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return hazardDB.count
    }
    
    
}

extension InboxReportedViewController : UITableViewDataSource {
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! ReportedInboxDataTableViewCell
        
        
        cell.statusTextView.tag = indexPath.row
        
        
        cell.statusOfHazard.text = self.hazardDB[indexPath.row].Idea_Status
        cell.idOfHazard.text = "ID #"+String(describing: self.hazardDB[indexPath.row].ID)
        
        
        
        
        switch cell.statusOfHazard.text! {
        case "Implemented":
            cell.statusOfHazard.textColor = UIColor.colorwithHexString("#5c9834", alpha: 1)
            cell.viewStatus.backgroundColor = UIColor.colorwithHexString("#5c9834", alpha: 1)
            break;
        case "Pending":
            cell.statusOfHazard.textColor = UIColor.colorwithHexString("#cfcf00", alpha: 1)
            cell.viewStatus.backgroundColor = UIColor.colorwithHexString("#cfcf00", alpha: 1)
            break;
        case "Assigned":
            cell.statusOfHazard.textColor = UIColor.colorwithHexString("#00800ff", alpha: 1)
            cell.viewStatus.backgroundColor = UIColor.colorwithHexString("#00800ff", alpha: 1)
            break;
        case "Rejected":
            cell.statusOfHazard.textColor = UIColor.colorwithHexString("#FF0000", alpha: 1)
            cell.viewStatus.backgroundColor = UIColor.colorwithHexString("#FF0000", alpha: 1)
            break;
        case "Accepted":
            cell.statusOfHazard.textColor = UIColor.colorwithHexString("#FF69B4", alpha: 1)
            cell.viewStatus.backgroundColor = UIColor.colorwithHexString("#FF69B4", alpha: 1)
            break;
        default:
            break;
        }
        
        
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = NSTimeZone.system
        dateFormatter.dateFormat = "MMM dd yyyy HH:mm:ss:SSSa"
        let date = dateFormatter.date(from: self.hazardDB[indexPath.row].Status_DateTime!)
        
        let dateFormatter2 = DateFormatter()
        dateFormatter2.timeZone = NSTimeZone.system
        dateFormatter2.dateFormat = "dd MMM yyyy"
        
        switch date {
        case nil:
            let date_TimeStr = dateFormatter2.string(from: Date())
            cell.dateLabel.text = date_TimeStr
            break;
        default:
            let date_TimeStr = dateFormatter2.string(from: date!)
            
            cell.dateLabel.text = date_TimeStr
            break;
        }
        
        let dateFormatter1 = DateFormatter()
        dateFormatter1.timeZone = NSTimeZone.system
        dateFormatter1.dateFormat = "MMM dd yyyy HH:mm:ss:SSSa"
        let date1 = dateFormatter.date(from: self.hazardDB[indexPath.row].Create_Date_Time!)
        
        let dateFormatter21 = DateFormatter()
        dateFormatter21.timeZone = NSTimeZone.system
        dateFormatter21.dateFormat = "dd MMM yyyy"
        
        
        let dateFormatterTime1 = DateFormatter()
        dateFormatterTime1.timeZone = NSTimeZone.system
        dateFormatterTime1.dateFormat = "HH:mm a"
        var dateStr1 = String()
        switch date1 {
        case nil:
            let date_TimeStr = dateFormatter21.string(from: Date())
            //  let time_TimeStr = dateFormatterTime.string(from: Date())
            dateStr1 = date_TimeStr
            break;
        default:
            let date_TimeStr = dateFormatter21.string(from: date1!)
            
            // let time_TimeStr = dateFormatterTime.string(from: date!)
            dateStr1 = date_TimeStr
            break;
        }
        let statusTextViewTapGesture = UITapGestureRecognizer(target: self, action: #selector(self.tapCell(_:)))
        cell.statusTextView.addGestureRecognizer(statusTextViewTapGesture)
        cell.statusTextView.isUserInteractionEnabled = true
        statusTextViewTapGesture.numberOfTapsRequired = 1
        
        
        // cell.titleTextView.text = self.hazardDB[indexPath.row].descriptionn
        cell.titleTextView.font = UIFont.systemFont(ofSize: 15.0)
        let paragraph = NSMutableParagraphStyle()
        paragraph.alignment = .left
        paragraph.lineSpacing = 0
        let empId : String = UserDefaults.standard.string(forKey: "EmployeeID")!
        var startString : String = "You "
        if(empId != hazardDB[indexPath.row].Employee_ID){
            startString =  hazardDB[indexPath.row].Employee_Name! + "-" +   String(hazardDB[indexPath.row].Employee_ID!) + " "
        }
        
        let  mBuilder = startString  + "submitted Idea for benefit " + self.hazardDB[indexPath.row].Benefit! + " under department " +  self.hazardDB[indexPath.row].Department_Name! +
            " for theme " + self.hazardDB[indexPath.row].ThemeName! + " on " + dateStr1
        
        let agreeAttributedString = NSMutableAttributedString(string: mBuilder, attributes: [NSAttributedStringKey.foregroundColor: UIColor.black, NSAttributedStringKey.font: UIFont.systemFont(ofSize: CGFloat(15.0))])
        
        
        //Submitted
        let SubmittedAttributedString = NSAttributedString(string:"submitted Idea for benefit", attributes: [NSAttributedStringKey.foregroundColor: UIColor.gray, NSAttributedStringKey.font: UIFont.systemFont(ofSize: 15.0)])
        
        let range: NSRange = (agreeAttributedString.string as NSString).range(of: "submitted Idea for benefit")
        if range.location != NSNotFound {
            agreeAttributedString.replaceCharacters(in: range, with: SubmittedAttributedString)
        }
        
        //for
        let forAttributedString = NSAttributedString(string:"under department", attributes: [NSAttributedStringKey.foregroundColor: UIColor.gray, NSAttributedStringKey.font: UIFont.systemFont(ofSize: 15.0)])
        
        let forRange: NSRange = (agreeAttributedString.string as NSString).range(of: "under department")
        if forRange.location != NSNotFound {
            agreeAttributedString.replaceCharacters(in: forRange, with: forAttributedString)
        }
        let subAreaAttributedString = NSAttributedString(string:"for theme", attributes: [NSAttributedStringKey.foregroundColor: UIColor.gray, NSAttributedStringKey.font: UIFont.systemFont(ofSize: 15.0)])
        let subAreaRange: NSRange = (agreeAttributedString.string as NSString).range(of: "for theme")
        if subAreaRange.location != NSNotFound {
            agreeAttributedString.replaceCharacters(in: subAreaRange, with: subAreaAttributedString)
        }
        
        
        let onAttributedString = NSAttributedString(string:"on", attributes: [NSAttributedStringKey.foregroundColor: UIColor.gray, NSAttributedStringKey.font: UIFont.systemFont(ofSize: 15.0)])
        let onRange: NSRange = (agreeAttributedString.string as NSString).range(of: "on")
        if onRange.location != NSNotFound {
            agreeAttributedString.replaceCharacters(in: onRange, with: onAttributedString)
        }
               agreeAttributedString.addAttribute(NSAttributedStringKey.paragraphStyle, value: paragraph, range: NSRange(location: 0, length: agreeAttributedString.length))
        
        cell.statusTextView.attributedText = agreeAttributedString
        
        cell.statusTextView.font = UIFont.systemFont(ofSize: 15.0)
        self.data = self.hazardDB[indexPath.row].Description!
        self.lastObject = self.hazardDB[indexPath.row].Description!
        
        
        let imageViewDataCell = UIImageView()
        imageViewDataCell.image = UIImage(named : "placed")
        let postImageTapGesture = UITapGestureRecognizer(target: self, action: #selector(self.ImageViewTapped(_:)))
        if(cell.imageFile != nil) {
            cell.imageFile.addGestureRecognizer(postImageTapGesture)
            cell.imageFile.isUserInteractionEnabled = true
            postImageTapGesture.numberOfTapsRequired = 1
        }
        
        
        
        
        
        
        let paragraph1 = NSMutableParagraphStyle()
        paragraph1.alignment = .left
        paragraph1.lineSpacing = 0
        let titleStr = NSMutableAttributedString(string: self.hazardDB[indexPath.row].Description!, attributes: [NSAttributedStringKey.foregroundColor: UIColor.black, NSAttributedStringKey.font: UIFont.systemFont(ofSize: CGFloat(15.0))])
        titleStr.addAttribute(NSAttributedStringKey.paragraphStyle, value: paragraph1, range: NSRange(location: 0, length: titleStr.length))
        cell.titleTextView.attributedText = titleStr
        
        
        
        cell.statusTextView.isEditable = false
        cell.statusTextView.isSelectable = false
        cell.statusTextView.isScrollEnabled = false
        cell.titleTextView.isEditable = false
        cell.titleTextView.isSelectable = false
        cell.titleTextView.isScrollEnabled = false
        cell.titleTextView.textContainer.maximumNumberOfLines = 3
        cell.titleTextView.textContainer.lineBreakMode = .byTruncatingTail
        cell.selectionStyle = .none
       
        print(self.hazardDB[indexPath.row].Image_URL!)
        // cell.imageFile.isHidden = true
        let imageView = UIImageView()
        let urlString = self.hazardDB[indexPath.row].Image_URL!
        let urlShow = urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
        if let url = NSURL(string: self.hazardDB[indexPath.row].Image_URL!) {
            
            SDWebImageManager.shared().downloadImage(with: NSURL(string: urlShow!) as URL!, options: .continueInBackground, progress: {
                (receivedSize :Int, ExpectedSize :Int) in
                
            }, completed: {
                (image : UIImage?, error : Error?, cacheType : SDImageCacheType, finished : Bool, url : URL?) in
                if(image != nil) {
                    let size = CGSize(width: 100, height: 100)
                    let imageCell = self.imageResize(image: image!, sizeChange: size)
                    cell.imageFile.image = imageCell
                }else{
                    imageView.image =  UIImage(named: "placed")
                    let size = CGSize(width: 100, height: 100)
                    let imageCell = self.imageResize(image: imageView.image!, sizeChange: size)
                    cell.imageFile.image = imageCell
                    //  cell.viewImage.backgroundColor = UIColor(patternImage: imageCell)
                }
            })
        }else{
            imageView.image =  UIImage(named: "placed")
            let size = CGSize(width: 100, height: 100)
            let imageCell = imageResize(image: imageView.image!, sizeChange: size)
            cell.imageFile.image = imageCell
            //cell.viewImage.backgroundColor = UIColor(patternImage: imageCell)
        }
        
        
        cell.titleTextView.textColor = UIColor(hexString: "2c3e50", alpha: 1.0)
        
        
        
        if ( self.data ==  self.lastObject && indexPath.row == self.hazardDB.count - 1)
        {
            
            self.callMyReportedReportedHazardLoadMore( Id: String(Int(self.hazardDB[indexPath.row].ID)))
            
        }
        
        cell.separatorInset = UIEdgeInsetsMake(0, 10000, 0, 0);
        
        return cell
        
        
        
        
        
        
    }
}



