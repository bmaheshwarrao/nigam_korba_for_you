//
//  AssignedMangerIdeaModel.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 11/02/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import Foundation


class AreaManagersListDataInDetailsModel: NSObject {
    
    var name = String()
    
    var Is_Implemented = String()
    var Employee_ID = String()
    
    func setDataInModel(str:[String:AnyObject])
    {
        
        if str["Employee_Name"] is NSNull || str["Employee_Name"] == nil{
            self.name = ""
        }else{
            let emp1 = str["Employee_Name"]
            
            self.name = (emp1?.description)!
        }
        
        if str["Employee_ID"] is NSNull || str["Employee_ID"] == nil{
            
        }else{
            let emp1 = str["Employee_ID"]
            
            self.Employee_ID = (emp1?.description)!
        }
        if str["Is_Implemented"] is NSNull || str["Is_Implemented"] == nil{
            self.Is_Implemented = ""
        }else{
            let emp1 = str["Is_Implemented"]
            
            self.Is_Implemented = (emp1?.description)!
        }
        
        
    }
}




class AreaManagersListDataInDetailsAPI
{
    
    var reachablty = Reachability()!
    
    
    func serviceCalling(obj:InboxIdeaDetailsViewController, Idea_ID:String, success:@escaping (AnyObject)-> Void)
    {
        
        obj.startLoading()
        
        
        if(reachablty.connection != .none)
        {
            
            let url =  URLConstants.Area_Managers_List + "?Idea_ID=\(Idea_ID)"
            WebServices.sharedInstances.sendGetRequest(Url: url, successHandler: { (dict) in
                
                
                if let response = dict["response"]{
                    
                    let statusString : String = response["status"] as! String
                    
                    if(statusString == "success")
                    {
                        
                        //                        obj.tableView.isHidden = false
                        //                        obj.noDataLabel(text: "" )
                        
                        if let data = dict["data"] as? [AnyObject]
                        {
                            
                            var dataArray : [AreaManagersListDataInDetailsModel] = []
                            
                            for i in 0..<data.count
                            {
                                
                                if let cell = data[i] as? [String:AnyObject]
                                {
                                    let object = AreaManagersListDataInDetailsModel()
                                    object.setDataInModel(str: cell)
                                    dataArray.append(object)
                                    
                                }
                                
                            }
                            obj.lblInt = 0
                            obj.tableView.reloadData()
                            success(dataArray as AnyObject)
                            
                            
                        }
                        
                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                    }
                    else
                    {
                        obj.lblInt = 1
                        obj.tableView.reloadData()
                        print("DATA:fail")
                        //                        obj.tableView.isHidden = true
                        //                        obj.noDataLabel(text: "Sorry! No area manager found with name "+name)
                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                        
                    }
                    
                    
                }
                
                
                
            }, failureHandler: { (error) in
                let errorStr : String = error.description
                obj.lblInt = 1
                obj.tableView.reloadData()
                print("DATA:error",errorStr)
                
                //  obj.errorChecking(error: error)
                obj.stopLoading()
            })
            
            
        }
        else{
            //            obj.noDataLabel(text:  "No internet connection found. Check your internet connection and try again.")
            //            obj.tableView.isHidden = true
            obj.refresh.endRefreshing()
            obj.stopLoading()
            obj.lblInt = 1
            obj.tableView.reloadData()
        }
        
        
    }
    
    
}
import Foundation
import Reachability

class AreaManagersListDataModel: NSObject {
    
    var name = String()
    //var mobileNo = String()
    var email = String()
    var empId = String()
    
    func setDataInModel(str:[String:AnyObject])
    {
        
        if str["Employee_Name"] is NSNull  || str["Employee_Name"] == nil{
            self.name = ""
        }else{
            
            let emp1 = str["Employee_Name"]
            
            self.name = (emp1?.description)!
            
            
        }
  
        if str["Employee_ID"] is NSNull || str["Employee_ID"] == nil{
            self.empId = ""
        }else{
            let emp1 = str["Employee_ID"]
            
            self.empId = (emp1?.description)!
        }
        if str["Email_ID"] is NSNull || str["Email_ID"] == nil{
            self.email = ""
        }else{
            let emp1 = str["Email_ID"]
            
            self.email = (emp1?.description)!
            
          
        }
        
        
    }
}

class AreaManagersListDataAPI
{
    
    var reachablty = Reachability()!
    
    
    func serviceCalling(obj:AssignSavedIdeaViewController, name:String, success:@escaping (AnyObject)-> Void)
    {
        
        obj.startLoading()
        
        if(reachablty.connection != .none)
        {
            WebServices.sharedInstances.sendPostRequest(url: URLConstants.Serach_Employee, parameters: ["Employee_Name":name], successHandler: { (dict) in
                
                if let response = dict["response"]{
                    
                    let statusString : String = response["status"] as! String
                    
                    if(statusString == "success")
                    {
                        
                        obj.tableView.isHidden = false
                        obj.noDataLabel(text: "" )
                        
                        if let data = dict["data"] as? [AnyObject]
                        {
                            
                            var dataArray : [AreaManagersListDataModel] = []
                            
                            for i in 0..<data.count
                            {
                                
                                if let cell = data[i] as? [String:AnyObject]
                                {
                                    let object = AreaManagersListDataModel()
                                    object.setDataInModel(str: cell)
                                    dataArray.append(object)
                                }
                                
                            }
                            
                            success(dataArray as AnyObject)
                            
                            
                        }
                        
                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                    }
                    else
                    {
                        
                        print("DATA:fail")
                        obj.tableView.isHidden = true
                        obj.noDataLabel(text: "Sorry! No area manager found with name "+name)
                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                        
                    }
                    
                    
                }
                
                
                
            }, failureHandler: { (error) in
                let errorStr : String = error.description
                
                print("DATA:error",errorStr)
                
                // obj.errorChecking(error: error)
                obj.stopLoading()
            })
            
            
        }
        else{
            obj.noDataLabel(text:  "No internet connection found. Check your internet connection and try again.")
            obj.tableView.isHidden = true
            obj.refresh.endRefreshing()
            obj.stopLoading()
        }
        
        
    }
    
    
}








