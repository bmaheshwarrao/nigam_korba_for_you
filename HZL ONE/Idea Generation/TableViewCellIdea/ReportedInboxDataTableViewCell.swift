//
//  ReportedInboxDataTableViewCell.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 11/02/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit

class ReportedInboxDataTableViewCell: UITableViewCell {
    
    @IBOutlet weak var imageFile: UIImageView!
    @IBOutlet weak var statusTextView: UITextView!
    @IBOutlet weak var titleLabel: UILabel!
    
    // @IBOutlet weak var statusColorView: UIViewX!
    
    @IBOutlet weak var viewImage: UIView!
    @IBOutlet weak var titleViewHeight: NSLayoutConstraint!
    @IBOutlet weak var statusViewHeight: NSLayoutConstraint!
    @IBOutlet weak var heightImage: NSLayoutConstraint!
    @IBOutlet weak var viewData: UIViewX!
    @IBOutlet weak var statusOfHazard: UILabel!
    
    @IBOutlet weak var viewStatus: UIView!
    @IBOutlet weak var heightRiskLevel: NSLayoutConstraint!
    @IBOutlet weak var viewRiskLevel: UIView!
    @IBOutlet weak var image1left: NSLayoutConstraint!
    @IBOutlet weak var statusColorView: UIImageView!
    @IBOutlet weak var idOfHazard: UILabel!
    @IBOutlet weak var titleTextView: UITextView!
    @IBOutlet weak var dateLabel: UILabel!
    
    @IBOutlet weak var image2left: NSLayoutConstraint!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        statusTextView.font = UIFont.systemFont(ofSize: 15.0)
        dateLabel.font = UIFont.systemFont(ofSize: 15.0)
        idOfHazard.font = UIFont.systemFont(ofSize: 15.0)
        statusOfHazard.font = UIFont.systemFont(ofSize: 15.0)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}



