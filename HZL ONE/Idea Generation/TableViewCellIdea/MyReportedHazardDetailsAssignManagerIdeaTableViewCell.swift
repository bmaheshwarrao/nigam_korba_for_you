//
//  MyReportedHazardDetailsAssignManagerIdeaTableViewCell.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 11/02/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit

class MyReportedHazardDetailsAssignManagerIdeaTableViewCell:  UITableViewCell {
    @IBOutlet weak var employeeNameLabel: UILabel!
    
    @IBOutlet weak var heightEmp: NSLayoutConstraint!
    @IBOutlet weak var employeeImplementedlabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    @IBOutlet weak var bottomArea: NSLayoutConstraint!
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
