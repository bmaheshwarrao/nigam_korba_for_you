//
//  AssignIdeaViewController.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 11/02/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//
import UIKit
import CoreData

class AssignIdeaViewController: CommonVSClass,UITableViewDataSource,UITableViewDelegate ,UITextViewDelegate{
    var Idea_Id = String()
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        textViewAssign.clipsToBounds = true;
        textViewAssign.layer.cornerRadius = 10;
        textViewAssign.layer.borderWidth = 1.0
        textViewAssign.layer.borderColor = UIColor.black.cgColor
        textViewAssign.delegate = self;
        textViewAssign.text = PLACEHOLDER_TEXT
        textViewAssign.textColor = UIColor.lightGray
        textViewNo.isHidden = false
        textViewNoHeight.constant = 21
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBOutlet weak var textViewNoHeight: NSLayoutConstraint!
    @IBOutlet weak var textViewNo: UITextView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var textViewAssign: UITextView!
    @IBOutlet weak var btnAdd: UIButton!
    @IBAction func btnAddEmployeeAction(_ sender: UIButton) {
        
        let ZIVC = self.storyboard?.instantiateViewController(withIdentifier: "AssignSavedIdeaViewController") as! AssignSavedIdeaViewController
        
        self.navigationController?.pushViewController(ZIVC, animated: true)
        
    }
    var AreaManagerDB : [AreaManager] = []
    @objc func getAreaManagerListData() {
        
        
        
        self.AreaManagerDB = [AreaManager]()
        let fetchRequest: NSFetchRequest<AreaManager> = AreaManager.fetchRequest()
        fetchRequest.returnsObjectsAsFaults = false
        
        do {
            self.AreaManagerDB = try context.fetch(fetchRequest)
            if(self.AreaManagerDB.count != 0) {
                self.tableView.isHidden = false
                self.noDataLabel(text: "")
                self.label.isHidden = true
                self.tableView.reloadData()
                textViewNo.isHidden = true
                textViewNoHeight.constant = 0
                print(self.AreaManagerDB.count)
            } else {
                self.tableView.isHidden = true
                //self.noDataLabel(text: "No Area Manager Found")
                self.label.isHidden = true
                textViewNo.isHidden = false
                textViewNoHeight.constant = 48
            }
            
        } catch {
            self.tableView.isHidden = true
            self.label.isHidden = false
            
            textViewNo.isHidden = false
            textViewNoHeight.constant = 48
            print("Cannot fetch Expenses")
        }
        
        
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        
        return self.AreaManagerDB.count
        
        
    }
    
    var LastSelected : IndexPath?
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "area", for: indexPath) as! AreaManagerAssignIdeaTableViewCell
       
        cell.employeeLbl.text = self.AreaManagerDB[indexPath.row].name
        
        cell.clearBtn.tag = indexPath.row
        
        
        cell.clearBtn.addTarget(self, action: #selector(pressClear(_:)), for: .touchUpInside)
        
        
        
        return cell
    }
    @objc func pressClear(_ sender : UIButton) {
        
        self.getContext().delete(self.AreaManagerDB[sender.tag])
        (UIApplication.shared.delegate as! AppDelegate).saveContext()
        getAreaManagerListData()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        getAreaManagerListData()
        self.title = "Assign Idea"
        self.tabBarController?.tabBar.isHidden = true
        let nav = self.navigationController?.navigationBar
        nav?.tintColor = UIColor.white
       nav?.barTintColor = UIColor(hexString: "2c3e50", alpha: 1.0)
        nav?.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        //  callMyReportedHazardData()
        
        
        
    }
    
    @IBOutlet weak var btnAssign: UIButton!
    var reachablty = Reachability()!
    @IBAction func btnAssignClicked(_ sender: UIButton) {
        var empIdArray : [String] = []
        if(self.AreaManagerDB.count > 0) {
            
            for data in self.AreaManagerDB{
                empIdArray.append(data.empId!)
            }
        }
        
        if reachablty.connection == .none{
            
            self.view.makeToast("Internet is not available, please check your internet connection try again.")
            
        }
        
        if textViewAssign.text == "" || textViewAssign.text == PLACEHOLDER_TEXT {
            
            self.view.makeToast("Please Write Remark")
        }else if empIdArray.count == 0 {
            
            self.view.makeToast("Please Select Representative")
        }else{
            
            self.startLoadingPK(view: self.view)
            print(empIdArray)
            print(UserDefaults.standard.string(forKey: "EmployeeID")!)
            print(Idea_Id)
            let parameter = [
                "Platform":"iOS" ,
                "Message":textViewAssign.text,
                "ids" : empIdArray ,
                "Idea_ID" : Idea_Id ,
                "Employee_ID": UserDefaults.standard.string(forKey: "EmployeeID")!
                
                ] as [String : Any]
            
            
            
            print("parameter",parameter)
            
            WebServices.sharedInstances.sendPostRequest(url: URLConstants.Assign_Idea, parameters: parameter, successHandler: { (response:[String : AnyObject]) in
                
                let respon = response["response"] as! [String:AnyObject]
                self.stopLoadingPK(view: self.view)
                let objectmsg = MessageCallServerModel()
                let msg = objectmsg.sealizeMessage(cell: respon as! [String : AnyObject])
                if respon["status"] as! String == "success" {
                  
                   
                        
                        MoveStruct.isMove = true
                    MoveStruct.message = msg
                        self.navigationController?.popViewController(animated: false)
                    
                    
                }else{
                    self.stopLoadingPK(view: self.view)
                    self.view.makeToast(msg)
                }
                
            }) { (err) in
                self.stopLoadingPK(view: self.view)
                print(err.description)
            }
            
        }
        
        
        
        
        
        
        
    }
    
    
    
    func getContext () -> NSManagedObjectContext {
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        return appDelegate.persistentContainer.viewContext
    }
    func applyPlaceholderStyle(aTextview: UITextView, placeholderText: String)
    {
        // make it look (initially) like a placeholder
        aTextview.textColor = UIColor.lightGray
        aTextview.text = placeholderText
    }
    
    var PLACEHOLDER_TEXT = "Write here..."
    func applyNonPlaceholderStyle(aTextview: UITextView)
    {
        // make it look like normal text instead of a placeholder
        aTextview.textColor = UIColor.darkText
        aTextview.alpha = 1.0
    }
    func textViewShouldBeginEditing(aTextView: UITextView) -> Bool
    {
        if aTextView == textViewAssign && aTextView.text == PLACEHOLDER_TEXT
        {
            // move cursor to start
            moveCursorToStart(aTextView: aTextView)
        }
        return true
    }
    func moveCursorToStart(aTextView: UITextView)
    {
        DispatchQueue.main.async {
            aTextView.selectedRange = NSMakeRange(0, 0);
        }
        
    }
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        let newLength = textView.text.utf16.count + text.utf16.count - range.length
        if newLength > 0 // have text, so don't show the placeholder
        {
            // check if the only text is the placeholder and remove it if needed
            // unless they've hit the delete button with the placeholder displayed
            if textView == textViewAssign && textView.text == PLACEHOLDER_TEXT
            {
                if text.utf16.count == 0 // they hit the back button
                {
                    return false // ignore it
                }
                applyNonPlaceholderStyle(aTextview: textView)
                textView.text = ""
            }
            return true
        }
        else  // no text, so show the placeholder
        {
            applyPlaceholderStyle(aTextview: textView, placeholderText: PLACEHOLDER_TEXT)
            moveCursorToStart(aTextView: textView)
            return false
        }
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
