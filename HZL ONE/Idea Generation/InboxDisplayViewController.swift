//
//  InboxDisplayViewController.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 11/02/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit

class InboxDisplayViewController: CommonVSClass {
    
    @IBOutlet weak var tableDisplayHazAction: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        hazarddisplay = UserDefaults.standard.integer(forKey: "hazard")
        
        //barSetup()
        tableDisplayHazAction.rowHeight = 70
        tableDisplayHazAction.delegate = self;
        tableDisplayHazAction.dataSource = self;
        refresh.addTarget(self, action: #selector(Reportdata), for: .valueChanged)
        
        self.tableDisplayHazAction.addSubview(refresh)
        
        tableDisplayHazAction.tableFooterView = UIView()
        
        // Do any additional setup after loading the view.
    }
    
    func setUp() {
        if(hazarddisplay == 1) {
            self.title = "My Inbox"
        } else {
            self.title = "HOD Inbox"
        }
        self.tabBarController?.tabBar.isHidden = true
        let nav = self.navigationController?.navigationBar
        nav?.tintColor = UIColor.white
       nav?.barTintColor = UIColor(hexString: "2c3e50", alpha: 1.0)
        nav?.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        Reportdata()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        setUp()
    }
    override func viewWillDisappear(_ animated: Bool) {
        
    }
    var hazarddisplay = Int()
    var arrayHazAction = ["Reported","Open" ,"Closed","Rejected"]
    var arrayHazActionCount : [Int] = [0,0,0,0]
    
    
    
    @objc func tapBack(_ sender : UIButton) {
        dismiss(animated: true, completion: nil)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    let reachability = Reachability()!
    var InboxListModelDB:[InboxListModel] = []
    @objc func Reportdata() {
        if(self.reachability.connection != .none) {
            let pno : String = (UserDefaults.standard.string(forKey: "EmployeeID")!)
            
            let parameters = ["Employee_ID": pno ]
            var url = String()
            if(hazarddisplay == 1) {
                url =  URLConstants.My_Inbox_Count
            } else {
                url =  URLConstants.HOD_Inbox_Count
            }
            self.startLoading()
            print(parameters)
            WebServices.sharedInstances.sendPostRequest(url: url,parameters: parameters, successHandler: { (dict) in
                
                
                
                print("",dict)
                
                if let response = dict["response"] {
                    
                    let statusString : String = response["status"] as! String
                    let objectmsg = MessageCallServerModel()
            let msg = objectmsg.sealizeMessage(cell: response as! [String : AnyObject])
                    print(msg)
                    if(statusString == "success")
                    {
                        
                        
                        if let data = dict["data"] as? [AnyObject]
                        {
                            
                            var dataArray : [InboxListModel] = []
                            
                            for i in 0..<data.count
                            {
                                
                                if let cell = data[i] as? [String:AnyObject]
                                {
                                    let object = InboxListModel()
                                    
                                    if cell["Total"] is NSNull{
                                        object.Total = ""
                                    }else{
                                        let idd : Int = (cell["Total"] as? Int)!
                                        object.Total = String(idd)
                                    }
                                    
                                    if cell["Flag"] is NSNull{
                                        object.Flag = ""
                                    }else{
                                        object.Flag = (cell["Flag"] as? String)!
                                    }
                                    
                                    if cell["Title"] is NSNull{
                                        object.Title = ""
                                    }else{
                                        
                                        object.Title = (cell["Title"] as? String)!
                                    }
                                    
                                    
                                    dataArray.append(object)
                                    
                                }
                                self.refresh.endRefreshing()
                                self.stopLoading()
                                
                                
                                
                            }
                            
                            self.InboxListModelDB = dataArray
                            self.tableDisplayHazAction.reloadData();
                            
                            
                            
                        }
                        
                        
                    }
                    else
                    {
                        self.refresh.endRefreshing()
                        self.stopLoading()
                        self.showSingleButtonWithMessage(title: statusString, message: msg, buttonName: "OK")
                    }
                }
                
                
                
                
            }, failureHandler: { (error) in
                
                
                self.stopLoading()
                self.refresh.endRefreshing()
                self.errorChecking(error: error[0])
                
                
                
            })
            
            
            
        }
        else {
            
            // self.showSingleButtonWithMessage(title: "No Network!", message: "No internet connection found. Check your internet connection or try again.", buttonName: "OK")
            self.stopLoading()
            self.refresh.endRefreshing()
            self.view.makeToast("No internet connection found. Check your internet connection or try again.", duration: 2.0)
            
            
        }
        
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
}
extension InboxDisplayViewController : UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        
        
        
        let status = InboxListModelDB[indexPath.row].Flag
        
        UserDefaults.standard.set(status, forKey: "Status")
        UserDefaults.standard.set(InboxListModelDB[indexPath.row].Title, forKey: "StatusTitle")
        
        let ReportedHazardVC = self.storyboard?.instantiateViewController(withIdentifier: "InboxReportedViewController") as! InboxReportedViewController

        self.navigationController?.pushViewController(ReportedHazardVC, animated: true)
        
    }
}
extension InboxDisplayViewController : UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return  InboxListModelDB.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! DisplayHazardTableViewCell
        cell.selectionStyle = UITableViewCellSelectionStyle.none
        
        
        
        cell.lblShowHazAction.text = InboxListModelDB[indexPath.row].Title
        cell.lblShowHazAction.font =  UIFont.systemFont(ofSize: 16.0, weight: .medium)
        cell.lblAddData.text = InboxListModelDB[indexPath.row].Total
        return cell
        
        
    }
    
}
class InboxListModel: NSObject {
    
    var Total = String()
    var Flag = String()
    var Title = String()
    
    
    
}
