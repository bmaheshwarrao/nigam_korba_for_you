//
//  ELPolicyViewController.swift
//  Balco Sustainability Plus
//
//  Created by sudheer-kumar on 09/11/17.
//  Copyright © 2017 safiqul islam. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation
import SDWebImage
struct ELPolicyTempData {
    static var Cat_ID = String()
    static var ELCat_ID = String()
    static var file_Url = String()
    static var file_Name = String()
    static var policy = String()
}

class ELPolicyViewController: CommonVSClass,UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    var QuizId = String()
    var ID = String()
    var ELPolicyAPI = ELPolicyDataAPI()
    var ELPolicyDB:[ELPolicyDataModel] = []
    
    var videoPlayer = AVPlayer()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        callELPolicyData()
        
        
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.estimatedRowHeight = 62
        tableView.rowHeight = UITableViewAutomaticDimension
        
        refresh.addTarget(self, action: #selector(callELPolicyData), for: .valueChanged)
        let swipe = UISwipeGestureRecognizer(target: self, action: #selector(self.swipeRecognizer))
        swipe.direction = ([.down])
        self.view.addGestureRecognizer(swipe)
        
        self.tableView.addSubview(refresh)
        
    }
    var reachability = Reachability()!
    @objc func swipeRecognizer(_ sender: UISwipeGestureRecognizer) {
        if (sender.direction == .down) {
            
            if(self.reachability.connection != .none)
            {
                
                self.callELPolicyData()
            }
            else
            {
                self.noDataLabel(text: "No internet connection found. Check your internet connection and try again.")
            }
            
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.tabBarController?.tabBar.isHidden = true
        self.title = "Study Material"
        let nav = self.navigationController?.navigationBar
        nav?.tintColor = UIColor.white
       nav?.barTintColor = UIColor(hexString: "2c3e50", alpha: 1.0)
        nav?.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.ELPolicyDB.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! ELPolicyTableViewCell
        
        cell.titleLabel.text = self.ELPolicyDB[indexPath.row].Title

        if let url = NSURL(string: self.ELPolicyDB[indexPath.section].Thumb_Path) {
       cell.imageType.sd_setImage(with: url as URL?, placeholderImage: UIImage.init(named: "placed"))
          }else {
            cell.imageType.image = UIImage.init(named: "placed")
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(self.ELPolicyDB[indexPath.row].File_Type)
        
        if  self.ELPolicyDB[indexPath.row].File_Type == "PDF"{
            
            let ELPVC = self.storyboard?.instantiateViewController(withIdentifier: "pdfViewer") as! PDFFileViewController
            ELPolicyTempData.ELCat_ID = String(self.ELPolicyDB[indexPath.row].ID)
            print(self.ELPolicyDB[indexPath.row].File_Path)
            ELPolicyTempData.file_Url = self.ELPolicyDB[indexPath.row].File_Path
          ELPolicyTempData.file_Name = self.ELPolicyDB[indexPath.row].Title
            self.navigationController?.pushViewController(ELPVC, animated: true)
            
        }else{
            if self.ELPolicyDB[indexPath.row].File_Path != ""{
                videoPlay(str: self.ELPolicyDB[indexPath.row].File_Path)
            }
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    @objc func callELPolicyData(){
      
        var para = [String:String]()
    let parameter = ["Quiz_ID":QuizId  ]
        
        para = parameter.filter { $0.value != ""}
        print("para",para)
        
        self.ELPolicyAPI.serviceCalling(obj: self, parameter: para)
        { (dict) in
            self.ELPolicyDB = dict as! [ELPolicyDataModel]
            
            self.tableView.reloadData()
            
        }
    }
    
    
    func playerDidFinishPlaying(note: NSNotification) {
        
        self.videoPlayer.pause()
        
    }
    
    func videoPlay(str: String) {
        
        let videoURL = URL(string: str)
        self.videoPlayer = AVPlayer(url: videoURL!)
        let playerViewController = AVPlayerViewController()
        playerViewController.player = videoPlayer
        NotificationCenter.default.addObserver(self, selector: Selector(("playerDidFinishPlaying:")), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: videoPlayer)
        self.present(playerViewController, animated: true)
        {
            playerViewController.player!.play()
        }
        do {
            try AVAudioSession.sharedInstance().overrideOutputAudioPort(AVAudioSessionPortOverride.speaker)
        } catch _ {
            
        }
    }
    
}
