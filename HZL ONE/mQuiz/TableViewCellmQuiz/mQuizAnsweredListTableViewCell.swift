//
//  mQuizAnsweredListTableViewCell.swift
//  mQuiz
//
//  Created by SARVANG INFOTCH on 14/11/18.
//  Copyright © 2018 SARVANG INFOTCH. All rights reserved.
//

import UIKit

class mQuizAnsweredListTableViewCell: UITableViewCell {

   
    @IBOutlet weak var textViewQuestion: UITextView!
   
    
    @IBOutlet weak var viewHeight: NSLayoutConstraint!
    @IBOutlet weak var lblCorrect: UILabel!
    @IBOutlet weak var lblGiven: UILabel!
    @IBOutlet weak var lblMarksObtain: UILabel!
    @IBOutlet weak var btnArrow: UIButton!
    @IBOutlet weak var btnCorrectWrongImge: UIButton!
    @IBOutlet weak var view1leading: NSLayoutConstraint!
    @IBOutlet weak var view2leading: NSLayoutConstraint!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
