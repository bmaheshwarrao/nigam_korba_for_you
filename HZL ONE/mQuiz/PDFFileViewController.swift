//
//  PDFFileViewController.swift
//  City Tender Korba
//
//  Created by sudheer-kumar on 27/10/17.
//  Copyright © 2017 safiqul islam. All rights reserved.
//

import UIKit
import WebKit


class PDFFileViewController: CommonVSClass,UIWebViewDelegate {
    
    
    @IBOutlet weak var webView: UIWebView!
    
   var prentId = String()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        webView.delegate = self
        self.prentId = ELPolicyTempData.Cat_ID
        
        loadPdf()
        
        
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.tabBarController?.tabBar.isHidden = true
        self.title = ELPolicyTempData.file_Name
        let nav = self.navigationController?.navigationBar
        nav?.tintColor = UIColor.white
       nav?.barTintColor = UIColor(hexString: "2c3e50", alpha: 1.0)
        nav?.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        
    }
    
    
    func loadPdf() {
        
        if ELPolicyTempData.file_Url != ""{
             DispatchQueue.main.asyncAfter(deadline: .now() + 0.0) {
                
                self.startLoading()
            
                self.checkPDFIsAvailable(linkName: ELPolicyTempData.ELCat_ID+self.prentId , success: { (filepath) in
                    
                    
                    var pdfURL = (FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)).last! as URL
                    pdfURL = pdfURL.appendingPathComponent(ELPolicyTempData.ELCat_ID+self.prentId ) as URL
                    
                    let data = try! Data(contentsOf: pdfURL)
                    
                    self.webView.load(data, mimeType: "application/pdf", textEncodingName:"", baseURL: pdfURL.deletingLastPathComponent())
                    
                    self.stopLoading()
                    
                    
                    
                }, failure: { () in
                    
                    self.downloadPDF(linkString: ELPolicyTempData.file_Url, linkName: ELPolicyTempData.ELCat_ID+self.prentId , completionHandler: { () in
                        
                        
                        self.checkPDFIsAvailable(linkName: ELPolicyTempData.ELCat_ID+self.prentId , success: { (filepath) in
                            
                            
                            var pdfURL = (FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)).last! as URL
                            pdfURL = pdfURL.appendingPathComponent(ELPolicyTempData.ELCat_ID+self.prentId ) as URL
                            
                            let data = try! Data(contentsOf: pdfURL)
                            DispatchQueue.main.async {
                            self.webView.load(data, mimeType: "application/pdf", textEncodingName:"", baseURL: pdfURL.deletingLastPathComponent())
                            }
                            self.stopLoading()
                            
                            
                            
                        }, failure: { () in
                            
                            //print(error)
                            self.stopLoading()
                            
                        })
                        
                        
                    })
                    
                })
            }
        }
    }
    
}
