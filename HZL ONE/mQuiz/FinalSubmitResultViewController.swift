//
//  FinalSubmitResultViewController.swift
//  mQuiz
//
//  Created by SARVANG INFOTCH on 16/11/18.
//  Copyright © 2018 SARVANG INFOTCH. All rights reserved.
//

import UIKit

struct quizMove {
    static var isQuizMove = Bool()
}

class FinalSubmitResultViewController: UIViewController {

    @IBOutlet weak var imageGifIcon: UIImageView!
    @IBOutlet weak var resultLbl: UILabel!
    @IBOutlet weak var achieveLbl: UILabel!
    @IBOutlet weak var totalLbl: UILabel!
    var resultString: String?
    var totalString: String?
    var achieveString: String?
    override func viewDidLoad() {
        super.viewDidLoad()
        resultLbl.text = resultString
        totalLbl.text = totalString
        achieveLbl.text = achieveString
        
        
    }
   
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func btnContinueClicked(_ sender: UIButton) {
        self.navigationController?.isNavigationBarHidden = false
        quizMove.isQuizMove = true
       self.navigationController?.popViewController(animated: true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}




