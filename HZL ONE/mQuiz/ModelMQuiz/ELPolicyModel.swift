//
//  marketService.swift
//  MVCDemo
//
//  Created by Safiqul Islam on 10/05/17.
//  Copyright © 2017 Safiqul Islam. All rights reserved.
//

import Foundation
import Reachability
import CoreData

class ELPolicyDataModel: NSObject {
    
    
    var Title = String()
    var File_Type = String()
    var File_Path = String()
    var File_Size = String()
    var ID = Int()
     var Test_ID = Int()
    var Thumb_Path = String()
        var Description = String()
    

    
   
    
    
    
    
    func setELPolicyDataInModel(str:[String:AnyObject])
    {
        if str["Title"] is NSNull{
            self.Title = ""
        }else{
            self.Title = (str["Title"] as? String)!
        }
        if str["File_Type"] is NSNull{
            self.File_Type = ""
        }else{
            self.File_Type = (str["File_Type"] as? String)!
        }
        if str["File_Path"] is NSNull{
            self.File_Path = ""
        }else{
            self.File_Path = (str["File_Path"] as? String)!
        }
        if str["File_Size"] is NSNull{
            self.File_Size = ""
        }else{
            self.File_Size = (str["File_Size"] as? String)!
        }
        
        if str["Thumb_Path"] is NSNull{
            self.Thumb_Path = ""
        }else{
            self.Thumb_Path = (str["Thumb_Path"] as? String)!
        }
        if str["Description"] is NSNull{
            self.Description = ""
        }else{
            self.Description = (str["Description"] as? String)!
        }
        if str["ID"] is NSNull{
            self.ID = 0
        }else{
            self.ID = (str["ID"] as? Int)!
        }
        
        if str["Test_ID"] is NSNull{
            self.Test_ID = 0
        }else{
            self.Test_ID = (str["Test_ID"] as? Int)!
        }
        
    }
}

class ELPolicyDataAPI
{
    
    var reachablty = Reachability()!
    var file_Type = String()
    var file_Url = String()
    var file_Name = String()
    var date_Time = String()
    var ID = Int()
    
    func serviceCalling(obj:ELPolicyViewController,parameter : [String:String], success:@escaping (AnyObject)-> Void)
    {
        
        obj.startLoading()
        
        if(reachablty.connection != .none)
        {
            
            WebServices.sharedInstances.sendPostRequest(url: URLConstants.List_Study_Material, parameters: parameter, successHandler: { (dict) in
                
                if let response = dict["response"]{
                    
                    let statusString : String = response["status"] as! String
                    let objectmsg = MessageCallServerModel()
            let msg = objectmsg.sealizeMessage(cell: response as! [String : AnyObject])
                    print(response)
                    if(statusString == "success")
                    {
                        obj.tableView.isHidden = false
                        obj.noDataLabel(text: "" )
                        obj.label.isHidden = true
                        if let data = dict["data"] as? [AnyObject]
                        {
                            var dataArray : [ELPolicyDataModel] = []
                            
                            for i in 0..<data.count
                            {
                                
                                if let cell = data[i] as? [String:AnyObject]
                                {
                                    
                                    let object = ELPolicyDataModel()
                                    object.setELPolicyDataInModel(str: cell)
                                    dataArray.append(object)
 
                    
                                }
                                
                            }
                            
                            success(dataArray as AnyObject)
                            
                            
                        }
                        
                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                    }
                    else
                    {
                        print("DATA:fail")
                        obj.tableView.isHidden = true
                        obj.label.isHidden = false
                        obj.noDataLabel(text: msg)
                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                        
                    }
                    
                    
                }
                
                
                
                
            })
            { (error) in
                
                let errorStr : String = error.description
                
                print("DATA:error",errorStr)
                
               // obj.errorChecking(error: error)
                obj.refresh.endRefreshing()
                obj.stopLoading()
            }
            
            
        }
        else{
            obj.tableView.isHidden = true
             obj.label.isHidden = false
            obj.noDataLabel(text: "Internet is not available, Please check internet connection and try again." )
            obj.refresh.endRefreshing()
            obj.stopLoading()
           
        }
        
        
    }
    
    
}




