
//
//  ModelQuizQuestion.swift
//  mQuiz
//
//  Created by SARVANG INFOTCH on 12/11/18.
//  Copyright © 2018 SARVANG INFOTCH. All rights reserved.
//


import Foundation
import Reachability



class QuizListModel: NSObject {
    
    
    
    var Test_Title: String?
    
    var From_Date : String?
    var To_Date : String?
    var No_of_Questions : String?
    var Question_Per_Subject : String?
    
    var ID  : String?
    
    var Total_Marks : String?
    var Passing_Marks : String?
    var Test_Owner : String?
    var Quiz_Status : String?
    var Is_Test : String?
    
    var Marks_Per_Question : String?
    
    
    
    //    var Sbu_Department : String?
    //  var location_ID  : Int?
    // var sub_location_ID : Int?
    //   var Sub_location_Name : String?
    //  var main_location : String?
    // var business : String?
    //var main_location_ID : Int?
    
    func setDataInModel(cell:[String:AnyObject])
    {
        
        if cell["Test_Title"] is NSNull{
            self.Test_Title = ""
        }else{
        
            let idd1 = cell["Test_Title"]
            self.Test_Title = idd1?.description
        }
        if cell["From_Date"] is NSNull{
            self.From_Date = ""
        }else{
         
            let idd1 = cell["From_Date"]
            self.From_Date = idd1?.description
        }
        if cell["To_Date"] is NSNull{
            self.To_Date = ""
        }else{
          //  self.To_Date = (cell["To_Date"] as? String)!
            
            let idd1 = cell["To_Date"]
            self.To_Date = idd1?.description
        }
        if cell["No_of_Questions"] is NSNull{
            self.No_of_Questions = ""
        }else{
            
            let idd1 = cell["No_of_Questions"]
            self.No_of_Questions = idd1?.description
        }
        if cell["Question_Per_Subject"] is NSNull{
            self.Question_Per_Subject = ""
        }else{
          
            
            let idd1 = cell["Question_Per_Subject"]
            self.Question_Per_Subject = idd1?.description
        }
        
        
        if cell["Marks_Per_Question"] is NSNull{
            self.Marks_Per_Question = ""
        }else{
            let idd1 = cell["Marks_Per_Question"]
            self.Marks_Per_Question = idd1?.description
           
        }
        if cell["Total_Marks"] is NSNull{
            self.Total_Marks = ""
        }else{
            let idd1 = cell["Total_Marks"]
            self.Total_Marks = idd1?.description
            
          
        }
        
        if cell["Passing_Marks"] is NSNull{
            self.Passing_Marks = ""
        }else{
            let idd1 = cell["Passing_Marks"]
            self.Passing_Marks = idd1?.description
          
        }
        
        if cell["Test_Owner"] is NSNull{
            self.Test_Owner = ""
        }else{
            let idd1 = cell["Test_Owner"]
            self.Test_Owner = idd1?.description
         
        }
        
        
//        if cell["Quiz_Status"] is NSNull{
//            self.Quiz_Status = ""
//        }else{
//            let idd1 = cell["Passing_Marks"]
//            self.Passing_Marks = idd1?.description
//            self.Quiz_Status = (cell["Quiz_Status"] as? String)!
//        }
        
        
        if cell["Quiz_Status"] is NSNull{
            self.Quiz_Status = ""
        }else{
            let idd1 = cell["Quiz_Status"]
            self.Quiz_Status = idd1?.description
           
        }
        
        if cell["Is_Test"] is NSNull{
            self.Is_Test = "0"
        }else{
            let idd1 = cell["Is_Test"]
            self.Is_Test = idd1?.description
            
        }
        if cell["ID"] is NSNull{
             self.ID = "0"
        }else{
            
            let idd1 = cell["ID"]
            self.ID = idd1?.description
          print(ID)
        }
        
       
        
    }
}


class QuizListAPI
{
    
    var reachablty = Reachability()!
    
    
    func serviceCalling(obj:mQuizFirstViewController,parameter:[String:String], success:@escaping (AnyObject)-> Void)
    {
        
        obj.startLoading()
        obj.tableView.isHidden = true
        if(reachablty.connection != .none)
        {
            
            WebServices.sharedInstances.sendPostRequest(url: URLConstants.Quiz_List, parameters: parameter, successHandler: { (dict) in
                
                if let response = dict["response"]{
                    let objectmsg = MessageCallServerModel()
            let msg = objectmsg.sealizeMessage(cell: response as! [String : AnyObject])
                    print(response)
                    let statusString : String = response["status"] as! String
                    
                    if(statusString == "success")
                    {
                        
                        obj.tableView.isHidden = false
                        obj.noDataLabel(text: "" )
                        obj.label.isHidden = true
                        
                        if let data = dict["data"] as? [AnyObject]
                        {
                            
                            var dataArray : [QuizListModel] = []
                            
                            for i in 0..<data.count
                            {
                                
                                if let celll = data[i] as? [String:AnyObject]
                                {
                                    let object = QuizListModel()
                                    object.setDataInModel(cell: celll)
                                    dataArray.append(object)
                                }
                                
                            }
                            
                            success(dataArray as AnyObject)
                            
                            
                        }
                        
                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                    }
                    else
                    {
                        
                        
                        print("DATA:fail")
                        obj.tableView.isHidden = true
                        obj.noDataLabel(text: msg)
                        obj.refresh.endRefreshing()
                        obj.label.isHidden = false
                        obj.stopLoading()
                        
                    }
                    
                    
                }
                
                
                
            }, failureHandler: { (error) in
                let errorStr : String = error.description
                
                print("DATA:error",errorStr)
                
                // obj.errorChecking(error: error)
                obj.stopLoading()
            })
            
            
        }
        else{
            obj.noDataLabel(text:  "No internet connection found. Check your internet connection and try again.")
            obj.tableView.isHidden = true
            obj.label.isHidden = false
            obj.refresh.endRefreshing()
            obj.stopLoading()
        }
        
        
    }
    
    
}

class QuizListLoadMoreAPI
{
    
    var reachablty = Reachability()!
    
    
    func serviceCalling(obj:mQuizFirstViewController,parameter:[String:String], success:@escaping (AnyObject)-> Void)
    {
        
        obj.startLoading()
        
        if(reachablty.connection != .none)
        {
            
            WebServices.sharedInstances.sendPostRequest(url: URLConstants.Quiz_List, parameters: parameter, successHandler: { (dict) in
                
                if let response = dict["response"]{
                    
                    let statusString : String = response["status"] as! String
                    
                    if(statusString == "success")
                    {
                        
                        //                        obj.tableView.isHidden = false
                        //                        obj.noDataLabel(text: "" )
                        
                        if let data = dict["data"] as? [AnyObject]
                        {
                            
                            var dataArray : [QuizListModel] = []
                            
                            for i in 0..<data.count
                            {
                                
                                if let celll = data[i] as? [String:AnyObject]
                                {
                                    let object = QuizListModel()
                                    object.setDataInModel(cell: celll)
                                    dataArray.append(object)
                                }
                                
                            }
                            
                            success(dataArray as AnyObject)
                            
                            
                        }
                        
                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                    }
                    else
                    {
                        
                        
                        print("DATA:fail")
                        //                        obj.tableView.isHidden = true
                        //                        obj.noDataLabel(text: "No hazard found with "+parameter["Search"]! )
                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                        
                    }
                    
                    
                }
                
                
                
            }, failureHandler: { (error) in
                let errorStr : String = error.description
                
                print("DATA:error",errorStr)
                
                //obj.errorChecking(error: error)
                obj.stopLoading()
            })
            
            
        }
        else{
            //            obj.noDataLabel(text:  "No internet connection found. Check your internet connection and try again.")
            //            obj.tableView.isHidden = true
            obj.refresh.endRefreshing()
            obj.stopLoading()
        }
        
        
    }
    
    
}

