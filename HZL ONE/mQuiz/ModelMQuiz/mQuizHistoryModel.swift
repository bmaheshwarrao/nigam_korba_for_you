//
//  mQuizHistoryModel.swift
//  mQuiz
//
//  Created by SARVANG INFOTCH on 14/11/18.
//  Copyright © 2018 SARVANG INFOTCH. All rights reserved.
//


//
//  ModelQuizQuestion.swift
//  mQuiz
//
//  Created by SARVANG INFOTCH on 12/11/18.
//  Copyright © 2018 SARVANG INFOTCH. All rights reserved.
//


import Foundation
import Reachability



class QuizHistoryModel: NSObject {
    
    
    
    var Test_Title: String?
    var ID  = Int()
    var Quiz_ID : Int?
    var Achieved_Marks : String?
  var Total_Marks : String?
    var Passing_Marks : String?
    var Date_Time : String?
    var Result : String?
    
  
    
    //    var Sbu_Department : String?
    //  var location_ID  : Int?
    // var sub_location_ID : Int?
    //   var Sub_location_Name : String?
    //  var main_location : String?
    // var business : String?
    //var main_location_ID : Int?
    
    func setDataInModel(cell:[String:AnyObject])
    {
        
        if cell["Test_Title"] is NSNull{
            self.Test_Title = ""
        }else{
            self.Test_Title = (cell["Test_Title"] as? String)!
        }
        if cell["Date_Time"] is NSNull{
            self.Date_Time = ""
        }else{
            self.Date_Time = (cell["Date_Time"] as? String)!
        }
        if cell["Result"] is NSNull{
            self.Result = ""
        }else{
            self.Result = (cell["Result"] as? String)!
        }
        if cell["Achieved_Marks"] is NSNull{
            self.Achieved_Marks = ""
        }else{
            let idd1 : Int = (cell["Achieved_Marks"] as? Int)!
            self.Achieved_Marks = String(idd1)
        }
        if cell["Total_Marks"] is NSNull{
            self.Total_Marks = ""
        }else{
            let idd1 : Int = (cell["Total_Marks"] as? Int)!
            self.Total_Marks = String(idd1)
        }
        if cell["Passing_Marks"] is NSNull{
            self.Passing_Marks = ""
        }else{
            let idd1 : Int = (cell["Passing_Marks"] as? Int)!
            self.Passing_Marks = String(idd1)
        }
        
        
      
       
        
        if cell["Quiz_ID"] is NSNull{
            self.Quiz_ID = 0
        }else{
            self.Quiz_ID = (cell["Quiz_ID"] as? Int)!
        }
        if cell["ID"] is NSNull{
            
        }else{
            self.ID = (cell["ID"] as? Int)!
        }
        
        
        
    }
}


class QuizHistoryAPI
{
    
    var reachablty = Reachability()!
    
    
    func serviceCalling(obj:mQuizHistoryViewController,parameter:[String:String], success:@escaping (AnyObject)-> Void)
    {
        
        obj.startLoading()
        
        if(reachablty.connection != .none)
        {
            
            WebServices.sharedInstances.sendPostRequest(url: URLConstants.Quiz_History, parameters: parameter, successHandler: { (dict) in
                
                if let response = dict["response"]{
                    
                    let statusString : String = response["status"] as! String
                    let objectmsg = MessageCallServerModel()
            let msg = objectmsg.sealizeMessage(cell: response as! [String : AnyObject])
                    print(response)
                    if(statusString == "success")
                    {
                        
                        obj.tableView.isHidden = false
                        obj.noDataLabel(text: "" )
                        obj.label.isHidden = true
                        
                        if let data = dict["data"] as? [AnyObject]
                        {
                            
                            var dataArray : [QuizHistoryModel] = []
                            
                            for i in 0..<data.count
                            {
                                
                                if let celll = data[i] as? [String:AnyObject]
                                {
                                    let object = QuizHistoryModel()
                                    object.setDataInModel(cell: celll)
                                    dataArray.append(object)
                                }
                                
                            }
                            
                            success(dataArray as AnyObject)
                            
                            
                        }
                        
                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                    }
                    else
                    {
                        
                        
                        print("DATA:fail")
                        obj.tableView.isHidden = true
                        obj.noDataLabel(text: msg)
                        obj.refresh.endRefreshing()
                        obj.label.isHidden = false
                        obj.stopLoading()
                        
                    }
                    
                    
                }
                
                
                
            }, failureHandler: { (error) in
                let errorStr : String = error.description
                
                print("DATA:error",errorStr)
                
                // obj.errorChecking(error: error)
                obj.stopLoading()
            })
            
            
        }
        else{
            obj.noDataLabel(text:  "No internet connection found. Check your internet connection and try again.")
            obj.tableView.isHidden = true
            obj.label.isHidden = false
            obj.refresh.endRefreshing()
            obj.stopLoading()
        }
        
        
    }
    
    
}

class QuizHistoryLoadMoreAPI
{
    
    var reachablty = Reachability()!
    
    
    func serviceCalling(obj:mQuizHistoryViewController,parameter:[String:String], success:@escaping (AnyObject)-> Void)
    {
        
        obj.startLoading()
        
        if(reachablty.connection != .none)
        {
            
            WebServices.sharedInstances.sendPostRequest(url: URLConstants.Quiz_History, parameters: parameter, successHandler: { (dict) in
                
                if let response = dict["response"]{
                    
                    let statusString : String = response["status"] as! String
                    let objectmsg = MessageCallServerModel()
            let msg = objectmsg.sealizeMessage(cell: response as! [String : AnyObject])
                    print(response)
                    if(statusString == "success")
                    {
                        
                        //                        obj.tableView.isHidden = false
                        //                        obj.noDataLabel(text: "" )
                        
                        if let data = dict["data"] as? [AnyObject]
                        {
                            
                            var dataArray : [QuizHistoryModel] = []
                            
                            for i in 0..<data.count
                            {
                                
                                if let celll = data[i] as? [String:AnyObject]
                                {
                                    let object = QuizHistoryModel()
                                    object.setDataInModel(cell: celll)
                                    dataArray.append(object)
                                }
                                
                            }
                            
                            success(dataArray as AnyObject)
                            
                            
                        }
                        
                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                    }
                    else
                    {
                        
                        
                        print("DATA:fail")
                        //                        obj.tableView.isHidden = true
                        //                        obj.noDataLabel(text: "No hazard found with "+parameter["Search"]! )
                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                        
                    }
                    
                    
                }
                
                
                
            }, failureHandler: { (error) in
                let errorStr : String = error.description
                
                print("DATA:error",errorStr)
                
                //obj.errorChecking(error: error)
                obj.stopLoading()
            })
            
            
        }
        else{
            //            obj.noDataLabel(text:  "No internet connection found. Check your internet connection and try again.")
            //            obj.tableView.isHidden = true
            obj.refresh.endRefreshing()
            obj.stopLoading()
        }
        
        
    }
    
    
}


