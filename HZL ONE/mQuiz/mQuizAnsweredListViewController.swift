//
//  mQuizAnsweredListViewController.swift
//  mQuiz
//
//  Created by SARVANG INFOTCH on 14/11/18.
//  Copyright © 2018 SARVANG INFOTCH. All rights reserved.
//

import UIKit

class mQuizAnsweredListViewController: CommonVSClass {
    
    var quizDB:[QuizAnswerdModel] = []
    var quizLoadMoreDB:[QuizAnswerdModel] = []
    var QuizAPI = QuizAnswredAPI()
    var QuizLoadMoreAPI = QuizAnswredLoadMoreAPI()
    var Quiz_Id = String()
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        QuizResult()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
        tableView.tableFooterView?.backgroundColor = UIColor.groupTableViewBackground
        // Do any additional setup after loading the view.
    }
    
    @IBAction func btnArrowClicked(_ sender: UIButton) {
        let buttonPosition:CGPoint = sender.convert(.zero, to:self.tableView)
        let indexPath = self.tableView.indexPathForRow(at: buttonPosition)
        if(arrayOb[(indexPath?.row)!] == true){
            arrayOb[(indexPath?.row)!] = false
        }else{
            arrayOb[(indexPath?.row)!] = true
        }
        self.tableView.reloadRows(at: [indexPath!], with: .automatic)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        
        self.title = "Answer Key"
        self.tabBarController?.tabBar.isHidden = true
        let nav = self.navigationController?.navigationBar
        nav?.tintColor = UIColor.white
       nav?.barTintColor = UIColor(hexString: "2c3e50", alpha: 1.0)
        nav?.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        
    }
    var leadingConst : CGFloat = 0
    @objc func QuizResult() {
        var para = [String:String]()
        let parameter = ["Employee_ID":UserDefaults.standard.string(forKey: "EmployeeID")!,"Quiz_ID" : Quiz_Id]
      
        para = parameter.filter { $0.value != ""}
        print("para",para)
        self.QuizAPI.serviceCalling(obj: self, parameter: para) { (dict) in
            self.arrayOb = []
            self.quizDB = [QuizAnswerdModel]()
            self.quizDB = dict as! [QuizAnswerdModel]
            print(self.quizDB)
            if(self.quizDB.count > 0){
                for i in 0...self.quizDB.count - 1{
                    self.arrayOb.append(false)
                }
            }
            self.tableView.reloadData()
        }
        
    }
      var arrayOb : [Bool] = []
    @objc func QuizResultLoadMore(Id:String) {
        
        
//        var para = [String:String]()
//        let parameter = ["ID":Id
//            , "Employee_ID":"1002"]
//
//        para = parameter.filter { $0.value != ""}
//        print("para",para)
//
//        self.QuizLoadMoreAPI.serviceCalling(obj: self, parameter: para) { (dict) in
//
//            self.quizLoadMoreDB = [QuizAnswerdModel]()
//            self.quizLoadMoreDB = dict as! [QuizAnswerdModel]
//
//            switch self.quizLoadMoreDB.count {
//            case 0:
//                break;
//            default:
//                self.quizDB.append(contentsOf: self.quizLoadMoreDB)
//                self.tableView.reloadData()
//                break;
//            }
//
//        }
        
    }
  
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

extension mQuizAnsweredListViewController : UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if(section == 0) {
            return 10.0
        } else {
            return 20.0
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
        // return 200
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return quizDB.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    
}

extension mQuizAnsweredListViewController : UITableViewDataSource {
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellAnswer", for: indexPath) as! mQuizAnsweredListTableViewCell
        let paragraph = NSMutableParagraphStyle()
        paragraph.alignment = .left
        paragraph.lineSpacing = 0
        let  mBuilder = "Q" + String(indexPath.section + 1) + ") " + quizDB[indexPath.section].Question!
        
        let quesAttributedString = NSMutableAttributedString(string: mBuilder, attributes: [NSAttributedStringKey.foregroundColor: UIColor.black, NSAttributedStringKey.font: UIFont.systemFont(ofSize: CGFloat(15.0))])
        
        
      
        
        quesAttributedString.addAttribute(NSAttributedStringKey.paragraphStyle, value: paragraph, range: NSRange(location: 0, length: quesAttributedString.length))
        
        
        cell.textViewQuestion.attributedText = quesAttributedString

       
        cell.lblGiven.text = quizDB[indexPath.section].Given_Ans
         if(quizDB[indexPath.section].Correct_Answer == quizDB[indexPath.section].Given_Answer ){
            cell.btnCorrectWrongImge.setImage(UIImage(named: "tick"), for: .normal)
        }else{
              cell.btnCorrectWrongImge.setImage(UIImage(named: "wrong"), for: .normal)
        }
        cell.lblCorrect.text = quizDB[indexPath.section].Correct_Ans! + " ) " + quizDB[indexPath.section].Correct_Answer!
        cell.lblGiven.text = quizDB[indexPath.section].Given_Ans! + " ) " + quizDB[indexPath.section].Given_Answer!
       cell.lblMarksObtain.text = quizDB[indexPath.section].Mark
        
        if(self.arrayOb[indexPath.row] == true){
            
            cell.frame.size.height = cell.frame.size.height
            
            cell.btnArrow.setImage(UIImage(named: "arrowDown"), for: .normal)
           
            cell.viewHeight.constant = 80
        }else{
            
         
            
            cell.frame.size.height = cell.frame.size.height - 80
            
            cell.btnArrow.setImage(UIImage(named: "arrowRight"), for: .normal)
            
            cell.viewHeight.constant = 0
        }
        
    
        return cell;
    }
}
