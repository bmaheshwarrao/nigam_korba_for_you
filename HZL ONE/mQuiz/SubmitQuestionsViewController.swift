//
//  SubmitQuestionsViewController.swift
//  mQuiz
//
//  Created by SARVANG INFOTCH on 13/11/18.
//  Copyright © 2018 SARVANG INFOTCH. All rights reserved.
//

import UIKit
import DLRadioButton
import EzPopup
import CountdownLabel
var IntVal = 0
class SubmitQuestionsViewController: CommonVSClass {
var QuizId = String()
   
    
 
    @IBOutlet weak var lblTimeLeft: CountdownLabel!
    @IBOutlet weak var leading2: NSLayoutConstraint!
    @IBOutlet weak var leading1: NSLayoutConstraint!
    @IBOutlet weak var viewTop: UIView!
    @IBOutlet weak var pendingLabel: UILabel!
    @IBOutlet weak var attemptLabel: UILabel!
    @IBOutlet weak var totalQuesLabel: UILabel!
    var quizSubmitAPI = QuizSubmitListAPI()
     var quizSubmitDB:[QuizSubmitModel] = []
    @IBOutlet weak var tableView: UITableView!
    var timer = Timer()
    override func viewDidLoad() {
        super.viewDidLoad()
        let cons = self.view.frame.width 
        leading1.constant = cons/3
        leading2.constant = cons/3
         dataArrayLoad = []
        QuizResult()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
       
//        tableView.tableFooterView?.backgroundColor = UIColor.groupTableViewBackground
       // self.timer = Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(self.sendReport), userInfo: nil, repeats: true)
          NotificationCenter.default.addObserver(self, selector: #selector(self.pressSubmitQuiz), name: NSNotification.Name?.init(NSNotification.Name(rawValue: "submitQuiz")), object: nil)
        // Do any additional setup after loading the view.
    }
    @IBAction func btnSubmitClicked(_ sender: UIButton) {
        
        viewOnSubmit()
        

        
    }
    
    @IBAction func btnPreviousClicked(_ sender: UIButton) {
        
      
        QuizPreResult()
        
        
    }
    @IBAction func btnCloseClicked(_ sender: UIButton) {
        
        let buttonPosition:CGPoint = sender.convert(.zero, to:self.tableView)
        let indexPath = self.tableView.indexPathForRow(at: buttonPosition)
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath!) as! QuestionsSubmitTableViewCell
        
     strAction = ""
        
        
        self.tableView.reloadRows(at: [indexPath!], with: .none)
        
        
        
    }
    
    
    @IBOutlet weak var btnPlayPause: UIButton!
    
    
       var viewSubmitAll = UIView()
    var line1View = UIView()
    var line2View = UIView()
    var viewSubmitVal = UIView()
    var viewSubmit = UIView()
    var lbltQuestions = UILabel()
    var lbltValQuestions = UILabel()
    var lblAtteQuestions = UILabel()
    var lblAtteValQuestions = UILabel()
    var lblPenQuestions = UILabel()
    var lblPenValQuestions = UILabel()
    var submitQuiz = UILabel()
     var btnSubmitAll = UIButton()
    var  btnCancel = UIButton()
    var lblSubmit = UILabel()
    var lblCancel = UILabel()
    let customAlertVC = CustomAlertViewController.instantiate()
    func viewOnSubmit(){
     
        if(strAction == ""){
        
            
            self.view.makeToast("Please attempt your question...")
        } else{
//        guard let customAlertVC = customAlertVC else { return }
//
//
//        let popupVC = PopupViewController(contentController: customAlertVC, popupWidth: self.view.frame.width - 50)
//        popupVC.cornerRadius = 5
//        present(popupVC, animated: true, completion: nil)
            
            pressSubmitQuiz()
        }
       
    }
    
    
    @IBAction func btnPlayPauseClicked(_ sender: UIButton) {
        
        if self.lblTimeLeft.isPaused {
            self.lblTimeLeft.start()
           btnPlayPause.setImage(UIImage(named: "pause"), for: UIControlState.normal)
        } else {
            
            pressSendTimeQuiz()
           
        }
        
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        if(quizMove.isQuizMove == true){
            quizMove.isQuizMove = false
            self.navigationController?.popViewController(animated: false)
        }
        
        self.title = "Quiz"
        self.tabBarController?.tabBar.isHidden = true
        let nav = self.navigationController?.navigationBar
        nav?.isTranslucent = false
        nav?.tintColor = UIColor.white
       nav?.barTintColor = UIColor(hexString: "2c3e50", alpha: 1.0)
        nav?.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        
        
    }
    
    @objc func pressSendTimeQuiz()  {
        
        
        
        
        self.startLoadingPK(view: self.view)
        
        let parameter = [
            "Quiz_Time":self.lblTimeLeft.text,
            "Employee_ID": UserDefaults.standard.string(forKey: "EmployeeID")! ,
            "Status" : "Pause Quiz Timer" ,
            "Sesstion_ID" : "0" ,
            "Question_ID":Question_Id,
            "Quiz_ID": QuizId
            
            ] as! [String:String]
        
        
        
        print("parameter",parameter)
        
        WebServices.sharedInstances.sendPostRequest(url: URLConstants.Temp_Timing_Updates, parameters: parameter, successHandler: { (response:[String : AnyObject]) in
            
            let respon = response["response"] as! [String:AnyObject]
            
            if respon["status"] as! String == "success" {
                  self.stopLoadingPK(view: self.view)
                self.lblTimeLeft.pause()
                self.btnPlayPause.setImage(UIImage(named: "play"), for: UIControlState.normal)
            }else{
                    self.stopLoadingPK(view: self.view)
              
                //  self.view.makeToast("Please try again, something went wrong.")
            }
            
        }) { (err) in
            self.stopLoadingPK(view: self.view)
           
            print(err.description)
        }
    }
    
    @objc func pressFinalSubmitQuiz(){
        
       
  
        let parameter = [
            "Quiz_Time":self.lblTimeLeft.text,
            "Employee_ID": UserDefaults.standard.string(forKey: "EmployeeID")! ,
            "Quiz_ID": QuizId
            
            ] as! [String:String]
            
        
            
            
            
            print("parameter",parameter)
            
            WebServices.sharedInstances.sendPostRequest(url: URLConstants.Final_Submit_Quiz, parameters: parameter, successHandler: { (response:[String : AnyObject]) in
                
                let respon = response["response"] as! [String:AnyObject]
                
                if respon["status"] as! String == "success" {
                    self.view.makeToast("Final Question Submit")
                    var Total_Marks = String()
                    var Achieved_Marks = String()
                    var Result = String()
                    if let data = response["data"]
                    {
                        
                        
                        
                        
                        if let str = data as? [String : AnyObject]
                        {
                            if str["Total_Marks"] is NSNull || str["Total_Marks"] == nil{
                                Total_Marks =  "0"
                            }else{
                                
                                let desc = str["Total_Marks"]
                                
                                Total_Marks = (desc?.description)!
                                
                            }
                            if str["Achieved_Marks"] is NSNull || str["Achieved_Marks"] == nil{
                                Achieved_Marks =  "0"
                            }else{
                                
                                let desc = str["Achieved_Marks"]
                                
                                Achieved_Marks = (desc?.description)!
                                
                            }
                            if str["Result"] is NSNull || str["Result"] == nil{
                                Result =  "Fail"
                            }else{
                                
                                let desc = str["Result"]
                                
                                Result = (desc?.description)!
                                
                            }
                        }
                        
                        
                        
                        
                        
                        
                    }
                    
                    
                    let storyboard = UIStoryboard(name: "eLearning", bundle: nil)
                    let ZIVC = storyboard.instantiateViewController(withIdentifier: "ContinuemLearningViewController") as! ContinuemLearningViewController
                    self.navigationController?.isNavigationBarHidden = true
                    ZIVC.Achieved_Marks =  Achieved_Marks
                    ZIVC.Total_Marks =  Total_Marks
                    ZIVC.Result =  Result
                    self.navigationController?.pushViewController(ZIVC, animated: true)
                    
                }else{
                //    self.stopLoadingPK(view: self.view)
                    
                  //  self.view.makeToast("Please try again, something went wrong.")
                }
                
            }) { (err) in
                self.stopLoadingPK(view: self.view)
                
                print(err.description)
            }
        }
    
    @objc func pressSubmitQuiz(){
        
        if(strAction == ""){
            
            
            self.view.makeToast("Please attempt your questions...")
        } else{
        
                self.startLoadingPK(view: self.view)
      
        
            
            let parameter = ["Que_ID":Question_Id,
                             "Sr_No":srno,
                                 "Given_Ans":strAction,
                                 
                                 "Quiz_Time":self.lblTimeLeft.text,
                                 "Employee_ID": UserDefaults.standard.string(forKey: "EmployeeID")! ,
                    "Quiz_ID": QuizId
        
                    ] as! [String:String]
        
       
                print("parameter",parameter)
        
                WebServices.sharedInstances.sendPostRequest(url: URLConstants.Submit_Quiz, parameters: parameter, successHandler: { (response:[String : AnyObject]) in
        
                    let respon = response["response"] as! [String:AnyObject]
                    let objectmsg = MessageCallServerModel()
                    let message = objectmsg.sealizeMessage(cell: respon as! [String : AnyObject])
                    if respon["status"] as! String == "success" {
                      //  self.showSingleButtonWithMessage(title: "Validation!", message: "Successfully Submitted.", buttonName: "OK")
                        self.view.makeToast(message)
                        self.stopLoadingPK(view: self.view)
                       
                     self.strAction = ""
                         self.QuizResult()
                      
                        self.quizPreSubmitDB = []
                        
                    
                    
                    
                    
                 
                        
        
                    }else{
                        self.stopLoadingPK(view: self.view)
                       
                        self.view.makeToast(message)
                    }
        
                }) { (err) in
                    self.stopLoadingPK(view: self.view)
                   
                    print(err.description)
                }
        }
    }

    @objc func QuizResult() {
        var para = [String:String]()
        let parameter = ["Quiz_ID":QuizId, "Employee_ID":UserDefaults.standard.string(forKey: "EmployeeID")!]
        
        para = parameter.filter { $0.value != ""}
        print("para",para)
        self.quizSubmitAPI.serviceCalling(obj: self, parameter: para) { (dict) in
            
            self.quizSubmitDB = [QuizSubmitModel]()
            self.quizSubmitDB = dict as! [QuizSubmitModel]
            
            
          
           
            self.tableView.reloadData()
            
            self.Question_Id = String(self.quizSubmitDB[0].ID)
            self.QuizPreResult()
        }
        
    }
    var quizPreSubmitAPI = QuizPreSubmitListAPI()
    var quizPreSubmitDB:[QuizPreSubmitModel] = []
    @objc func QuizPreResult() {
        
        self.view.makeToast(lblTimeLeft.text)
        
        
        var para = [String:String]()
        let parameter = ["Quiz_ID":QuizId, "Employee_ID":UserDefaults.standard.string(forKey: "EmployeeID")! , "Question_ID" : Question_Id]
        
        para = parameter.filter { $0.value != ""}
        print("para",para)
        
        self.startLoading()
        self.quizPreSubmitAPI.serviceCalling(obj: self, parameter: para) { (dict) in
            
            self.quizPreSubmitDB = [QuizPreSubmitModel]()
            self.quizPreSubmitDB = dict as! [QuizPreSubmitModel]
            
         self.stopLoading()
            
            self.tableView.reloadData()
        }
        
    }
    
    
    
    var strAction = String()
    @IBAction func radioButtonClicked(_ sender: DLRadioButton){
        
        let buttonPosition:CGPoint = sender.convert(.zero, to:self.tableView)
        let indexPath = self.tableView.indexPathForRow(at: buttonPosition)
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath!) as! QuestionsSubmitTableViewCell
        
        for button in sender.selectedButtons() {
            print(String(format: "%@ is selected.\n", (button.titleLabel!.text!.characters.first?.description)!));
            strAction = (button.titleLabel!.text!.characters.first?.description)!
        }
      
     
        
        self.tableView.reloadRows(at: [indexPath!], with: .none)
      
    }
    var srno = String()
    var Question_Id = String()
    var attemptVal = Int()
     var cellData : ShowPreviousTableViewCell!
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
extension SubmitQuestionsViewController : UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if(section == 0) {
            return 10.0
        }else if(section == 3) {
            return 0.0
        }
        else {
            return 20.0
        }
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView?
    {
        if(section != 0){
            let headerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: 20))
            
            headerView.backgroundColor = UIColor.white
            
            return headerView
        }else{
            let headerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.bounds.size.width, height: 10))
            
            headerView.backgroundColor = UIColor.groupTableViewBackground
            
            return headerView
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if(indexPath.section == 2){
            if(self.quizPreSubmitDB.count == 0){
                return 81
            }else{
                return 50
            }
        }else{
            return UITableViewAutomaticDimension
        }
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 4
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(section == 0){
        return quizSubmitDB.count
        }else  if(section == 1){
            return 1
        }else{
            return 1
        }
    }
    
    
}

extension SubmitQuestionsViewController : UITableViewDataSource {
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if(indexPath.section == 0) {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! QuestionsSubmitTableViewCell
       
        cell.questionNoLbl.text = "Q." + quizSubmitDB[indexPath.section].SR_No!
//            if(Int(quizSubmitDB[indexPath.section].SR_No!)!  >= 10) {
//                cell.widthQuestionCon.constant = 40
//            }else{
//                cell.widthQuestionCon.constant = 30
//            }
             cell.widthQuestionCon.constant = cell.questionNoLbl.intrinsicContentSize.width
        cell.radioOptionA.setTitle("A. " + quizSubmitDB[indexPath.section].Option_A! , for: .normal)
        cell.radioOptionB.setTitle("B. " + quizSubmitDB[indexPath.section].Option_B!, for: .normal)
        cell.radioOptionC.setTitle("C. " + quizSubmitDB[indexPath.section].Option_C!, for: .normal)
        cell.radioOptionD.setTitle("D. " + quizSubmitDB[indexPath.section].Option_D!, for: .normal)
        var i = indexPath.section
        cell.radioOptionA.tag = indexPath.section
        cell.radioOptionB.tag = indexPath.section
        cell.radioOptionC.tag = indexPath.section
        cell.radioOptionD.tag = indexPath.section
        cell.textViewQuestion.text = quizSubmitDB[indexPath.section].Question!
        cell.closeButton.tag = i
            srno = quizSubmitDB[indexPath.section].SR_No!
       Question_Id = String(quizSubmitDB[indexPath.section].ID)
        if(strAction == "A"){
            cell.radioOptionA.isSelected = true
             cell.closeButton.isHidden = false
            
        }else if(strAction == "B"){
            cell.radioOptionB.isSelected = true
             cell.closeButton.isHidden = false
        }else if(strAction == "C"){
            cell.radioOptionC.isSelected = true
             cell.closeButton.isHidden = false
        }else if(strAction == "D"){
            cell.radioOptionD.isSelected = true
             cell.closeButton.isHidden = false
        }else{
            cell.radioOptionA.deselectOtherButtons()
            cell.radioOptionB.deselectOtherButtons()
            cell.radioOptionC.deselectOtherButtons()
            cell.radioOptionD.deselectOtherButtons()
            cell.closeButton.isHidden = true
            cell.closeButton.tag = i
        }
        
        
        return cell;
        }else if (indexPath.section == 1){
                let cell = tableView.dequeueReusableCell(withIdentifier: "button", for: indexPath) as! SubmitQuizTableViewCell
            
            return cell
        }else if (indexPath.section == 2){
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellHeader", for: indexPath) as! ShowPreviousTableViewCell
            cellData = cell
            return cell
        }
            
        else {
            
            
            if(quizPreSubmitDB.count > 0){
            
                
                let cell = tableView.dequeueReusableCell(withIdentifier: "cellPre", for: indexPath) as! SubmittedQuizTableViewCell
                
                cell.questionNoLbl.text = "Q."
                cell.lblCorrectAns.text = quizPreSubmitDB[0].Correct_Option! + ". " + quizPreSubmitDB[0].Correct_Answer!
                cell.lblGivenAns.text = quizPreSubmitDB[0].Given_Option! + ". " + quizPreSubmitDB[0].Given_Answer!
                cell.textViewQuestion.text = quizPreSubmitDB[0].Question!
                
            return cell
            }else{
                return UITableViewCell()
            }
        }
    }
}
