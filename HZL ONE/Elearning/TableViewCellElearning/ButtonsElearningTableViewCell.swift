//
//  ButtonsElearningTableViewCell.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 07/03/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit
import SDWebImage
import CoreData
var dataArrayCell1Elearning: [DashBoardElearningCourseDataModel] = []
var dataArrayCell2Elearning: [DashBoardElearningDataModel] = []
class ButtonsElearningTableViewCell: UITableViewCell,UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    var view = UIView()
    var timer = Timer()
    var lastXAxis = CGFloat()
    var contentOffset = CGPoint()
    var i = CGFloat()
    var imageDB :[BannerImageIdea] = []
    
    var scrollingTimer = Timer()
    
    @IBOutlet weak var imageCollectionView: UICollectionView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        
        self.imageCollectionView.delegate = self
        self.imageCollectionView.dataSource = self
        
        
        
        // Initialization code
    }
    
    func pageFromMain()
    {
        self.timer.invalidate()
        
        
        self.timer = Timer()
        self.lastXAxis = CGFloat()
        self.contentOffset = CGPoint()
        self.imageCollectionView.contentOffset = CGPoint.init(x: 0, y: 0)
        
    }
    
    func saveimagePathData (imagePath:String, id:Int64,text_Message:String){
        
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        
        let tasks = BannerImageIdea(context: context)
        
        tasks.imagePath = imagePath
        tasks.id = id
        tasks.text_Message = text_Message
        
        (UIApplication.shared.delegate as! AppDelegate).saveContext()
        
        
    }
    
    @objc func getBannerImageData() {
        
        self.imageDB = [BannerImageIdea]()
        do {
            
            self.imageDB = try context.fetch(BannerImageIdea.fetchRequest())
            //self.refresh.endRefreshing()
            if(self.imageDB.count > 0) {
                //  BannerStruct.bannerPath = self.imageDB[0].text_Message!
            }
            self.imageCollectionView.reloadData()
            
        } catch {
            print("Fetching Failed")
        }
    }
    
    func deleteimagePathData()
    {
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        
        let req = NSFetchRequest<NSFetchRequestResult>.init(entityName: "BannerImageIdea")
        let batchDeleteRequest = NSBatchDeleteRequest(fetchRequest: req)
        
        do {
            try context.execute(batchDeleteRequest)
            
        } catch {
            // Error Handling
        }
    }
    
    var reachablty = Reachability()!
    
    func pageViewCell1(obj : eLearningDashboardViewController)
    {
        
        if(reachablty.connection != .none)
        {
            self.timer.invalidate()
            self.timer = Timer()
            self.lastXAxis = CGFloat()
            self.contentOffset = CGPoint()
            
            self.isHidden = true
            self.imageCollectionView.contentOffset = CGPoint.init(x: 0, y: 0)
            var param = [String:String]()
            let empId : String = UserDefaults.standard.string(forKey: "EmployeeID")!
            param =  ["Employee_ID":empId]
            WebServices.sharedInstances.sendPostRequest(url: URLConstants.Dashboad_E_Learning_Course, parameters: param , successHandler: { (dict) in
                
                if let response = dict["response"]{
                    
                    let statusString : String = response["status"] as! String
                     print(response)
                    if(statusString == "success")
                    {
                        
                        if let data = dict["data"] as? [AnyObject]
                        {
                            
                            dataArrayCell1Elearning = []
                            
                            for i in 0..<data.count
                            {
                                
                                if let cell = data[i] as? [String:AnyObject]
                                {
                                    let object = DashBoardElearningCourseDataModel()
                                    object.setDataInModel(str: cell)
                                    dataArrayCell1Elearning.append(object)
                                }
                                
                            }
                            
                            self.imageCollectionView.reloadData()
                      obj.homeTableView.reloadData()
                            obj.dataInt1 = 1
                            obj.dataIntCount1 = 1
                              self.isHidden = false
                            
                            
                            
                        }
                        
                    }
                    else
                    {
                        obj.dataInt1 = 1
                        obj.dataIntCount1 = 0
                         self.isHidden = true
                    }
                    
                    
                }
                
                
                
                
            })
            { (error) in
                
                let errorStr : String = error.description
                
                print("DATA:error",errorStr)
                 self.isHidden = true
                
            }
            
            
        }
        else{
            self.isHidden = true
        }
        
        
    }
    
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return dataArrayCell1Elearning.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellButton", for: indexPath) as! ButtonElearningCollectionViewCell
        
      
        
        cell.lblTitle.text = dataArrayCell1Elearning[indexPath.row].Title
        cell.BtnShow.tag = indexPath.row
       
            if let url = NSURL(string: dataArrayCell1Elearning[indexPath.row].CoverImage) {
                cell.imageShow.sd_setImage(with: url as URL?, placeholderImage: UIImage.init(named: "placed"))
            }
        
        return cell
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        return UIEdgeInsetsMake(0, 0, 0, 0)
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
         return CGSize(width: 112, height: 122)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        
        //print(indexPath.item)
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if let myCell = cell as? ImageSlideCollectionViewCell {
            myCell.pageControl.currentPage = indexPath.item
        }
        
    }
    
 
    
   
    
    
    
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}

