//
//  ShowPreviousTableViewCell.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 14/03/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit

class ShowPreviousTableViewCell: UITableViewCell {

    @IBOutlet weak var lblNoDataHeight: NSLayoutConstraint!
    @IBOutlet weak var lblNoData: UILabel!
    @IBOutlet weak var btnShowPrevious: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
