//
//  FavouriteListViewController.swift
//  BaseProject
//
//  Created by SARVANG INFOTCH on 15/11/18.
//  Copyright © 2018 SARVANG INFOTCH. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation
class FavouriteListViewController:CommonVSClass {
    var FavouriteDB:[FavouriteDataModel] = []
    var FavouriteLoadMoreDB:[FavouriteDataModel] = []
    var FavouriteAPI = FavouriteDataAPI()
    var FavouriteLoadMoreAPI = FavouriteListLoadMoreAPI()
    var Category_ID = String()
    var data: String?
    var lastObject: String?
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        FavouriteResult()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
        tableView.tableFooterView?.backgroundColor = UIColor.groupTableViewBackground
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        
        self.title = "Favourites"
        self.tabBarController?.tabBar.isHidden = true
        let nav = self.navigationController?.navigationBar
        nav?.barStyle = UIBarStyle.black
        nav?.tintColor = UIColor.white
        nav?.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        
    }
    @objc func FavouriteResult() {
        var para = [String:String]()
        
        
        let parameter = ["Employee_ID" : UserDefaults.standard.string(forKey: "EmployeeID")! ]

        
        para = parameter.filter { $0.value != ""}
        print("para",para)
        self.FavouriteAPI.serviceCalling(obj: self, parameter: para) { (dict) in
            
            self.FavouriteDB = [FavouriteDataModel]()
            self.FavouriteDB = dict as! [FavouriteDataModel]
            print(self.FavouriteDB)
            self.tableView.reloadData()
        }
        
    }
    
    @objc func FavouriteResultLoadMore(Id:String) {
        
        
        var para = [String:String]()
        let parameter =  ["Employee_ID" : UserDefaults.standard.string(forKey: "EmployeeID")!,
                          "ID":Id]
        
        para = parameter.filter { $0.value != ""}
        print("para",para)
        
        self.FavouriteLoadMoreAPI.serviceCalling(obj: self, parameter: para) { (dict) in
            
            self.FavouriteLoadMoreDB = [FavouriteDataModel]()
            self.FavouriteLoadMoreDB = dict as! [FavouriteDataModel]
            
            switch self.FavouriteLoadMoreDB.count {
            case 0:
                break;
            default:
                self.FavouriteDB.append(contentsOf: self.FavouriteLoadMoreDB)
                self.tableView.reloadData()
                break;
            }
            
        }
        
    }
    @objc func tapCellFavourite(_ sender: UITapGestureRecognizer) {
        let buttonPosition:CGPoint = sender.view!.convert(.zero, to:self.tableView)
        let indexPath = self.tableView.indexPathForRow(at: buttonPosition)
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath!) as! eLearningListTableViewCell
        let fileId = FavouriteDB[(indexPath?.section)!].ID
        self.startLoading(view: self.view)
        var urlDeploy = String()
        var messageDeploy = String()
        let parameter = ["File_ID":String(fileId),
                         "Employee_ID": UserDefaults.standard.string(forKey: "EmployeeID")!
            
        ]
        if(FavouriteDB[(indexPath?.section)!].Is_Favourite != "0"){
            urlDeploy = URLConstants.Favourite_List_Remove
            
        }else{
            urlDeploy = URLConstants.Add_Favourite
            
        }
        
        print("parameter",parameter)
        
        WebServices.sharedInstances.sendPostRequest(url: urlDeploy, parameters: parameter, successHandler: { (response:[String : AnyObject]) in
            
            let respon = response["response"] as! [String:AnyObject]
            self.stopLoading(view: self.view)
            if respon["status"] as! String == "success" {
                
                
                if(self.FavouriteDB[(indexPath?.section)!].Is_Favourite != "0"){
                    self.FavouriteDB[(indexPath?.section)!].Is_Favourite = "0"
                    messageDeploy = "Successfully Removed from Favourite List"
                }else{
                    self.FavouriteDB[(indexPath?.section)!].Is_Favourite = "1"
                    messageDeploy = "Successfully Added to Favourite List"
                }
                self.FavouriteResult()
                self.view.makeToast(messageDeploy)
                
                
            }else{
                self.stopLoading(view: self.view)
                self.view.makeToast("Please try again, something went wrong.")
            }
            
        }) { (err) in
            self.stopLoading(view: self.view)
            print(err.description)
        }
        
        
        
        
        
        
        
    }
    @objc func tapCellWatchList(_ sender: UITapGestureRecognizer) {
        let buttonPosition:CGPoint = sender.view!.convert(.zero, to:self.tableView)
        let indexPath = self.tableView.indexPathForRow(at: buttonPosition)
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath!) as! eLearningListTableViewCell
        let fileId = FavouriteDB[(indexPath?.section)!].ID
        self.startLoading(view: self.view)
        var urlDeploy = String()
        var messageDeploy = String()
        print(UserDefaults.standard.string(forKey: "EmployeeID")!)
        let parameter = ["File_ID":String(fileId),
                         "Employee_ID": UserDefaults.standard.string(forKey: "EmployeeID")!
            
        ]
        if(FavouriteDB[(indexPath?.section)!].Is_Watchlist != "0"){
            urlDeploy = URLConstants.Watchlist_List_Remove
            
        }else{
            urlDeploy = URLConstants.Add_Watchlist
            
        }
        
        print("parameter",parameter)
        
        WebServices.sharedInstances.sendPostRequest(url: urlDeploy, parameters: parameter, successHandler: { (response:[String : AnyObject]) in
            
            let respon = response["response"] as! [String:AnyObject]
            self.stopLoading(view: self.view)
            if respon["status"] as! String == "success" {
                
                if(self.FavouriteDB[(indexPath?.section)!].Is_Watchlist != "0"){
                    self.FavouriteDB[(indexPath?.section)!].Is_Watchlist = "0"
                    messageDeploy = "Successfully Removed from Watch List"
                }else{
                    self.FavouriteDB[(indexPath?.section)!].Is_Watchlist = "1"
                    //  cell.imageWatchList.image = UIImage.init(named: "watchListSel")
                    messageDeploy = "Successfully Added to Watch List"
                }
                self.tableView.reloadRows(at: [indexPath!], with: UITableViewRowAnimation.none)
                
                self.view.makeToast(messageDeploy)
                
                
            }else{
                self.stopLoading(view: self.view)
                self.view.makeToast("Please try again, something went wrong.")
            }
            
        }) { (err) in
            self.stopLoading(view: self.view)
            print(err.description)
        }
        
        
        
        
        
    }
    @objc func tapCell(_ sender: UITapGestureRecognizer) {
        let buttonPosition:CGPoint = sender.view!.convert(.zero, to:self.tableView)
        let indexPath = self.tableView.indexPathForRow(at: buttonPosition)
        if  self.FavouriteDB[(indexPath?.section)!].Filetype == "PDF"{
            
            
            let urlString = self.FavouriteDB[(indexPath?.section)!].FilePath
            let urlShow = urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
            
//            let ELPVC = self.storyboard?.instantiateViewController(withIdentifier: "pdfViewer") as! PDFFileViewController
//            ELPVC.indexId = String((indexPath?.section)!)
//
//            ELPVC.FileName = self.FavouriteDB[(indexPath?.section)!].Filename
//            ELPVC.FilePath = urlShow!
//            self.navigationController?.pushViewController(ELPVC, animated: true)
            
            
        }else{
            if self.FavouriteDB[(indexPath?.section)!].FilePath != ""{
                let urlString = self.FavouriteDB[(indexPath?.section)!].FilePath
                let urlShow = urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
                
                
                videoPlay(str: urlShow!)
                
            }
        }
    }
    func playerDidFinishPlaying(note: NSNotification) {
        
        self.videoPlayer.pause()
        
    }
    var videoPlayer = AVPlayer()
    func videoPlay(str: String) {
        
        let videoURL = URL(string: str)
        self.videoPlayer = AVPlayer(url: videoURL!)
        let playerViewController = AVPlayerViewController()
        playerViewController.player = videoPlayer
        NotificationCenter.default.addObserver(self, selector: Selector(("playerDidFinishPlaying:")), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: videoPlayer)
        self.present(playerViewController, animated: true)
        {
            playerViewController.player!.play()
        }
        do {
            try AVAudioSession.sharedInstance().overrideOutputAudioPort(AVAudioSessionPortOverride.speaker)
        } catch _ {
            
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
extension FavouriteListViewController : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if(section == 0) {
            return 10.0
        } else {
            return 20.0
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
  
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
        // return 200
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return FavouriteDB.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    
}

extension FavouriteListViewController : UITableViewDataSource {
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! eLearningListTableViewCell
        
        
        
        cell.CategoryTitle.text = FavouriteDB[indexPath.section].Category_Title
        cell.textViewDesc.text = FavouriteDB[indexPath.section].Description
        //   cell.imageFile.image = FavouriteDB[indexPath.section].Favourite_Title
        if(  FavouriteDB[indexPath.section].Filetype == "PDF"){
            cell.imageFile.image = UIImage.init(named: "pdfFile")
        } else {
            cell.imageFile.image = UIImage.init(named: "videoPlay")
        }
        if(  FavouriteDB[indexPath.section].Is_Favourite == "0"){
            cell.imageFavourite.image = UIImage.init(named: "fav")
        } else {
            cell.imageFavourite.image = UIImage.init(named: "favSel")
        }
        if(  FavouriteDB[indexPath.section].Is_Watchlist == "0"){
            cell.imageWatchList.image = UIImage.init(named: "watchList")
        } else {
            cell.imageWatchList.image = UIImage.init(named: "watchListSel")
        }
        //
        let tapGestureCell = UITapGestureRecognizer(target: self, action: #selector(tapCell(_:)))
        cell.isUserInteractionEnabled = true;
        cell.addGestureRecognizer(tapGestureCell)
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tapCellFavourite(_:)))
        cell.imageFavourite.isUserInteractionEnabled = true;
        cell.imageFavourite.addGestureRecognizer(tapGesture)
        
        
        let tapGestureWatch = UITapGestureRecognizer(target: self, action: #selector(tapCellWatchList(_:)))
        cell.imageWatchList.isUserInteractionEnabled = true;
        cell.imageWatchList.addGestureRecognizer(tapGestureWatch)
        
        self.data = String(self.FavouriteDB[indexPath.section].ID)
        self.lastObject = String(self.FavouriteDB[indexPath.section].ID)
        
        if ( self.data ==  self.lastObject && indexPath.section == self.FavouriteDB.count - 1)
        {
            
            self.FavouriteResultLoadMore(Id: String(self.FavouriteDB[indexPath.section].ID))
            
        }
        return cell;
    }
}
