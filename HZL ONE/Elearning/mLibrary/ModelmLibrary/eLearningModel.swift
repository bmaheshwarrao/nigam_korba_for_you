//
//  eLearningModel.swift
//  BaseProject
//
//  Created by SARVANG INFOTCH on 15/11/18.
//  Copyright © 2018 SARVANG INFOTCH. All rights reserved.
//

//
//  CategoryListModel.swift
//  BaseProject
//


import Foundation
import Reachability
import CoreData

class Download {
    
    var track: elearningDataModel
    init(track: elearningDataModel) {
        self.track = track
        print(self.track)
    }
    
    // Download service sets these values:
    var task: URLSessionDownloadTask!
    var isDownloading = false
    var resumeData: Data?
    
    // Download delegate sets this value:
    var progress: Float = 0
    
}

class elearningDataModel: NSObject {
    
    
    var Catgory_ID = String()
    var Filename = String()
    var Filetype = String()
    var Description = String()
     var File_Description = String()
    var FilePath = String()
    var FilePathSaved = String()
    var ThumbPath = String()
    
    

    var Created_Date = String()
    var Is_Favourite = String()
    var Is_Watchlist = String()
     var Is_Downloaded = Bool()
     var downloaded = false
    var filePathSaved = String()
    var ID = Int()
     var  index = Int()
    
   
    
  
  
    
    
    
    func setDataInModel(str:[String:AnyObject],index : Int)
    {
        self.index = index
        if str["Is_Watchlist"] is NSNull || str["Is_Watchlist"] == nil{
            self.Is_Watchlist = ""
        }else{
            let app = str["Is_Watchlist"]
            self.Is_Watchlist = (app?.description)!
            
        }
        if str["File_Description"] is NSNull || str["File_Description"] == nil{
            self.File_Description = ""
        }else{
            let app = str["File_Description"]
            self.File_Description = (app?.description)!
            
        }
        
        
        if str["Is_Favourite"] is NSNull || str["Is_Favourite"] == nil{
            self.Is_Favourite = ""
        }else{
            let app = str["Is_Favourite"]
            self.Is_Favourite = (app?.description)!
            
        }
        if str["Created_Date"] is NSNull || str["Created_Date"] == nil{
            self.Created_Date = ""
        }else{
            let app = str["Created_Date"]
            self.Created_Date = (app?.description)!
            
        }
        
        if str["Catgory_ID"] is NSNull || str["Catgory_ID"] == nil{
            self.Catgory_ID = ""
        }else{
            let app = str["Catgory_ID"]
            self.Catgory_ID = (app?.description)!
            
        }
        if str["Filename"] is NSNull || str["Filename"] == nil{
            self.Filename = ""
        }else{
            let app = str["Filename"]
            self.Filename = (app?.description)!
            
        }
        if str["Filetype"] is NSNull || str["Filetype"] == nil{
            self.Filetype = ""
        }else{
            let app = str["Filetype"]
            self.Filetype = (app?.description)!
            
        }
        if str["Description"] is NSNull || str["Description"] == nil{
            self.Description = ""
        }else{
            let app = str["Description"]
            self.Description = (app?.description)!
            
        }
        if str["FilePath"] is NSNull || str["FilePath"] == nil{
            self.FilePath = ""
        }else{
            let app = str["FilePath"]
            self.FilePath = (app?.description)!
            
        }
        if str["FilePath"] is NSNull || str["FilePath"] == nil{
            self.FilePathSaved = ""
        }else{
            let app = str["FilePath"]
            self.FilePathSaved = (app?.description)!
            
        }
        
        if str["ThumbPath"] is NSNull || str["ThumbPath"] == nil{
            self.ThumbPath = ""
        }else{
            let app = str["ThumbPath"]
            self.ThumbPath = (app?.description)!
            
        }
        
       
        if str["ID"] is NSNull{
            self.ID = 0
        }else{
            self.ID = (str["ID"] as? Int)!
        }
        self.Is_Downloaded = false
        
    }
}

class elearningDataAPI
{
    
    var reachablty = Reachability()!
    
    
    func serviceCalling(obj:eLearningListViewController,parameter : [String:String],serviceUrl : String , success:@escaping (AnyObject)-> Void)
    {
        
        obj.startLoading()
        
        if(reachablty.connection != .none)
        {
            print(serviceUrl)
            WebServices.sharedInstances.sendPostRequest(url: serviceUrl, parameters: parameter, successHandler: { (dict) in
                
                if let response = dict["response"]{
                    
                    let statusString : String = response["status"] as! String
                    let objectmsg = MessageCallServerModel()
                    let msg = objectmsg.sealizeMessage(cell: response as! [String : AnyObject])
                    if(statusString == "success")
                    {
                        obj.tableView.isHidden = false
                        obj.noDataLabel(text: "" )
                        obj.label.isHidden = true
                        if let data = dict["data"] as? [AnyObject]
                        {
                            var dataArray : [elearningDataModel] = []
                            
                            for i in 0..<data.count
                            {
                                
                                if let cell = data[i] as? [String:AnyObject]
                                {
                                    
                                    let object = elearningDataModel()
                                    object.setDataInModel(str: cell , index : i)
                                    dataArray.append(object)
                                    
                                    
                                }
                                
                            }
                            
                            success(dataArray as AnyObject)
                            
                            
                        }
                        
                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                    }
                    else
                    {
                        print("DATA:fail")
                        obj.tableView.isHidden = true
                        obj.label.isHidden = false
                        obj.noDataLabel(text: msg )
                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                        
                    }
                    
                    
                }
                
                
                
                
            })
            { (error) in
                
                let errorStr : String = error.description
                
                print("DATA:error",errorStr)
                
                // obj.errorChecking(error: error)
                obj.refresh.endRefreshing()
                obj.stopLoading()
            }
            
            
        }
        else{
           obj.tableView.isHidden = true
            obj.label.isHidden = false
            obj.noDataLabel(text: "Internet is not available, Please check internet connection and try again." )
            obj.refresh.endRefreshing()
            obj.stopLoading()
            
        }
        
        
    }
    
    
}


class elearningListLoadMoreAPI
{
    
    var reachablty = Reachability()!
    
    
    func serviceCalling(obj:eLearningListViewController,parameter:[String:String],serviceUrl : String, success:@escaping (AnyObject)-> Void)
    {
        
        obj.startLoading()
        
        if(reachablty.connection != .none)
        {
            
            WebServices.sharedInstances.sendPostRequest(url: serviceUrl, parameters: parameter, successHandler: { (dict) in
                
                if let response = dict["response"]{
                    
                    let statusString : String = response["status"] as! String
                    
                    if(statusString == "success")
                    {
                   
                        
                        if let data = dict["data"] as? [AnyObject]
                        {
                            
                            var dataArray : [elearningDataModel] = []
                            
                            for i in 0..<data.count
                            {
                                let index1 = obj.eLearningDB.count + i
                                if let celll = data[i] as? [String:AnyObject]
                                {
                                    let object = elearningDataModel()
                                    object.setDataInModel(str: celll, index: index1)
                                    dataArray.append(object)
                                }
                                
                            }
                            
                            success(dataArray as AnyObject)
                            
                            
                        }
                        
                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                    }
                    else
                    {
                        
                        
                        print("DATA:fail")
                        //                        obj.tableView.isHidden = true
                        //                        obj.noDataLabel(text: "No hazard found with "+parameter["Search"]! )
                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                        
                    }
                    
                    
                }
                
                
                
            }, failureHandler: { (error) in
                let errorStr : String = error.description
                
                print("DATA:error",errorStr)
                
                //obj.errorChecking(error: error)
                obj.stopLoading()
            })
            
            
        }
        else{
            //            obj.noDataLabel(text:  "No internet connection found. Check your internet connection and try again.")
            //            obj.tableView.isHidden = true
            obj.refresh.endRefreshing()
            obj.stopLoading()
        }
        
        
    }
    
    
}


































