//
//  FavouriteModel.swift
//  BaseProject
//
//  Created by SARVANG INFOTCH on 15/11/18.
//  Copyright © 2018 SARVANG INFOTCH. All rights reserved.
//

//
//  eLearningModel.swift
//  BaseProject
//
//  Created by SARVANG INFOTCH on 15/11/18.
//  Copyright © 2018 SARVANG INFOTCH. All rights reserved.
//

//
//  CategoryListModel.swift
//  BaseProject
//


import Foundation
import Reachability
import CoreData

class FavouriteDataModel: NSObject {
    
    
    var Category_ID = String()
    var Filename = String()
    var Filetype = String()
    var Description = String()
    
    var FilePath = String()
    var FileSize = String()
    var ThumbPath = String()
    var Category_Title = String()
    
    var Filetype1 = String()
    var Created_Date = String()
    var Is_Favourite = String()
    var Is_Watchlist = String()
    var ID = Int()
    
    
    
  
    
    
    func setDataInModel(str:[String:AnyObject])
    {
        if str["Category_ID"] is NSNull{
            self.Category_ID = ""
        }else{
            let aidd = (str["Category_ID"] as? Int)!
            self.Category_ID = String(aidd)
        }
        if str["Is_Favourite"] is NSNull{
            self.Is_Favourite = ""
        }else{
            let aidd = (str["Is_Favourite"] as? Int)!
            self.Is_Favourite = String(aidd)
        }
        if str["Is_Watchlist"] is NSNull{
            self.Is_Watchlist = ""
        }else{
            let aidd = (str["Is_Watchlist"] as? Int)!
            self.Is_Watchlist = String(aidd)
        }
        if str["Filename"] is NSNull{
            self.Filename = ""
        }else{
            self.Filename = (str["Filename"] as? String)!
        }
        if str["Filetype"] is NSNull{
            self.Filetype = ""
        }else{
            self.Filetype = (str["Filetype"] as? String)!
        }
        
        
        if str["Description"] is NSNull{
            self.Description = ""
        }else{
            self.Description = (str["Description"] as? String)!
        }
        
        if str["FilePath"] is NSNull{
            self.FilePath = ""
        }else{
            self.FilePath = (str["FilePath"] as? String)!
        }
        if str["FileSize"] is NSNull{
            self.FileSize = ""
        }else{
            self.FileSize = (str["FileSize"] as? String)!
        }
        if str["ThumbPath"] is NSNull{
            self.ThumbPath = ""
        }else{
            self.ThumbPath = (str["ThumbPath"] as? String)!
        }
        if str["Category_Title"] is NSNull{
            self.Category_Title = ""
        }else{
            self.Category_Title = (str["Category_Title"] as? String)!
        }
        if str["Filetype1"] is NSNull{
            self.Filetype1 = ""
        }else{
            self.Filetype1 = (str["Filetype1"] as? String)!
        }
        if str["Created_Date"] is NSNull{
            self.Created_Date = ""
        }else{
            self.Created_Date = (str["Created_Date"] as? String)!
        }
        if str["ID"] is NSNull{
            self.ID = 0
        }else{
            self.ID = (str["ID"] as? Int)!
        }
        
        
    }
}

class FavouriteDataAPI
{
    
    var reachablty = Reachability()!
    
    
    func serviceCalling(obj:FavouriteListViewController,parameter : [String:String], success:@escaping (AnyObject)-> Void)
    {
        
        obj.startLoading()
        
        if(reachablty.connection != .none)
        {
            
            WebServices.sharedInstances.sendPostRequest(url: URLConstants.Favourite_List, parameters: parameter, successHandler: { (dict) in
                
                if let response = dict["response"]{
                    
                    let statusString : String = response["status"] as! String
                    let objectmsg = MessageCallServerModel()
                    let msg = objectmsg.sealizeMessage(cell: response as! [String : AnyObject])
                    if(statusString == "success")
                    {
                        obj.tableView.isHidden = false
                        obj.noDataLabel(text: "" )
                        obj.label.isHidden = true
                        if let data = dict["data"] as? [AnyObject]
                        {
                            var dataArray : [FavouriteDataModel] = []
                            
                            for i in 0..<data.count
                            {
                                
                                if let cell = data[i] as? [String:AnyObject]
                                {
                                    
                                    let object = FavouriteDataModel()
                                    object.setDataInModel(str: cell)
                                    dataArray.append(object)
                                    
                                    
                                }
                                
                            }
                            
                            success(dataArray as AnyObject)
                            
                            
                        }
                        
                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                    }
                    else
                    {
                        print("DATA:fail")
                        obj.tableView.isHidden = true
                        obj.label.isHidden = false
                        obj.noDataLabel(text: msg )
                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                        
                    }
                    
                    
                }
                
                
                
                
            })
            { (error) in
                
                let errorStr : String = error.description
                
                print("DATA:error",errorStr)
                
                // obj.errorChecking(error: error)
                obj.refresh.endRefreshing()
                obj.stopLoading()
            }
            
            
        }
        else{
            obj.tableView.isHidden = true
            obj.label.isHidden = false
            obj.noDataLabel(text: "Internet is not available, Please check internet connection and try again." )
            obj.refresh.endRefreshing()
            obj.stopLoading()
            
        }
        
        
    }
    
    
}


class FavouriteListLoadMoreAPI
{
    
    var reachablty = Reachability()!
    
    
    func serviceCalling(obj:FavouriteListViewController,parameter:[String:String], success:@escaping (AnyObject)-> Void)
    {
        
        obj.startLoading()
        
        if(reachablty.connection != .none)
        {
            
            WebServices.sharedInstances.sendPostRequest(url: URLConstants.Favourite_List, parameters: parameter, successHandler: { (dict) in
                
                if let response = dict["response"]{
                    
                    let statusString : String = response["status"] as! String
                    
                    if(statusString == "success")
                    {
                        
                        //                        obj.tableView.isHidden = false
                        //                        obj.noDataLabel(text: "" )
                        
                        if let data = dict["data"] as? [AnyObject]
                        {
                            
                            var dataArray : [FavouriteDataModel] = []
                            
                            for i in 0..<data.count
                            {
                                
                                if let celll = data[i] as? [String:AnyObject]
                                {
                                    let object = FavouriteDataModel()
                                    object.setDataInModel(str: celll)
                                    dataArray.append(object)
                                }
                                
                            }
                            
                            success(dataArray as AnyObject)
                            
                            
                        }
                        
                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                    }
                    else
                    {
                        
                        
                        print("DATA:fail")
                        //                        obj.tableView.isHidden = true
                        //                        obj.noDataLabel(text: "No hazard found with "+parameter["Search"]! )
                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                        
                    }
                    
                    
                }
                
                
                
            }, failureHandler: { (error) in
                let errorStr : String = error.description
                
                print("DATA:error",errorStr)
                
                //obj.errorChecking(error: error)
                obj.stopLoading()
            })
            
            
        }
        else{
            //            obj.noDataLabel(text:  "No internet connection found. Check your internet connection and try again.")
            //            obj.tableView.isHidden = true
            obj.refresh.endRefreshing()
            obj.stopLoading()
        }
        
        
    }
    
    
}



































