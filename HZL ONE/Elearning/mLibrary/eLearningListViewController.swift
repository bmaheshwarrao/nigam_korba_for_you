//
//  eLearningListViewController.swift
//  BaseProject
//
//  Created by SARVANG INFOTCH on 15/11/18.
//  Copyright © 2018 SARVANG INFOTCH. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation
import Foundation
import MZDownloadManager
import EzPopup
import iAd
import MediaPlayer
  var dataArrayLoadFile : [DataObjectFile] = []
var pathData = NSString()
class eLearningListViewController: CommonVSClass ,AVPlayerViewControllerDelegate,URLSessionDelegate{
    
    
    var eLearningDB:[elearningDataModel] = []
    var eLearningLoadMoreDB:[elearningDataModel] = []
    var eLearningAPI = elearningDataAPI()
    var eLearningLoadMoreAPI = elearningListLoadMoreAPI()
    var Category_ID = String()
     var Category_Title = String()
    var ControlSet = Int()
    var data: String?
    var lastObject: String?
    //  let downloadService = DownloadService()
    @IBOutlet weak var tableView: UITableView!
    lazy var downloadsSession: URLSession = {
        //    let configuration = URLSessionConfiguration.default
        let configuration = URLSessionConfiguration.background(withIdentifier: "bgSessionConfiguration")
        return URLSession(configuration: configuration, delegate: self, delegateQueue: nil)
    }()

    // Get local file path: download task stores tune here; AV player plays it.
    let documentsPath = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
    func localFilePath(for url: URL) -> URL {
        return documentsPath.appendingPathComponent(url.lastPathComponent)
    }
    var mzDownloadingViewObj    : MZDownloadManagerViewController?
    var availableDownloadsArray: [String] = []
    
    let myDownloadPath = MZUtility.baseFilePath + "/DownloadData"
    var serviceUrl = String()
     var strData = String()
    override func viewDidLoad() {
        super.viewDidLoad()
       NotificationCenter.default.addObserver(self, selector: #selector(self.cancelReload), name: NSNotification.Name?.init(NSNotification.Name(rawValue: "cancelReload")), object: nil)
        let nav = self.navigationController?.navigationBar
        nav?.barStyle = UIBarStyle.black
        nav?.tintColor = UIColor.white
        nav?.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
       
//        self.navigationController?.navigationBar.topItem?.title = Category_Title
    
          NotificationCenter.default.addObserver(self, selector: #selector(self.refreshData), name: NSNotification.Name?.init(NSNotification.Name(rawValue: "refreshData")), object: nil)
        if !FileManager.default.fileExists(atPath: myDownloadPath) {
            try! FileManager.default.createDirectory(atPath: myDownloadPath, withIntermediateDirectories: true, attributes: nil)
        }
        debugPrint("custom download path: \(myDownloadPath)")
        if(ControlSet == 1){
            serviceUrl = URLConstants.E_Learning
            strData = "E_Learning"
            self.title = Category_Title
            
        }else if(ControlSet == 2){
            serviceUrl = URLConstants.Favourite_List
            strData = "Favourite_List"
            self.title = "Favourites"
            
        }
        else if(ControlSet == 3){
            serviceUrl = URLConstants.Watchlist_List
            self.title = "Watch List"
            
            strData = "Watchlist_List"
        }
        
       
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
        tableView.tableFooterView?.backgroundColor = UIColor.groupTableViewBackground
        self.setUpDownloadingViewController()
        refresh.addTarget(self, action: #selector(refreshData), for: .valueChanged)
        
        self.tableView.addSubview(refresh)
        let swipe = UISwipeGestureRecognizer(target: self, action: #selector(self.swipeRecognizer))
        swipe.direction = ([.down])
        self.view.addGestureRecognizer(swipe)
         //   downloadService.downloadsSession = downloadsSession
        // Do any additional setup after loading the view.
    }
    
    
    
    var reachability = Reachability()!
    @objc func swipeRecognizer(_ sender: UISwipeGestureRecognizer) {
        if (sender.direction == .down) {
            
            if(self.reachability.connection != .none)
            {
                
                self.refreshData()
            }
            else
            {
                self.startLoading(view: self.view)
                DispatchQueue.main.asyncAfter(deadline: .now() + 4.5) {
                    self.noDataLabel(text: "No internet connection found. Check your internet connection and try again.")
                    self.stopLoading(view: self.view)
                }
            }
            
        }
    }
    @objc func refreshData(){
        getofflineFileDetailsData()
        eLearningResult()
    }
    func setUpDownloadingViewController() {
       

        
        getofflineFileDetailsData()
        eLearningResult()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
      
//        PrakrstaFileDownloader.shared.progressCallback   = downloadProgress(progress:id:)
//        PrakrstaFileDownloader.shared.completionDownload = downloadCompleted(success:id:)
        if(ControlSet == 1){
            serviceUrl = URLConstants.E_Learning
            strData = "E_Learning"
            self.title = Category_Title
            
        }else if(ControlSet == 2){
            serviceUrl = URLConstants.Favourite_List
            strData = "Favourite_List"
            self.title = "Favourites"
            
        }
        else if(ControlSet == 3){
            serviceUrl = URLConstants.Watchlist_List
            self.title = "Watch List"
            
            strData = "Watchlist_List"
        }
        self.tabBarController?.tabBar.isHidden = true
        let nav = self.navigationController?.navigationBar
        nav?.barStyle = UIBarStyle.black
        nav?.tintColor = UIColor.white
        nav?.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        
    }
    @objc func pressFileHistory(Id : String ,fileType : String){
 
        let parameter = [
            
            "Employee_ID": UserDefaults.standard.string(forKey: "EmployeeID")! ,
            "File_ID": Id ,
            "Action_Type" : fileType ,
            "Activity_Type" : "" ,
            "MSG" : "View Library File"
            
            ] as! [String:String]
        
        
        
        
        print("parameter",parameter)
        
        WebServices.sharedInstances.sendPostRequest(url: URLConstants.ELibrary_History_Write, parameters: parameter, successHandler: { (response:[String : AnyObject]) in
            
            let respon = response["response"] as! [String:AnyObject]
            
            if respon["status"] as! String == "success" {
             
                
            }else{
              
            }
            
        }) { (err) in
        
            
            print(err.description)
        }
    }

    @objc func eLearningResult() {
    
        
        if(strData != ""){
            if(strData == "E_Learning"){
                serviceUrl = URLConstants.E_Learning
                
            }else if(strData == "Favourite_List"){
                serviceUrl = URLConstants.Favourite_List
                
            }
            else if(strData == "Watchlist_List"){
                serviceUrl = URLConstants.Watchlist_List
                
            }
        }
        
        
        print()
        
        
        
          let empId : String = (UserDefaults.standard.string(forKey: "EmployeeID")!)
        
        
        var parameter : [String: String] = [:]
    if(serviceUrl == URLConstants.E_Learning){
        parameter = ["Employee_ID": empId ,"Category_ID": Category_ID  ]
    } else {
        parameter = ["Employee_ID" : empId ]
        }
        
        
        var para = [String:String]()
   
        
         para = parameter.filter { $0.value != ""}
          print("para",para)
        self.eLearningAPI.serviceCalling(obj: self, parameter: para, serviceUrl: serviceUrl) { (dict) in
            
            self.eLearningDB = [elearningDataModel]()
            self.eLearningDB = dict as! [elearningDataModel]
            print(self.eLearningDB)
            
            
            
            
            
            
            self.tableView.reloadData()
        }
        
    }
    
    @objc func eLearningResultLoadMore(Id:String) {
        
        let empId : String = (UserDefaults.standard.string(forKey: "EmployeeID")!)
        var para = [String:String]()
       
        var parameter : [String: String] = [:]
        if(serviceUrl == URLConstants.E_Learning){
            parameter = [
                        "Employee_ID": empId ,"Category_ID": Category_ID ,
                         
                "ID":Id
                         
            ]
        } else {
            parameter = ["Employee_ID" : empId , "ID":Id]
        }
        
        
        
        para = parameter.filter { $0.value != ""}
        print("para",para)
        
        self.eLearningLoadMoreAPI.serviceCalling(obj: self, parameter: para, serviceUrl: serviceUrl) { (dict) in
            
            self.eLearningLoadMoreDB = [elearningDataModel]()
            self.eLearningLoadMoreDB = dict as! [elearningDataModel]
            
            switch self.eLearningLoadMoreDB.count {
            case 0:
                break;
            default:
                self.eLearningDB.append(contentsOf: self.eLearningLoadMoreDB)
                self.tableView.reloadData()
                break;
            }
            
        }
        
    }
    @objc func tapCellFavourite(_ sender: UITapGestureRecognizer) {
        let buttonPosition:CGPoint = sender.view!.convert(.zero, to:self.tableView)
        let indexPath = self.tableView.indexPathForRow(at: buttonPosition)
           let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath!) as! eLearningListTableViewCell
        let fileId = eLearningDB[(indexPath?.row)!].ID
        self.startLoading(view: self.view)
        var urlDeploy = String()
         var messageDeploy = String()
        let parameter = ["File_ID":String(fileId),
                         "Employee_ID": UserDefaults.standard.string(forKey: "EmployeeID")!
            
            ]
        if(eLearningDB[(indexPath?.row)!].Is_Favourite != "0"){
            urlDeploy = URLConstants.Favourite_List_Remove
            
        }else{
             urlDeploy = URLConstants.Add_Favourite
            
        }
      
        print("parameter",parameter)
        
        WebServices.sharedInstances.sendPostRequest(url: urlDeploy, parameters: parameter, successHandler: { (response:[String : AnyObject]) in
            
            let respon = response["response"] as! [String:AnyObject]
            self.stopLoading(view: self.view)
            let objectmsg = MessageCallServerModel()
            let msg = objectmsg.sealizeMessage(cell: respon as! [String : AnyObject])
            if respon["status"] as! String == "success" {
                
                
                if(self.eLearningDB[(indexPath?.row)!].Is_Favourite != "0"){
                  self.eLearningDB[(indexPath?.row)!].Is_Favourite = "0"
                    messageDeploy = "Successfully Removed from Favourite List"
                }else{
                   self.eLearningDB[(indexPath?.row)!].Is_Favourite = "1"
                    messageDeploy = "Successfully Added to Favourite List"
                }
                self.tableView.reloadRows(at: [indexPath!], with: UITableViewRowAnimation.none)
                self.tableView.makeToast(messageDeploy)
               
                
            }else{
                self.stopLoading(view: self.view)
                self.view.makeToast(msg)
            }
            
        }) { (err) in
            self.stopLoading(view: self.view)
            print(err.description)
        }
        
        
        
        
        
      
        
    }
    @objc func tapCellWatchList(_ sender: UITapGestureRecognizer) {
        let buttonPosition:CGPoint = sender.view!.convert(.zero, to:self.tableView)
        let indexPath = self.tableView.indexPathForRow(at: buttonPosition)
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath!) as! eLearningListTableViewCell
        let fileId = eLearningDB[(indexPath?.row)!].ID
        self.startLoading(view: self.view)
        var urlDeploy = String()
        var messageDeploy = String()
        print(UserDefaults.standard.string(forKey: "EmployeeID")!)
        let parameter = ["File_ID":String(fileId),
                         "Employee_ID": UserDefaults.standard.string(forKey: "EmployeeID")!
            
        ]
        if(eLearningDB[(indexPath?.row)!].Is_Watchlist != "0"){
            urlDeploy = URLConstants.Watchlist_List_Remove
           
        }else{
            urlDeploy = URLConstants.Add_Watchlist
           
        }
        
        print("parameter",parameter)
        
        WebServices.sharedInstances.sendPostRequest(url: urlDeploy, parameters: parameter, successHandler: { (response:[String : AnyObject]) in
            
            let respon = response["response"] as! [String:AnyObject]
            self.stopLoading(view: self.view)
            let objectmsg = MessageCallServerModel()
            let msg = objectmsg.sealizeMessage(cell: respon as! [String : AnyObject])
            if respon["status"] as! String == "success" {
                
                if(self.eLearningDB[(indexPath?.row)!].Is_Watchlist != "0"){
                    self.eLearningDB[(indexPath?.row)!].Is_Watchlist = "0"
                    messageDeploy = "Successfully Removed from Watch List"
                }else{
                    self.eLearningDB[(indexPath?.row)!].Is_Watchlist = "1"
                 //  cell.imageWatchList.image = UIImage.init(named: "watchListSel")
                    messageDeploy = "Successfully Added to Watch List"
                }
                   self.tableView.reloadRows(at: [indexPath!], with: UITableViewRowAnimation.none)
                
                self.view.makeToast(messageDeploy)
                
                
            }else{
                self.stopLoading(view: self.view)
                self.view.makeToast(msg)
            }
            
        }) { (err) in
            self.stopLoading(view: self.view)
            print(err.description)
        }
        
        
        
        
        
    }
    func verifyUrl(url: String) -> Bool {
        
        var exists: Bool = false
        let url: NSURL = NSURL(string: url)!
        var request: NSMutableURLRequest = NSMutableURLRequest(url: url as URL)
        request.httpMethod = "HEAD"
        var response: URLResponse?
        do{
       try NSURLConnection.sendSynchronousRequest(request as URLRequest, returning: &response )
        }
        catch{
            
        }
        if let httpResponse = response as? HTTPURLResponse {
            
            if httpResponse.statusCode == 200 {
                
                exists =  true
            }else{
                exists  = false
            }
            
        }
        return exists
    }
    @objc func tapCellDownloadList(_ sender: UITapGestureRecognizer) {

    }
    
    var index : IndexPath!
    @objc func tapCell(_ sender: UITapGestureRecognizer) {
        let buttonPosition:CGPoint = sender.view!.convert(.zero, to:self.tableView)
        let indexPath = self.tableView.indexPathForRow(at: buttonPosition)
        if(self.eLearningDB[(indexPath?.row)!].Filetype == "Folder"){
            let storyBoard = UIStoryboard(name: "mLibrary", bundle: nil)
            let ELPVC = storyBoard.instantiateViewController(withIdentifier: "eLearningListViewController") as! eLearningListViewController
            ELPVC.ControlSet = 1
              ELPVC.Category_Title = eLearningDB[(indexPath?.row)!].Filename
            ELPVC.Category_ID = String(self.eLearningDB[(indexPath?.row)!].ID)
            self.navigationController?.pushViewController(ELPVC, animated: true)
            
        }else{
        
        
     
        print(self.eLearningDB[(indexPath?.row)!].Filetype)
        
        var fileSavePath = String()

        if(fileSavePath == String()){
            fileSavePath = self.eLearningDB[(indexPath?.row)!].FilePathSaved
        }
         if(self.eLearningDB[(indexPath?.row)!].Filetype == "PDF"){
            let storyBoard = UIStoryboard(name: "HZLONE", bundle: nil)
            let ELPVC = storyBoard.instantiateViewController(withIdentifier: "HzlPdfReaderViewController") as! HzlPdfReaderViewController
            HZlPolicyTempData.ELCat_ID = String(self.eLearningDB[(indexPath?.row)!].ID)
            
            HZlPolicyTempData.file_Url = fileSavePath
            HZlPolicyTempData.file_Name = self.eLearningDB[(indexPath?.row)!].Filename
            self.navigationController?.pushViewController(ELPVC, animated: true)
         }else if(self.eLearningDB[(indexPath?.row)!].Filetype == "PPT" || self.eLearningDB[(indexPath?.row)!].Filetype == "Document" ||  self.eLearningDB[(indexPath?.row)!].Filetype == "Excel"){
            let urlValue = "https://view.officeapps.live.com/op/view.aspx?src=" + self.eLearningDB[(indexPath?.row)!].FilePath
            linkHzl(urlData: urlValue, title: self.eLearningDB[(indexPath?.row)!].Filename)
         }else if(self.eLearningDB[(indexPath?.row)!].Filetype == "RAR" || self.eLearningDB[(indexPath?.row)!].Filetype == "ZIP" ){
          self.view.makeToast("Cannot Open Zip/RAR file in this Application")
         }else{
            if(self.eLearningDB[(indexPath?.row)!].Is_Downloaded == true){
            let urlString = fileSavePath
            print(urlString)
          
            videoLocalPlay(str: urlString)
            }else{
                let urlString = fileSavePath
                print(urlString)
                let urlShow = urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
                videoPlay(str: urlShow!)
            }
        }
        }
        
        
        
        
        
        
        
       
        

    }
    func linkHzl(urlData : String , title : String){
        let storyBoard = UIStoryboard(name: "HZLONE", bundle: nil)
        let proNav = storyBoard.instantiateViewController(withIdentifier: "ProgressWebNavViewController") as! ProgressWebNavViewController
        
        let  progressWebViewController = proNav.topViewController as! ProgressWebViewController
        guard   let url = URL(string: urlData) else {
            return
        }
        progressWebViewController.disableZoom = true
        progressWebViewController.url = url
        progressWebViewController.bypassedSSLHosts = [url.host!]
        progressWebViewController.userAgent = "ProgressWebViewController/1.0.0"
        progressWebViewController.websiteTitleInNavigationBar = false
        progressWebViewController.navigationItem.title = title
        progressWebViewController.leftNavigaionBarItemTypes = [.reload]
        progressWebViewController.toolbarItemTypes = [.back, .forward, .activity]
        self.present(proNav, animated: true, completion: nil)
    }
    var offlineFileDB =  [OfflineFilesSaved]()
    @objc func getofflineFileDetailsData() {
        
        self.offlineFileDB = [OfflineFilesSaved]()
        
        do {
            
            self.offlineFileDB = try context.fetch(OfflineFilesSaved.fetchRequest())
            
            
            
        } catch {
            print("Fetching Failed")
        }
    }
   
    var progressLinerar = Float()
    func downloadProgress(progress: Double, id: String){
        print("Progress \(progress) for id : \(id)")
         progressLinerar = Float(progress)
        self.tableView.reloadRows(at: [index!], with: UITableViewRowAnimation.none)
        //linearBar.progressValue = CGFloat(progress)
    }
    
    func downloadCompleted(success: Bool, id: String){
        print("downloadCompleted \(success) for id : \(id)")
        progressLinerar = 0.0
        self.tableView.reloadRows(at: [index!], with: UITableViewRowAnimation.none)
        
        
    }
      let customAlertVC = MZDownloadManagerViewController.instantiate()
    var selectedIndexpath : IndexPath!
    @IBAction func downloadTapped(_ sender: UIButton) {
        let buttonPosition:CGPoint = sender.convert(.zero, to:self.tableView)
        let indexPath = self.tableView.indexPathForRow(at: buttonPosition)
        selectedIndexpath = indexPath
        if(self.eLearningDB[(indexPath?.row)!].Is_Downloaded == false){
        var ii = 0
           
        if(dataArrayLoadFile.count > 0){
            for i in 0...dataArrayLoadFile.count - 1 {
                if(dataArrayLoadFile[i].data?.FilePath == self.eLearningDB[(indexPath?.row)!].FilePath){
                    ii = 1
                    break;
                }
            }
        }
        
            let urlVerify : Bool =  verifyUrl(url: self.eLearningDB[(indexPath?.row)!].FilePath)
            print(urlVerify)
            
            
            if(urlVerify == true){
        if(ii == 0){
            let track = self.eLearningDB[(indexPath?.row)!]
            let urlTrack = URL(fileURLWithPath: track.FilePath)
            let dataOb = DataObjectFile()
            
            dataOb.data = self.eLearningDB[(indexPath?.row)!]
            dataOb.Id = String(dataArrayLoadFile.count + 1)
               dataOb.isDownload = false
            dataOb.progress = 0.0
            dataOb.start = ""
            dataArrayLoadFile.append(dataOb)
           
           sender.rotate360Degrees()
           
            
            guard let customAlertVC = customAlertVC else { return }
            
            customAlertVC.strChange = "Change"
            let popupVC = PopupViewController(contentController: customAlertVC, popupWidth: self.view.frame.width - 50,popupHeight : 250)
            popupVC.cornerRadius = 5
            popupVC.canTapOutsideToDismiss = false
            present(popupVC, animated: true, completion: nil)
          
            

            let path = track.FilePath.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed)

            let fileURL  : NSString  = path as! NSString
            var fileName : NSString = fileURL.lastPathComponent as NSString
            fileName = MZUtility.getUniqueFileNameWithPath((myDownloadPath as NSString).appendingPathComponent(fileName as String) as NSString)

           // Use it download at default path i.e document directory
            pathData = fileURL
        mzDownloadingViewObj?.downloadManager.addDownloadTask(fileName as String, fileURL: fileURL as String)

        }else{
            self.view.makeToast("Already in Queue...")
        }
        
        
            }else{
                self.view.makeToast("Cannot download , Invalid Url!")
            }
        }
        
       
    }
    
    @objc func cancelReload(){
       self.tableView.reloadRows(at: [selectedIndexpath], with: .none)
    }
    
//    lazy var downloadManager: MZDownloadManager = {
//        [unowned self] in
//
//        let sessionIdentifer: String = "com.iosDevelopment.sarvang.BackgroundSession"
//
//        let appDelegate = UIApplication.shared.delegate as! AppDelegate
//        var completion = appDelegate.backgroundSessionCompletionHandler
//
//        let downloadmanager = MZDownloadManager(session: sessionIdentifer, delegate: self, completion: completion)
//        return downloadmanager
//        }()
    func playerDidFinishPlaying(note: NSNotification) {
        
        self.videoPlayer.pause()
        
    }
 
 
    var videoPlayer = AVPlayer()
    func videoPlay(str: String) {
        
        let videoURL = URL(string: str)
        self.videoPlayer = AVPlayer(url: videoURL!)
        let playerViewController = AVPlayerViewController()
        playerViewController.player = videoPlayer
        NotificationCenter.default.addObserver(self, selector: Selector(("playerDidFinishPlaying:")), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: videoPlayer)
        self.present(playerViewController, animated: true)
        {
            playerViewController.player!.play()
        }
        do {
            try AVAudioSession.sharedInstance().overrideOutputAudioPort(AVAudioSessionPortOverride.speaker)
        } catch _ {
            
        }
    }
 
    func videoLocalPlay(str: String) {
        
      let downloadURLHD = str
//        let fileName = downloadURL.characters.split("/").map(String.init).last as String!
        let fileNameHD = downloadURLHD.characters.split(separator: "/").map(String.init).last as String?
        let documents = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0] as! String
        
            let myDownPath = MZUtility.baseFilePath + "/DownloadData/"
        
        let downloadFilePath = myDownPath + "\(fileNameHD!)"
        
        let checkValidation = FileManager.default
        
        if checkValidation.fileExists(atPath: downloadFilePath){
            print("video found")
            let mediaURL = URL(fileURLWithPath: downloadFilePath)
            let asset = AVURLAsset(url: mediaURL)
            let playerItem = AVPlayerItem(asset: asset)
            let playerViewController = AVPlayerViewController()
            let player = AVPlayer(playerItem: playerItem)
            playerViewController.player = player
            NotificationCenter.default.addObserver(self, selector: Selector(("playerDidFinishPlaying:")), name: NSNotification.Name.AVPlayerItemDidPlayToEndTime, object: player)
            self.present(playerViewController, animated: true)
            {
                playerViewController.player!.play()
            }
            do {
                try AVAudioSession.sharedInstance().overrideOutputAudioPort(AVAudioSessionPortOverride.speaker)
            } catch _ {
                
            }
        }
        
       
        
        
    }
    @IBAction func btnBarClicked(_ sender: UIBarButtonItem) {
//        let storyboard = UIStoryboard(name: "mLearning", bundle: nil)
//        let MainTabVC = storyboard.instantiateViewController(withIdentifier: "MZDownloadManagerViewController") as! MZDownloadManagerViewController
//        self.navigationController?.pushViewController(MainTabVC, animated: true)
        
       // self.tabBarController?.selectedIndex = 2
    }
    @IBAction func btnCancelClicked(_ sender: UIButton) {
        let buttonPosition:CGPoint = sender.convert(.zero, to:self.tableView)
        let indexPath = self.tableView.indexPathForRow(at: buttonPosition)
          // PrakrstaFileDownloader.shared.cancelDownload(url:  self.eLearningDB[(indexPath?.row)!].FilePath)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
extension eLearningListViewController : UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if(section == 0) {
            return 10.0
        } else {
            return 20.0
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
        // return 200
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return eLearningDB.count
    }
    
    
}

extension eLearningListViewController : UITableViewDataSource {
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if(eLearningDB[indexPath.row].Filetype == "Folder"){
               let cell = tableView.dequeueReusableCell(withIdentifier: "cellFolder", for: indexPath) as! FolderListTableViewCell
            cell.lblFolder.text = eLearningDB[indexPath.row].Filename
             cell.lblDesc.text = eLearningDB[indexPath.row].Description
            self.data = String(self.eLearningDB[indexPath.row].ID)
            self.lastObject = String(self.eLearningDB[indexPath.row].ID)
            let tapGestureCell = UITapGestureRecognizer(target: self, action: #selector(tapCell(_:)))
            cell.isUserInteractionEnabled = true;
            cell.addGestureRecognizer(tapGestureCell)
            if ( self.data ==  self.lastObject && indexPath.row == self.eLearningDB.count - 1)
            {
                
                self.eLearningResultLoadMore(Id: String(self.eLearningDB[indexPath.row].ID))
                
            }
            return cell;
        }else{
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! eLearningListTableViewCell
        cell.delegate = self
        
        var type = String()
        cell.CategoryTitle.text = eLearningDB[indexPath.row].Filename
        cell.textViewDesc.text = eLearningDB[indexPath.row].Description 
      
            
    if(eLearningDB[indexPath.row].ThumbPath == ""){
            
        if(  eLearningDB[indexPath.row].Filetype == "PDF"){
            cell.imageFile.image = UIImage.init(named: "pdfFile")
            type = ".pdf"
        } else if(eLearningDB[indexPath.row].Filetype == "PPT" ){
            cell.imageFile.image = UIImage(named: "ppt")
            type = ".ppt"
        }else if(eLearningDB[indexPath.row].Filetype == "Document" ){
            cell.imageFile.image = UIImage(named: "word")
            type = ".docx"
        }else if(eLearningDB[indexPath.row].Filetype == "Excel" ){
            cell.imageFile.image = UIImage(named: "excel")
            type = ".xls"
        }else if(eLearningDB[indexPath.row].Filetype == "Audio" ){
            cell.imageFile.image = UIImage(named: "music")
            type = ".mp3"
        }else if(eLearningDB[indexPath.row].Filetype == "RAR" ){
            cell.imageFile.image = UIImage(named: "rar")
            type = ".rar"
        }else if(eLearningDB[indexPath.row].Filetype == "ZIP" ){
            cell.imageFile.image = UIImage(named: "zip")
            type = ".zip"
        } else {
             cell.imageFile.image = UIImage.init(named: "video")
               type = ".mp4"
        }
    }else{
        if let url = NSURL(string: eLearningDB[indexPath.row].ThumbPath) {
            cell.imageFile.sd_setImage(with: url as URL?, placeholderImage: UIImage.init(named: "placed"))
        }
            }
        
        
        if(  eLearningDB[indexPath.row].Is_Favourite == "0"){
            cell.imageFavourite.image = UIImage.init(named: "fav")
        } else {
            cell.imageFavourite.image = UIImage.init(named: "favSel")
        }
        if(  eLearningDB[indexPath.row].Is_Watchlist == "0"){
            cell.imageWatchList.image = UIImage.init(named: "watchList")
        } else {
            cell.imageWatchList.image = UIImage.init(named: "watchListSel")
        }
        
//
        let tapGestureCell = UITapGestureRecognizer(target: self, action: #selector(tapCell(_:)))
        cell.isUserInteractionEnabled = true;
        cell.addGestureRecognizer(tapGestureCell)
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tapCellFavourite(_:)))
        cell.imageFavourite.isUserInteractionEnabled = true;
        cell.imageFavourite.addGestureRecognizer(tapGesture)
        
        
        let tapGestureWatch = UITapGestureRecognizer(target: self, action: #selector(tapCellWatchList(_:)))
        cell.imageWatchList.isUserInteractionEnabled = true;
        cell.imageWatchList.addGestureRecognizer(tapGestureWatch)
        
        if dataArrayLoadFile.count > 0 {
            for i in 0...dataArrayLoadFile.count - 1 {
                if( String (dataArrayLoadFile[i].data!.ID) == String (self.eLearningDB[indexPath.row].ID)) {
                    
                    cell.imageDownload.rotate360Degrees()
                    break;
            }
        }
        }
//        let filelocalName = self.eLearningDB[indexPath.row].FilePath.fileName() + type
        var saved = false
        if(self.offlineFileDB.count > 0){
            for i in 0...self.offlineFileDB.count - 1 {
                if(self.offlineFileDB[i].id == String (self.eLearningDB[indexPath.row].ID)) {
                    saved = true;
                    self.eLearningDB[indexPath.row].Is_Downloaded = true
                    if(self.eLearningDB[indexPath.row].Filetype != "PPT" || self.eLearningDB[indexPath.row].Filetype != "Document" ||  self.eLearningDB[indexPath.row].Filetype != "Excel"){
                    self.eLearningDB[indexPath.row].FilePathSaved = self.offlineFileDB[i].fileSavedPath!
                        break;
                    }
                    
        }
            }
        }
            if(saved == true){
                cell.imageDownload.setImage(UIImage.init(named: "downloaded"), for: .normal)
            }else{
                cell.imageDownload.setImage(UIImage.init(named: "download"), for: .normal)
            }
       
      //  cell.configure(track: track, downloaded: track.downloaded, download: downloadService.activeDownloads[urlTrack])
        
        self.data = String(self.eLearningDB[indexPath.row].ID)
        self.lastObject = String(self.eLearningDB[indexPath.row].ID)
        
        if ( self.data ==  self.lastObject && indexPath.row == self.eLearningDB.count - 1)
        {
            
            self.eLearningResultLoadMore(Id: String(self.eLearningDB[indexPath.row].ID))
            
        }
        return cell;
        }
    
}
}
extension eLearningListViewController: eLearningListTableViewCellDelegate {
    
    func downloadTapped(_ cell: eLearningListTableViewCell) {
        if let indexPath = tableView.indexPath(for: cell) {
            let track = self.eLearningDB[indexPath.row]
           // downloadService.startDownload(track)
            reload(indexPath.row)
        }
    }
    

    
    func cancelTapped(_ cell: eLearningListTableViewCell) {
        if let indexPath = tableView.indexPath(for: cell) {
            let track = self.eLearningDB[indexPath.row]
          //  downloadService.cancelDownload(track)
            reload(indexPath.row)
        }
    }
    
    // Update track cell's buttons
    func reload(_ row: Int) {
        tableView.reloadRows(at: [IndexPath(row: 0, section: row)], with: .none)
    }
    
}



class DataObjectFile : NSObject{
    var Id : String?
    var isDownload : Bool?
    var data : elearningDataModel?
    var progress : Float?
    var totalSize : String?
    var indexPathData : IndexPath?
    var start : String?
}
extension UIButton {
    func rotate360Degrees(duration: CFTimeInterval = 1.0, completionDelegate: AnyObject? = nil) {
        let rotateAnimation = CABasicAnimation(keyPath: "transform.rotation")
       rotateAnimation.fromValue = 0.0
        rotateAnimation.toValue = CGFloat(M_PI * 2)
        rotateAnimation.isRemovedOnCompletion = false
        rotateAnimation.duration = duration
        rotateAnimation.repeatCount=Float.infinity
        
        if let delegate: AnyObject = completionDelegate {
            rotateAnimation.delegate = delegate as? CAAnimationDelegate
        }
        self.layer.add(rotateAnimation, forKey: nil)
    }
}
