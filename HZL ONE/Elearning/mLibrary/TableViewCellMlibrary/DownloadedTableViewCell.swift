//
//  DownloadedTableViewCell.swift
//  mLearning
//
//  Created by SARVANG INFOTCH on 10/12/18.
//  Copyright © 2018 SARVANG INFOTCH. All rights reserved.
//

import UIKit
import CoreData
class DownloadedTableViewCell: UITableViewCell {
   // var delegate: DownloadListTableViewCellDelegate?
    
 
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var textViewDesc: UITextView!
    @IBOutlet weak var CategoryTitle: UILabel!
    @IBOutlet weak var imageFile: UIImageView!

    
    @IBOutlet weak var btnDelete: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        
        
        
        // Initialization code
    }
    func getContext () -> NSManagedObjectContext {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        return appDelegate.persistentContainer.viewContext
    }
    var offlineFileDB =  [OfflineFilesSaved]()
    var indexpathh : [IndexPath] = []
    let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    @objc func getofflineFileDetailsData() {
        
        
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
 func updateCellForRowAtIndexPath(_ indexPath : IndexPath) {
    self.offlineFileDB = [OfflineFilesSaved]()
    
    do {
        indexpathh = []
        self.offlineFileDB = try context.fetch(OfflineFilesSaved.fetchRequest())
        if(self.offlineFileDB.count > 0){
                            for i in 0...self.offlineFileDB.count - 1 {
                                 let index = IndexPath(row: i, section: 1)
                                indexpathh.append(index)
            
                            }
            // self.tableView.reloadRows(at: indexpathh, with: .none)
            
        }
        
        
    } catch {
        print("Fetching Failed")
    }
    
    }
}
