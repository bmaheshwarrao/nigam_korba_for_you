//
//  QuizHistorymLearningViewController.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 08/03/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit

class QuizHistorymLearningViewController: CommonVSClass {
    
    var quizDB:[QuizHistoryElearningModel] = []
    var quizLoadMoreDB:[QuizHistoryElearningModel] = []
    var QuizAPI = QuizHistoryElearningAPI()
    var QuizLoadMoreAPI = QuizHistoryElearningLoadMoreAPI()
    
    var data: String?
    var lastObject: String?
    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        QuizResult()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
        tableView.tableFooterView?.backgroundColor = UIColor.groupTableViewBackground
        refresh.addTarget(self, action: #selector(QuizResult), for: .valueChanged)
        
        self.tableView.addSubview(refresh)
        let swipe = UISwipeGestureRecognizer(target: self, action: #selector(self.swipeRecognizer))
        swipe.direction = ([.down])
        self.view.addGestureRecognizer(swipe)
        // Do any additional setup after loading the view.
    }
    
    var reachability = Reachability()!
    @objc func swipeRecognizer(_ sender: UISwipeGestureRecognizer) {
        if (sender.direction == .down) {
            
            if(self.reachability.connection != .none)
            {
                
                self.QuizResult()
            }
            else
            {
                self.startLoading(view: self.view)
                DispatchQueue.main.asyncAfter(deadline: .now() + 4.5) {
                    self.noDataLabel(text: "No internet connection found. Check your internet connection and try again.")
                    self.stopLoading(view: self.view)
                }
            }
            
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        
        self.title = "Quiz History"
        self.tabBarController?.tabBar.isHidden = true
        let nav = self.navigationController?.navigationBar
        nav?.tintColor = UIColor.white
        nav?.barTintColor = UIColor(hexString: "2c3e50", alpha: 1.0)
        nav?.isTranslucent = false
        nav?.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        
    }
    var leadingConst : CGFloat = 0
    @objc func QuizResult() {
        var para = [String:String]()
        let parameter = ["Employee_ID":UserDefaults.standard.string(forKey: "EmployeeID")!]
        
        para = parameter.filter { $0.value != ""}
        print("para",para)
        self.QuizAPI.serviceCalling(obj: self, parameter: para) { (dict) in
            
            self.quizDB = [QuizHistoryElearningModel]()
            self.quizDB = dict as! [QuizHistoryElearningModel]
            print(self.quizDB)
            self.tableView.reloadData()
        }
        
    }
    
    @objc func QuizResultLoadMore(Id:String) {
        
        
        var para = [String:String]()
        let parameter = ["ID":Id
            , "Employee_ID":UserDefaults.standard.string(forKey: "EmployeeID")!]
        
        para = parameter.filter { $0.value != ""}
        print("para",para)
        
        self.QuizLoadMoreAPI.serviceCalling(obj: self, parameter: para) { (dict) in
            
            self.quizLoadMoreDB = [QuizHistoryElearningModel]()
            self.quizLoadMoreDB = dict as! [QuizHistoryElearningModel]
            
            switch self.quizLoadMoreDB.count {
            case 0:
                break;
            default:
                self.quizDB.append(contentsOf: self.quizLoadMoreDB)
                self.tableView.reloadData()
                break;
            }
            
        }
        
    }
    @objc func tapCell(_ sender: UITapGestureRecognizer) {
        let buttonPosition:CGPoint = sender.view!.convert(.zero, to:self.tableView)
        let indexPath = self.tableView.indexPathForRow(at: buttonPosition)
        let storyboard = UIStoryboard(name: "eLearning", bundle: nil)
        let ZIVC = storyboard.instantiateViewController(withIdentifier: "QuizAnswermLearningViewController") as! QuizAnswermLearningViewController
        ZIVC.Quiz_Id  = String(quizDB[(indexPath?.row)!].Quiz_ID!)
        self.navigationController?.pushViewController(ZIVC, animated: true)
        
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

extension QuizHistorymLearningViewController : UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if(section == 0) {
            return 10.0
        } else {
            return 20.0
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
        // return 200
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return quizDB.count
    }
    
    
}

extension QuizHistorymLearningViewController : UITableViewDataSource {
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! mQuizHistoryTableViewCell
        
        
        
        cell.titleLabel.text = quizDB[indexPath.row].Title
        cell.passLabel.text = quizDB[indexPath.row].Passing_Mark
        cell.resultLabel.text = quizDB[indexPath.row].Result
        //            if(quizDB[indexPath.row].Result == "Pass"){
        //                cell.resultLabel.textColor = UIColor.green
        //            }else{
        //                cell.resultLabel.textColor = UIColor.red
        //            }
        
        cell.achieveLabel.text = quizDB[indexPath.row].Achieved_Marks! + "/" + quizDB[indexPath.row].Total_Mark!
        
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = NSTimeZone.system
        dateFormatter.dateFormat = "MMM dd yyyy HH:mm:ss:SSSa"
        let date = dateFormatter.date(from: self.quizDB[indexPath.row].Date_Time!)
        
        let dateFormatter2 = DateFormatter()
        dateFormatter2.timeZone = NSTimeZone.system
        dateFormatter2.dateFormat = "dd MMM yyyy"
        var dateStr = String()
        switch date {
        case nil:
            let date_TimeStr = dateFormatter2.string(from: Date())
            dateStr = date_TimeStr
            break;
        default:
            let date_TimeStr = dateFormatter2.string(from: date!)
            
            dateStr = date_TimeStr
            break;
        }
        
        
        cell.dateLabel.text = dateStr
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tapCell(_:)))
        cell.isUserInteractionEnabled = true;
        cell.addGestureRecognizer(tapGesture)
        
        self.data = String(self.quizDB[indexPath.row].Quiz_ID!)
        self.lastObject = String(self.quizDB[indexPath.row].Quiz_ID!)
        //            if(indexPath.row == 0){
        //                leadingConst = self.view.frame.width/3
        //            }
        
        if ( self.data ==  self.lastObject && indexPath.row == self.quizDB.count - 1)
        {
            
            self.QuizResultLoadMore(Id: String(self.quizDB[indexPath.row].ID))
            
        }
        return cell;
    }
}
