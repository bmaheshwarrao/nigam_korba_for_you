//
//  CategorymLearningModel.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 08/03/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import Foundation


class CategorymLearningDataModel: NSObject {
    
    
    var ID = Int()
    var Category_Title = String()
    var Description = String()
    
    
    

    
    
    
    
    
    
    func setDataInModel(str:[String:AnyObject])
    {
        if str["Category_Title"] is NSNull || str["Category_Title"] == nil{
            self.Category_Title = ""
        }else{
            
            let tit = str["Category_Title"]
            
            self.Category_Title = (tit?.description)!
            
            
        }
        if str["Description"] is NSNull || str["Description"] == nil{
            self.Description =  ""
        }else{
            
            let desc = str["Description"]
            
            self.Description = (desc?.description)!
            
            print(self.Description)
            //  self.Description = (str["Description"] as? String)!
        }
        if str["ID"] is NSNull || str["ID"] == nil{
            self.ID = 0
        }else{
            self.ID = (str["ID"] as? Int)!
        }
       
        
    }
    
}





class CategorymLearningDataAPI
{
    
    var reachablty = Reachability()!
    
    func serviceCalling(obj:mLearningCategoryViewController, param : [String: String] , success:@escaping (AnyObject)-> Void)
    {
        obj.startLoading()
        
        if(reachablty.connection != .none)
        {
            
            
            WebServices.sharedInstances.sendPostRequest(url: URLConstants.E_Learning_Category, parameters: param , successHandler: { (dict) in
                
                if let response = dict["response"]{
                    
                    let statusString : String = response["status"] as! String
                    let objectmsg = MessageCallServerModel()
            let msg = objectmsg.sealizeMessage(cell: response as! [String : AnyObject])
                      print(response)
                    if(statusString == "success")
                    {
                        obj.tableView.isHidden = false
                        obj.noDataLabel(text: "" )
                        obj.label.isHidden = true
                        if let data = dict["data"] as? [AnyObject]
                        {
                            
                            var dataArray: [CategorymLearningDataModel] = []
                            
                            for i in 0..<data.count
                            {
                                
                                if let cell = data[i] as? [String:AnyObject]
                                {
                                    let object = CategorymLearningDataModel()
                                    object.setDataInModel(str: cell)
                                    dataArray.append(object)
                                }
                                
                            }
                            
                            success(dataArray as AnyObject)
                            
                            
                        }
                        
                        obj.refreshControl.endRefreshing()
                        obj.stopLoading()
                    }
                    else
                    {
                        obj.label.isHidden = false
                        obj.tableView.isHidden = true
                        obj.noDataLabel(text: msg )
                        obj.refreshControl.endRefreshing()
                        obj.stopLoading()
                        
                    }
                    
                    
                }
                
                
                
                
            })
            { (error) in
                
                let errorStr : String = error.description
                
                print("DATA:error",errorStr)
                
                //                obj.errorChecking(error: error)
                obj.stopLoading()
            }
            
            
        }
        else{
            obj.label.isHidden = false
            obj.tableView.isHidden = true
            obj.noDataLabel(text: "Internet is not available, please check your internet connection try again." )
            obj.stopLoading()
        }
        
        
    }
    
    
}












class QuizmLearningDataModel: NSObject {
    
    
    var ID = Int()
    var Title = String()
    var Category_ID = String()
    var Category_Title = String()
    var No_of_Question = String()
    var Mark_Per_Question = String()
    var Total_Mark = String()
    var Passing_Mark = String()
    var CoverImage = String()
    var Description = String()
    var DateTime = String()
    var Create_By = String()
    var Status = String()
    var Is_Test = String()
     var Total_Question = String()
    

    
    
    
    
    
    
    func setDataInModel(str:[String:AnyObject])
    {
        if str["Title"] is NSNull || str["Title"] == nil{
            self.Title = ""
        }else{
            
            let tit = str["Title"]
            
            self.Title = (tit?.description)!
            
            
        }
        if str["Description"] is NSNull || str["Description"] == nil{
            self.Description =  ""
        }else{
            
            let desc = str["Description"]
            
            self.Description = (desc?.description)!
            
            print(self.Description)
            //  self.Description = (str["Description"] as? String)!
        }
        if str["ID"] is NSNull || str["ID"] == nil{
            self.ID = 0
        }else{
            self.ID = (str["ID"] as? Int)!
        }
      
        
        if str["Category_ID"] is NSNull || str["Category_ID"] == nil{
            self.Category_ID =  ""
        }else{
            
            let desc = str["Category_ID"]
            
            self.Category_ID = (desc?.description)!
            
            print(self.Description)
            //  self.Description = (str["Description"] as? String)!
        }
        if str["Category_Title"] is NSNull || str["Category_Title"] == nil{
            self.Category_Title =  ""
        }else{
            
            let desc = str["Category_Title"]
            
            self.Category_Title = (desc?.description)!
            
            print(self.Description)
            //  self.Description = (str["Description"] as? String)!
        }
        if str["No_of_Question"] is NSNull || str["No_of_Question"] == nil{
            self.No_of_Question =  ""
        }else{
            
            let desc = str["No_of_Question"]
            
            self.No_of_Question = (desc?.description)!
            
            print(self.Description)
            //  self.Description = (str["Description"] as? String)!
        }
        if str["Mark_Per_Question"] is NSNull || str["Mark_Per_Question"] == nil{
            self.Mark_Per_Question =  ""
        }else{
            
            let desc = str["Mark_Per_Question"]
            
            self.Mark_Per_Question = (desc?.description)!
            
            print(self.Description)
            //  self.Description = (str["Description"] as? String)!
        }
       
       
        if str["Total_Mark"] is NSNull || str["Total_Mark"] == nil{
            self.Total_Mark =  ""
        }else{
            
            let desc = str["Total_Mark"]
            
            self.Total_Mark = (desc?.description)!
            
            print(self.Description)
            //  self.Description = (str["Description"] as? String)!
        }
        if str["Passing_Mark"] is NSNull || str["Passing_Mark"] == nil{
            self.Passing_Mark =  ""
        }else{
            
            let desc = str["Passing_Mark"]
            
            self.Passing_Mark = (desc?.description)!
            
            print(self.Description)
            //  self.Description = (str["Description"] as? String)!
        }
        if str["CoverImage"] is NSNull || str["CoverImage"] == nil{
            self.CoverImage =  ""
        }else{
            
            let desc = str["CoverImage"]
            
            self.CoverImage = (desc?.description)!
            
            print(self.Description)
            //  self.Description = (str["Description"] as? String)!
        }
       
        if str["DateTime"] is NSNull || str["DateTime"] == nil{
            self.DateTime =  ""
        }else{
            
            let desc = str["DateTime"]
            
            self.DateTime = (desc?.description)!
            
            print(self.Description)
            //  self.Description = (str["Description"] as? String)!
        }

        if str["Create_By"] is NSNull || str["Create_By"] == nil{
            self.Create_By =  ""
        }else{
            
            let desc = str["Create_By"]
            
            self.Create_By = (desc?.description)!
            
            print(self.Description)
            //  self.Description = (str["Description"] as? String)!
        }
        if str["Status"] is NSNull || str["Status"] == nil{
            self.Status =  ""
        }else{
            
            let desc = str["Status"]
            
            self.Status = (desc?.description)!
            
            print(self.Description)
            //  self.Description = (str["Description"] as? String)!
        }
        if str["Is_Test"] is NSNull || str["Is_Test"] == nil{
            self.Is_Test =  ""
        }else{
            
            let desc = str["Is_Test"]
            
            self.Is_Test = (desc?.description)!
            
            print(self.Description)
            //  self.Description = (str["Description"] as? String)!
        }
        if str["Total_Question"] is NSNull || str["Total_Question"] == nil{
            self.Total_Question =  ""
        }else{
            
            let desc = str["Total_Question"]
            
            self.Total_Question = (desc?.description)!
            
            print(self.Description)
            //  self.Description = (str["Description"] as? String)!
        }
    }
    
}





class QuizmLearningDataAPI
{
    
    var reachablty = Reachability()!
    
    func serviceCalling(obj:QuizListmLearningViewController, param : [String: String] , success:@escaping (AnyObject)-> Void)
    {
        obj.startLoading()
        
        if(reachablty.connection != .none)
        {
            
            
            WebServices.sharedInstances.sendPostRequest(url: URLConstants.Quiz_ListmLearning, parameters: param , successHandler: { (dict) in
                
                if let response = dict["response"]{
                    
                    let statusString : String = response["status"] as! String
                    let objectmsg = MessageCallServerModel()
            let msg = objectmsg.sealizeMessage(cell: response as! [String : AnyObject])
                    print(response)
                    if(statusString == "success")
                    {
                        obj.tableView.isHidden = false
                        obj.noDataLabel(text: "" )
                        obj.label.isHidden = true
                        if let data = dict["data"] as? [AnyObject]
                        {
                            
                            var dataArray: [QuizmLearningDataModel] = []
                            
                            for i in 0..<data.count
                            {
                                
                                if let cell = data[i] as? [String:AnyObject]
                                {
                                    let object = QuizmLearningDataModel()
                                    object.setDataInModel(str: cell)
                                    dataArray.append(object)
                                }
                                
                            }
                            
                            success(dataArray as AnyObject)
                            
                            
                        }
                        
                        obj.refreshControl.endRefreshing()
                        obj.stopLoading()
                    }
                    else
                    {
                        obj.label.isHidden = false
                        obj.tableView.isHidden = true
                        obj.noDataLabel(text: msg )
                        obj.refreshControl.endRefreshing()
                        obj.stopLoading()
                        
                    }
                    
                    
                }
                
                
                
                
            })
            { (error) in
                
                let errorStr : String = error.description
                
                print("DATA:error",errorStr)
                
                //                obj.errorChecking(error: error)
                obj.stopLoading()
            }
            
            
        }
        else{
            obj.label.isHidden = false
            obj.tableView.isHidden = true
            obj.noDataLabel(text: "Internet is not available, please check your internet connection try again." )
            obj.stopLoading()
        }
        
        
    }
    
    
}
class QuizmLearningStartDataModel: NSObject {
    
    
    var ID = Int()
  var Category_Title = String()
     var Description = String()
    var Category_ID = String()
    var Filename = String()
    var Filetype = String()
    var FilePath = String()
    var FileSize = String()
    var ThumbPath = String()
    var Course_Title = String()
    var Course_ID = String()
    var Created_Date = String()
    var IS_Learning = String()
  
    
    

    
    
    
    func setDataInModel(str:[String:AnyObject])
    {
        if str["Category_Title"] is NSNull || str["Category_Title"] == nil{
            self.Category_Title = ""
        }else{
            
            let tit = str["Category_Title"]
            
            self.Category_Title = (tit?.description)!
            
            
        }
        if str["Description"] is NSNull || str["Description"] == nil{
            self.Description =  ""
        }else{
            
            let desc = str["Description"]
            
            self.Description = (desc?.description)!
            
          
            //  self.Description = (str["Description"] as? String)!
        }
        if str["ID"] is NSNull || str["ID"] == nil{
            self.ID = 0
        }else{
            self.ID = (str["ID"] as? Int)!
        }
        
        
        if str["Category_ID"] is NSNull || str["Category_ID"] == nil{
            self.Category_ID =  ""
        }else{
            
            let desc = str["Category_ID"]
            
            self.Category_ID = (desc?.description)!
            
   
        }
        if str["Filename"] is NSNull || str["Filename"] == nil{
            self.Filename =  ""
        }else{
            
            let desc = str["Filename"]
            
            self.Filename = (desc?.description)!

        }
        if str["Filetype"] is NSNull || str["Filetype"] == nil{
            self.Filetype =  ""
        }else{
            
            let desc = str["Filetype"]
            
            self.Filetype = (desc?.description)!
            

        }
        if str["FilePath"] is NSNull || str["FilePath"] == nil{
            self.FilePath =  ""
        }else{
            
            let desc = str["FilePath"]
            
            self.FilePath = (desc?.description)!
            
        
            //  self.Description = (str["Description"] as? String)!
        }
        
        
        if str["FileSize"] is NSNull || str["FileSize"] == nil{
            self.FileSize =  ""
        }else{
            
            let desc = str["FileSize"]
            
            self.FileSize = (desc?.description)!

        }
        if str["ThumbPath"] is NSNull || str["ThumbPath"] == nil{
            self.ThumbPath =  ""
        }else{
            
            let desc = str["ThumbPath"]
            
            self.ThumbPath = (desc?.description)!
            
     
        }
        if str["Course_Title"] is NSNull || str["Course_Title"] == nil{
            self.Course_Title =  ""
        }else{
            
            let desc = str["Course_Title"]
            
            self.Course_Title = (desc?.description)!
            
            print(self.Description)
            //  self.Description = (str["Description"] as? String)!
        }
        
        if str["Course_ID"] is NSNull || str["Course_ID"] == nil{
            self.Course_ID =  ""
        }else{
            
            let desc = str["Course_ID"]
            
            self.Course_ID = (desc?.description)!
            
            
        }
        
        if str["Created_Date"] is NSNull || str["Created_Date"] == nil{
            self.Created_Date =  ""
        }else{
            
            let desc = str["Created_Date"]
            
            self.Created_Date = (desc?.description)!
          
        }
        if str["IS_Learning"] is NSNull || str["IS_Learning"] == nil{
            self.IS_Learning =  ""
        }else{
            
            let desc = str["IS_Learning"]
            
            self.IS_Learning = (desc?.description)!
            
        
        }
       
    }
    
}

class QuizmLearningStartDataAPI
{
    
    var reachablty = Reachability()!
    
    func serviceCalling(obj:StartQuizListViewController, param : [String: String] , success:@escaping (AnyObject)-> Void)
    {
        obj.startLoading()
        
        if(reachablty.connection != .none)
        {
            
            
            WebServices.sharedInstances.sendPostRequest(url: URLConstants.E_Learning_Files, parameters: param , successHandler: { (dict) in
                
                if let response = dict["response"]{
                    
                    let statusString : String = response["status"] as! String
                    let objectmsg = MessageCallServerModel()
            let msg = objectmsg.sealizeMessage(cell: response as! [String : AnyObject])
                    print(response)
                    if(statusString == "success")
                    {
                        obj.btnStartQuiz.isHidden = true
                        obj.tableView.isHidden = false
                        obj.noDataLabel(text: "" )
                        obj.label.isHidden = true
                        if let data = dict["data"] as? [AnyObject]
                        {
                            
                            var dataArray: [QuizmLearningStartDataModel] = []
                            
                            for i in 0..<data.count
                            {
                                
                                if let cell = data[i] as? [String:AnyObject]
                                {
                                    let object = QuizmLearningStartDataModel()
                                    object.setDataInModel(str: cell)
                                    dataArray.append(object)
                                }
                                
                            }
                            
                            success(dataArray as AnyObject)
                            
                            
                        }
                        
                        obj.refreshControl.endRefreshing()
                        obj.stopLoading()
                       
                    }
                    else
                    {
                        obj.label.isHidden = true
                        obj.tableView.isHidden = true
                        obj.btnStartQuiz.isHidden = false
                        obj.noDataLabel(text: msg )
                        obj.refreshControl.endRefreshing()
                        obj.stopLoading()
                        
                    }
                    
                    
                }
                
                
                
                
            })
            { (error) in
                
                let errorStr : String = error.description
                
                print("DATA:error",errorStr)
                
                //                obj.errorChecking(error: error)
                obj.stopLoading()
            }
            
            
        }
        else{
            obj.btnStartQuiz.isHidden = false
            obj.label.isHidden = true
            obj.tableView.isHidden = true
            //obj.noDataLabel(text: "Internet is not available, please check your internet connection try again." )
            obj.stopLoading()
        }
        
        
    }
    
    
}
class QuizCheckModel: NSObject {
    
    
    
    var S: String?
    var MSG: String?
    
  
    
    func setDataInModel(cell:[String:AnyObject])
    {
        
        if cell["S"] is NSNull || cell["S"] == nil {
            self.S = ""
        }else{
            let app = cell["S"]
            self.S = (app?.description)!
        }
        if cell["MSG"] is NSNull || cell["MSG"] == nil {
            self.MSG = ""
        }else{
            let app = cell["MSG"]
            self.MSG = (app?.description)!
        }
        
        
    }
}
class QuizmLearningCheckUserCourseDataAPI
{
    
    var reachablty = Reachability()!
    
    func serviceCalling(obj:StartQuizListViewController, param : [String: String] , success:@escaping (AnyObject)-> Void)
    {
        obj.startLoadingPK(view: obj.view)
       
        if(reachablty.connection != .none)
        {
            
            
            WebServices.sharedInstances.sendPostRequest(url: URLConstants.E_Learning_Course_Completed, parameters: param , successHandler: { (dict) in
                
                if let response = dict["response"]{
                    let objectmsg = MessageCallServerModel()
            let msg = objectmsg.sealizeMessage(cell: response as! [String : AnyObject])
                    print(response)
                    let statusString : String = response["status"] as! String
                    
                    if(statusString == "success")
                    {
                      
                        if let data = dict["data"] as? AnyObject
                        {
                            
                            var dataArray : [QuizCheckModel] = []
                            if let celll = data as? [String:AnyObject]
                            {
                                let object = QuizCheckModel()
                                object.setDataInModel(cell: celll)
                                dataArray.append(object)
                            }
                            
                          
                            
                            success(dataArray as AnyObject)
                            
                            
                        }
                        
                     
                        obj.stopLoadingPK(view: obj.view)
                        
                    }
                    else
                    {
                        
                       
                        obj.stopLoadingPK(view: obj.view)
                        
                    }
                    
                    
                }
                
                
                
                
            })
            { (error) in
                
                let errorStr : String = error.description
                
                print("DATA:error",errorStr)
                
                //                obj.errorChecking(error: error)
                 obj.stopLoadingPK(view: obj.view)
            }
            
            
        }
        else{
         
            //obj.noDataLabel(text: "Internet is not available, please check your internet connection try again." )
            obj.stopLoadingPK(view: obj.view)
        }
        
        
    }
    
    
}





