//
//  QuizListDatamLearningModel.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 08/03/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import Foundation

class QuizSubmitmLearningModel: NSObject {
    
    
    
    var Question: String?
    
    var Option_A : String?
    var Option_B : String?
    var Option_C : String?
    var Option_D : String?
    
    var ID  = Int()
    
    
    
    var Test_ID : String?
    
    var Description : String?
    
    var Que_Count : String?
    var SR_No : String?
   
    
    
    

    
    func setDataInModel(cell:[String:AnyObject])
    {
        
        if cell["Question"] is NSNull || cell["Question"] == nil{
            self.Question = ""
        }else{
            let qz = cell["Question"]
            
            self.Question = qz?.description
        }
        if cell["Option_A"] is NSNull || cell["Option_A"] == nil{
            self.Option_A = ""
        }else{
            let qz = cell["Option_A"]
            
            self.Option_A = qz?.description
        }
        if cell["Option_B"] is NSNull || cell["Option_B"] == nil{
            self.Option_B = ""
        }else{
            let qz = cell["Option_B"]
            
            self.Option_B = qz?.description
        }
        if cell["Option_C"] is NSNull || cell["Option_C"] == nil{
            self.Option_C = ""
        }else{
            
            let qz = cell["Option_C"]
            
            self.Option_C = qz?.description
        }
        if cell["Option_D"] is NSNull || cell["Option_D"] == nil{
            self.Option_D = ""
        }else{
            
            let qz = cell["Option_D"]
            
            self.Option_D = qz?.description
        }
      
        if cell["Description"] is NSNull || cell["Description"] == nil{
            self.Description = ""
        }else{
            
            let qz = cell["Description"]
            
            self.Description = qz?.description
        }
        if cell["Test_ID"] is NSNull || cell["Test_ID"] == nil{
            self.Test_ID = "0"
        }else{
            let qz = cell["Test_ID"]
            
            self.Test_ID = qz?.description
        }
        if cell["ID"] is NSNull || cell["ID"] == nil{
            self.ID = 0
        }else{
            self.ID = (cell["ID"] as? Int)!
        }
      
        
        
        
        
        
        
        
        
        
        if cell["Que_Count"] is NSNull || cell["Que_Count"] == nil{
            self.Que_Count = "0"
        }else{
            let qz = cell["Que_Count"]
            
            self.Que_Count = qz?.description
        }
        
        if cell["SR_No"] is NSNull || cell["SR_No"] == nil{
            self.SR_No = "0"
        }else{
            let qz = cell["SR_No"]
            
            self.SR_No = qz?.description
        }
       
        
    }
}

class QuizSubmitmLearningListAPI
{
    
    var reachablty = Reachability()!
    
    
    func serviceCalling(obj:SubmitQuizmLearningViewController,parameter:[String:String], success:@escaping (AnyObject)-> Void)
    {
        
        obj.startLoading()
        
        if(reachablty.connection != .none)
        {
            
            WebServices.sharedInstances.sendPostRequest(url: URLConstants.Temp_Question_ListElearning, parameters: parameter, successHandler: { (dict) in
                
                if let response = dict["response"]{
                    
                    let statusString : String = response["status"] as! String
                    let objectmsg = MessageCallServerModel()
            let msg = objectmsg.sealizeMessage(cell: response as! [String : AnyObject])
                    print(response)
                    if(statusString == "success")
                    {
                        
                        obj.tableView.isHidden = false
                        obj.noDataLabel(text: "" )
                        obj.label.isHidden = true
                        var QuestionLast = ""
                        if let data = dict["data"] as? AnyObject
                        {
                            let qz = data["Question"] as? AnyObject
                            
                            QuestionLast = (qz?.description)!
                            
                            if(QuestionLast != "" && QuestionLast != "<null>" && QuestionLast != nil ) {
                                var dataArray : [QuizSubmitmLearningModel] = []
                                
                                
                                if let celll = data as? [String:AnyObject]
                                {
                                    let object = QuizSubmitmLearningModel()
                                    object.setDataInModel(cell: celll)
                                    dataArray.append(object)
                                }
                                
                                
                             
                                
                            
                                
                                
                                obj.totalQuesLabel.text = dataArray[0].Que_Count
                                
                                let intQue = Int(dataArray[0].SR_No!)! - 1
                                obj.attemptLabel.text = String(intQue)
                                obj.pendingLabel.text = String(Int(dataArray[0].Que_Count!)! - intQue)
                                success(dataArray as AnyObject)
                            }else{
                          
                                obj.pressFinalSubmitQuiz()
                            }
                            
                        }
                        
                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                    }
                    else
                    {
                        
                        
                        print("DATA:fail")
                        obj.tableView.isHidden = true
                        obj.noDataLabel(text: msg)
                        obj.refresh.endRefreshing()
                        obj.label.isHidden = false
                        obj.stopLoading()
                        
                        obj.pressFinalSubmitQuiz()
                        print(response)
                        
                    }
                    
                    
                }
                
                
                
            }, failureHandler: { (error) in
                let errorStr : String = error.description
                
                print("DATA:error",errorStr)
                
                // obj.errorChecking(error: error)
                obj.stopLoading()
            })
            
            
        }
        else{
            obj.noDataLabel(text:  "No internet connection found. Check your internet connection and try again.")
            obj.tableView.isHidden = true
            obj.label.isHidden = false
            obj.refresh.endRefreshing()
            obj.stopLoading()
        }
        
        
    }
    
    
}

class QuizPreSubmitmLearningListAPI
{
    
    var reachablty = Reachability()!
    
    
    func serviceCalling(obj:SubmitQuizmLearningViewController,parameter:[String:String], success:@escaping (AnyObject)-> Void)
    {
        
        obj.startLoading()
        
        if(reachablty.connection != .none)
        {
            
            WebServices.sharedInstances.sendPostRequest(url: URLConstants.Preview_questions_with_answersElearning, parameters: parameter, successHandler: { (dict) in
                
                if let response = dict["response"]{
                    
                    let statusString : String = response["status"] as! String
                    let objectmsg = MessageCallServerModel()
            let msg = objectmsg.sealizeMessage(cell: response as! [String : AnyObject])
                    print(response)
                    if(statusString == "success")
                    {
                        
                        obj.tableView.isHidden = false
                        obj.noDataLabel(text: "" )
                        obj.label.isHidden = true
                        
                        if let data = dict["data"] as? [AnyObject]
                        {
                            
                            var dataArray : [QuizPreSubmitModel] = []
                            
                            for i in 0..<data.count
                            {
                                
                                if let celll = data[i] as? [String:AnyObject]
                                {
                                    let object = QuizPreSubmitModel()
                                    object.setDataInModel(cell: celll)
                                    dataArray.append(object)
                                }
                                
                            }
                            
                            success(dataArray as AnyObject)
                            
                            
                        }
                        obj.cellData.btnShowPrevious.isHidden = true
                         obj.cellData.lblNoData.isHidden = true
                          obj.cellData.lblNoDataHeight.constant = 0
                        obj.refresh.endRefreshing()
                        obj.stopLoading()
                    }
                    else
                    {
                        obj.stopLoading()
                        obj.cellData.btnShowPrevious.isHidden = false
                        print("DATA:fail")
                        obj.cellData.lblNoDataHeight.constant = 18
                        obj.cellData.lblNoData.isHidden = false
                        
                    }
                    
                    
                }
                
                
                
            }, failureHandler: { (error) in
                let errorStr : String = error.description
                obj.cellData.btnShowPrevious.isHidden = false
                 obj.cellData.lblNoData.isHidden = false
                print("DATA:error",errorStr)
                obj.cellData.lblNoDataHeight.constant = 18
                // obj.errorChecking(error: error)
                obj.stopLoading()
            })
            
            
        }
        else{
            //            obj.noDataLabel(text:  "No internet connection found. Check your internet connection and try again.")
            //            obj.tableView.isHidden = true
            //            obj.label.isHidden = false
            //            obj.refresh.endRefreshing()
            obj.cellData.btnShowPrevious.isHidden = false
             obj.cellData.lblNoData.isHidden = false
            obj.cellData.lblNoDataHeight.constant = 18
            obj.stopLoading()
        }
        
        
    }
    
    
}

