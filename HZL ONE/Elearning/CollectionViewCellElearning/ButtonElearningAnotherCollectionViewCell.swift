//
//  ButtonElearningAnotherCollectionViewCell.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 08/03/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit

class ButtonElearningAnotherCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var imageShow: UIImageView!
    @IBOutlet weak var BtnShow: UIButton!
}
