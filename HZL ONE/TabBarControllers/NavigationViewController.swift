//
//  NavigationViewController.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 20/11/18.
//  Copyright © 2018 SARVANG INFOTCH. All rights reserved.
//

import UIKit
import ProgressWebViewController
class NavigationViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
tableView.delegate = self
        tableView.dataSource = self
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func AboutHZLClicked(_ sender: UIButton) {
        let storyBoard = UIStoryboard(name: "HZLONE", bundle: nil)
        let ZIVC = storyBoard.instantiateViewController(withIdentifier: "AboutHZlViewController") as! AboutHZlViewController
        
        
        self.navigationController?.pushViewController(ZIVC, animated: true)
        
        
    }
    @IBAction func btnCeoClicked(_ sender: UIButton) {
        let storyBoard = UIStoryboard(name: "HZLONE", bundle: nil)
        let ZIVC = storyBoard.instantiateViewController(withIdentifier: "CEOMessageViewController") as! CEOMessageViewController
       
        
        self.navigationController?.pushViewController(ZIVC, animated: true)
    }
    
    @IBAction func btnFeedBackClicked(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let FBVC = storyboard.instantiateViewController(withIdentifier: "Feedback") as! FeedbackViewController
        FBVC.StrNav = "Feedback"
        self.navigationController?.pushViewController(FBVC, animated: true)
    }
    @IBAction func btnPolicyClicked(_ sender: UIButton) {
        let storyBoard = UIStoryboard(name: "HZLONE", bundle: nil)
        let ZIVC = storyBoard.instantiateViewController(withIdentifier: "HZLCategoryViewController") as! HZLCategoryViewController
        
        
        self.navigationController?.pushViewController(ZIVC, animated: true)
    }
    
     @IBAction func btnHzlIndiaLinkClicked(_ sender: UIButton) {
        linkHzl(urlData: "http://www.hzlindia.com", title: "Hind Zinc Limited")
    }
    @IBAction func btnvedenataLinkClicked(_ sender: UIButton) {
       
       
        
        
        linkHzl(urlData: "https://www.vedantaresources.com", title: "Vedanta")
    }
    @IBAction func btnvedenataLimitedLinkClicked(_ sender: UIButton) {
        
          linkHzl(urlData: "https://www.vedantalimited.com", title: "Vedanta India Limited")
    }
    @IBAction func btnFbLinkClicked(_ sender: UIButton) {
        linkHzl(urlData: "https://www.facebook.com/", title: "HZL Facebook")
    }
    @IBAction func btnInstaLinkClicked(_ sender: UIButton) {
        linkHzl(urlData: "http://instagram.com/balco_india", title: "HZL Instagram")
    }
    @IBAction func btnTwiterLinkClicked(_ sender: UIButton) {
        linkHzl(urlData: "https://twitter.com/#!/Hindustan_Zinc", title: "HZL Twitter")
    }
    func linkHzl(urlData : String , title : String){
        let storyBoard = UIStoryboard(name: "HZLONE", bundle: nil)
        let proNav = storyBoard.instantiateViewController(withIdentifier: "ProgressWebNavViewController") as! ProgressWebNavViewController
        
        let  progressWebViewController = proNav.topViewController as! ProgressWebViewController
        guard   let url = URL(string: urlData) else {
            return
        }
        progressWebViewController.disableZoom = true
        progressWebViewController.url = url
        progressWebViewController.bypassedSSLHosts = [url.host!]
        progressWebViewController.userAgent = "ProgressWebViewController/1.0.0"
        progressWebViewController.websiteTitleInNavigationBar = false
        progressWebViewController.navigationItem.title = title
        progressWebViewController.leftNavigaionBarItemTypes = [.reload]
        progressWebViewController.toolbarItemTypes = [.back, .forward, .activity]
        self.present(proNav, animated: true, completion: nil)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        // NotificationCenter.default.post(name: NSNotification.Name.init("sendReportFromHome"), object: nil)
        if(MoveStruct.isMove == true){
            MoveStruct.isMove = false
            self.view.makeToast(MoveStruct.message)
        }
        self.title = "Navigation"
        self.tabBarController?.tabBar.isHidden = false
        let nav = self.navigationController?.navigationBar
        nav?.tintColor = UIColor.white
       nav?.barTintColor = UIColor(hexString: "2c3e50", alpha: 1.0)
        nav?.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        
        
        
    }
//    override func viewWillDisappear(_ animated: Bool) {
//        self.tabBarController?.tabBar.isHidden = false
//    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        
        
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        return 20.0
        
        
        
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if(indexPath.section == 0){
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellQuick", for: indexPath)
            
         
            
            return cell
            
        }else if(indexPath.section == 1){
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellLinks", for: indexPath)
            
            
            
            return cell
        }
        else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellSocial", for: indexPath)
            
            
            
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableViewAutomaticDimension
        
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
