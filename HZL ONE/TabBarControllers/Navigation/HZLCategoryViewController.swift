//
//  HZLCategoryViewController.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 28/02/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit

class HZLCategoryViewController: CommonVSClass,UINavigationBarDelegate,UINavigationControllerDelegate,UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    
    
    
    var CategoryDB:[HzlCategoryDataModel] = []
    var CategoryAPI = HzlcategoryDataAPI()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        callCategoryData()
       
        
      
        
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.estimatedRowHeight = 62
        tableView.rowHeight = UITableViewAutomaticDimension
        
        
        refresh.addTarget(self, action: #selector(callCategoryData), for: .valueChanged)
       
        
        self.tableView.addSubview(refresh)
        let swipe = UISwipeGestureRecognizer(target: self, action: #selector(self.swipeRecognizer))
        swipe.direction = ([.down])
        self.view.addGestureRecognizer(swipe)
        
        
        //        if reachability.connection == .none{
        //            self.view.makeToast("Internet is not available, please check your internet connection try again." )
        //        }
        
    }
    var reachability = Reachability()!
    @objc func swipeRecognizer(_ sender: UISwipeGestureRecognizer) {
        if (sender.direction == .down) {
            
            if(self.reachability.connection != .none)
            {
                
                self.callCategoryData()
            }
            else
            {
                self.noDataLabel(text: "No internet connection found. Check your internet connection and try again.")
            }
            
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.tabBarController?.tabBar.isHidden = true
        self.title = "Categories"
        let nav = self.navigationController?.navigationBar
        nav?.tintColor = UIColor.white
       nav?.barTintColor = UIColor(hexString: "2c3e50", alpha: 1.0)
        nav?.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.CategoryDB.count
        //        if let count = fetchResultController.sections?[section].numberOfObjects {
        //            return count
        //        }
        //        return 0
        
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! HzlCategoryTableViewCell
        
        
        // let ELibrary = fetchResultController.object(at: indexPath) as! ELibraryPolicy
        
        cell.containerView.layer.cornerRadius = 8
        cell.policyLabel.text = self.CategoryDB[indexPath.row].Category_Name
        
        
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
         let storyBoard = UIStoryboard(name: "HZLONE", bundle: nil)
       let ELPVC = storyBoard.instantiateViewController(withIdentifier: "HZlPolicyViewController") as! HZlPolicyViewController

      HZlPolicyTempData.Cat_ID = String(self.CategoryDB[indexPath.row].ID)
        HZlPolicyTempData.policy = self.CategoryDB[indexPath.row].Category_Name
     self.navigationController?.pushViewController(ELPVC, animated: true)
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    @objc func callCategoryData(){
        
       
            var param = [String:String]()
            
            param =  [:]
            CategoryAPI.serviceCalling(obj: self,  param: param ) { (dict) in
                
                self.CategoryDB = dict as! [HzlCategoryDataModel]
                
                self.tableView.reloadData()
                
            }
        }
    
   
    
    
    
}
