//
//  HzlCategoryTableViewCell.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 28/02/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit

class HzlCategoryTableViewCell:  UITableViewCell {
    
    @IBOutlet weak var policyLabel: UILabel!
    
    @IBOutlet weak var containerView: UIViewX!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
