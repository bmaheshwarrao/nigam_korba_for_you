//
//  ModelInbox.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 09/02/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import Foundation
import Reachability

var catArray :[CategoryInboxDataModel] = []
class MyInboxDataModel: NSObject {
    
    
  
    var Action_Text = String()
    var Count_Record = String()
    var Record_Date_Time = String()
    var Action_Data = String()
   
    
    
    
    
   
    
    
    func setDataInModel(str:[String:AnyObject])
    {
        if str["Action_Text"] is NSNull || str["Action_Text"] == nil{
            self.Action_Text = ""
        }else{
            
            let ros = str["Action_Text"]
            
            self.Action_Text = (ros?.description)!
            
            
        }
        if str["Count_Record"] is NSNull || str["Count_Record"] == nil{
            self.Count_Record =  ""
        }else{
            
            let fross = str["Count_Record"]
            
            self.Count_Record = (fross?.description)!
            
            
            //  self.Description = (str["Description"] as? String)!
        }
        
        if str["Record_Date_Time"] is NSNull || str["Record_Date_Time"] == nil{
            self.Record_Date_Time = "0"
        }else{
            let emp1 = str["Record_Date_Time"]
            
            self.Record_Date_Time = (emp1?.description)!
            // print(self.Employee_1)
        }
        if str["Action_Data"] is NSNull || str["Action_Data"] == nil{
            self.Action_Data = ""
        }else{
            
            let empname = str["Action_Data"]
            
            self.Action_Data = (empname?.description)!
            
        }
        
        
    }
    
}

public struct CategoryInboxDataModel {
    
    
    
    var Category : String
    var AppData : String
    var InboxData : [MyInboxDataModel]
    
   
}

class MyInboxDataAPI
{
    
    var reachablty = Reachability()!
    
    func serviceCalling(obj:InboxViewController, param : [String: String] , success:@escaping (AnyObject)-> Void)
    {
        
        
        if(reachablty.connection != .none)
        {
            
            
            obj.startLoading()
            WebServices.sharedInstances.sendGetRequest(Url: URLConstants.HZL_ONE_MY_Inbox_Action, successHandler: { (dict:[String:AnyObject]) in
                if let response = dict["response"]{
                    
                    let statusString : String = response["status"] as! String
                    let objectmsg = MessageCallServerModel()
                    
                    let msg = objectmsg.sealizeMessage(cell: response as! [String : AnyObject])
                    if(statusString == "success")
                    {
                        obj.tableView.isHidden = false
                        obj.noDataLabel(text: "" )
                        obj.label.isHidden = true
                        if let data = dict["data"] as? [AnyObject]
                        {
                            
                            var dataArray: [MyInboxDataModel] = []
                            var dataArrayCat : [CategoryInboxDataModel] = []
                            var cat1 = String()
                            var cat2 = String()
                            var category1 = String()
                            var category2 = String()
                            for i in 0..<data.count
                            {
                                
                                
                                
                                
                                if let cell = data[i] as? [String:AnyObject]
                                {
                                    if(i != 0) {
                                        cat1 = (data[i]["Action_Data"] as? String)!
                                        cat2 = (data[i-1]["Action_Data"] as? String)!
                                        category1 = (data[i]["Display_Name"] as? String)!
                                        category2 = (data[i-1]["Display_Name"] as? String)!
                                    } else {
                                        cat1 = (data[i]["Action_Data"] as? String)!
                                        category1 = (data[i]["Display_Name"] as? String)!
                                    }
                                    
                                    
                                    print(cat1)
                                    print(cat2)
                                    
                                    
                                    if(i != data.count-1) {
                                        if(cat1 != cat2) {
                                            if(cat2 != "") {
                                                let objectData1 = CategoryInboxDataModel(Category: category2,AppData : cat2, InboxData: dataArray)
                                                dataArrayCat.append(objectData1)
                                                dataArray = []
                                                let object = MyInboxDataModel()
                                                object.setDataInModel(str: cell)
                                                dataArray.append(object)
                                            }
                                            else {
                                                let object = MyInboxDataModel()
                                                object.setDataInModel(str: cell)
                                                dataArray.append(object)
                                            }
                                        } else {
                                            let object = MyInboxDataModel()
                                            object.setDataInModel(str: cell)
                                            dataArray.append(object)
                                        }
                                    } else {
                                        if(cat1 != cat2) {
                                            dataArray = []
                                        }
                                        let object = MyInboxDataModel()
                                        object.setDataInModel(str: cell)
                                        dataArray.append(object)
                                        let objectData1 = CategoryInboxDataModel(Category: category1,AppData : cat1, InboxData: dataArray)
                                        dataArrayCat.append(objectData1)
                                    }
                                }
                                
                            }
                        print(dataArrayCat[0].Category)
                            success(dataArrayCat as AnyObject)
                            obj.refreshControl.endRefreshing()
                            obj.stopLoading()
                            
                        }else
                        {
                            obj.label.isHidden = false
                            obj.tableView.isHidden = true
                            obj.noDataLabel(text: msg )
                            obj.refreshControl.endRefreshing()
                            obj.stopLoading()
                            
                        }
                        
                        
                    }
                    
                    
                }
                
                
                
                
            })
            { (error) in
                
                let errorStr : String = error.description
                
                print("DATA:error",errorStr)
                
                //                obj.errorChecking(error: error)
                obj.stopLoading()
            }
        }else{
            obj.label.isHidden = false
            obj.tableView.isHidden = true
            obj.noDataLabel(text: "Internet is not available, please check your internet connection try again." )
            obj.stopLoading()
        }
    }
}

//class MyInboxDataAPI
//{
//
//    var reachablty = Reachability()!
//
//    func serviceCalling(obj:InboxViewController, param : [String: String] , success:@escaping (AnyObject)-> Void)
//    {
//
//
//        if(reachablty.connection != .none)
//        {
//
//
//
//                WebServices.sharedInstances.sendGetRequest(Url: URLConstants.HZL_ONE_MY_Inbox_Action_TBS, successHandler: { (dict:[String:AnyObject]) in
//                if let response = dict["response"]{
//
//                    let statusString : String = response["status"] as! String
//
//                    if(statusString == "success")
//                    {
//                        obj.tableView.isHidden = false
//                        obj.noDataLabel(text: "" )
//                        obj.label.isHidden = true
//                        if let data = dict["data"] as? [AnyObject]
//                        {
//
//                            var dataArray: [MyInboxDataModel] = []
//
//                            for i in 0..<data.count
//                            {
//
//                                if let cell = data[i] as? [String:AnyObject]
//                                {
//                                    let object = MyInboxDataModel()
//                                    object.setDataInModel(str: cell)
//                                    dataArray.append(object)
//                                }
//
//                            }
//                            let obCat = CategoryInboxDataModel()
//                            obCat.Category = dataArray[0].Action_Data
//                            obCat.InboxData = dataArray
//                            catArray.append(obCat)
//                            success(dataArray as AnyObject)
//
//
//                        }
//
//
//                    }
//
//
//                }
//
//
//
//
//            })
//            { (error) in
//
//                let errorStr : String = error.description
//
//
//            }
//
//
//                   WebServices.sharedInstances.sendGetRequest(Url: URLConstants.HZL_ONE_MY_Inbox_Action_EQ, successHandler: { (dict:[String:AnyObject]) in
//                if let response = dict["response"]{
//
//                    let statusString : String = response["status"] as! String
//
//                    if(statusString == "success")
//                    {
//                        obj.tableView.isHidden = false
//                        obj.noDataLabel(text: "" )
//                        obj.label.isHidden = true
//                        if let data = dict["data"] as? [AnyObject]
//                        {
//
//                            var dataArray: [MyInboxDataModel] = []
//
//                            for i in 0..<data.count
//                            {
//
//                                if let cell = data[i] as? [String:AnyObject]
//                                {
//                                    let object = MyInboxDataModel()
//                                    object.setDataInModel(str: cell)
//                                    dataArray.append(object)
//                                }
//
//                            }
//                            let obCat = CategoryInboxDataModel()
//                            obCat.Category = dataArray[0].Action_Data
//                            obCat.InboxData = dataArray
//                            catArray.append(obCat)
//                            success(dataArray as AnyObject)
//
//
//                        }
//
//
//                    }
//
//
//                }
//
//
//
//
//            })
//            { (error) in
//
//                let errorStr : String = error.description
//
//
//            }
//
//
//                 WebServices.sharedInstances.sendGetRequest(Url: URLConstants.HZL_ONE_MY_Inbox_Action_NDSO, successHandler: { (dict:[String:AnyObject]) in
//                if let response = dict["response"]{
//
//                    let statusString : String = response["status"] as! String
//
//                    if(statusString == "success")
//                    {
//                        obj.tableView.isHidden = false
//                        obj.noDataLabel(text: "" )
//                        obj.label.isHidden = true
//                        if let data = dict["data"] as? [AnyObject]
//                        {
//
//                            var dataArray: [MyInboxDataModel] = []
//
//                            for i in 0..<data.count
//                            {
//
//                                if let cell = data[i] as? [String:AnyObject]
//                                {
//                                    let object = MyInboxDataModel()
//                                    object.setDataInModel(str: cell)
//                                    dataArray.append(object)
//                                }
//
//                            }
//                            let obCat = CategoryInboxDataModel()
//                            obCat.Category = dataArray[0].Action_Data
//                            obCat.InboxData = dataArray
//                            catArray.append(obCat)
//                            success(dataArray as AnyObject)
//
//
//                        }
//
//
//                    }
//
//
//                }
//
//
//
//
//            })
//            { (error) in
//
//                let errorStr : String = error.description
//
//
//            }
//
//
//                    WebServices.sharedInstances.sendGetRequest(Url: URLConstants.HZL_ONE_MY_Inbox_Action_ODSO, successHandler: { (dict:[String:AnyObject]) in
//                if let response = dict["response"]{
//
//                    let statusString : String = response["status"] as! String
//
//                    if(statusString == "success")
//                    {
//                        obj.tableView.isHidden = false
//                        obj.noDataLabel(text: "" )
//                        obj.label.isHidden = true
//                        if let data = dict["data"] as? [AnyObject]
//                        {
//
//                            var dataArray: [MyInboxDataModel] = []
//
//                            for i in 0..<data.count
//                            {
//
//                                if let cell = data[i] as? [String:AnyObject]
//                                {
//                                    let object = MyInboxDataModel()
//                                    object.setDataInModel(str: cell)
//                                    dataArray.append(object)
//                                }
//
//                            }
//                            let obCat = CategoryInboxDataModel()
//                            obCat.Category = dataArray[0].Action_Data
//                            obCat.InboxData = dataArray
//                            catArray.append(obCat)
//                            success(dataArray as AnyObject)
//
//
//                        }
//
//
//                    }
//
//
//                }
//
//
//
//
//            })
//            { (error) in
//
//                let errorStr : String = error.description
//
//
//            }
//
//                WebServices.sharedInstances.sendGetRequest(Url: URLConstants.HZL_ONE_MY_Inbox_Action_IDEA, successHandler: { (dict:[String:AnyObject]) in
//                if let response = dict["response"]{
//
//                    let statusString : String = response["status"] as! String
//
//                    if(statusString == "success")
//                    {
//                        obj.tableView.isHidden = false
//                        obj.noDataLabel(text: "" )
//                        obj.label.isHidden = true
//                        if let data = dict["data"] as? [AnyObject]
//                        {
//
//                            var dataArray: [MyInboxDataModel] = []
//
//                            for i in 0..<data.count
//                            {
//
//                                if let cell = data[i] as? [String:AnyObject]
//                                {
//                                    let object = MyInboxDataModel()
//                                    object.setDataInModel(str: cell)
//                                    dataArray.append(object)
//                                }
//
//                            }
//                            let obCat = CategoryInboxDataModel()
//                            obCat.Category = dataArray[0].Action_Data
//                            obCat.InboxData = dataArray
//                            catArray.append(obCat)
//                            success(dataArray as AnyObject)
//
//
//                        }
//
//
//                    }
//
//
//                }
//
//
//
//
//            })
//            { (error) in
//
//                let errorStr : String = error.description
//
//
//            }
//
//
//        }
//        else{
//
//        }
//
//
//    }
//
//
//}
