//
//  PendingDoneTaskMOMViewController.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 08/04/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit

class PendingDoneTaskMOMViewController: CommonVSClass ,UITableViewDelegate, UITableViewDataSource {
    var typeData = String()
    @IBOutlet weak var tableView: UITableView!
    var refreshControl = UIRefreshControl()
    
    var ManageMeetingDB:[ManageMeetingModel] = []
    var ManageMeetingAPI = MyTaskMOMModelDataAPI()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getMeetingData()
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.estimatedRowHeight = 65
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.tableFooterView = UIView()
        
        refreshControl.addTarget(self, action: #selector(getMeetingData), for: .valueChanged)
        
        self.tableView.addSubview(refreshControl)
        let swipe = UISwipeGestureRecognizer(target: self, action: #selector(self.swipeRecognizer))
        swipe.direction = ([.down])
        self.view.addGestureRecognizer(swipe)
        
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.tabBarController?.tabBar.isHidden = true
        self.title = "My Task"
        let nav = self.navigationController?.navigationBar
        nav?.tintColor = UIColor.white
        nav?.barTintColor = UIColor(hexString: "2c3e50", alpha: 1.0)
        nav?.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        
    }
    var reachability = Reachability()!
    @objc func swipeRecognizer(_ sender: UISwipeGestureRecognizer) {
        if (sender.direction == .down) {
            
            if(self.reachability.connection != .none)
            {
                
                self.getMeetingData()
            }
            else
            {
                self.startLoading(view: self.view)
                DispatchQueue.main.asyncAfter(deadline: .now() + 4.5) {
                    self.noDataLabel(text: "No internet connection found. Check your internet connection and try again.")
                    self.stopLoading(view: self.view)
                }
            }
            
        }
    }
    
    var ReqId = String()
    //  let ActionTakenAlertVC = RACPopupViewController.instantiate()
    @objc func tapViewTapClicked(_ sender: UITapGestureRecognizer) {
                let buttonPosition:CGPoint = sender.view!.convert(.zero, to:self.tableView)
                let indexPath = self.tableView.indexPathForRow(at: buttonPosition)
       
        let storyboard1 = UIStoryboard(name: "MOM", bundle: nil)
        let auditVC = storyboard1.instantiateViewController(withIdentifier: "PendingDoneTaskDetailsViewController") as! PendingDoneTaskDetailsViewController
        auditVC.status = flagMeeting
        auditVC.task_Id = self.ManageMeetingDB[(indexPath?.row)!].Task_ID
        auditVC.PendingDoneDB = self.ManageMeetingDB[(indexPath?.row)!]
        self.navigationController?.pushViewController(auditVC, animated: true)
        

        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    var flagMeeting = String()
    @objc func getMeetingData(){
        var param = [String:String]()
        let pno : String = (UserDefaults.standard.string(forKey: "EmployeeID")!)
        
        param = ["Employee_ID": pno ,"Status": flagMeeting  ]
        print(param)
        
        
        ManageMeetingAPI.serviceCalling(obj: self,  parameter: param ) { (dict) in
            
            self.ManageMeetingDB = dict as! [ManageMeetingModel]
            
            self.tableView.reloadData()
            
        }
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    var data: String?
    var lastObject: String?
    var indPath = IndexPath()
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        
        
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if(section == 0) {
            return 10.0
        } else {
            return 0.0
        }
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.ManageMeetingDB.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! MyTaskMOMTableViewCell
        
        let TapGesture = UITapGestureRecognizer(target: self, action: #selector(self.tapViewTapClicked(_:)))
        cell.addGestureRecognizer(TapGesture)
        cell.isUserInteractionEnabled = true
        TapGesture.numberOfTapsRequired = 1
//        cell.lblOwner.text = "Owner : " + self.ManageMeetingDB[indexPath.row].Meeting_Owner_Name + " - " + self.ManageMeetingDB[indexPath.row].Meeting_Owner
 
         cell.lblOwner.text = "Owner : " + self.ManageMeetingDB[indexPath.row].Meeting_Owner
        cell.textViewTask.text = "Task : " + self.ManageMeetingDB[indexPath.row].Task
        cell.textViewDesc.text = self.ManageMeetingDB[indexPath.row].Meeting_Title
        cell.containerView.layer.cornerRadius = 10
        cell.lblDate.text = "Date : " + self.ManageMeetingDB[indexPath.row].Meeting_Date
      
        
        
        
        
        
        
        
        
        
        cell.separatorInset = UIEdgeInsetsMake(0, 10000, 0, 0);
        cell.selectionStyle = .none
        return cell
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableViewAutomaticDimension
        
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}
