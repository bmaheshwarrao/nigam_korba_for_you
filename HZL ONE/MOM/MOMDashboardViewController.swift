//
//  MOMDashboardViewController.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 08/04/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit
import Charts


import MobileCoreServices
import Alamofire
import CoreData
import CRToast
import IQKeyboardManagerSwift
import AVFoundation


class MOMDashboardViewController:  CommonVSClass,UINavigationBarDelegate,UINavigationControllerDelegate,UITableViewDelegate, UITableViewDataSource,ChartViewDelegate{
    
    
    
    @IBOutlet weak var homeTableView: UITableView!
    
    
    var applicationName = String()
    
    
    var statusStrNav = String()
    
    
    var indPath = IndexPath()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.frame.size.height = self.homeTableView.frame.size.height
        
        self.homeTableView.delegate = self
        self.homeTableView.dataSource = self
        
        self.homeTableView.sectionIndexBackgroundColor = UIColor.clear
        self.homeTableView.sectionIndexColor = UIColor.lightGray
        
        self.homeTableView.sectionIndexColor = UIColor.blue
        
        
        
        
        //        self.homeTableView.addGestureRecognizer(leftToRight)
        //        self.homeTableView.addGestureRecognizer(rightToLeft)
        //        self.view.addGestureRecognizer(leftToRight)
        //        self.view.addGestureRecognizer(rightToLeft)
        
        //        NotificationCenter.default.addObserver(self, selector: #selector(graphDataInsert), name: NSNotification.Name?.init(NSNotification.Name(rawValue: "graphDataUpdate")), object: nil)
        //        NotificationCenter.default.addObserver(self, selector: #selector(GetBusinessData), name: NSNotification.Name?.init(NSNotification.Name(rawValue: "mainLocationDataUpdate")), object: nil)
        //
        //
        //        graphDataInsert()
        
        
        
        
        
        getBannerImageData()
        if reachability.connection == .none{
            self.view.makeToast("Internet is not available, please check your internet connection try again." )
        }
        
        
    }
    
    
    var reachability = Reachability()!
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        // NotificationCenter.default.post(name: NSNotification.Name.init("sendReportFromHome"), object: nil)
        self.title = statusStrNav
        self.tabBarController?.tabBar.isHidden = true
        let nav = self.navigationController?.navigationBar
        nav?.tintColor = UIColor.white
        nav?.barTintColor = UIColor(hexString: "2c3e50", alpha: 1.0)
        nav?.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        
        
        
    }
    @objc func getBannerImageData() {
        
        var imageDB = [BannerImage]()
        do {
            
            imageDB = try context.fetch(BannerImage.fetchRequest())
            //self.refresh.endRefreshing()
            if(imageDB.count > 0) {
                //BannerStruct.bannerPath = imageDB[0].text_Message!
            }
            self.homeTableView.reloadData()
            
        } catch {
            print("Fetching Failed")
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 5
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == 0{
            return 1
        }else{
            return 1
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 {
            let page = "page"
            
            
            let cell = tableView.dequeueReusableCell(withIdentifier: page) as! PageIdeaTableViewCell
            
            cell.contentView.frame.size.height = cell.imageCollectionView.frame.size.height
            cell.contentView.frame.size.width = cell.imageCollectionView.frame.size.width
            
            
            cell.pageView(ApplicationName: applicationName)
            
            return cell
            
        }else if indexPath.section == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell1") as! UITableViewCell
            
            
            return cell
        }else if indexPath.section == 2 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell2") as! UITableViewCell
            
            
            return cell
        }else if indexPath.section == 3 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell3") as! UITableViewCell
            
            
            return cell
        }else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell4") as! UITableViewCell
            
            
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        
        
        let screenSize = UIScreen.main.bounds
        //let screenHeight = screenSize.height-navheight
        
        let screenWidth = screenSize.width
        if indexPath.section == 0{
            return (screenWidth/2) + 25
        }else{
            return 123
        }
        
    }
    
    
    @IBAction func CreateMeetingClicked(_ sender: UIButton) {
        
        FilterGraphStruct.fromDate = String()
        FilterGraphStruct.toDate = String()
        let storyboard1 = UIStoryboard(name: "MOM", bundle: nil)
        let auditVC = storyboard1.instantiateViewController(withIdentifier: "CreateMeetingViewController") as! CreateMeetingViewController
        
        self.navigationController?.pushViewController(auditVC, animated: true)
    }
    @IBAction func MyMeetingClicked(_ sender: UIButton) {
        
        
        FilterGraphStruct.fromDate = String()
        FilterGraphStruct.toDate = String()
        let storyboard1 = UIStoryboard(name: "MOM", bundle: nil)
        let auditVC = storyboard1.instantiateViewController(withIdentifier: "UpMissedCompSlideViewController") as! UpMissedCompSlideViewController
        
        self.navigationController?.pushViewController(auditVC, animated: true)
        
    }
    @IBAction func MyTaskClicked(_ sender: UIButton) {
        FilterGraphStruct.fromDate = String()
        FilterGraphStruct.toDate = String()
        let storyboard1 = UIStoryboard(name: "MOM", bundle: nil)
        let auditVC = storyboard1.instantiateViewController(withIdentifier: "MyTaskMOMViewController") as! MyTaskMOMViewController
        
        self.navigationController?.pushViewController(auditVC, animated: true)
    }
    @IBAction func ManageMeetingClicked(_ sender: UIButton) {
        FilterGraphStruct.fromDate = String()
        FilterGraphStruct.toDate = String()
        let storyboard1 = UIStoryboard(name: "MOM", bundle: nil)
        let auditVC = storyboard1.instantiateViewController(withIdentifier: "ManageMeetingViewController") as! ManageMeetingViewController
        
        self.navigationController?.pushViewController(auditVC, animated: true)
    }
    
    
    
    
    var reachablty = Reachability()!
    
    
    func getContext () -> NSManagedObjectContext {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        return appDelegate.persistentContainer.viewContext
    }
    
    
    
    
    
    
}
