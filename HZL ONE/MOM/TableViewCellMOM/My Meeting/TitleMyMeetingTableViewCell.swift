//
//  TitleMyMeetingTableViewCell.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 09/04/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit

class TitleMyMeetingTableViewCell: UITableViewCell {

    @IBOutlet weak var textViewAgenda: UITextView!
    @IBOutlet weak var lblTitle: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
