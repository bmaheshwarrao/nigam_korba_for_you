//
//  ExternalPartcipantsTableViewCell.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 10/04/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit

class ExternalPartcipantsTableViewCell: UITableViewCell {
    @IBOutlet weak var btnSelect: UIButton!
    @IBOutlet weak var lblEmpName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        btnSelect.layer.borderWidth = 1.0
        btnSelect.layer.borderColor = UIColor.black.cgColor
        btnSelect.layer.cornerRadius = 15.0
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
