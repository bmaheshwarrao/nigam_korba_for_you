//
//  CreateMeetingTableViewCell.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 08/04/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit

class CreateMeetingTableViewCell: UITableViewCell {

    @IBOutlet weak var txtTitle: UITextField!
    @IBOutlet weak var viewTitle: UIView!
    @IBOutlet weak var viewAgenda: UIView!
    
    @IBOutlet weak var viewDuration: UIView!
    @IBOutlet weak var txtDuration: UITextField!
    @IBOutlet weak var viewTime: UIView!
    @IBOutlet weak var txtTime: UITextField!
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var viewDate: UIView!
    @IBOutlet weak var viewVenue: UIView!
    @IBOutlet weak var textViewVenue: UITextView!
    @IBOutlet weak var textViewAgenda: UITextView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
      //  viewDuration.
        
        viewTitle.layer.borderWidth = 1.0
        viewTitle.layer.borderColor = UIColor.black.cgColor
        viewTitle.layer.cornerRadius = 10.0
        
        viewAgenda.layer.borderWidth = 1.0
        viewAgenda.layer.borderColor = UIColor.black.cgColor
        viewAgenda.layer.cornerRadius = 10.0
        
        viewDuration.layer.borderWidth = 1.0
        viewDuration.layer.borderColor = UIColor.black.cgColor
        viewDuration.layer.cornerRadius = 10.0
        
        viewTime.layer.borderWidth = 1.0
        viewTime.layer.borderColor = UIColor.black.cgColor
        viewTime.layer.cornerRadius = 10.0
        
        viewDate.layer.borderWidth = 1.0
        viewDate.layer.borderColor = UIColor.black.cgColor
        viewDate.layer.cornerRadius = 10.0
        
        viewVenue.layer.borderWidth = 1.0
        viewVenue.layer.borderColor = UIColor.black.cgColor
        viewVenue.layer.cornerRadius = 10.0
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
