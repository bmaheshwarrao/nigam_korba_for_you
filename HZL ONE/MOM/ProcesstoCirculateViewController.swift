//
//  ProcesstoCirculateViewController.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 10/04/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit

class ProcesstoCirculateViewController: CommonVSClass ,UITableViewDelegate,UITableViewDataSource{
    @IBOutlet weak var tableView: UITableView!
    var refreshControl = UIRefreshControl()
    
    var meetingDetailsDB = ViewMeetingManagerModel()
    var meetingCellDB = ManageMeetingModel()
    var meeting_Id = String()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
      
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.estimatedRowHeight = 65
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.tableFooterView = UIView()
        if( self.meetingDetailsDB.viewInternal.count > 0){
           InternalArray = []
            for i in 0...self.meetingDetailsDB.viewInternal.count - 1 {
                InternalArray.append(false)
            }
        }
        if( self.meetingDetailsDB.viewExternal.count > 0){
            ExternalArray = []
            for i in 0...self.meetingDetailsDB.viewExternal.count - 1 {
               ExternalArray.append(false)
            }
        }
     

        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.tabBarController?.tabBar.isHidden = true
        self.title = "Process Meeting"
        let nav = self.navigationController?.navigationBar
        nav?.tintColor = UIColor.white
        nav?.barTintColor = UIColor(hexString: "2c3e50", alpha: 1.0)
        nav?.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        
    }
    var InternalArray : [Bool] = []
    var ExternalArray : [Bool] = []
    
    var InternalListArray : [ViewInternalModel] = []
    var ExternalListArray : [ViewExternalModel] = []
    @IBAction func btnSelectClicked(_ sender: UIButton) {
        if(InternalArray[sender.tag] == false){
            InternalArray[sender.tag] = true
        
        }else{
            InternalArray[sender.tag] = false
          
        }
        if( self.InternalArray.count > 0){
            InternalListArray = []
            for i in 0...self.InternalArray.count - 1 {
                if(self.InternalArray[i] == true){
                    InternalListArray.append(self.meetingDetailsDB.viewInternal[i])
                }
            }
        }
        
        
        tableView.reloadData()
    }
    @IBAction func btnSelectExternalClicked(_ sender: UIButton) {
        
        if(ExternalArray[sender.tag] == false){
            ExternalArray[sender.tag] = true
            
        }else{
            ExternalArray[sender.tag] = false
        }
        
        if( self.ExternalArray.count > 0){
            ExternalListArray = []
            for i in 0...self.ExternalArray.count - 1 {
                if(self.ExternalArray[i] == true){
                    ExternalListArray.append(self.meetingDetailsDB.viewExternal[i])
                }
            }
        }
        tableView.reloadData()
        
    }
     @IBAction func btnSubmitClicked(_ sender: UIButton) {
        
        if(cellData.textViewDiscussion.text == ""){
            self.view.makeToast("Enter Discussion")
        }else{
          submitData()
        }
        
    }
    
    func submitData(){
        let pno : String = (UserDefaults.standard.string(forKey: "EmployeeID")!)
        var paramInternal  : [[String:String]] = []
        var iiVal = 0
        if(InternalListArray.count > 0) {
        for value in InternalListArray {
            
            
            
            
            
            
            let paramInternalValue = [  "Meeting_ID": value.Meeting_ID,
                                        "Employee_ID": value.Employee_ID,
                                        "Updated_BY": pno
                
                
            ]
            
            iiVal = iiVal + 1;
            
            paramInternal.append(paramInternalValue)
            
            
            
            
        }
        }
        
        var paramExternal  : [[String:String]] = []
        if(ExternalListArray.count > 0) {
        for valueData in ExternalListArray {
            
            
            
            
            
            
            let paramExternalValue = [ "Meeting_ID": valueData.Meeting_ID,
                                       "ID": String(valueData.ID),
                                       "Updated_BY": pno
                
                
            ]
            
            
            
            paramExternal.append(paramExternalValue)
            
            }
            
            
        }
        
        self.startLoadingPK(view: self.view)
        
        
        
        
        
        
        
        
        
        
        let paramMeeting = [  "Meeting_ID": meetingCellDB.ID,
                              "Meeting_Discussion": cellData.textViewDiscussion.text!,
                              "Updated_BY": pno
            
            ] as [String:Any]
        
        let parameterAll = [ "Meeting_Data": paramMeeting,
                          "External_Member":paramExternal,
                          "Internal_Member": paramInternal
            
            
            ] as [String:Any]
        
        let parameter = [ "data": parameterAll

            
            ] as [String:Any]
        
        print("parameter",parameter)
        
        WebServices.sharedInstances.sendPostRequest(url: URLConstants.Process_Meeting_MOM, parameters: parameter, successHandler: { (response:[String : AnyObject]) in
            
            
            self.stopLoadingPK(view: self.view)
            
            let respon = response["response"] as! [String:AnyObject]
            self.stopLoadingPK(view: self.view)
            let objectmsg = MessageCallServerModel()
            let msg = objectmsg.sealizeMessage(cell: respon as! [String : AnyObject])
            if respon["status"] as! String == "success" {
               
                self.stopLoadingPK(view: self.view)
                
                
                    MoveStruct.message = msg
                    MoveStruct.isMove = true
                    self.navigationController?.popViewController(animated: false)
                    
           
                
                
                
            }else{
                self.stopLoadingPK(view: self.view)
                self.view.makeToast(msg)
            }
            
            
        }) { (err) in
            self.stopLoadingPK(view: self.view)
            print(err.description)
        }
        
        
    }
    var cellData : DiscusionProcessTableViewCell!
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        
        
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let header = tableView.dequeueReusableCell(withIdentifier: "header") as! HeaderMyMeetingTableViewCell
        if(section == 2) {
            header.lblHeader.text = "INTERNAL PARTICIPANTS"
        }else if(section == 3) {
            header.lblHeader.text = "EXTERNAL PARTICIPANTS"
        }else{
            header.lblHeader.text = ""
        }
        
        return header
        
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if(section == 0 || section == 1) {
            return 0.0
        } else {
            return 50.0
        }
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 4
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(section == 0 || section == 1){
        return 1
        }else if (section == 2){
            if(self.meetingDetailsDB.viewInternal.count > 0)  {
            return self.meetingDetailsDB.viewInternal.count
            }else{
                return 1
            }
        }else{
            if(self.meetingDetailsDB.viewExternal.count > 0)  {

            return self.meetingDetailsDB.viewExternal.count
            }else{
                return 1
            }
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if(indexPath.section == 0){
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! ManageMeetingMOMTableViewCell
            
            
            cell.lblOwner.text = "Owner : " + self.meetingCellDB.Meeting_Owner_Name + " - " + self.meetingCellDB.Meeting_Owner
            cell.lblVenue.text = "Venue : " + self.meetingCellDB.Meeting_Venue
            cell.lblDesc.text = self.meetingCellDB.Meeting_Title
            cell.containerView.layer.cornerRadius = 10
            
            let milisecondDate = self.meetingCellDB.Meeting_DateTime
            let dateVarDate = Date.init(timeIntervalSince1970: TimeInterval(milisecondDate)!/1000)
            let dateFormatterday = DateFormatter()
            dateFormatterday.dateFormat = "dd"
            let valDate =  dateFormatterday.string(from: dateVarDate)
            cell.lblDate.text = valDate
            let dateFormattermon = DateFormatter()
            dateFormattermon.dateFormat = "MMMM yy"
            let valDateMon =  dateFormattermon.string(from: dateVarDate)
            cell.lblMonYear.text = valDateMon
            
            let dateFormatterWeekDay = DateFormatter()
            dateFormatterWeekDay.dateFormat = "EEEE"
            let valDateWeekDay =  dateFormatterWeekDay.string(from: dateVarDate)
            cell.lblDay.text = valDateWeekDay
            
            cell.lblTime.text = "Time : " + self.meetingCellDB.Meeting_Time
            
            
            
            
            
            
            
            
            
            
            cell.separatorInset = UIEdgeInsetsMake(0, 10000, 0, 0);
            cell.selectionStyle = .none
            return cell
        } else if(indexPath.section == 1) {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellDiscuss", for: indexPath) as! DiscusionProcessTableViewCell
            
            
          cellData = cell
            cell.separatorInset = UIEdgeInsetsMake(0, 10000, 0, 0);
            cell.selectionStyle = .none
            return cell
        } else if(indexPath.section == 2) {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellInternal", for: indexPath) as! InternalExternalParticipantsTableViewCell
            if(self.meetingDetailsDB.viewInternal.count > 0) {
                
                cell.lblEmpName.text =  self.meetingDetailsDB.viewInternal[indexPath.row].Employee_Name + " - " + self.meetingDetailsDB.viewInternal[indexPath.row].Employee_ID
                cell.btnSelect.isHidden = false
                if(InternalArray[indexPath.row] == false){
                    cell.btnSelect.setImage(nil, for: .normal)
                    
                    
                    
                }else{
                    cell.btnSelect.setImage(UIImage(named: "tickSelect"), for: .normal)
                }
            }else{
                cell.lblEmpName.text = "No Internal member added"
                cell.lblEmpName.textAlignment = NSTextAlignment.center
                cell.btnSelect.isHidden = true
            }
           cell.btnSelect.tag = indexPath.row
            cell.separatorInset = UIEdgeInsetsMake(0, 10000, 0, 0);
            cell.selectionStyle = .none
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellExternal", for: indexPath) as! ExternalPartcipantsTableViewCell
            if(self.meetingDetailsDB.viewExternal.count > 0) {
                
                cell.lblEmpName.text =  self.meetingDetailsDB.viewExternal[indexPath.row].Email_ID
                cell.lblEmpName.textAlignment = NSTextAlignment.left
                cell.btnSelect.isHidden = false
                if(ExternalArray[indexPath.row] == false){
                    cell.btnSelect.setImage(nil, for: .normal)
                    
              
                    
                }else{
                     cell.btnSelect.setImage(UIImage(named: "tickSelect"), for: .normal)
                }
            }else{
                cell.lblEmpName.text = "No External member added"
                cell.lblEmpName.textAlignment = NSTextAlignment.center
                cell.btnSelect.isHidden = true
            }
           cell.btnSelect.tag = indexPath.row
            cell.separatorInset = UIEdgeInsetsMake(0, 10000, 0, 0);
            cell.selectionStyle = .none
            return cell
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableViewAutomaticDimension
        
    }
}
