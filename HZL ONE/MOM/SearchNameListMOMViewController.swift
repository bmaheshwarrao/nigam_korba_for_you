//
//  SearchNameListMOMViewController.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 13/04/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit

class SearchNameListMOMViewController:  CommonVSClass , UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate,UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    @IBOutlet weak var employeeSearchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    
    
    @IBOutlet weak var collectionView: UICollectionView!
    
    let reachablty = Reachability()!
    //    var indPath = IndexPath()
    //    var spec = Bool()
    var searchBarActiveOrNot : Bool = false
    var Idea_ID = String()
    
    
    
    var searchNamesModel: [EmpListMOMModel] = []
    var searchNamesAPI = EmpListModelDataAPI()
    
    //Save locally
    
    @IBOutlet weak var HeightCollectionCon: NSLayoutConstraint!
    @IBAction func DoneAction(_ sender: UIBarButtonItem) {
        
        if(dataArray.count>0){
            for object in EmpSearchDB {
                EmpDB.append(object)
            }
            self.navigationController?.popViewController(animated: true)
        }else {
            self.navigationController?.popViewController(animated: true)
        }
        
    }
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.title = "Search Employee"
        searchNameFunction()
        self.collectionView.delegate = self
        self.collectionView.dataSource = self
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.employeeSearchBar.delegate = self
        self.employeeSearchBar.returnKeyType = UIReturnKeyType.done
        // self.employeeSearchBar.becomeFirstResponder()
        
        tableView.estimatedRowHeight = 124
        tableView.rowHeight = UITableViewAutomaticDimension
        
        tableView.tableFooterView = UIView()
        // Do any additional setup after loading the view.
    }
    func saveNameData (name:String,partenerID:String){
        
        //        let entries : NSArray = self.AreaManagerDB as NSArray
        //        let predicate = NSPredicate(format: "empId = %@ ", empId)
        //        let FavouriteData = entries.filtered(using: predicate) as! [SearchName]
        //        switch FavouriteData.count {
        //        case 0:
        
        let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        let tasks = SearchName(context: context)
        
        
        
        tasks.name = name
        tasks.id = partenerID
        
        (UIApplication.shared.delegate as! AppDelegate).saveContext()
        searchNameFunction()
        //   CheckingAreaManagerListData()
        
        //            break;
        //        default:
        //            break;
        //        }
        
    }
    func searchNameFunction(){
        
        if(EmpSearchDB.count == 0){
            HeightCollectionCon.constant = 0
        }else{
            HeightCollectionCon.constant = 120
        }
        self.collectionView.reloadData()
        
    }
    
    @IBAction func btnCloseClicked(_ sender: UIButton) {
        
        //   dataArray.remove(at: sender.tag)
        print(sender.tag)
        EmpSearchDB.remove(at: sender.tag)
        searchNameFunction()
        self.tableView.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        
        return self.searchNamesModel.count
        
        
    }
    
    var LastSelected : IndexPath?
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "area", for: indexPath) as! SearchNameListTableViewCell
        
        
        
        switch searchBarActiveOrNot {
        case false:
            
            
            
            break;
        default:
            
            
            
            
            switch searchNamesModel[indexPath.row].Employee_Name {
            case "":
                cell.lblEmpName.text = "**********"
                break;
            default:
                cell.lblEmpName.text = "\(searchNamesModel[indexPath.row].Employee_Name) "
                cell.lblSelect.isHidden = true
                if(EmpDB.count > 0){
                    for i in 0...EmpDB.count - 1{
                        if(searchNamesModel[indexPath.row].Employee_ID == EmpDB[i].Employee_ID){
                            cell.lblSelect.isHidden = false
                            searchNamesModel[indexPath.row].isSelected = true
                            break;
                        }
                    }
                }
                if(EmpSearchDB.count > 0){
                    for i in 0...EmpSearchDB.count - 1{
                        if(searchNamesModel[indexPath.row].Employee_ID == EmpSearchDB[i].Employee_ID){
                           
                            cell.containerView.backgroundColor = UIColor(red: 0, green: 0, blue: 200, alpha: 0.4)
                            break;
                        }else{
                            cell.containerView.backgroundColor = UIColor.white
                        }
                    }
                }
                
                
                break;
            }
            
            
            break;
        }
        
        
        
        return cell
    }
    var dataArray : [searchNameList] = []
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell:SearchNameListTableViewCell = tableView.cellForRow(at: indexPath)! as! SearchNameListTableViewCell
        switch searchBarActiveOrNot {
        case false:
            // managerPNo = EmpSearchDB[indexPath.row].empId!
            break;
        default:
            if(searchNamesModel[indexPath.row].isSelected == false){
//                let cellBGView = UIView()
//                cellBGView.backgroundColor = UIColor(red: 0, green: 0, blue: 200, alpha: 0.4)
//                cell.containerView.backgroundColor = UIColor(red: 0, green: 0, blue: 200, alpha: 0.4)
                let object = searchNameList()
                var isSelect = true;
                
                
                
                if(EmpSearchDB.count > 0) {
                    
                    for data in EmpSearchDB{
                        if(data.Employee_ID == searchNamesModel[indexPath.row].Employee_ID){
                            isSelect = false;
                            break;
                        }
                    }
                    
                }
                
                
                
                if(isSelect == true) {
                    object.id = searchNamesModel[indexPath.row].Employee_ID
                    
                    
                    
                    object.name = searchNamesModel[indexPath.row].Employee_Name
                    
                    tableView.reloadData()
                    
                    dataArray.append(object)
                    
                    EmpSearchDB.append(searchNamesModel[indexPath.row])
                    
                    
                } else {
                    self.view.makeToast("Already Selected ")
                }
                
                searchNameFunction()
            }else {
                cell.selectionStyle = .none
                self.view.makeToast("Already Selected ")
            }
            
            break;
        }
        
        
    }
    
    @objc func CheckingsearchNameListData() {
        
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    /// SEARCH BAR DELEGATES
    
    public func searchBarShouldBeginEditing(_ searchBar: UISearchBar) -> Bool
    {
        // return NO to not become first responder
        
        return true
    }
    
    public func searchBarTextDidBeginEditing(_ searchBar: UISearchBar){
        
    }
    
    public func searchBarShouldEndEditing(_ searchBar: UISearchBar) -> Bool
    {
        // return NO to not resign first responder
        
        return true
    }
    
    public func searchBarTextDidEndEditing(_ searchBar: UISearchBar)
    {
        
        self.employeeSearchBar.endEditing(true)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar)
    {
        self.employeeSearchBar.endEditing(true)
        // self.employeeSearchBar.resignFirstResponder()
        // IQKeyboardManager.shared.enable = true;
    }
    
    
    
    public func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String)
    {
        searchBarActiveOrNot = true
        
        
        if searchText != ""{
            searchsearchNameFromServer(searchText: searchText)
        }else{
            self.tableView.isHidden = true
            self.noDataLabel(text: "Enter Text For Searching...")
            self.label.isHidden = false
        }
        
    }
    
    func searchsearchNameFromServer(searchText:String)
    {
        
       let param = ["Employee_ID":searchText ]
        self.searchNamesAPI.serviceCalling(obj: self, parameter: param) { (dict) in
            
            self.searchNamesModel = dict as! [EmpListMOMModel]
            
            if self.searchNamesModel.count>0{
                self.dataArray  = []
                self.tableView.isHidden = false
                self.noDataLabel(text: "")
                self.label.isHidden = true
                
            }else{
                self.tableView.isHidden = true
                self.noDataLabel(text: "Sorry! No Data Found from Employee List")
                self.label.isHidden = false
            }
            
            self.tableView.reloadData()
            
        }
        
        
    }
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        self.employeeSearchBar.endEditing(true)
        
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return EmpSearchDB.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellName", for: indexPath) as! NameCollectionViewCell
        
        //        cell.contentView.frame.size.width = cell.textViewName.frame.size.width
        //        cell.contentView.frame.size.height = cell.textViewName.frame.size.height
        cell.imgProfile.image = UIImage(named: "profile")
        cell.textViewName.text = EmpSearchDB[indexPath.row].Employee_Name
        cell.btnClose.tag = indexPath.row
        
        return cell
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        return UIEdgeInsetsMake(0, 0, 0, 0)
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        return CGSize(width: 120, height: 98)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        
        //print(indexPath.item)
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
        
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
   
    
}
