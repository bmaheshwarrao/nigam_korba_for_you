//
//  SelectMembersViewController.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 08/04/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit
import ViewPager_Swift
class SelectMembersViewController:  CommonVSClass {
    
    var objectData = CreateMeetingObject()
    @IBOutlet weak var viewSplitter: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        let myOptions = ViewPagerOptions(viewPagerWithFrame: CGRect(x: 0, y: 0, width: viewSplitter.frame.width, height: viewSplitter.frame.height))
        
        myOptions.tabType = ViewPagerTabType.basic
        
        
        
        
        
        
        myOptions.isTabHighlightAvailable = true
        // myOptions.is
        // If I want indicator bar to show below current page tab
        myOptions.isTabIndicatorAvailable = true
        myOptions.fitAllTabsInView = true;
        myOptions.tabViewTextFont = UIFont.systemFont(ofSize: 15.0, weight: .medium)
        // Oh! and let's change color of tab to red
        myOptions.tabIndicatorViewBackgroundColor = UIColor.yellow
        myOptions.tabViewBackgroundHighlightColor = UIColor(hexString: "2c3e50", alpha: 1.0)!
        myOptions.tabViewBackgroundDefaultColor = UIColor(hexString: "2c3e50", alpha: 1.0)!
        myOptions.tabViewTextDefaultColor = UIColor.yellow
        myOptions.tabViewTextHighlightColor = UIColor.yellow
        
        let viewPager = ViewPagerController()
        
        viewPager.options = myOptions
        viewPager.dataSource = self
        
        //Now let me add this to my viewcontroller
        self.addChildViewController(viewPager)
        
        
        
        viewSplitter.addSubview(viewPager.view)
        viewPager.didMove(toParentViewController: self)
        // Do any additional setup after loading the view.
    }
    
    @IBAction func btnSubmitClicked(_ sender: UIButton) {
        if(EmpDB.count == 0 && EmailDB.count == 0) {
            let actionSheetController: UIAlertController = UIAlertController(title: "Create Meeting", message: "Are you sure you want to create meeting without adding members ?", preferredStyle: .actionSheet)
            
            let camera = UIAlertAction(title: "YES", style: .default, handler: { (alert) in
                
                 self.submitData()
             
                
            })
            
            let cancel = UIAlertAction(title: "NO", style: .cancel, handler: { (alert) in
                
            })
            
            actionSheetController.addAction(camera)
            
            actionSheetController.addAction(cancel)
            self.present(actionSheetController, animated: true, completion: nil)
        }else{
            self.submitData()
        }
        
    }
    func submitData(){
         let pno : String = (UserDefaults.standard.string(forKey: "EmployeeID")!)
        var paramInternal  : [[String:String]] = []
        var iiVal = 0
        for value in EmpDB {
            
            
          
            
            
            
            let paramInternalValue = ["Employee_ID": value.Employee_ID,
                                     "Created_By": pno
                
                
                ]
            
            iiVal = iiVal + 1;
            
            paramInternal.append(paramInternalValue)
            
            
            
            
        }
        

        var paramExternal  : [[String:String]] = []
        
        for valueData in EmailDB {
            
            
            
            
            
            
            let paramExternalValue = ["Email_ID": valueData,
                                        "Created_By": pno
                
                
                ]
            
            
            
            paramExternal.append(paramExternalValue)
            
            
            
            
        }
        
        self.startLoadingPK(view: self.view)
        
        
        
        
        
        
      
        
        
       
        let paramMeeting = [ "Meeting_Title": objectData.Meeting_Title,
                          "Meeting_Agenda": objectData.Meeting_Agenda,
                          "Meeting_Venue": objectData.Meeting_Venue,
                          "Meeting_Date": objectData.Meeting_Date,
                          "Meeting_Time": objectData.Meeting_Time,
                          "Meeting_Duration": objectData.Meeting_Duration,
                          "Meeting_Owner": objectData.Meeting_Owner,
                          "Created_By": objectData.Created_By
                         
                         ] as [String:Any]
        
        let parameterAll = [ "Meeting_Data": paramMeeting,
                          "External_Member":paramExternal,
                          "Internal_Member": paramInternal
            
            
            ] as [String:Any]
        
        let parameter = [ "data": parameterAll ] as [String:Any]
        
        
        print("parameter",parameter)
        
        WebServices.sharedInstances.sendPostRequest(url: URLConstants.CreateMeeting_MOM, parameters: parameter, successHandler: { (response:[String : AnyObject]) in
            
            
            self.stopLoadingPK(view: self.view)
            
            let respon = response["response"] as! [String:AnyObject]
            self.stopLoadingPK(view: self.view)
            if respon["status"] as! String == "success" {
                
                self.stopLoadingPK(view: self.view)
                
           
             
                
                
                let actionSheetController: UIAlertController = UIAlertController(title: "Successfully Created", message: "Meeting created successfully !", preferredStyle: .actionSheet)
                
                let camera = UIAlertAction(title: "OK", style: .default, handler: { (alert) in
                    EmpDB = []
                    EmailDB = []
                    MoveStruct.isMove = true
                    self.navigationController?.popViewController(animated: false)
                    
                    
                })
                
               
                
                actionSheetController.addAction(camera)
                
                
                self.present(actionSheetController, animated: true, completion: nil)
                
                
                
            }else{
                self.stopLoadingPK(view: self.view)
                self.view.makeToast("Please try again, something went wrong.")
            }
            
            
        }) { (err) in
            self.stopLoadingPK(view: self.view)
            print(err.description)
        }
        

    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        
        self.title = "Select Members"
        self.tabBarController?.tabBar.isHidden = true
        let nav = self.navigationController?.navigationBar
        nav?.tintColor = UIColor.white
        nav?.barTintColor = UIColor(hexString: "2c3e50", alpha: 1.0)
        nav?.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        
        
        
    }
    
    
}
extension SelectMembersViewController : ViewPagerControllerDataSource {
    func numberOfPages() -> Int {
        return 2
    }
    
    func viewControllerAtPosition(position: Int) -> UIViewController {
        let storyBoard : UIStoryboard = UIStoryboard(name: "MOM",bundle : nil)
        if(position == 0) {
            
            let empVC = storyBoard.instantiateViewController(withIdentifier: "InternalMembersViewController") as! InternalMembersViewController
          
            return  empVC as CommonVSClass
        }else {
            
            let empVC = storyBoard.instantiateViewController(withIdentifier: "ExternalMembersViewController") as! ExternalMembersViewController
          
            return  empVC as CommonVSClass
        }
        
    }
    
    func tabsForPages() -> [ViewPagerTab] {
        return [ViewPagerTab(title: "INTERNAL" , image: #imageLiteral(resourceName: "BackBlack")) , ViewPagerTab(title: "EXTERNAL" , image: #imageLiteral(resourceName: "BackBlack"))]
    }
    
    
}
extension SelectMembersViewController : ViewPagerControllerDelegate {
    
}


