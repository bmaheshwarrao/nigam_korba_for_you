//
//  CreateMeetingViewController.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 08/04/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit

class CreateMeetingViewController:CommonVSClass , UITextFieldDelegate, UIPickerViewDelegate, UIPickerViewDataSource, WWCalendarTimeSelectorProtocol {
    
    @IBOutlet weak var tableView: UITableView!
    var Question = String()
    var visit_Id = String()
    var Question_Id = String()
    var type_Id = String()
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
        // Do any additional setup after loading the view.
    }
   
    @IBAction func btnNextClicked(_ sender: UIButton) {
        if(cellData.txtTitle.text == ""){
            self.view.makeToast("Enter Title")
        }else if(cellData.textViewAgenda.text == ""){
            self.view.makeToast("Enter Agenda")
        }else if(cellData.textViewVenue.text == ""){
            self.view.makeToast("Enter Venue")
        }else if(cellData.txtTime.text == ""){
            self.view.makeToast("Select Time")
        }else if(cellData.txtDuration.text == ""){
            self.view.makeToast("Select Duration")
        }else{
              let pno : String = (UserDefaults.standard.string(forKey: "EmployeeID")!)
            let objectData = CreateMeetingObject()
            objectData.Meeting_Title = cellData.txtTitle.text!
            objectData.Meeting_Agenda = cellData.textViewAgenda.text!
            objectData.Meeting_Venue = cellData.textViewVenue.text!
            objectData.Meeting_Date = dateMeeting
            objectData.Meeting_Time = cellData.txtTime.text!
            objectData.Meeting_Duration = cellData.txtDuration.text!
            objectData.Meeting_Owner = pno
            objectData.Created_By = pno
            EmpSearchDB = []
            EmpDB = []
            let storyboard1 = UIStoryboard(name: "MOM", bundle: nil)
            let auditVC = storyboard1.instantiateViewController(withIdentifier: "SelectMembersViewController") as! SelectMembersViewController
            auditVC.objectData = objectData
            self.navigationController?.pushViewController(auditVC, animated: true)
           
        }
    }
   
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    fileprivate var singleDate: Date = Date()
    fileprivate var multipleDates: [Date] = []
    var dateMeeting = String()
    var selectdateVal = 0
    @IBAction func btnDateClicked(_ sender: UIButton) {
        
        let selector = UIStoryboard(name: "WWCalendarTimeSelector", bundle: nil).instantiateViewController(withIdentifier: "WWCalendarTimeSelector") as! WWCalendarTimeSelector
        
        
        selectdateVal = 3
        selector.delegate = self
        selector.optionCurrentDate = singleDate
        selector.optionCurrentDates = Set(multipleDates)
        selector.optionCurrentDateRange.setStartDate(multipleDates.first ?? singleDate)
        selector.optionCurrentDateRange.setEndDate(multipleDates.last ?? singleDate)
        
        
        selector.optionStyles.showDateMonth(true)
        selector.optionStyles.showMonth(false)
        selector.optionStyles.showYear(true)
        selector.optionStyles.showTime(false)
        
        
        present(selector, animated: true, completion: nil)
    }
    var millisecDate = String()
    func getMilliseconds(date1 : Date) -> Int{
        //let strDate = date1
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd MMM yyyy  hh:mma"
        //let date = dateFormatter.date(from: strDate)
        
        let millieseconds = self.getDiffernce(toTime: date1)
        
        return millieseconds
    }
    func getDiffernce(toTime:Date) -> Int{
        let elapsed = toTime.timeIntervalSince1970
        return Int(elapsed * 1000)
    }
    func WWCalendarTimeSelectorDone(_ selector: WWCalendarTimeSelector, date: Date) {
        print("Selected \n\(date)\n---")
        singleDate = date
        
        
        
        if(selectdateVal == 3){
            cellData.lblDate.text = date.stringFromFormat("d' 'MMM' 'yyyy")
            startDate  = getMilliseconds(date1: date)
            millisecDate = String(startDate)
            
            dateMeeting = date.stringFromFormat("dd'-'MMM'-'yyyy")
            print(dateMeeting)
        }
       
      
        
        
    }
    
    func WWCalendarTimeSelectorDone(_ selector: WWCalendarTimeSelector, dates: [Date]) {
        print("Selected Multiple Dates \n\(dates)\n---")
        if let date = dates.first {
            singleDate = date
            // dateLabel.text = date.stringFromFormat("d' 'MMMM' 'yyyy', 'h':'mma")
        }
        else {
            //dateLabel.text = "No Date Selected"
        }
        multipleDates = dates
    }
    
    
    var cellData : CreateMeetingTableViewCell!
    
    var QuestionStrNo = String()
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        if( MoveStruct.isMove == true){
            MoveStruct.isMove = false
            self.navigationController?.popViewController(animated: false)
        }
        self.title = "Create Meeting"
        
        let nav = self.navigationController?.navigationBar
        nav?.tintColor = UIColor.white
        nav?.barTintColor = UIColor(hexString: "2c3e50", alpha: 1.0)
        nav?.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
       
       
        
        
    }
    
    var datePicker  = UIDatePicker()
    var datePicker1 = UIDatePicker()
    var dateFormatter = DateFormatter()
    
    var hoursPicker = UIPickerView()
    
    var hoursArray = [String]()
    var minutesArray = [String]()
    func pickUpDate(_ textField : UITextField){
        
        
        
         if(textField == cellData.txtTime)
        {
            
            
            self.datePicker1 = UIDatePicker(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.datePicker1.backgroundColor = UIColor.white
            self.datePicker1.datePickerMode = .time
            self.datePicker1.locale = Locale.init(identifier: "en_GB")
            cellData.txtTime.inputView = self.datePicker1
            self.datePicker1.minuteInterval = 10
            // ToolBar
            let toolBar1 = UIToolbar()
            toolBar1.barStyle = .default
            toolBar1.isTranslucent = true
            toolBar1.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
            toolBar1.sizeToFit()
            
            // Adding Button ToolBar
            let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(self.doneClick(_:)))
            let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
            let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(self.cancelClick(_:)))
            
            
            
            
            
            
            toolBar1.setItems([cancelButton, spaceButton, doneButton], animated: false)
            toolBar1.isUserInteractionEnabled = true
            cellData.txtTime.inputAccessoryView = toolBar1
            
            
            
            doneButton.tag = 2
            cancelButton.tag = 2
            
        }
        else
        {
            
            
            self.datePicker = UIDatePicker(frame:CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 216))
            self.datePicker.backgroundColor = UIColor.white
            self.datePicker.datePickerMode = .time
            self.datePicker.locale = Locale.init(identifier: "en_GB")
            cellData.txtDuration.inputView = self.datePicker
            self.datePicker.minuteInterval = 10
            // ToolBar
            let toolBar1 = UIToolbar()
            toolBar1.barStyle = .default
            toolBar1.isTranslucent = true
            toolBar1.tintColor = UIColor(red: 92/255, green: 216/255, blue: 255/255, alpha: 1)
            toolBar1.sizeToFit()
            
            // Adding Button ToolBar
            let doneButton = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(self.doneClick(_:)))
            let spaceButton = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
            let cancelButton = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(self.cancelClick(_:)))
            
            
            
            
            
            
            toolBar1.setItems([cancelButton, spaceButton, doneButton], animated: false)
            toolBar1.isUserInteractionEnabled = true
            cellData.txtDuration.inputAccessoryView = toolBar1
            
            
            
            doneButton.tag = 3
            cancelButton.tag = 3
            
            
            
            
            
        }
        
        
        
        
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        
        return 2
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        
        if(component == 0)
        {
            return self.hoursArray.count
        }
        else
        {
            return self.minutesArray.count
        }
        
    }
    
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if(component == 0)
        {
            return self.hoursArray[row]
        }
        else
        {
            return self.minutesArray[row]
        }
        
        
    }
    
    
    
    
    @objc func doneClick(_ sender:UIBarButtonItem)
    {
        
        
      if(sender.tag == 2)
        {
            print("TimeTF")
            
            
            let dateFormatter1 = DateFormatter()
            dateFormatter1.dateStyle = .medium
            dateFormatter1.timeStyle = .none
            dateFormatter1.dateFormat =  "HH:mm"
            
            cellData.txtTime.text = dateFormatter1.string(from: self.datePicker1.date)
            cellData.txtTime.resignFirstResponder()
        }
        else
        {
            
            let dateFormatter1 = DateFormatter()
            dateFormatter1.dateStyle = .medium
            dateFormatter1.timeStyle = .none
            dateFormatter1.dateFormat =  "HH:mm"
            
            cellData.txtDuration.text = dateFormatter1.string(from: self.datePicker.date)
            cellData.txtDuration.resignFirstResponder()
            
            
            
            
        }
        
        
        
    }
    
    @objc func cancelClick(_ sender:UIBarButtonItem)
    {
        if(sender.tag == 2)
        {
            print("TimeTF")
            cellData.txtTime.resignFirstResponder()
        }
        else
        {
            
            cellData.txtDuration.resignFirstResponder()
            
        }
        
        
    }
    
    
    
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return true
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if(textField == cellData.txtTime)
        {
            self.pickUpDate(cellData.txtTime)
        }
        else
        {
            self.pickUpDate(cellData.txtDuration)
        }
        
        
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
extension CreateMeetingViewController : UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
    }
}
extension CreateMeetingViewController : UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! CreateMeetingTableViewCell
        cellData = cell
        cell.txtTime.delegate = self
        cell.txtDuration.delegate = self
        let dateFormatter1 = DateFormatter()
        dateFormatter1.timeZone = NSTimeZone.system
        dateFormatter1.dateFormat = "dd MMM yyyy"
        if(cell.lblDate.text == ""){
            let date_TimeDate = dateFormatter1.string(from: Date())
            let dateFormatter2 = DateFormatter()
            dateFormatter2.timeZone = NSTimeZone.system
            dateFormatter2.dateFormat = "dd-MMM-yyyy"
            let date_TimeDateMeeting = dateFormatter2.string(from: Date())
            dateMeeting = date_TimeDateMeeting
            cell.lblDate.text = date_TimeDate
            millisecDate = String(startDate)
        }
        cell.separatorInset = UIEdgeInsetsMake(0, 10000, 0, 0);
        return cell
        
        
        
    }
}
class CreateMeetingObject: NSObject {
    
    
    
    var Meeting_Title = String()
    var Meeting_Agenda = String()
    var Meeting_Venue = String()
    var Meeting_Date = String()
    
    
    var Meeting_Time = String()
    var Meeting_Duration = String()
    var Meeting_Owner = String()
    var Created_By = String()
}



