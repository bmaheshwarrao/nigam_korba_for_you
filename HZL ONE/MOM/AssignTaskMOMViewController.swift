//
//  AssignTaskMOMViewController.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 09/04/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit

class AssignTaskMOMViewController: CommonVSClass {

    @IBOutlet weak var viewAssigned: UIView!
    @IBOutlet weak var lblAssigned: UILabel!
    @IBOutlet weak var textViewTask: UITextView!
    @IBOutlet weak var viewTask: UIView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.lblAssigned.text = "Select"
        self.lblAssigned.textColor = UIColor.lightGray
        viewAssigned.layer.borderWidth = 1.0
        viewAssigned.layer.borderColor = UIColor.black.cgColor
        viewAssigned.layer.cornerRadius = 10.0
        viewTask.layer.borderWidth = 1.0
        viewTask.layer.borderColor = UIColor.black.cgColor
        viewTask.layer.cornerRadius = 10.0
        
        if(meetingDetailsDB.viewInternal.count > 0){
            for i in 0...meetingDetailsDB.viewInternal.count - 1 {
                assinedEmp.append(meetingDetailsDB.viewInternal[i].Employee_Name)
                 assinedEmpId.append(meetingDetailsDB.viewInternal[i].Employee_ID)
            }
        }
        
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        if(EmpDB.count > 0){
            lblAssigned.text = EmpDB[0].Employee_Name
            self.idEmp = EmpDB[0].Employee_ID
            lblAssigned.textColor = UIColor.black
        }
    }
    var assinedEmp : [String] = []
    var assinedEmpId : [String] = []
    var idEmp = String()
    var meetingDetailsDB = ViewMeetingManagerModel()
    @IBAction func btnAssignedToClicked(_ sender: UIButton) {
        let storyboard1 = UIStoryboard(name: "MOM", bundle: nil)
        let auditVC = storyboard1.instantiateViewController(withIdentifier: "AssignSearchViewController") as! AssignSearchViewController

        self.navigationController?.pushViewController(auditVC, animated: true)
    }
    @IBAction func btnAssignedTaskClicked(_ sender: UIButton) {
         if(self.lblAssigned.text == "Select"){
            self.view.makeToast("Select Employee")
        }else if(textViewTask.text == ""){
            self.view.makeToast("Enter Task")
        }
        
        else{
            self.startLoadingPK(view: self.view)
            
        
            
            let parameter = [
                "Meeting_ID":self.meetingDetailsDB.viewMeet[0].ID,
                "Task":textViewTask.text!,
                "Assigned_To":self.idEmp
                
                ] as [String:String]
            
            
            print("parameter",parameter)
            
            WebServices.sharedInstances.sendPostRequest(url: URLConstants.Temp_Task_Insert_MOM, parameters: parameter, successHandler: { (response:[String : AnyObject]) in
                
                let respon = response["response"] as! [String:AnyObject]
                self.stopLoadingPK(view: self.view)
                let objectmsg = MessageCallServerModel()
                
                let msg = objectmsg.sealizeMessage(cell: respon as! [String : AnyObject])
                if respon["status"] as! String == "success" {
                    
                    self.stopLoadingPK(view: self.view)
                   self.textViewTask.text = ""
                   self.view.makeToast(msg)
                    
                    
                }else{
                    self.stopLoadingPK(view: self.view)
                    self.view.makeToast(msg)
                }
                
            }) { (err) in
                self.stopLoadingPK(view: self.view)
                print(err.description)
            }
            
        }
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
