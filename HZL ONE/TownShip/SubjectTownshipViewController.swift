//
//  SubjectTownshipViewController.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 09/03/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit

class SubjectTownshipViewController: CommonVSClass ,UITableViewDelegate, UITableViewDataSource{
    var CatId = String()
    var CatName = String()
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var lblCategory: UILabel!
    var refreshControl = UIRefreshControl()
    
    var SubjectTownshipDB:[SubjectTownshipDataModel] = []
    var SubjectTownshipAPI = SubjectTownshipDataAPI()
    
    
    
    var StrNav = String()
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        lblCategory.text = CatName
        getSubjectData()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.estimatedRowHeight = 65
        tableView.rowHeight = UITableViewAutomaticDimension
        
        tableView.tableFooterView = UIView()
        refreshControl.addTarget(self, action: #selector(getSubjectData), for: .valueChanged)
        
        self.tableView.addSubview(refreshControl)
        let swipe = UISwipeGestureRecognizer(target: self, action: #selector(self.swipeRecognizer))
        swipe.direction = ([.down])
        self.view.addGestureRecognizer(swipe)
        
        
        
        
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        if(MoveStruct.isMove == true){
            self.navigationController?.popViewController(animated: false)
        }
        self.tabBarController?.tabBar.isHidden = true
        self.title = "Select Subject"
        let nav = self.navigationController?.navigationBar
        nav?.tintColor = UIColor.white
        nav?.barTintColor = UIColor(hexString: "2c3e50", alpha: 1.0)
        nav?.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        nav?.isTranslucent = false
        
    }
    var reachability = Reachability()!
    @objc func swipeRecognizer(_ sender: UISwipeGestureRecognizer) {
        if (sender.direction == .down) {
            
            if(self.reachability.connection != .none)
            {
                
                self.getSubjectData()
            }
            else
            {
                self.startLoading(view: self.view)
                DispatchQueue.main.asyncAfter(deadline: .now() + 4.5) {
                    self.noDataLabel(text: "No internet connection found. Check your internet connection and try again.")
                    self.stopLoading(view: self.view)
                }
            }
            
        }
    }
   
    @objc func tapViewTapClicked(_ sender: UITapGestureRecognizer) {
        let buttonPosition:CGPoint = sender.view!.convert(.zero, to:self.tableView)
        let indexPath = self.tableView.indexPathForRow(at: buttonPosition)
        
        let storyBoard = UIStoryboard(name: "Township", bundle: nil)
        let ZIVC = storyBoard.instantiateViewController(withIdentifier: "RegisterComplaintViewController") as! RegisterComplaintViewController

        ZIVC.CatId = CatId
        ZIVC.CatName =  CatName
        
        ZIVC.subjectId =  self.SubjectTownshipDB[(indexPath?.section)!].ID
        ZIVC.subjectName =  self.SubjectTownshipDB[(indexPath?.section)!].Subject
        self.navigationController?.pushViewController(ZIVC, animated: true)
        
        
    }
    @IBOutlet var tapView: UITapGestureRecognizer!
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    var data: String?
    var lastObject: String?
    var indPath = IndexPath()
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        
        
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.SubjectTownshipDB.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! SubjectTableViewCell
        
         cell.textViewSubject.text = self.SubjectTownshipDB[indexPath.section].Subject
        
        let TapGesture = UITapGestureRecognizer(target: self, action: #selector(self.tapViewTapClicked(_:)))
        cell.addGestureRecognizer(TapGesture)
        cell.isUserInteractionEnabled = true
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableViewAutomaticDimension
        
    }
    
    @objc func getSubjectData(){
       
        SubjectTownshipAPI.serviceCalling(obj: self,  Catid: CatId ) { (dict) in
            
            self.SubjectTownshipDB = dict as! [SubjectTownshipDataModel]
            
            self.tableView.reloadData()
            
        }
    }
    
    
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
