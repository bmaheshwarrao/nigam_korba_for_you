//
//  FeedbackTownshipViewController.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 09/03/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit
import DLRadioButton
var moveBack = Int()
class FeedbackTownshipViewController: CommonVSClass ,UITableViewDelegate, UITableViewDataSource{
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var textViewRequest: UITextView!
    @IBOutlet weak var lblRequestDate: UILabel!
    
    @IBOutlet weak var lblRequestId: UILabel!
    
    
    @IBOutlet weak var btnNo: DLRadioButton!
    @IBOutlet weak var btnYes: DLRadioButton!
    
    @IBOutlet weak var btnSubmit: UIButton!
    
    var FeedbackTownshipDB:[FeedbackDataModel] = []
    var FeedbackTownshipAPI = FeedBackTownshipDataAPI()
    
    override func viewDidLoad() {
        super.viewDidLoad()
     btnYes.isSelected = true
        strYesNo = "Yes"
        count = 5
         btnSubmit.setTitle("Submit Feedback", for: .normal)
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.estimatedRowHeight = 65
        tableView.rowHeight = UITableViewAutomaticDimension
        
        tableView.tableFooterView = UIView()
       
        
    }
    var feedbackId = String()
    @objc func getFeedbackData(){
        let empId : String = UserDefaults.standard.string(forKey: "EmployeeID")!
        var param = [String:String]()
        
        param =  ["EmployeeID":empId]
        FeedbackTownshipAPI.serviceCalling(obj: self,  param: param ) { (dict) in
            
            self.FeedbackTownshipDB = dict as! [FeedbackDataModel]
            if(self.FeedbackTownshipDB.count > 0){
                self.feedbackId = String(self.FeedbackTownshipDB[0].ID)
                self.lblRequestId.text = "ID #"+String(describing: self.FeedbackTownshipDB[0].ID)
                
                self.lblRequestDate.text = self.FeedbackTownshipDB[0].ComplaintDate
                
                
                let milisecondDate = self.FeedbackTownshipDB[0].ComplaintDate_Status
                let dateVarDate = Date.init(timeIntervalSince1970: TimeInterval(milisecondDate)!/1000)
                let dateFormatterday = DateFormatter()
                dateFormatterday.dateFormat = "dd MMM yyyy"
                let valDate =  dateFormatterday.string(from: dateVarDate)
               
               
                
                
                
                
                let paragraph = NSMutableParagraphStyle()
                paragraph.alignment = .left
                paragraph.lineSpacing = 0
               
                let startString : String = "You "
           
                
                let  mBuilder = startString  + "register complaint for " + self.FeedbackTownshipDB[0].ComplaintSubject_Name + " under category " +  self.FeedbackTownshipDB[0].category +
                    " with " + self.FeedbackTownshipDB[0].Remarks + " remark . Complaint status is " +  self.FeedbackTownshipDB[0].ComplaintStatus + " on " + valDate
                
                let agreeAttributedString = NSMutableAttributedString(string: mBuilder, attributes: [NSAttributedStringKey.foregroundColor: UIColor.black, NSAttributedStringKey.font: UIFont.systemFont(ofSize: 15.0, weight: .medium)])
                
                
                //Submitted
                let SubmittedAttributedString = NSAttributedString(string:"register complaint for ", attributes: [NSAttributedStringKey.foregroundColor: UIColor.gray, NSAttributedStringKey.font: UIFont.systemFont(ofSize: 15.0, weight: .medium)])
                
                let range: NSRange = (agreeAttributedString.string as NSString).range(of: "register complaint for ")
                if range.location != NSNotFound {
                    agreeAttributedString.replaceCharacters(in: range, with: SubmittedAttributedString)
                }
                
                //for
                let forAttributedString = NSAttributedString(string:"under category", attributes: [NSAttributedStringKey.foregroundColor: UIColor.gray, NSAttributedStringKey.font: UIFont.systemFont(ofSize: 15.0, weight: .medium)])
                
                let forRange: NSRange = (agreeAttributedString.string as NSString).range(of: "under category")
                if forRange.location != NSNotFound {
                    agreeAttributedString.replaceCharacters(in: forRange, with: forAttributedString)
                }
                let subAreaAttributedString = NSAttributedString(string:"with", attributes: [NSAttributedStringKey.foregroundColor: UIColor.gray, NSAttributedStringKey.font: UIFont.systemFont(ofSize: 15.0, weight: .medium)])
                let subAreaRange: NSRange = (agreeAttributedString.string as NSString).range(of: "with")
                if subAreaRange.location != NSNotFound {
                    agreeAttributedString.replaceCharacters(in: subAreaRange, with: subAreaAttributedString)
                }
                let remarkAttributedString = NSAttributedString(string:" remark . Complaint status is ", attributes: [NSAttributedStringKey.foregroundColor: UIColor.gray, NSAttributedStringKey.font: UIFont.systemFont(ofSize: 15.0, weight: .medium)])
                let remarkRange: NSRange = (agreeAttributedString.string as NSString).range(of: " remark . Complaint status is ")
                if remarkRange.location != NSNotFound {
                    agreeAttributedString.replaceCharacters(in: remarkRange, with: remarkAttributedString)
                }
                
                let onAttributedString = NSAttributedString(string:" on ", attributes: [NSAttributedStringKey.foregroundColor: UIColor.gray, NSAttributedStringKey.font: UIFont.systemFont(ofSize: 15.0, weight: .medium)])
                let onRange: NSRange = (agreeAttributedString.string as NSString).range(of: " on ")
                if onRange.location != NSNotFound {
                    agreeAttributedString.replaceCharacters(in: onRange, with: onAttributedString)
                }
                
                self.textViewRequest.attributedText = agreeAttributedString
            }
            
            
        }
    }
    @IBAction func btnSubmitClicked(_ sender: UIButton) {
        if(strYesNo == "Yes"){
            submitTab()
        }else{
            ReopenTab()
        }
    }
    
    @objc func submitTab(){
        if(feedbackId == ""){
            self.showTwoButtonAlertWithTwoAction(title: "Alert", message: "Internet is not available , please check", buttonTitleLeft: "Close", buttonTitleRight: "Retry", completionHandlerLeft: {
                self.closeTab()
            }) {
                self.retryTab()
            }
        }else if(FeedbackTownship.RateQuality == String()){
            self.view.makeToast("Please rate Rate of work")
        }
        else if(FeedbackTownship.Material == String()){
            self.view.makeToast("Please rate Material Quality")
        }
        else if(FeedbackTownship.WorkerResponse == String()){
            self.view.makeToast("Please rate Worker Response")
        }
        else if(FeedbackTownship.ResponseTime == String()){
            self.view.makeToast("Please rate Response time")
        }
        else if(FeedbackTownship.Worksite == String()){
            self.view.makeToast("Please rate Worksite housekeeping measures")
        }else{
        let parameter = [
            
            "EmployeeID": UserDefaults.standard.string(forKey: "EmployeeID")! ,
            "Complaint_ID": feedbackId ,
            "Quality_of_Work" : FeedbackTownship.RateQuality ,
            "Material_Quality" : FeedbackTownship.Material ,
            "Worker_Response" : FeedbackTownship.WorkerResponse ,
            "Response_Time" : FeedbackTownship.ResponseTime ,
            "Housekeeping_Measures" : FeedbackTownship.Worksite ,
            "Created_By" : UserDefaults.standard.string(forKey: "EmployeeID")!
            
            ] as! [String:String]
        
    
        
        
        
        self.startLoadingPK(view: self.view)
        
        print("parameter",parameter)
        
        WebServices.sharedInstances.sendPostRequest(url: URLConstants.Insert_Feedback, parameters: parameter, successHandler: { (response:[String : AnyObject]) in
            
            let respon = response["response"] as! [String:AnyObject]
            
            if respon["status"] as! String == "success" {
                self.stopLoadingPK(view: self.view)
                self.getFeedbackData()
                
            }else{
                self.stopLoadingPK(view: self.view)
            }
            
        }) { (err) in
            self.stopLoadingPK(view: self.view)
            
            print(err.description)
        }
        }
    }
    @objc func ReopenTab(){
        if(feedbackId == ""){
            self.showTwoButtonAlertWithTwoAction(title: "Alert", message: "Internet is not available , please check", buttonTitleLeft: "Close", buttonTitleRight: "Retry", completionHandlerLeft: {
                self.closeTab()
            }) {
                self.retryTab()
            }
        }
        else if(cellData.textViewRemark.text! == ""){
            self.view.makeToast("Please write remark")
        }
        else{
        let parameter = [
            
            
            "Complaint_ID": feedbackId ,
           
            "Remarks" :cellData.textViewRemark.text!,
            "Created_By" : UserDefaults.standard.string(forKey: "EmployeeID")!
            
            ] as! [String:String]
        
        
        
    
        self.startLoadingPK(view: self.view)
        
        print("parameter",parameter)
        
        WebServices.sharedInstances.sendPostRequest(url: URLConstants.Reopen_Complaint, parameters: parameter, successHandler: { (response:[String : AnyObject]) in
            
            let respon = response["response"] as! [String:AnyObject]
            
            if respon["status"] as! String == "success" {
                self.stopLoadingPK(view: self.view)
                self.getFeedbackData()
                
            }else{
                self.stopLoadingPK(view: self.view)
            }
            
        }) { (err) in
            self.stopLoadingPK(view: self.view)
            
            print(err.description)
        }
        }
    }
    
    @objc func movetoregisterTab(){
          let storyBoard = UIStoryboard(name: "Township", bundle: nil)
        let ZIVC = storyBoard.instantiateViewController(withIdentifier: "CategoryTownshipViewController") as! CategoryTownshipViewController
      
        self.navigationController?.pushViewController(ZIVC, animated: true)
    }
    @objc func closeTab(){
        self.navigationController?.popViewController(animated: false)
    }
    @objc func retryTab(){
        getFeedbackData()
    }
    var count = Int()
    var strYesNo = String()
    @IBAction func btnDLRadioButtonClickedd(_ sender: DLRadioButton) {
        
       
            for button in sender.selectedButtons() {
                
                strYesNo =   button.titleLabel!.text!
            }
        if(strYesNo == "Yes"){
            count = 5
            btnSubmit.setTitle("Submit Feedback", for: .normal)
        }else{
            count = 1
            btnSubmit.setTitle("Reopen Complaint", for: .normal)
        }
      self.tableView.reloadData()
        
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        if(moveBack == 1){
            self.navigationController?.popViewController(animated: false)
        }else{
           getFeedbackData()
        self.tabBarController?.tabBar.isHidden = true
        self.title = "Feedback Township"
        let nav = self.navigationController?.navigationBar
        nav?.tintColor = UIColor.white
        nav?.barTintColor = UIColor(hexString: "2c3e50", alpha: 1.0)
        nav?.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        nav?.isTranslucent = false
        }
        
    }
 
    @IBAction func tapViewClicked(_ sender: Any) {
        
        
        
        
        
        
        
    }
    var cellData : FeedbackNoTableViewCell!
    @IBOutlet var tapView: UITapGestureRecognizer!
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    var data: String?
    var lastObject: String?
    var indPath = IndexPath()
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        
        
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
         if(strYesNo == "Yes"){
            if(indexPath.section == 0){
        let cell = tableView.dequeueReusableCell(withIdentifier: "cellRateQuality", for: indexPath) as! RatequalityTableViewCell
        
      
        return cell
            }else if(indexPath.section == 1){
                let cell = tableView.dequeueReusableCell(withIdentifier: "cellMaterialQuality", for: indexPath) as! MaterialTableViewCell
                
                
                return cell
            }else if(indexPath.section == 2){
                let cell = tableView.dequeueReusableCell(withIdentifier: "cellWork", for: indexPath) as! WorkerResponseTableViewCell
                
                
                return cell
            }else if(indexPath.section == 3){
                let cell = tableView.dequeueReusableCell(withIdentifier: "cellResponseTime", for: indexPath) as! ResponseTimeTableViewCell
                
                
                return cell
            }else{
                let cell = tableView.dequeueReusableCell(withIdentifier: "cellWorkSite", for: indexPath) as! WorkSiteTableViewCell
                
                
                return cell
            }
         }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! FeedbackNoTableViewCell
            cellData = cell
            
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if(strYesNo == "Yes"){
           return  UITableViewAutomaticDimension
        }else{
            return 134
        }
        
        
    }
    
    
    
    
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
