//
//  RegisterComplaintViewController.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 09/03/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit
import Alamofire
class RegisterComplaintViewController: CommonVSClass, UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    @IBOutlet weak var textSubject: UITextView!
    @IBOutlet weak var lblCategory: UILabel!
    @IBOutlet weak var tableView: UITableView!
    var CatId = String()
    var CatName = String()
    var subjectId = String()
    var subjectName = String()
    
    var locationId = String()
    var locationName = String()
    override func viewDidLoad() {
        super.viewDidLoad()
        lblCategory.text = CatName
        textSubject.text = subjectName
        tableView.delegate = self
        tableView.dataSource = self
        
        
        
        // Do any additional setup after loading the view.
    }
   
    let reachablty = Reachability()!
    @IBAction func btnregisterClicked(_ sender: UIButton) {
        
        
        if reachablty.connection == .none{
            
            self.view.makeToast("Internet is not available, please check your internet connection try again.")
            
        }
            
        else if (cellData.textViewRemark.text == "") {
            
            self.view.makeToast("Please Write Remark")
        }
      else{
            
           
            
         locationId = UserDefaults.standard.string(forKey: "LocationIdTownship")!
         locationName = UserDefaults.standard.string(forKey: "LocationNameTownship")!
            
            self.startLoadingPK(view: self.view)
            
        let empId : String = UserDefaults.standard.string(forKey: "EmployeeID")!
        
            
            let parameter = ["EmployeeID":empId,
                             "CategoryId":CatId,
                             "ComplaintSubjectId":subjectId,
                             "ComplainVia": "iOS",
                             "Remarks":cellData.textViewRemark.text!,
                             "Address": locationName,
                             "LocationID":locationId
                
                ] as [String:String]
            
            print(parameter)
            let jsonData: Data? = try? JSONSerialization.data(withJSONObject: parameter, options: [])
            let para: [String: Data] = ["data": jsonData!]
            
            Alamofire.upload(multipartFormData: { multipartFormData in
                
                if !(self.imagedata == nil) {
                    var res : UIImage = UIImage()
                
                    let size = CGSize(width: 150, height: 150)
                    res = self.imageResize(image: self.cellData.imageViewPhoto.image!,sizeChange: size)
                    
                    self.imagedata = UIImageJPEGRepresentation(res, 1.0)!
                    multipartFormData.append(self.imagedata!, withName: "file", fileName: "file.jpg", mimeType: "image/jpeg")
                }
                
                for (key, value) in para {
                    multipartFormData.append(value, withName: key)
                    
                    
                }
                
            },
                             to:URLConstants.Complaint_Register)
            { (result) in
                switch result {
                case .success(let upload, _, _):
                    
                    upload.uploadProgress(closure: { (progress) in
                        
                    })
                    
                    
                    upload.responseJSON { response in
                        
                        
                        if let json = response.result.value
                        {
                            var dict = json as! [String: AnyObject]
                            
                            if let response = dict["response"]{
                                print(response)
                                let statusString = response["status"] as! String
                                let objectmsg = MessageCallServerModel()
                    let msg = objectmsg.sealizeMessage(cell: response as! [String : AnyObject])
                                print("Township Dict :",dict)
                                if(statusString == "success")
                                {
                                    
                                    
                                    self.stopLoadingPK(view: self.view)
                                    MoveStruct.isMove = true
                                    
                                    MoveStruct.message = msg
                                   
                                    let actionSheetController: UIAlertController = UIAlertController(title:"Complaint Registered", message: msg, preferredStyle: .actionSheet)
                                    
                                    let cancelAction: UIAlertAction = UIAlertAction(title: "OK", style: .cancel) { action -> Void in
                                        
                                        
                                        
                                        self.navigationController?.popViewController(animated: false)
                                        
                                        
                                        
                                    }
                           
                                    
                                  actionSheetController.addAction(cancelAction)
                                    
                                    self.present(actionSheetController, animated: true, completion: nil)
                                    
                                    
                                    
                                   
                                    
                                    
                                    
                                    
                                }else if(statusString == "fail")
                                {
                                    
                                    
                                    self.stopLoadingPK(view: self.view)
                                    MoveStruct.isMove = true
                                    
                                    MoveStruct.message = msg
                                    
                                    let actionSheetController: UIAlertController = UIAlertController(title:"Complaint Registered", message: msg, preferredStyle: .actionSheet)
                                    
                                    let cancelAction: UIAlertAction = UIAlertAction(title: "OK", style: .cancel) { action -> Void in
                                        
                                        
                                        
                                        self.navigationController?.popViewController(animated: true)
                                        
                                        
                                        
                                    }
                                    
                                    
                                    actionSheetController.addAction(cancelAction)
                                    
                                    self.present(actionSheetController, animated: true, completion: nil)
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                }
                                else{
                                    
                                    self.stopLoadingPK(view: self.view)
                                    self.view.makeToast(msg)
                                    
                                }
                                
                                
                                
                            }
                            
                            
                        }
                        
                        
                    }
                    
                case .failure(let encodingError):
                    
                    self.stopLoadingPK(view: self.view)
                    self.errorChecking(error: encodingError)
                    
                    print(encodingError.localizedDescription)
                }
            }
        }
        
        
        
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.title = "Register Complaint"
        
        let nav = self.navigationController?.navigationBar
        nav?.tintColor = UIColor.white
        nav?.barTintColor = UIColor(hexString: "2c3e50", alpha: 1.0)
        nav?.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
      

        
        
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    @IBAction func btnImageButtonClicked(_ sender: UIButton) {
        
        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
            self.openCamera()
        }))
        
        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
            self.openGallary()
        }))
        
        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
        
    }
    
    
    @objc func pressCamera(_ sender : UITapGestureRecognizer) {
        
    }
    let imagePicker = UIImagePickerController()
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.camera))
        {
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.camera
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    func openGallary()
    {
        imagePicker.delegate = self
        imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        imagePicker.allowsEditing = true
        self.present(imagePicker, animated: true, completion: nil)
    }
    internal func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            cellData.imageViewPhoto.image = image
            imagedata = UIImageJPEGRepresentation(image, 1.0)!
            cellData.btnCamera.isHidden = true
            cellData.lblOptional.isHidden = true
            cellData.btnClose.isHidden = false
            cellData.imageViewPhoto.isHidden = false
            updateConst()
            
            
        }else{
            print("Something went wrong")
        }
        self.dismiss(animated: true, completion: nil)
    }
    
    // var reachability = Reachability()!
    var imagedata: Data? = nil
    @IBAction func btnCloseButtonClicked(_ sender: UIButton) {
        
        self.imagedata = nil
        self.cellData.btnClose.isHidden = true
        cellData.btnCamera.isHidden = false
        cellData.lblOptional.isHidden = false
        updateConst()
    }
    func updateConst(){
  
            
       
        
            if(self.cellData.btnClose.isHidden == true){
                self.cellData.heightViewofClose.constant = 0
                self.cellData.heightImageselected.constant = 0
                self.cellData.heightViewImage.constant = 40
            }else{
                self.cellData.heightViewofClose.constant = 20
                self.cellData.heightImageselected.constant = 100
                self.cellData.heightViewImage.constant = 120
            }
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /// Fusuma
    
    
    func imageResize (image:UIImage, sizeChange:CGSize)-> UIImage{
        
        let hasAlpha = true
        let scale: CGFloat = 0.0 // Use scale factor of main screen
        
        UIGraphicsBeginImageContextWithOptions(sizeChange, !hasAlpha, scale)
        image.draw(in: CGRect(origin: CGPoint.zero, size: sizeChange))
        
        let scaledImage = UIGraphicsGetImageFromCurrentImageContext()
        return scaledImage!
    }
    
    var indexing : Int = 1
    var cellData : RegisterComplaintTableViewCell!
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
extension RegisterComplaintViewController : UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 344
        
    }
}
extension RegisterComplaintViewController : UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! RegisterComplaintTableViewCell
        
        cellData = cell
        cell.btnClose.isHidden = true
       updateConst()
        cell.separatorInset = UIEdgeInsetsMake(0, 10000, 0, 0);
        return cell
        
        
        
    }
}
