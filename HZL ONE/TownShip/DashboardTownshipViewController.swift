//
//  DashboardTownshipViewController.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 09/03/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit
import Charts


import MobileCoreServices
import Alamofire
import CoreData
import CRToast
import IQKeyboardManagerSwift
import AVFoundation

class DashboardTownshipViewController: CommonVSClass,UINavigationBarDelegate,UINavigationControllerDelegate,UITableViewDelegate, UITableViewDataSource,ChartViewDelegate{
    
    
    
   
    @IBOutlet weak var homeTableView: UITableView!
    
    
    var applicationName = String()
    
    
    var statusStrNav = String()
    
    var DataAPI = UserTownshipDataAPI()
    var TownshipUserDB : [UserTownshipDataModel] = []
    var indPath = IndexPath()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.frame.size.height = self.homeTableView.frame.size.height
        
        self.homeTableView.delegate = self
        self.homeTableView.dataSource = self
        
        self.homeTableView.sectionIndexBackgroundColor = UIColor.clear
        self.homeTableView.sectionIndexColor = UIColor.lightGray
        
        self.homeTableView.sectionIndexColor = UIColor.blue
        
        
      
        

        
        
        
        
        getBannerImageData()
        if reachability.connection == .none{
            self.view.makeToast("Internet is not available, please check your internet connection try again." )
        }
        
        
    }
    var returnStrMsg = String()
    @objc func ReportdatabyUser() {
        
        
        
  
        
        
 self.returnStrMsg = ""
        self.cellData.textViewAddress.text = ""
        
        let empId : String = UserDefaults.standard.string(forKey: "EmployeeID")!
        locId = ""
        locName = ""
        
        var parameter : [String : String ] = [:]
        var  dictWithoutNilValues = [String:String]()
        
        
        dictWithoutNilValues = ["EmployeeID":empId]
        parameter = dictWithoutNilValues.filter { $0.value != ""}
        
        print(parameter)
        
        self.cellDataSwitch.btnLock.image = UIImage(named: "lock")
      
        
        self.DataAPI.serviceCalling(obj: self, param: parameter ) { (dict) in
            
            self.TownshipUserDB = [UserTownshipDataModel]()
            self.TownshipUserDB = dict as! [UserTownshipDataModel]
            if(self.TownshipUserDB.count > 0){
                if(self.TownshipUserDB[0].QuarterNo != "" && self.TownshipUserDB[0].QuarterType != "" && self.TownshipUserDB[0].Sector != "" &&  self.TownshipUserDB[0].Location_Name != ""){
                let mBuilder = "Quarter no " + self.TownshipUserDB[0].QuarterNo + " type " +  self.TownshipUserDB[0].QuarterType + " sector " +  self.TownshipUserDB[0].Sector + " , " +   self.TownshipUserDB[0].Location_Name
                let agreeAttributedString = NSMutableAttributedString(string: mBuilder, attributes: [NSAttributedStringKey.foregroundColor: UIColor.black, NSAttributedStringKey.font: UIFont.systemFont(ofSize: CGFloat(15.0))])
                    self.locName = self.TownshipUserDB[0].Location_Name
                    self.locId = self.TownshipUserDB[0].Location_ID
                
                self.cellData.textViewAddress.attributedText = agreeAttributedString
                    UserDefaults.standard.set(self.locId, forKey: "LocationIdTownship")
                    
                    UserDefaults.standard.set(self.locName, forKey: "LocationNameTownship")
                    self.returnStrMsg = ""
                    self.cellDataSwitch.btnLock.image = UIImage(named: "arrowRight")
                    
                }else{
                    self.cellDataSwitch.btnLock.image = UIImage(named: "lock")
                  
                }
                
            }else{
                self.cellDataSwitch.btnLock.image = UIImage(named: "lock")
               
            }
        }
        
    }
    var locName = String()
    var locId = String()
    var locNameQuarter = String()
    var locIdQuarter = String()
    var reachability = Reachability()!
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        if(MoveStruct.isMove == true){
            self.view.makeToast(MoveStruct.message)
            MoveStruct.isMove = false
        }
        
        self.title = statusStrNav
        self.tabBarController?.tabBar.isHidden = true
        let nav = self.navigationController?.navigationBar
        nav?.tintColor = UIColor.white
        nav?.barTintColor = UIColor(hexString: "2c3e50", alpha: 1.0)
        nav?.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        
        
        
    }
    var cellData : buttonTownshipTableViewCell!
    var cellDataSwitch : RegisterTownshipTableViewCell!
    var cellDataQuarter : LocationQuarterTableViewCell!
    @objc func getBannerImageData() {
        
        var imageDB = [BannerImage]()
        do {
            
            imageDB = try context.fetch(BannerImage.fetchRequest())
            //self.refresh.endRefreshing()
            if(imageDB.count > 0) {
                //BannerStruct.bannerPath = imageDB[0].text_Message!
            }
            self.homeTableView.reloadData()
            
        } catch {
            print("Fetching Failed")
        }
    }
    
    func numberOfSections(in tableView: UITableView) -> Int
    {
        return 5
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == 0{
            return 1
        }else{
            return 1
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if indexPath.section == 0 {
            let page = "page"
            
            
            let cell = tableView.dequeueReusableCell(withIdentifier: page) as! PageIdeaTableViewCell
            
            cell.contentView.frame.size.height = cell.imageCollectionView.frame.size.height
            cell.contentView.frame.size.width = cell.imageCollectionView.frame.size.width
            
            
            cell.pageView(ApplicationName: applicationName)
            
            return cell
            
        }else if indexPath.section == 1 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "buttons") as! buttonTownshipTableViewCell
            cellData = cell
            
            return cell
        }else if indexPath.section == 2 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellRegister") as! RegisterTownshipTableViewCell
            cellDataSwitch = cell
           ReportdatabyUser()
//            cellData.textViewAddress.text = locName
//            if(locName == ""){
//            cell.btnLock.image = UIImage(named: "lock")
//            }else{
//               cell.btnLock.image = UIImage(named: "arrowRight")
//            }
            return cell
        }else if indexPath.section == 3 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellComplaint") as! UITableViewCell
            
            return cell
        }else  {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellQuarter") as! LocationQuarterTableViewCell
            cellDataQuarter = cell
            locIdQuarter  = UserDefaults.standard.string(forKey: "LocationId")!
            locNameQuarter = UserDefaults.standard.string(forKey: "LocationName")!
            cell.btnLock.image = UIImage(named: "arrowRight")
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        
     
        let screenSize = UIScreen.main.bounds
        //let screenHeight = screenSize.height-navheight
        
        let screenWidth = screenSize.width
        if indexPath.section == 0{
            return (screenWidth/2) + 25
        }else if indexPath.section == 2 || indexPath.section == 3 || indexPath.section == 4{
            return 123
        } else{
            return 83
        }
        
    }
    
    
    @IBAction func RegisterComplaintClicked(_ sender: UIButton) {
        
        if(self.cellData.textViewAddress.text != ""){
   moveBack = 0
        let storyBoard = UIStoryboard(name: "Township", bundle: nil)
        let submitVC = storyBoard.instantiateViewController(withIdentifier: "FeedbackTownshipViewController") as! FeedbackTownshipViewController
        
        self.navigationController?.pushViewController(submitVC, animated: true)
        }else{
          //  self.view.makeToast(returnStrMsg)
            self.view.makeToast("Room not alloted for you.")
        }
        
    }
    @IBAction func QuarterRequestClicked(_ sender: UIButton) {
       
        let storyBoard = UIStoryboard(name: "Township", bundle: nil)
        let submitVC = storyBoard.instantiateViewController(withIdentifier: "LocationQuarterSubmitViewController") as! LocationQuarterSubmitViewController
            submitVC.LocationId = locIdQuarter
            submitVC.LocationName = locNameQuarter
        self.navigationController?.pushViewController(submitVC, animated: true)
       
    }
    @IBAction func MyComplaintClicked(_ sender: UIButton) {
        
        let storyBoard = UIStoryboard(name: "Township", bundle: nil)
        let submitVC = storyBoard.instantiateViewController(withIdentifier: "PPCRMyComplaintViewController") as! PPCRMyComplaintViewController
        
        self.navigationController?.pushViewController(submitVC, animated: true)
    
        
 
    }
    
 
    
   
    
    
    var reachablty = Reachability()!
    
    
    func getContext () -> NSManagedObjectContext {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        return appDelegate.persistentContainer.viewContext
    }
    
    
    
    
    
    
}
