//
//  FeedbackModel.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 11/03/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import Foundation



class FeedbackDataModel: NSObject {
    
    
    var ID = Int()
    var EmployeeID = String()
    var CategoryId = String()
    var ComplaintSubjectId = String()
    var category = String()
    var Subject = String()
    var ComplainVia = String()
    var ComplainBy = String()
    var Remarks = String()
    var Process_Remarks = String()
    var Attended_Remarks = String()
    var ComplaintStatus = String()
    var Process_Date = String()
    var Attended_Date = String()
    var Target_Date = String()
    var Hold_Remarks = String()
    var Hold_Date = String()
    var ComplaintDate_Status = String()
    var ComplaintDate = String()
    var Address = String()
    var ComplaintSubject_Name = String()
    var Feedback_Status = String()
    
    
    
    
    func setDataInModel(str:[String:AnyObject])
    {
        if str["EmployeeID"] is NSNull || str["EmployeeID"] == nil{
            self.EmployeeID = ""
        }else{
            
            let tit = str["EmployeeID"]
            
            self.EmployeeID = (tit?.description)!
            
            
        }
        if str["CategoryId"] is NSNull || str["CategoryId"] == nil{
            self.CategoryId =  ""
        }else{
            
            let desc = str["CategoryId"]
            
            self.CategoryId = (desc?.description)!
            
        }
        if str["ID"] is NSNull || str["ID"] == nil{
            self.ID = 0
        }else{
            self.ID = (str["ID"] as? Int)!
        }
        
        
        if str["ComplaintSubjectId"] is NSNull || str["ComplaintSubjectId"] == nil{
            self.ComplaintSubjectId =  ""
        }else{
            
            let desc = str["ComplaintSubjectId"]
            
            self.ComplaintSubjectId = (desc?.description)!
            
            
        }
        if str["ComplaintSubject_Name"] is NSNull || str["ComplaintSubject_Name"] == nil{
            self.ComplaintSubject_Name =  ""
        }else{
            
            let desc = str["ComplaintSubject_Name"]
            
            self.ComplaintSubject_Name = (desc?.description)!
            
            
        }
        if str["category"] is NSNull || str["category"] == nil{
            self.category =  ""
        }else{
            
            let desc = str["category"]
            
            self.category = (desc?.description)!
            
            
        }
        if str["ComplainVia"] is NSNull || str["ComplainVia"] == nil{
            self.ComplainVia =  ""
        }else{
            
            let desc = str["ComplainVia"]
            
            self.ComplainVia = (desc?.description)!
            
            
        }
        
        
        if str["Remarks"] is NSNull || str["Remarks"] == nil{
            self.Remarks =  ""
        }else{
            
            let desc = str["Remarks"]
            
            self.Remarks = (desc?.description)!
            
            
        }
        
        if str["ComplaintStatus"] is NSNull || str["ComplaintStatus"] == nil{
            self.ComplaintStatus =  ""
        }else{
            
            let desc = str["ComplaintStatus"]
            
            self.ComplaintStatus = (desc?.description)!
            
        }
        if str["ComplaintDate_Status"] is NSNull || str["ComplaintDate_Status"] == nil{
            self.ComplaintDate_Status =  ""
        }else{
            
            let desc = str["ComplaintDate_Status"]
            
            self.ComplaintDate_Status = (desc?.description)!
            
        }
        
        if str["ComplaintDate"] is NSNull || str["ComplaintDate"] == nil{
            self.ComplaintDate =  ""
        }else{
            
            let desc = str["ComplaintDate"]
            
            self.ComplaintDate = (desc?.description)!
            
            
        }
        
        if str["Address"] is NSNull || str["Address"] == nil{
            self.Address =  ""
        }else{
            
            let desc = str["Address"]
            
            self.Address = (desc?.description)!
            
            
        }
        
       
        
        
        if str["Subject"] is NSNull || str["Subject"] == nil{
            self.Subject =  ""
        }else{
            
            let desc = str["Subject"]
            
            self.Subject = (desc?.description)!
            
            
        }
        if str["ComplainBy"] is NSNull || str["ComplainBy"] == nil{
            self.ComplainBy =  ""
        }else{
            
            let desc = str["ComplainBy"]
            
            self.ComplainBy = (desc?.description)!
            
            
        }
        
      
        if str["Process_Remarks"] is NSNull || str["Process_Remarks"] == nil{
            self.Process_Remarks =  ""
        }else{
            
            let desc = str["Process_Remarks"]
            
            self.Process_Remarks = (desc?.description)!
            
            
        }
        
        
        if str["Attended_Remarks"] is NSNull || str["Attended_Remarks"] == nil{
            self.Attended_Remarks =  ""
        }else{
            
            let desc = str["Attended_Remarks"]
            
            self.Attended_Remarks = (desc?.description)!
            
            
        }
        if str["Process_Date"] is NSNull || str["Process_Date"] == nil{
            self.Process_Date =  ""
        }else{
            
            let desc = str["Process_Date"]
            
            self.Process_Date = (desc?.description)!
            
            
        }
       
        if str["Attended_Date"] is NSNull || str["Attended_Date"] == nil{
            self.Attended_Date =  ""
        }else{
            
            let desc = str["Attended_Date"]
            
            self.Attended_Date = (desc?.description)!
            
            
        }
        if str["Target_Date"] is NSNull || str["Target_Date"] == nil{
            self.Target_Date =  ""
        }else{
            
            let desc = str["Target_Date"]
            
            self.Target_Date = (desc?.description)!
            
            
        }
        if str["Hold_Remarks"] is NSNull || str["Hold_Remarks"] == nil{
            self.Hold_Remarks =  ""
        }else{
            
            let desc = str["Hold_Remarks"]
            
            self.Hold_Remarks = (desc?.description)!
            
            
        }
        if str["Hold_Date"] is NSNull || str["Hold_Date"] == nil{
            self.Hold_Date =  ""
        }else{
            
            let desc = str["Hold_Date"]
            
            self.Hold_Date = (desc?.description)!
            
            
        }
        if str["ComplaintSubject_Name"] is NSNull || str["ComplaintSubject_Name"] == nil{
            self.ComplaintSubject_Name =  ""
        }else{
            
            let desc = str["ComplaintSubject_Name"]
            
            self.ComplaintSubject_Name = (desc?.description)!
            
            
        }
        if str["Feedback_Status"] is NSNull || str["Feedback_Status"] == nil{
            self.Feedback_Status =  ""
        }else{
            
            let desc = str["Feedback_Status"]
            
            self.Feedback_Status = (desc?.description)!
            
            
        }
        
       
     
      
        
    }
    
}





class FeedBackTownshipDataAPI
{
    
    var reachablty = Reachability()!
    
    func serviceCalling(obj:FeedbackTownshipViewController, param : [String: String] , success:@escaping (AnyObject)-> Void)
    {
        obj.startLoadingPK(view: obj.view)
        
        if(reachablty.connection != .none)
        {
            
            
            WebServices.sharedInstances.sendPostRequest(url: URLConstants.Feedback_Data, parameters: param , successHandler: { (dict) in
                
                if let response = dict["response"]{
                    
                    let statusString : String = response["status"] as! String
                    let objectmsg = MessageCallServerModel()
            let msg = objectmsg.sealizeMessage(cell: response as! [String : AnyObject])
                    print(response)
                    if(statusString == "success")
                    {
                        obj.tableView.isHidden = false
                        obj.noDataLabel(text: "" )
                        obj.label.isHidden = true
                        if let data = dict["data"] as? [AnyObject]
                        {
                            
                            var dataArray: [FeedbackDataModel] = []
                            
                            for i in 0..<data.count
                            {
                                
                                if let cell = data[i] as? [String:AnyObject]
                                {
                                    let object = FeedbackDataModel()
                                    object.setDataInModel(str: cell)
                                    dataArray.append(object)
                                }
                                
                            }
                            
                            success(dataArray as AnyObject)
                         
                            
                            
                            
                        }
                        
                        obj.refresh.endRefreshing()
                        obj.stopLoadingPK(view: obj.view)
                    }
                    else
                    {
                      
                        
                        obj.refresh.endRefreshing()
                        obj.stopLoadingPK(view: obj.view)
                        obj.feedbackId = ""
                        obj.movetoregisterTab()
                        
                        
                    }
                    
                    
                }
                
                
                
                
            })
            { (error) in
                
                let errorStr : String = error.description
                
                print("DATA:error",errorStr)
                
                //                obj.errorChecking(error: error)
                obj.stopLoadingPK(view: obj.view)
            }
            
            
        }
        else{
            obj.label.isHidden = false
            obj.tableView.isHidden = true
            obj.noDataLabel(text: "Internet is not available, please check your internet connection try again." )
           obj.stopLoadingPK(view: obj.view)
             obj.feedbackId = ""
            obj.showTwoButtonAlertWithTwoAction(title: "Alert", message: "Internet is not available , please check", buttonTitleLeft: "Close", buttonTitleRight: "Retry", completionHandlerLeft: {
                obj.closeTab()
            }) {
                obj.retryTab()
            }
        }
        
        
    }
    
    
}
