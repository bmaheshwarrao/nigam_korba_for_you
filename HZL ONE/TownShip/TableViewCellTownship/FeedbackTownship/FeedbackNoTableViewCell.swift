//
//  FeedbackNoTableViewCell.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 09/03/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit

class FeedbackNoTableViewCell: UITableViewCell {
 @IBOutlet weak var textViewRemark: UITextView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        textViewRemark.layer.borderWidth = 1.0
        textViewRemark.layer.borderColor = UIColor.black.cgColor
        textViewRemark.layer.cornerRadius = 10.0
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
