//
//  RegisterTownshipTableViewCell.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 15/03/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit

class RegisterTownshipTableViewCell: UITableViewCell {

    
    @IBOutlet weak var btnRegister: UIButton!
    @IBOutlet weak var btnLock: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
