
//
//  ComplaintDetailsViewController.swift
//  HZL ONE
//
//  Created by SARVANG INFOTCH on 09/03/19.
//  Copyright © 2019 SARVANG INFOTCH. All rights reserved.
//

import UIKit
import SDWebImage
class ComplaintDetailsViewController: CommonVSClass ,UITableViewDelegate, UITableViewDataSource{
    
    @IBOutlet weak var tableView: UITableView!
    var ComplaintDB = PPCRTownshipDataModel()
    var refreshControl = UIRefreshControl()
    
    var CategoryTownshipDB:[CategoryTownshipDataModel] = []
    var CategoryTownshipAPI = CategoryTownshipDataAPI()
    
    
    
    var StrNav = String()
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        getData()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.estimatedRowHeight = 65
        tableView.rowHeight = UITableViewAutomaticDimension
        
        tableView.tableFooterView = UIView()
        refreshControl.addTarget(self, action: #selector(getData), for: .valueChanged)
        
        self.tableView.addSubview(refreshControl)
        let swipe = UISwipeGestureRecognizer(target: self, action: #selector(self.swipeRecognizer))
        swipe.direction = ([.down])
        self.view.addGestureRecognizer(swipe)
        
        
        
        
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        self.tabBarController?.tabBar.isHidden = true
        self.title = "Complaint's Detail"
        let nav = self.navigationController?.navigationBar
        nav?.tintColor = UIColor.white
        nav?.barTintColor = UIColor(hexString: "2c3e50", alpha: 1.0)
        nav?.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        nav?.isTranslucent = false
        
    }
    var reachability = Reachability()!
    @objc func swipeRecognizer(_ sender: UISwipeGestureRecognizer) {
        if (sender.direction == .down) {
            
            if(self.reachability.connection != .none)
            {
                
                self.getData()
            }
            else
            {
                self.startLoading(view: self.view)
                DispatchQueue.main.asyncAfter(deadline: .now() + 4.5) {
                    self.noDataLabel(text: "No internet connection found. Check your internet connection and try again.")
                    self.stopLoading(view: self.view)
                }
            }
            
        }
    }
    @objc func getData(){
        tableView.reloadData()
    }
  
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    var data: String?
    var lastObject: String?
    var indPath = IndexPath()
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        
        
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "details", for: indexPath) as! ComplaintDetailsTableViewCell
        
//        cell.lblCategory.text = self.CategoryTownshipDB[indexPath.section].category
       
    

         cell.textViewRemark.text = ComplaintDB.Remarks
        cell.lblId.text = "ID #"+String(describing: ComplaintDB.ID)
         cell.textViewStatus.text = ComplaintDB.ComplaintStatus
         cell.textViewSubject.text = ComplaintDB.ComplaintSubject_Name
        cell.textViewCategory.text = ComplaintDB.Category_Name
         cell.textViewRemark.text = ComplaintDB.Remarks
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = NSTimeZone.system
        dateFormatter.dateFormat = "MMM dd yyyy HH:mm:ss:SSSa"
        let date = dateFormatter.date(from: ComplaintDB.ComplaintDate)
        
        let dateFormatter2 = DateFormatter()
        dateFormatter2.timeZone = NSTimeZone.system
        dateFormatter2.dateFormat = "dd MMM yyyy"
        
        switch date {
        case nil:
            let date_TimeStr = dateFormatter2.string(from: Date())
            cell.lblDate.text = date_TimeStr
            break;
        default:
            let date_TimeStr = dateFormatter2.string(from: date!)
            
            cell.lblDate.text = date_TimeStr
            break;
        }
        
        let dateFormatter1 = DateFormatter()
        dateFormatter1.timeZone = NSTimeZone.system
        dateFormatter1.dateFormat = "MMM dd yyyy HH:mm:ss:SSSa"
        let date1 = dateFormatter1.date(from: ComplaintDB.ComplaintDate_Status)
        
        let dateFormatter21 = DateFormatter()
        dateFormatter21.timeZone = NSTimeZone.system
        dateFormatter21.dateFormat = "dd MMM yyyy"
        
        switch date1 {
        case nil:
            let date_TimeStr = dateFormatter21.string(from: Date())
            cell.textViewUpdateOn.text = date_TimeStr
            break;
        default:
            let date_TimeStr = dateFormatter21.string(from: date1!)
            
            cell.textViewUpdateOn.text = date_TimeStr
            break;
        }
        let imageView = UIImageView()
        let urlString = ComplaintDB.ImagePath
        let urlShow = urlString.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
        if let url = NSURL(string: ComplaintDB.ImagePath) {
            
            SDWebImageManager.shared().downloadImage(with: NSURL(string: urlShow!) as URL!, options: .continueInBackground, progress: {
                (receivedSize :Int, ExpectedSize :Int) in
                
            }, completed: {
                (image : UIImage?, error : Error?, cacheType : SDImageCacheType, finished : Bool, url : URL?) in
                if(image != nil) {
                 
                    cell.imageCompalint.image = image
                }else{
                    imageView.image =  UIImage(named: "placed")
                   
                    cell.imageCompalint.image = UIImage(named: "placed")
                    //  cell.viewImage.backgroundColor = UIColor(patternImage: imageCell)
                }
            })
        }else{
           
            cell.imageCompalint.image = UIImage(named: "placed")
            //cell.viewImage.backgroundColor = UIColor(patternImage: imageCell)
        }
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return UITableViewAutomaticDimension
        
    }
    
  
    
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
